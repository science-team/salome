EnSight Model Geometry File
Total Structured Model
node id assign
element id assign
extents
 0.00000e+00 4.00000e+00
 0.00000e+00 4.00000e+00
 0.00000e+00 0.00000e+00
part
         1
left bottom
block uniform range
         5         5         1
         1         3         1         3         1         1
 0.00000e+00
 0.00000e+00
 0.00000e+00
 1.00000e+00
 1.00000e+00
 0.00000e+00
part
         2
bottom right
block uniform range
         5         5         1
         3         5         1         3         1         1
 2.00000e+00
 0.00000e+00
 0.00000e+00
 1.00000e+00
 1.00000e+00
 0.00000e+00
part
         3
top left
block uniform range
         5         5         1
         1         3         3         5         1         1
 0.00000e+00
 2.00000e+00
 0.00000e+00
 1.00000e+00
 1.00000e+00
 0.00000e+00
part
         4
top right
block uniform range
         5         5         1
         3         5         3         5         1         1
 2.00000e+00
 2.00000e+00
 0.00000e+00
 1.00000e+00
 1.00000e+00
 0.00000e+00
