#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphInputInLoop_2_1
#
from SuperV import *

# Graph creation of GraphInputInLoop_2_1
def DefGraphInputInLoop_2_1() :
    GraphInputInLoop_2_1 = Graph( 'GraphInputInLoop_2_1' )
    GraphInputInLoop_2_1.SetName( 'GraphInputInLoop_2_1' )
    GraphInputInLoop_2_1.SetAuthor( '' )
    GraphInputInLoop_2_1.SetComment( '' )
    GraphInputInLoop_2_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *      ' )
    PyIsOdd.append( 'def IsOdd(a,Even,Const) :  ' )
    PyIsOdd.append( '    print 'IsOdd',a,Even,Const  ' )
    PyIsOdd.append( '    sleep( 1 )      ' )
    PyIsOdd.append( '    return a,Const ' )
    IsOdd = GraphInputInLoop_2_1.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 397 , 5 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddEven = IsOdd.InPort( 'Even' , 'boolean' )
    IIsOddConst = IsOdd.InPort( 'Const' , 'string' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddConst = IsOdd.OutPort( 'Const' , 'string' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInitLoop = []
    PyMoreInitLoop = []
    PyMoreInitLoop.append( '' )
    PyNextInitLoop = []
    PyNextInitLoop.append( '' )
    InitLoop,EndOfInitLoop = GraphInputInLoop_2_1.LNode( '' , PyInitLoop , '' , PyMoreInitLoop , '' , PyNextInitLoop )
    EndOfInitLoop.SetName( 'EndOfInitLoop' )
    EndOfInitLoop.SetAuthor( '' )
    EndOfInitLoop.SetComment( 'Compute Node' )
    EndOfInitLoop.Coords( 770 , 147 )
    PyEndOfInitLoop = []
    PyEndOfInitLoop.append( 'def EndOfInitLoop( DoLoop , Index , Min , Max ) :' )
    PyEndOfInitLoop.append( '	Index = Index + 1     ' )
    PyEndOfInitLoop.append( '	if Index <= Max :   ' )
    PyEndOfInitLoop.append( '		DoLoop = 1     ' )
    PyEndOfInitLoop.append( '	else :     ' )
    PyEndOfInitLoop.append( '		DoLoop = 0     ' )
    PyEndOfInitLoop.append( '	return DoLoop,Index,Min,Max     ' )
    EndOfInitLoop.SetPyFunction( 'EndOfInitLoop' , PyEndOfInitLoop )
    IInitLoopDoLoop = InitLoop.GetInPort( 'DoLoop' )
    IInitLoopIndex = InitLoop.InPort( 'Index' , 'long' )
    IInitLoopMin = InitLoop.InPort( 'Min' , 'long' )
    IInitLoopMax = InitLoop.InPort( 'Max' , 'long' )
    IInitLoopGate = InitLoop.GetInPort( 'Gate' )
    OInitLoopDoLoop = InitLoop.GetOutPort( 'DoLoop' )
    OInitLoopIndex = InitLoop.GetOutPort( 'Index' )
    OInitLoopMin = InitLoop.GetOutPort( 'Min' )
    OInitLoopMax = InitLoop.GetOutPort( 'Max' )
    IEndOfInitLoopDoLoop = EndOfInitLoop.GetInPort( 'DoLoop' )
    IEndOfInitLoopIndex = EndOfInitLoop.GetInPort( 'Index' )
    IEndOfInitLoopMin = EndOfInitLoop.GetInPort( 'Min' )
    IEndOfInitLoopMax = EndOfInitLoop.GetInPort( 'Max' )
    IEndOfInitLoopGate = EndOfInitLoop.GetInPort( 'Gate' )
    OEndOfInitLoopDoLoop = EndOfInitLoop.GetOutPort( 'DoLoop' )
    OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    OEndOfInitLoopGate = EndOfInitLoop.GetOutPort( 'Gate' )
    InitLoop.SetName( 'InitLoop' )
    InitLoop.SetAuthor( '' )
    InitLoop.SetComment( 'Compute Node' )
    InitLoop.Coords( 10 , 108 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import *  ' )
    PySwitch.append( 'def Switch(a) :    ' )
    PySwitch.append( '    if ( a & 1 ) == 0 :  ' )
    PySwitch.append( '        sleep(1)  ' )
    PySwitch.append( '    return a & 1,1-(a&1),a    ' )
    Switch,EndSwitch = GraphInputInLoop_2_1.SNode( 'Switch' , PySwitch )
    EndSwitch.SetName( 'EndSwitch' )
    EndSwitch.SetAuthor( '' )
    EndSwitch.SetComment( 'Compute Node' )
    EndSwitch.Coords( 588 , 147 )
    PyEndSwitch = []
    PyEndSwitch.append( 'def EndSwitch(a) :    ' )
    PyEndSwitch.append( '    if ( a & 1 ) == 0 :  ' )
    PyEndSwitch.append( '        sleep(1)  ' )
    PyEndSwitch.append( '    return a    ' )
    EndSwitch.SetPyFunction( 'EndSwitch' , PyEndSwitch )
    IEndSwitcha = EndSwitch.InPort( 'a' , 'long' )
    IEndSwitchDefault = EndSwitch.GetInPort( 'Default' )
    OEndSwitcha = EndSwitch.OutPort( 'a' , 'long' )
    OEndSwitchGate = EndSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 194 , 109 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndSwitcha = GraphInputInLoop_2_1.Link( OIsOdda , IEndSwitcha )
    LIsOddaEndSwitcha.AddCoord( 1 , 573 , 178 )
    LIsOddaEndSwitcha.AddCoord( 2 , 573 , 76 )
    
    LInitLoopIndexSwitcha = GraphInputInLoop_2_1.Link( OInitLoopIndex , ISwitcha )
    
    LInitLoopMinEndOfInitLoopMin = GraphInputInLoop_2_1.Link( OInitLoopMin , IEndOfInitLoopMin )
    
    LInitLoopMaxEndOfInitLoopMax = GraphInputInLoop_2_1.Link( OInitLoopMax , IEndOfInitLoopMax )
    
    LSwitchOddIsOddGate = GraphInputInLoop_2_1.Link( OSwitchOdd , IIsOddGate )
    
    LSwitchEvenIsOddEven = GraphInputInLoop_2_1.Link( OSwitchEven , IIsOddEven )
    
    LSwitchaIsOdda = GraphInputInLoop_2_1.Link( OSwitcha , IIsOdda )
    LSwitchaIsOdda.AddCoord( 1 , 366 , 71 )
    LSwitchaIsOdda.AddCoord( 2 , 366 , 182 )
    
    LSwitchDefaultEndSwitchDefault = GraphInputInLoop_2_1.Link( OSwitchDefault , IEndSwitchDefault )
    
    LEndSwitchaEndOfInitLoopIndex = GraphInputInLoop_2_1.Link( OEndSwitcha , IEndOfInitLoopIndex )
    
    # Input datas
    IIsOddConst.Input( 'Const Input Value' )
    IInitLoopIndex.Input( 0 )
    IInitLoopMin.Input( 0 )
    IInitLoopMax.Input( 23 )
    
    # Output Ports of the graph
    #OIsOddConst = IsOdd.GetOutPort( 'Const' )
    #OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    #OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    #OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    return GraphInputInLoop_2_1


GraphInputInLoop_2_1 = DefGraphInputInLoop_2_1()
