#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph aNewDataFlow
#
from SuperV import *

# Graph creation of aNewDataFlow
def DefaNewDataFlow() :
    aNewDataFlow = Graph( 'aNewDataFlow' )
    aNewDataFlow.SetName( 'aNewDataFlow' )
    aNewDataFlow.SetAuthor( '' )
    aNewDataFlow.SetComment( '' )
    aNewDataFlow.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    TranslateDXDYDZ = aNewDataFlow.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'TranslateDXDYDZ' )
    TranslateDXDYDZ.SetName( 'TranslateDXDYDZ' )
    TranslateDXDYDZ.SetAuthor( '' )
    TranslateDXDYDZ.SetContainer( 'localhost/FactoryServer' )
    TranslateDXDYDZ.SetComment( 'TranslateDXDYDZ from GEOM_Superv' )
    TranslateDXDYDZ.Coords( 735 , 14 )
    ITranslateDXDYDZtheObject = TranslateDXDYDZ.GetInPort( 'theObject' )
    ITranslateDXDYDZtheDX = TranslateDXDYDZ.GetInPort( 'theDX' )
    ITranslateDXDYDZtheDY = TranslateDXDYDZ.GetInPort( 'theDY' )
    ITranslateDXDYDZtheDZ = TranslateDXDYDZ.GetInPort( 'theDZ' )
    ITranslateDXDYDZGate = TranslateDXDYDZ.GetInPort( 'Gate' )
    OTranslateDXDYDZreturn = TranslateDXDYDZ.GetOutPort( 'return' )
    OTranslateDXDYDZGate = TranslateDXDYDZ.GetOutPort( 'Gate' )
    
    MakeCopy = aNewDataFlow.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeCopy' )
    MakeCopy.SetName( 'MakeCopy' )
    MakeCopy.SetAuthor( '' )
    MakeCopy.SetContainer( 'localhost/FactoryServer' )
    MakeCopy.SetComment( 'MakeCopy from GEOM_Superv' )
    MakeCopy.Coords( 477 , 14 )
    IMakeCopytheOriginal = MakeCopy.GetInPort( 'theOriginal' )
    IMakeCopyGate = MakeCopy.GetInPort( 'Gate' )
    OMakeCopyreturn = MakeCopy.GetOutPort( 'return' )
    OMakeCopyGate = MakeCopy.GetOutPort( 'Gate' )
    
    MakeBox = aNewDataFlow.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeBox' )
    MakeBox.SetName( 'MakeBox' )
    MakeBox.SetAuthor( '' )
    MakeBox.SetContainer( 'localhost/FactoryServer' )
    MakeBox.SetComment( 'MakeBox from GEOM_Superv' )
    MakeBox.Coords( 219 , 14 )
    IMakeBoxtheX1 = MakeBox.GetInPort( 'theX1' )
    IMakeBoxtheY1 = MakeBox.GetInPort( 'theY1' )
    IMakeBoxtheZ1 = MakeBox.GetInPort( 'theZ1' )
    IMakeBoxtheX2 = MakeBox.GetInPort( 'theX2' )
    IMakeBoxtheY2 = MakeBox.GetInPort( 'theY2' )
    IMakeBoxtheZ2 = MakeBox.GetInPort( 'theZ2' )
    IMakeBoxGate = MakeBox.GetInPort( 'Gate' )
    OMakeBoxreturn = MakeBox.GetOutPort( 'return' )
    OMakeBoxGate = MakeBox.GetOutPort( 'Gate' )
    
    MakeCopy_1 = aNewDataFlow.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeCopy' )
    MakeCopy_1.SetName( 'MakeCopy_1' )
    MakeCopy_1.SetAuthor( '' )
    MakeCopy_1.SetContainer( 'localhost/FactoryServer' )
    MakeCopy_1.SetComment( 'MakeCopy from GEOM_Superv' )
    MakeCopy_1.Coords( 482 , 306 )
    IMakeCopy_1theOriginal = MakeCopy_1.GetInPort( 'theOriginal' )
    IMakeCopy_1Gate = MakeCopy_1.GetInPort( 'Gate' )
    OMakeCopy_1return = MakeCopy_1.GetOutPort( 'return' )
    OMakeCopy_1Gate = MakeCopy_1.GetOutPort( 'Gate' )
    
    MakeFuse = aNewDataFlow.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeFuse' )
    MakeFuse.SetName( 'MakeFuse' )
    MakeFuse.SetAuthor( '' )
    MakeFuse.SetContainer( 'localhost/FactoryServer' )
    MakeFuse.SetComment( 'MakeFuse from GEOM_Superv' )
    MakeFuse.Coords( 950 , 121 )
    IMakeFusetheShape1 = MakeFuse.GetInPort( 'theShape1' )
    IMakeFusetheShape2 = MakeFuse.GetInPort( 'theShape2' )
    IMakeFuseGate = MakeFuse.GetInPort( 'Gate' )
    OMakeFusereturn = MakeFuse.GetOutPort( 'return' )
    OMakeFuseGate = MakeFuse.GetOutPort( 'Gate' )
    
    MakeSphere = aNewDataFlow.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeSphere' )
    MakeSphere.SetName( 'MakeSphere' )
    MakeSphere.SetAuthor( '' )
    MakeSphere.SetContainer( 'localhost/FactoryServer' )
    MakeSphere.SetComment( 'MakeSphere from GEOM_Superv' )
    MakeSphere.Coords( 227 , 409 )
    IMakeSpheretheX = MakeSphere.GetInPort( 'theX' )
    IMakeSpheretheY = MakeSphere.GetInPort( 'theY' )
    IMakeSpheretheZ = MakeSphere.GetInPort( 'theZ' )
    IMakeSpheretheRadius = MakeSphere.GetInPort( 'theRadius' )
    IMakeSphereGate = MakeSphere.GetInPort( 'Gate' )
    OMakeSpherereturn = MakeSphere.GetOutPort( 'return' )
    OMakeSphereGate = MakeSphere.GetOutPort( 'Gate' )
    
    MakeFuse_1 = aNewDataFlow.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeFuse' )
    MakeFuse_1.SetName( 'MakeFuse_1' )
    MakeFuse_1.SetAuthor( '' )
    MakeFuse_1.SetContainer( 'localhost/FactoryServer' )
    MakeFuse_1.SetComment( 'MakeFuse from GEOM_Superv' )
    MakeFuse_1.Coords( 1217 , 389 )
    IMakeFuse_1theShape1 = MakeFuse_1.GetInPort( 'theShape1' )
    IMakeFuse_1theShape2 = MakeFuse_1.GetInPort( 'theShape2' )
    IMakeFuse_1Gate = MakeFuse_1.GetInPort( 'Gate' )
    OMakeFuse_1return = MakeFuse_1.GetOutPort( 'return' )
    OMakeFuse_1Gate = MakeFuse_1.GetOutPort( 'Gate' )
    
    SetStudyID = aNewDataFlow.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'SetStudyID' )
    SetStudyID.SetName( 'SetStudyID' )
    SetStudyID.SetAuthor( '' )
    SetStudyID.SetContainer( 'localhost/FactoryServer' )
    SetStudyID.SetComment( 'SetStudyID from GEOM_Superv' )
    SetStudyID.Coords( 7 , 269 )
    ISetStudyIDtheStudyID = SetStudyID.GetInPort( 'theStudyID' )
    ISetStudyIDGate = SetStudyID.GetInPort( 'Gate' )
    OSetStudyIDGate = SetStudyID.GetOutPort( 'Gate' )
    
    # Creation of Macro Nodes
    aNewDataFlow_1 = DefaNewDataFlow_1()
    Macro_aNewDataFlow_1 = aNewDataFlow.GraphMNode( aNewDataFlow_1 )
    Macro_aNewDataFlow_1.SetCoupled( 'aNewDataFlow_1' )
    Macro_aNewDataFlow_1.SetName( 'Macro_aNewDataFlow_1' )
    Macro_aNewDataFlow_1.SetAuthor( '' )
    Macro_aNewDataFlow_1.SetComment( 'Macro Node' )
    Macro_aNewDataFlow_1.Coords( 480 , 145 )
    IMacro_aNewDataFlow_1sum__a = Macro_aNewDataFlow_1.GetInPort( 'sum__a' )
    IMacro_aNewDataFlow_1sum__b = Macro_aNewDataFlow_1.GetInPort( 'sum__b' )
    IMacro_aNewDataFlow_1Gate = Macro_aNewDataFlow_1.GetInPort( 'Gate' )
    OMacro_aNewDataFlow_1Mult__b = Macro_aNewDataFlow_1.GetOutPort( 'Mult__b' )
    OMacro_aNewDataFlow_1Gate = Macro_aNewDataFlow_1.GetOutPort( 'Gate' )
    
    # Creation of Links
    LMacro_aNewDataFlow_1Mult__bTranslateDXDYDZtheDY = aNewDataFlow.Link( OMacro_aNewDataFlow_1Mult__b , ITranslateDXDYDZtheDY )
    
    LTranslateDXDYDZreturnMakeFusetheShape1 = aNewDataFlow.Link( OTranslateDXDYDZreturn , IMakeFusetheShape1 )
    
    LMakeCopyreturnTranslateDXDYDZtheObject = aNewDataFlow.Link( OMakeCopyreturn , ITranslateDXDYDZtheObject )
    
    LMakeBoxreturnMakeCopytheOriginal = aNewDataFlow.Link( OMakeBoxreturn , IMakeCopytheOriginal )
    
    LMakeBoxreturnMakeCopy_1theOriginal = aNewDataFlow.Link( OMakeBoxreturn , IMakeCopy_1theOriginal )
    
    LMakeCopy_1returnMakeFusetheShape2 = aNewDataFlow.Link( OMakeCopy_1return , IMakeFusetheShape2 )
    
    LMakeFusereturnMakeFuse_1theShape1 = aNewDataFlow.Link( OMakeFusereturn , IMakeFuse_1theShape1 )
    
    LMakeSpherereturnMakeFuse_1theShape2 = aNewDataFlow.Link( OMakeSpherereturn , IMakeFuse_1theShape2 )
    
    LSetStudyIDGateMakeBoxGate = aNewDataFlow.Link( OSetStudyIDGate , IMakeBoxGate )
    
    LSetStudyIDGateMakeSphereGate = aNewDataFlow.Link( OSetStudyIDGate , IMakeSphereGate )
    
    # Input datas
    IMacro_aNewDataFlow_1sum__a.Input( 1 )
    IMacro_aNewDataFlow_1sum__b.Input( 2 )
    ITranslateDXDYDZtheDX.Input( 25 )
    ITranslateDXDYDZtheDZ.Input( 25 )
    IMakeBoxtheX1.Input( 0 )
    IMakeBoxtheY1.Input( 0 )
    IMakeBoxtheZ1.Input( 0 )
    IMakeBoxtheX2.Input( 50 )
    IMakeBoxtheY2.Input( 50 )
    IMakeBoxtheZ2.Input( 50 )
    IMakeSpheretheX.Input( 0 )
    IMakeSpheretheY.Input( 0 )
    IMakeSpheretheZ.Input( 0 )
    IMakeSpheretheRadius.Input( 12 )
    ISetStudyIDtheStudyID.Input( 1 )
    
    # Output Ports of the graph
    #OMakeFuse_1return = MakeFuse_1.GetOutPort( 'return' )
    return aNewDataFlow

# Graph creation of aNewDataFlow_1
def DefaNewDataFlow_1() :
    aNewDataFlow_1 = Graph( 'aNewDataFlow_1' )
    aNewDataFlow_1.SetCoupled( 'Macro_aNewDataFlow_1' )
    aNewDataFlow_1.SetName( 'aNewDataFlow_1' )
    aNewDataFlow_1.SetAuthor( '' )
    aNewDataFlow_1.SetComment( '' )
    aNewDataFlow_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyMult = []
    PyMult.append( 'def Mult(a): ' )
    PyMult.append( '   b = a*3 ' )
    PyMult.append( '   return b ' )
    Mult = aNewDataFlow_1.INode( 'Mult' , PyMult )
    Mult.SetName( 'Mult' )
    Mult.SetAuthor( '' )
    Mult.SetComment( 'Compute Node' )
    Mult.Coords( 382 , 78 )
    IMulta = Mult.InPort( 'a' , 'double' )
    IMultGate = Mult.GetInPort( 'Gate' )
    OMultb = Mult.OutPort( 'b' , 'double' )
    OMultGate = Mult.GetOutPort( 'Gate' )
    
    Pysum = []
    Pysum.append( 'def sum(a, b): ' )
    Pysum.append( '   return a+b' )
    sum = aNewDataFlow_1.INode( 'sum' , Pysum )
    sum.SetName( 'sum' )
    sum.SetAuthor( '' )
    sum.SetComment( 'Compute Node' )
    sum.Coords( 47 , 87 )
    Isuma = sum.InPort( 'a' , 'double' )
    Isumb = sum.InPort( 'b' , 'double' )
    IsumGate = sum.GetInPort( 'Gate' )
    Osums = sum.OutPort( 's' , 'double' )
    OsumGate = sum.GetOutPort( 'Gate' )
    
    # Creation of Links
    LsumsMulta = aNewDataFlow_1.Link( Osums , IMulta )
    
    # Input Ports of the graph
    #Isuma = sum.GetInPort( 'a' )
    #Isumb = sum.GetInPort( 'b' )
    
    # Output Ports of the graph
    #OMultb = Mult.GetOutPort( 'b' )
    return aNewDataFlow_1


aNewDataFlow = DefaNewDataFlow()
