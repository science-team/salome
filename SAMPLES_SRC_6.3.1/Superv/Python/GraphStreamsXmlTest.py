#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
from SuperV import *

FlowGraph = Graph( 'FlowGraph' )
Add = FlowGraph.FNode( 'AddComponent' , 'AddComponent' , 'Add' )

Sub = FlowGraph.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
Addz = Add.GetOutStreamPort( 'z' )
if Addz != None :
    print "Error : GetOutStreamPort of a FlowPort in a FlowGraph"
else :
    print 'None : Ok'

Addz = Add.Port( 'z' )
try :
    Suby = FlowGraph.StreamLink( Addz , Sub.Port( 'y' ) )
    print "Error : creation of a StreamLink in a FlowGraph"
except :
    print "Ok"

links = FlowGraph.StreamLinks()
if len(links) != 0 :
    print "Error : StreamLinks in a FlowGraph"

ports = FlowGraph.StreamPorts()
if len( ports ) != 0 :
    print "Error : StreamPorts in a FlowGraph"

links = Add.StreamLinks()
if len(links) != 0 :
    print "Error : StreamLinks in a FlowNode"

ports = Add.StreamPorts()
if len( ports ) != 0 :
    print "Error : StreamPorts in a FlowNode"

try :
    FlowGraph.SetStreamParams( 100 , SUPERV.WithoutTrace , 10. )
except :
    print "No SetStreamParams Ok"

try :
    Timeout,DataStreamTrace,DeltaTime = FlowGraph.StreamParams()
except :
    print "No StreamParams Ok"




from SuperV import *

anXmlFile = os.getenv('DATA_DIR') + '/Superv/Graphs/GraphStreams.xml'
GraphStreams = StreamGraph( anXmlFile )

GraphStreams.PrintPorts()
GraphStreams.PrintLinks()

exec GraphStreams.ListNodes()
Add.PrintPorts()
Add.PrintLinks()

exec Loop.ListPorts()
ILoopIndex.Print()
OLoopIndex.Print()
exec EndLoop.ListPorts()
IEndLoopGate.Print()
OEndLoopx_Sub.Print()
exec Add.ListPorts()
IAddx.Print()
IAddistream.Print()
OAddOStream.Print()
exec Sub.ListPorts()
exec Mul.ListPorts()
exec Div.ListPorts()
exec Add_1.ListPorts()
exec Sub_1.ListPorts()
exec Mul_1.ListPorts()
exec Div_1.ListPorts()
exec  Add.ListLinks()
LLoopIndexAddx.Print()
LAddzMuly.Print()
LDiv_1ostreamAddistream.Print()
LAddOStreamSubIstream.Print()


GraphStreams.IsExecutable()

GraphStreams.Run()
GraphStreams.DoneW()
GraphStreams.PrintPorts()



subgraphs = GraphStreams.SubGraphsNumber()
i = 1
while i <= subgraphs :
    nodes = GraphStreams.SubGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1


substreamgraphs = GraphStreams.SubStreamGraphsNumber()
i = 1
while i <= substreamgraphs :
    nodes = GraphStreams.SubStreamGraphsNodes( i )
    j = 0
    while j < len(nodes) :
        print 'SubStreamGraph',i,nodes[j].Name()
        j = j + 1
    i = i + 1

