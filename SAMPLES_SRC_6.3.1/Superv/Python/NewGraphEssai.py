#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphEssai
#
from SuperV import *
# Graph creation 
NewGraphEssai = Graph( 'NewGraphEssai' )
NewGraphEssai.SetName( 'NewGraphEssai' )
NewGraphEssai.SetAuthor( '' )
NewGraphEssai.SetComment( '' )
NewGraphEssai.Coords( 0 , 0 )

# Creation of Factory Nodes

Sub = NewGraphEssai.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
Sub.SetName( 'Sub' )
Sub.SetAuthor( '' )
Sub.SetContainer( 'SubDivServer' )
Sub.SetComment( 'Sub from SubComponent' )
Sub.Coords( 413 , 74 )

Mul = NewGraphEssai.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
Mul.SetName( 'Mul' )
Mul.SetAuthor( '' )
Mul.SetContainer( 'MulServer' )
Mul.SetComment( 'Mul from MulComponent' )
Mul.Coords( 617 , 268 )

Div = NewGraphEssai.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
Div.SetName( 'Div' )
Div.SetAuthor( '' )
Div.SetContainer( 'SubDivServer' )
Div.SetComment( 'Div from DivComponent' )
Div.Coords( 823 , 74 )

Addition = NewGraphEssai.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
Addition.SetName( 'Addition' )
Addition.SetAuthor( '' )
Addition.SetContainer( 'localhost/AddServer' )
Addition.SetComment( 'Addition from AddComponent' )
Addition.Coords( 11 , 268 )

# Creation of Computing Nodes
Add_ServiceinParameter = []
Add_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'Adder' , 'Adder' ) )
Add_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'x' ) )
Add_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'y' ) )
Add_ServiceoutParameter = []
Add_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'FuncValue' ) )
Add_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'z' ) )
Add_Service = SALOME_ModuleCatalog.Service( 'Add' , Add_ServiceinParameter , Add_ServiceoutParameter , 0 )
Add = NewGraphEssai.CNode( Add_Service )
Add.SetName( 'Add' )
Add.SetAuthor( '' )
Add.SetComment( 'Compute Node' )
Add.Coords( 215 , 268 )

# Creation of Links
Subz = Sub.Port( 'z' )
Divx = NewGraphEssai.Link( Subz , Div.Port( 'x' ) )

Mulx = NewGraphEssai.Link( Subz , Mul.Port( 'x' ) )
Mulx.AddCoord( 1 , 595 , 348 )
Mulx.AddCoord( 2 , 595 , 154 )

Mulz = Mul.Port( 'z' )
Divy = NewGraphEssai.Link( Mulz , Div.Port( 'y' ) )
Divy.AddCoord( 1 , 805 , 183 )
Divy.AddCoord( 2 , 806 , 348 )

AdditionAdder = Addition.Port( 'Adder' )
AddAdder = NewGraphEssai.Link( AdditionAdder , Add.Port( 'Adder' ) )

AddFuncValue = Add.Port( 'FuncValue' )
Suby = NewGraphEssai.Link( AddFuncValue , Sub.Port( 'y' ) )
Suby.AddCoord( 1 , 395 , 183 )
Suby.AddCoord( 2 , 395 , 349 )

Addz = Add.Port( 'z' )
Muly = NewGraphEssai.Link( Addz , Mul.Port( 'y' ) )

# Creation of Input datas
Subx = Sub.Input( 'x' , 4.5)
Addx = Add.Input( 'x' , 1.5)
Addy = Add.Input( 'y' , 3)

# Creation of Output variables
Divz = Div.Port( 'z' )

NewGraphEssai.Run()

NewGraphEssai.DoneW()

NewGraphEssai.State()

NewGraphEssai.PrintPorts()

