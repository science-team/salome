#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph TestVisu20_multi
#
from SuperV import *

# Graph creation of TestVisu20_multi
def DefTestVisu20_multi() :
    TestVisu20_multi = Graph( 'TestVisu20_multi' )
    TestVisu20_multi.SetName( 'TestVisu20_multi' )
    TestVisu20_multi.SetAuthor( '' )
    TestVisu20_multi.SetComment( '' )
    TestVisu20_multi.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyInitDisplay = []
    PyInitDisplay.append( 'import os          ' )
    PyInitDisplay.append( 'import time          ' )
    PyInitDisplay.append( 'from LifeCycleCORBA import *       ' )
    PyInitDisplay.append( 'from SALOME_NamingServicePy import *       ' )
    PyInitDisplay.append( 'import SALOMEDS       ' )
    PyInitDisplay.append( 'from VISU import *       ' )
    PyInitDisplay.append( 'def InitDisplay():          ' )
    PyInitDisplay.append( '    #  --- initialize the ORB         ' )
    PyInitDisplay.append( '    orb = CORBA.ORB_init([], CORBA.ORB_ID)       ' )
    PyInitDisplay.append( '    # --- create a LifeCycleCORBA instance            ' )
    PyInitDisplay.append( '    lcc = LifeCycleCORBA(orb)                      ' )
    PyInitDisplay.append( '    # --- create a naming service instance               ' )
    PyInitDisplay.append( '    naming_service = SALOME_NamingServicePy_i(orb)     ' )
    PyInitDisplay.append( '    # --- get Study Manager reference                      ' )
    PyInitDisplay.append( '    obj = naming_service.Resolve('myStudyManager')     ' )
    PyInitDisplay.append( '    StudyManager = obj._narrow(SALOMEDS.StudyManager)     ' )
    PyInitDisplay.append( '    # --- load visu component                           ' )
    PyInitDisplay.append( '    aVisu = lcc.FindOrLoadComponent("FactoryServer","VISU")        ' )
    PyInitDisplay.append( '    aStudy = StudyManager.GetStudyByID(1)       ' )
    PyInitDisplay.append( '    aVisu.SetCurrentStudy(aStudy)       ' )
    PyInitDisplay.append( '    print "InitDisplay --> aVisu ", aVisu          ' )
    PyInitDisplay.append( '    time.sleep(2)          ' )
    PyInitDisplay.append( '    #aViewManager=aVisu.GetViewManager()          ' )
    PyInitDisplay.append( '    #print "InitDisplay --> aViewManager ", aViewManager          ' )
    PyInitDisplay.append( '    #time.sleep(2)          ' )
    PyInitDisplay.append( '    #aView=aViewManager.GetCurrentView()          ' )
    PyInitDisplay.append( '    #aView=aViewManager.Create3DView()          ' )
    PyInitDisplay.append( '    #print "InitDisplay --> aView ", aView          ' )
    PyInitDisplay.append( '    #time.sleep(2)          ' )
    PyInitDisplay.append( '    medFile = "TimeStamps.med"        ' )
    PyInitDisplay.append( '    medFile = os.getenv('DATA_DIR') + '/MedFiles/' + medFile        ' )
    PyInitDisplay.append( '    myResult = aVisu.ImportFile(medFile)        ' )
    PyInitDisplay.append( '    print "InitDisplay --> myResult ", myResult          ' )
    PyInitDisplay.append( '    return aVisu,myResult        ' )
    PyInitDisplay.append( '' )
    InitDisplay = TestVisu20_multi.INode( 'InitDisplay' , PyInitDisplay )
    InitDisplay.SetName( 'InitDisplay' )
    InitDisplay.SetAuthor( '' )
    InitDisplay.SetComment( 'Compute Node' )
    InitDisplay.Coords( 13 , 173 )
    IInitDisplayGate = InitDisplay.GetInPort( 'Gate' )
    OInitDisplayaVisu = InitDisplay.OutPort( 'aVisu' , 'objref' )
    OInitDisplaymyResult = InitDisplay.OutPort( 'myResult' , 'objref' )
    OInitDisplayGate = InitDisplay.GetOutPort( 'Gate' )
    
    PyDisplayVitesse = []
    PyDisplayVitesse.append( 'import VISU                               ' )
    PyDisplayVitesse.append( 'import os                               ' )
    PyDisplayVitesse.append( 'import time                               ' )
    PyDisplayVitesse.append( 'def DisplayVitesse(index,aVisu,aView,aResult,x1):                          ' )
    PyDisplayVitesse.append( '    print "----- Display 1 index = ", index,aVisu,aView,aResult,x1                    ' )
    PyDisplayVitesse.append( '    aMeshName ="dom"                               ' )
    PyDisplayVitesse.append( '    myFieldName = "vitesse"                     ' )
    PyDisplayVitesse.append( '    anEntity = VISU.NODE                               ' )
    PyDisplayVitesse.append( '    aMesh = aVisu.MeshOnEntity(aResult, aMeshName, anEntity)     ' )
    PyDisplayVitesse.append( '    print "----- Display 2 ", aMesh                 ' )
    PyDisplayVitesse.append( '    aScalarMap=aVisu.ScalarMapOnField(aResult,aMeshName,anEntity,myFieldName,x1)  ' )
    PyDisplayVitesse.append( '    print "----- Display 4"                         ' )
    PyDisplayVitesse.append( '    if aScalarMap is not None:                               ' )
    PyDisplayVitesse.append( '        aScalarMap.SetScaling(VISU.LINEAR)                               ' )
    PyDisplayVitesse.append( '    else:                               ' )
    PyDisplayVitesse.append( '        print "Null scalar map is created"                               ' )
    PyDisplayVitesse.append( '    print "----- Display 5"           ' )
    PyDisplayVitesse.append( '    aView.DisplayOnly(aScalarMap)                               ' )
    PyDisplayVitesse.append( '    aView.FitAll()                               ' )
    PyDisplayVitesse.append( '    aView.Update()                              ' )
    PyDisplayVitesse.append( '    print "----- Display 6"                         ' )
    PyDisplayVitesse.append( '    theResult=aResult                           ' )
    PyDisplayVitesse.append( '    #time.sleep(2)                   ' )
    PyDisplayVitesse.append( '    return theResult                    ' )
    PyDisplayVitesse.append( '' )
    DisplayVitesse = TestVisu20_multi.INode( 'DisplayVitesse' , PyDisplayVitesse )
    DisplayVitesse.SetName( 'DisplayVitesse' )
    DisplayVitesse.SetAuthor( '' )
    DisplayVitesse.SetComment( 'Compute Node' )
    DisplayVitesse.Coords( 609 , 506 )
    IDisplayVitesseindex = DisplayVitesse.InPort( 'index' , 'string' )
    IDisplayVitesseaVisu = DisplayVitesse.InPort( 'aVisu' , 'objref' )
    IDisplayVitesseaView = DisplayVitesse.InPort( 'aView' , 'objref' )
    IDisplayVitesseaResult = DisplayVitesse.InPort( 'aResult' , 'objref' )
    IDisplayVitessex1 = DisplayVitesse.InPort( 'x1' , 'double' )
    IDisplayVitesseGate = DisplayVitesse.GetInPort( 'Gate' )
    ODisplayVitessetheResult = DisplayVitesse.OutPort( 'theResult' , 'objref' )
    ODisplayVitesseGate = DisplayVitesse.GetOutPort( 'Gate' )
    
    PyCreateView = []
    PyCreateView.append( 'def CreateView(aVisu,aResult) :     ' )
    PyCreateView.append( '   vm=aVisu.GetViewManager()        ' )
    PyCreateView.append( '   aView=vm.Create3DView()   ' )
    PyCreateView.append( '   theVisu=aVisu  ' )
    PyCreateView.append( '   theResult=aResult       ' )
    PyCreateView.append( '   return aView,theVisu,theResult      ' )
    CreateView = TestVisu20_multi.INode( 'CreateView' , PyCreateView )
    CreateView.SetName( 'CreateView' )
    CreateView.SetAuthor( '' )
    CreateView.SetComment( 'Compute Node' )
    CreateView.Coords( 221 , 563 )
    ICreateViewaVisu = CreateView.InPort( 'aVisu' , 'objref' )
    ICreateViewaResult = CreateView.InPort( 'aResult' , 'objref' )
    ICreateViewGate = CreateView.GetInPort( 'Gate' )
    OCreateViewaView = CreateView.OutPort( 'aView' , 'objref' )
    OCreateViewtheVisu = CreateView.OutPort( 'theVisu' , 'objref' )
    OCreateViewtheResult = CreateView.OutPort( 'theResult' , 'objref' )
    OCreateViewGate = CreateView.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInit = []
    PyInit.append( 'def Init(index,min,max,aVisu,aView,myResult,x1):   ' )
    PyInit.append( '    if max >= min :   ' )
    PyInit.append( '        index = min      ' )
    PyInit.append( '    else :     ' )
    PyInit.append( '        index = max  ' )
    PyInit.append( '    print "---- Init ",index,min,max,aVisu,aView,myResult,x1 ' )
    PyInit.append( '    return index,min,max,aVisu,aView,myResult,x1  ' )
    PyMoreInit = []
    PyMoreInit.append( 'def More(index,min,max,aVisu,aView,myResult,x1):   ' )
    PyMoreInit.append( '    if max >= index :       ' )
    PyMoreInit.append( '        DoLoop = 1       ' )
    PyMoreInit.append( '    else :       ' )
    PyMoreInit.append( '        DoLoop = 0       ' )
    PyMoreInit.append( '    print "---- More",index,min,max,aVisu,aView,myResult,x1 ' )
    PyMoreInit.append( '    return DoLoop,index,min,max,aVisu,aView,myResult,x1   ' )
    PyNextInit = []
    PyNextInit.append( 'def Next(index,min,max,aVisu,aView,myResult,x1):   ' )
    PyNextInit.append( '    index = index + 1 ' )
    PyNextInit.append( '    x1=x1+1.0 ' )
    PyNextInit.append( '    print "---- Next",index,min,max,aVisu,aView,myResult,x1   ' )
    PyNextInit.append( '    return index,min,max,aVisu,aView,myResult,x1   ' )
    Init,EndOfInit = TestVisu20_multi.LNode( 'Init' , PyInit , 'More' , PyMoreInit , 'Next' , PyNextInit )
    EndOfInit.SetName( 'EndOfInit' )
    EndOfInit.SetAuthor( '' )
    EndOfInit.SetComment( 'Compute Node' )
    EndOfInit.Coords( 797 , 507 )
    PyEndOfInit = []
    EndOfInit.SetPyFunction( '' , PyEndOfInit )
    IInitDoLoop = Init.GetInPort( 'DoLoop' )
    IInitindex = Init.InPort( 'index' , 'long' )
    IInitmin = Init.InPort( 'min' , 'long' )
    IInitmax = Init.InPort( 'max' , 'long' )
    IInitaVisu = Init.InPort( 'aVisu' , 'objref' )
    IInitaView = Init.InPort( 'aView' , 'objref' )
    IInitaResult = Init.InPort( 'aResult' , 'objref' )
    IInitx1 = Init.InPort( 'x1' , 'double' )
    IInitGate = Init.GetInPort( 'Gate' )
    OInitDoLoop = Init.GetOutPort( 'DoLoop' )
    OInitindex = Init.GetOutPort( 'index' )
    OInitmin = Init.GetOutPort( 'min' )
    OInitmax = Init.GetOutPort( 'max' )
    OInitaVisu = Init.GetOutPort( 'aVisu' )
    OInitaView = Init.GetOutPort( 'aView' )
    OInitaResult = Init.GetOutPort( 'aResult' )
    OInitx1 = Init.GetOutPort( 'x1' )
    IEndOfInitDoLoop = EndOfInit.GetInPort( 'DoLoop' )
    IEndOfInitindex = EndOfInit.GetInPort( 'index' )
    IEndOfInitmin = EndOfInit.GetInPort( 'min' )
    IEndOfInitmax = EndOfInit.GetInPort( 'max' )
    IEndOfInitaVisu = EndOfInit.GetInPort( 'aVisu' )
    IEndOfInitaView = EndOfInit.GetInPort( 'aView' )
    IEndOfInitaResult = EndOfInit.GetInPort( 'aResult' )
    IEndOfInitx1 = EndOfInit.GetInPort( 'x1' )
    IEndOfInitGate = EndOfInit.GetInPort( 'Gate' )
    OEndOfInitDoLoop = EndOfInit.GetOutPort( 'DoLoop' )
    OEndOfInitindex = EndOfInit.GetOutPort( 'index' )
    OEndOfInitmin = EndOfInit.GetOutPort( 'min' )
    OEndOfInitmax = EndOfInit.GetOutPort( 'max' )
    OEndOfInitaVisu = EndOfInit.GetOutPort( 'aVisu' )
    OEndOfInitaView = EndOfInit.GetOutPort( 'aView' )
    OEndOfInitaResult = EndOfInit.GetOutPort( 'aResult' )
    OEndOfInitx1 = EndOfInit.GetOutPort( 'x1' )
    OEndOfInitGate = EndOfInit.GetOutPort( 'Gate' )
    Init.SetName( 'Init' )
    Init.SetAuthor( '' )
    Init.SetComment( 'Compute Node' )
    Init.Coords( 428 , 493 )
    
    PyLoopOnViews = []
    PyLoopOnViews.append( 'def InitViews( aVisu,aResult,ViewCount,MinCount,MaxCount ):    ' )
    PyLoopOnViews.append( '   ViewCount=MinCount   ' )
    PyLoopOnViews.append( '   return aVisu,aResult,ViewCount,MinCount,MaxCount      ' )
    PyMoreLoopOnViews = []
    PyMoreLoopOnViews.append( 'def MoreViews( aVisu,aResult,ViewCount,MinCount,MaxCount ):      ' )
    PyMoreLoopOnViews.append( '   if ViewCount < MaxCount:     ' )
    PyMoreLoopOnViews.append( '      return 1,aVisu,aResult,ViewCount,MinCount,MaxCount     ' )
    PyMoreLoopOnViews.append( '   return 0,aVisu,aResult,ViewCount,MinCount,MaxCount     ' )
    PyNextLoopOnViews = []
    PyNextLoopOnViews.append( 'def NextViews( aVisu,aResult,ViewCount,MinCount,MaxCount ):     ' )
    PyNextLoopOnViews.append( '   ViewCount = ViewCount + 1     ' )
    PyNextLoopOnViews.append( '   return aVisu,aResult,ViewCount,MinCount,MaxCount     ' )
    LoopOnViews,EndOfLoopOnViews = TestVisu20_multi.LNode( 'InitViews' , PyLoopOnViews , 'MoreViews' , PyMoreLoopOnViews , 'NextViews' , PyNextLoopOnViews )
    EndOfLoopOnViews.SetName( 'EndOfLoopOnViews' )
    EndOfLoopOnViews.SetAuthor( '' )
    EndOfLoopOnViews.SetComment( 'Compute Node' )
    EndOfLoopOnViews.Coords( 773 , 224 )
    PyEndOfLoopOnViews = []
    EndOfLoopOnViews.SetPyFunction( 'EndLoop' , PyEndOfLoopOnViews )
    ILoopOnViewsDoLoop = LoopOnViews.GetInPort( 'DoLoop' )
    ILoopOnViewsaVisu = LoopOnViews.InPort( 'aVisu' , 'objref' )
    ILoopOnViewsaResult = LoopOnViews.InPort( 'aResult' , 'objref' )
    ILoopOnViewsViewCount = LoopOnViews.InPort( 'ViewCount' , 'int' )
    ILoopOnViewsMinCount = LoopOnViews.InPort( 'MinCount' , 'int' )
    ILoopOnViewsMaxCount = LoopOnViews.InPort( 'MaxCount' , 'int' )
    ILoopOnViewsGate = LoopOnViews.GetInPort( 'Gate' )
    OLoopOnViewsDoLoop = LoopOnViews.GetOutPort( 'DoLoop' )
    OLoopOnViewsaVisu = LoopOnViews.GetOutPort( 'aVisu' )
    OLoopOnViewsaResult = LoopOnViews.GetOutPort( 'aResult' )
    OLoopOnViewsViewCount = LoopOnViews.GetOutPort( 'ViewCount' )
    OLoopOnViewsMinCount = LoopOnViews.GetOutPort( 'MinCount' )
    OLoopOnViewsMaxCount = LoopOnViews.GetOutPort( 'MaxCount' )
    IEndOfLoopOnViewsDoLoop = EndOfLoopOnViews.GetInPort( 'DoLoop' )
    IEndOfLoopOnViewsaVisu = EndOfLoopOnViews.GetInPort( 'aVisu' )
    IEndOfLoopOnViewsaResult = EndOfLoopOnViews.GetInPort( 'aResult' )
    IEndOfLoopOnViewsViewCount = EndOfLoopOnViews.GetInPort( 'ViewCount' )
    IEndOfLoopOnViewsMinCount = EndOfLoopOnViews.GetInPort( 'MinCount' )
    IEndOfLoopOnViewsMaxCount = EndOfLoopOnViews.GetInPort( 'MaxCount' )
    IEndOfLoopOnViewsGate = EndOfLoopOnViews.GetInPort( 'Gate' )
    OEndOfLoopOnViewsDoLoop = EndOfLoopOnViews.GetOutPort( 'DoLoop' )
    OEndOfLoopOnViewsaVisu = EndOfLoopOnViews.GetOutPort( 'aVisu' )
    OEndOfLoopOnViewsaResult = EndOfLoopOnViews.GetOutPort( 'aResult' )
    OEndOfLoopOnViewsViewCount = EndOfLoopOnViews.GetOutPort( 'ViewCount' )
    OEndOfLoopOnViewsMinCount = EndOfLoopOnViews.GetOutPort( 'MinCount' )
    OEndOfLoopOnViewsMaxCount = EndOfLoopOnViews.GetOutPort( 'MaxCount' )
    OEndOfLoopOnViewsGate = EndOfLoopOnViews.GetOutPort( 'Gate' )
    LoopOnViews.SetName( 'LoopOnViews' )
    LoopOnViews.SetAuthor( '' )
    LoopOnViews.SetComment( 'Compute Node' )
    LoopOnViews.Coords( 219 , 218 )
    
    # Creation of Links
    LInitDisplayaVisuLoopOnViewsaVisu = TestVisu20_multi.Link( OInitDisplayaVisu , ILoopOnViewsaVisu )
    
    LInitDisplaymyResultLoopOnViewsaResult = TestVisu20_multi.Link( OInitDisplaymyResult , ILoopOnViewsaResult )
    
    LInitindexEndOfInitindex = TestVisu20_multi.Link( OInitindex , IEndOfInitindex )
    
    LInitindexDisplayVitesseindex = TestVisu20_multi.Link( OInitindex , IDisplayVitesseindex )
    LInitindexDisplayVitesseindex.AddCoord( 1 , 590 , 587 )
    LInitindexDisplayVitesseindex.AddCoord( 2 , 591 , 527 )
    
    LInitminEndOfInitmin = TestVisu20_multi.Link( OInitmin , IEndOfInitmin )
    
    LInitmaxEndOfInitmax = TestVisu20_multi.Link( OInitmax , IEndOfInitmax )
    
    LInitaVisuEndOfInitaVisu = TestVisu20_multi.Link( OInitaVisu , IEndOfInitaVisu )
    
    LInitaVisuDisplayVitesseaVisu = TestVisu20_multi.Link( OInitaVisu , IDisplayVitesseaVisu )
    
    LInitaViewEndOfInitaView = TestVisu20_multi.Link( OInitaView , IEndOfInitaView )
    
    LInitaViewDisplayVitesseaView = TestVisu20_multi.Link( OInitaView , IDisplayVitesseaView )
    
    LInitaResultDisplayVitesseaResult = TestVisu20_multi.Link( OInitaResult , IDisplayVitesseaResult )
    
    LInitx1EndOfInitx1 = TestVisu20_multi.Link( OInitx1 , IEndOfInitx1 )
    
    LInitx1DisplayVitessex1 = TestVisu20_multi.Link( OInitx1 , IDisplayVitessex1 )
    
    LEndOfInitaResultEndOfLoopOnViewsaResult = TestVisu20_multi.Link( OEndOfInitaResult , IEndOfLoopOnViewsaResult )
    LEndOfInitaResultEndOfLoopOnViewsaResult.AddCoord( 1 , 762 , 288 )
    LEndOfInitaResultEndOfLoopOnViewsaResult.AddCoord( 2 , 763 , 424 )
    LEndOfInitaResultEndOfLoopOnViewsaResult.AddCoord( 3 , 972 , 425 )
    LEndOfInitaResultEndOfLoopOnViewsaResult.AddCoord( 4 , 971 , 687 )
    
    LDisplayVitessetheResultEndOfInitaResult = TestVisu20_multi.Link( ODisplayVitessetheResult , IEndOfInitaResult )
    LDisplayVitessetheResultEndOfInitaResult.AddCoord( 1 , 787 , 687 )
    LDisplayVitessetheResultEndOfInitaResult.AddCoord( 2 , 786 , 585 )
    
    LLoopOnViewsaVisuEndOfLoopOnViewsaVisu = TestVisu20_multi.Link( OLoopOnViewsaVisu , IEndOfLoopOnViewsaVisu )
    
    LLoopOnViewsaVisuCreateViewaVisu = TestVisu20_multi.Link( OLoopOnViewsaVisu , ICreateViewaVisu )
    LLoopOnViewsaVisuCreateViewaVisu.AddCoord( 1 , 186 , 643 )
    LLoopOnViewsaVisuCreateViewaVisu.AddCoord( 2 , 186 , 442 )
    LLoopOnViewsaVisuCreateViewaVisu.AddCoord( 3 , 400 , 441 )
    LLoopOnViewsaVisuCreateViewaVisu.AddCoord( 4 , 401 , 251 )
    
    LLoopOnViewsaResultCreateViewaResult = TestVisu20_multi.Link( OLoopOnViewsaResult , ICreateViewaResult )
    LLoopOnViewsaResultCreateViewaResult.AddCoord( 1 , 169 , 671 )
    LLoopOnViewsaResultCreateViewaResult.AddCoord( 2 , 170 , 412 )
    LLoopOnViewsaResultCreateViewaResult.AddCoord( 3 , 386 , 413 )
    LLoopOnViewsaResultCreateViewaResult.AddCoord( 4 , 387 , 280 )
    
    LLoopOnViewsViewCountEndOfLoopOnViewsViewCount = TestVisu20_multi.Link( OLoopOnViewsViewCount , IEndOfLoopOnViewsViewCount )
    
    LLoopOnViewsMinCountEndOfLoopOnViewsMinCount = TestVisu20_multi.Link( OLoopOnViewsMinCount , IEndOfLoopOnViewsMinCount )
    
    LLoopOnViewsMaxCountEndOfLoopOnViewsMaxCount = TestVisu20_multi.Link( OLoopOnViewsMaxCount , IEndOfLoopOnViewsMaxCount )
    
    LCreateViewaViewInitaView = TestVisu20_multi.Link( OCreateViewaView , IInitaView )
    
    LCreateViewtheVisuInitaVisu = TestVisu20_multi.Link( OCreateViewtheVisu , IInitaVisu )
    
    LCreateViewtheResultInitaResult = TestVisu20_multi.Link( OCreateViewtheResult , IInitaResult )
    
    # Input datas
    IInitindex.Input( 1 )
    IInitmin.Input( 1 )
    IInitmax.Input( 10 )
    IInitx1.Input( 1 )
    ILoopOnViewsViewCount.Input( 0 )
    ILoopOnViewsMinCount.Input( 0 )
    ILoopOnViewsMaxCount.Input( 10 )
    
    # Output Ports of the graph
    #OEndOfInitindex = EndOfInit.GetOutPort( 'index' )
    #OEndOfInitmin = EndOfInit.GetOutPort( 'min' )
    #OEndOfInitmax = EndOfInit.GetOutPort( 'max' )
    #OEndOfInitaVisu = EndOfInit.GetOutPort( 'aVisu' )
    #OEndOfInitaView = EndOfInit.GetOutPort( 'aView' )
    #OEndOfInitx1 = EndOfInit.GetOutPort( 'x1' )
    #OEndOfLoopOnViewsaVisu = EndOfLoopOnViews.GetOutPort( 'aVisu' )
    #OEndOfLoopOnViewsaResult = EndOfLoopOnViews.GetOutPort( 'aResult' )
    #OEndOfLoopOnViewsViewCount = EndOfLoopOnViews.GetOutPort( 'ViewCount' )
    #OEndOfLoopOnViewsMinCount = EndOfLoopOnViews.GetOutPort( 'MinCount' )
    #OEndOfLoopOnViewsMaxCount = EndOfLoopOnViews.GetOutPort( 'MaxCount' )
    return TestVisu20_multi


TestVisu20_multi = DefTestVisu20_multi()
