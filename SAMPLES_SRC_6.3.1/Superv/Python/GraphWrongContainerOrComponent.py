#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphWrongContainerOrComponent
#
from SuperV import *

# Graph creation of GraphWrongContainerOrComponent
def DefGraphWrongContainerOrComponent() :
    GraphWrongContainerOrComponent = Graph( 'GraphWrongContainerOrComponent' )
    GraphWrongContainerOrComponent.SetName( 'GraphWrongContainerOrComponent' )
    GraphWrongContainerOrComponent.SetAuthor( 'JR' )
    GraphWrongContainerOrComponent.SetComment( '' )
    GraphWrongContainerOrComponent.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphWrongContainerOrComponent.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 99 , 304 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    Sub = GraphWrongContainerOrComponent.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 98 , 59 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    # Input datas
    IAddx.Input( 3 )
    IAddy.Input( 4.5 )
    ISubx.Input( 1.5 )
    ISuby.Input( 0 )
    
    # Output Ports of the graph
    #OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    #OAddz = Add.GetOutPort( 'z' )
    #OSubz = Sub.GetOutPort( 'z' )
    return GraphWrongContainerOrComponent


GraphWrongContainerOrComponent = DefGraphWrongContainerOrComponent()
