#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopSwitchsNOTValid
#
from SuperV import *

# Graph creation of GraphLoopSwitchsNOTValid
def DefGraphLoopSwitchsNOTValid() :
    GraphLoopSwitchsNOTValid = Graph( 'GraphLoopSwitchsNOTValid' )
    GraphLoopSwitchsNOTValid.SetName( 'GraphLoopSwitchsNOTValid' )
    GraphLoopSwitchsNOTValid.SetAuthor( 'JR' )
    GraphLoopSwitchsNOTValid.SetComment( '' )
    GraphLoopSwitchsNOTValid.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from time import *    ' )
    PyIsOdd.append( 'def IsOdd(a) :        ' )
    PyIsOdd.append( '    print a,"IsOdd"       ' )
    PyIsOdd.append( '    sleep( 1 )    ' )
    PyIsOdd.append( '    return a      ' )
    IsOdd = GraphLoopSwitchsNOTValid.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 476 , 50 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyIsEven = []
    PyIsEven.append( 'from time import *    ' )
    PyIsEven.append( 'def IsEven(a) :        ' )
    PyIsEven.append( '    print a,"IsEven"       ' )
    PyIsEven.append( '    sleep( 1 )    ' )
    PyIsEven.append( '    return a      ' )
    IsEven = GraphLoopSwitchsNOTValid.INode( 'IsEven' , PyIsEven )
    IsEven.SetName( 'IsEven' )
    IsEven.SetAuthor( '' )
    IsEven.SetComment( 'Python function' )
    IsEven.Coords( 479 , 311 )
    IIsEvena = IsEven.InPort( 'a' , 'long' )
    IIsEvenGate = IsEven.GetInPort( 'Gate' )
    OIsEvena = IsEven.OutPort( 'a' , 'long' )
    OIsEvenGate = IsEven.GetOutPort( 'Gate' )
    
    PySwitchsCompare = []
    PySwitchsCompare.append( 'def SwitchsCompare(aOdd,Odd,aEven,Even) : ' )
    PySwitchsCompare.append( '    return a ' )
    SwitchsCompare = GraphLoopSwitchsNOTValid.INode( 'SwitchsCompare' , PySwitchsCompare )
    SwitchsCompare.SetName( 'SwitchsCompare' )
    SwitchsCompare.SetAuthor( '' )
    SwitchsCompare.SetComment( 'Compute Node' )
    SwitchsCompare.Coords( 919 , 242 )
    ISwitchsCompareaOdd = SwitchsCompare.InPort( 'aOdd' , 'long' )
    ISwitchsCompareOdd = SwitchsCompare.InPort( 'Odd' , 'boolean' )
    ISwitchsCompareaEven = SwitchsCompare.InPort( 'aEven' , 'long' )
    ISwitchsCompareEven = SwitchsCompare.InPort( 'Even' , 'boolean' )
    ISwitchsCompareGate = SwitchsCompare.GetInPort( 'Gate' )
    OSwitchsComparea = SwitchsCompare.OutPort( 'a' , 'long' )
    OSwitchsCompareGate = SwitchsCompare.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoop = []
    PyLoop.append( 'def InitLoop(Index,Min,Max) :      ' )
    PyLoop.append( '	return Index,Min,Max     ' )
    PyMoreLoop = []
    PyMoreLoop.append( 'def MoreLoop(Index,Min,Max) :     ' )
    PyMoreLoop.append( '	if Index <= Max :   ' )
    PyMoreLoop.append( '		DoLoop = 1     ' )
    PyMoreLoop.append( '	else :     ' )
    PyMoreLoop.append( '		DoLoop = 0     ' )
    PyMoreLoop.append( '	return DoLoop,Index,Min,Max     ' )
    PyNextLoop = []
    PyNextLoop.append( 'def NextLoop(Index,Min,Max) :     ' )
    PyNextLoop.append( '	Index = Index + 1     ' )
    PyNextLoop.append( '	return Index,Min,Max     ' )
    Loop,EndOfLoop = GraphLoopSwitchsNOTValid.LNode( 'InitLoop' , PyLoop , 'MoreLoop' , PyMoreLoop , 'NextLoop' , PyNextLoop )
    EndOfLoop.SetName( 'EndOfLoop' )
    EndOfLoop.SetAuthor( '' )
    EndOfLoop.SetComment( 'Compute Node' )
    EndOfLoop.Coords( 1102 , 282 )
    PyEndOfLoop = []
    EndOfLoop.SetPyFunction( '' , PyEndOfLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopIndex = Loop.InPort( 'Index' , 'long' )
    ILoopMin = Loop.InPort( 'Min' , 'long' )
    ILoopMax = Loop.InPort( 'Max' , 'long' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopIndex = Loop.GetOutPort( 'Index' )
    OLoopMin = Loop.GetOutPort( 'Min' )
    OLoopMax = Loop.GetOutPort( 'Max' )
    IEndOfLoopDoLoop = EndOfLoop.GetInPort( 'DoLoop' )
    IEndOfLoopIndex = EndOfLoop.GetInPort( 'Index' )
    IEndOfLoopMin = EndOfLoop.GetInPort( 'Min' )
    IEndOfLoopMax = EndOfLoop.GetInPort( 'Max' )
    IEndOfLoopGate = EndOfLoop.GetInPort( 'Gate' )
    OEndOfLoopDoLoop = EndOfLoop.GetOutPort( 'DoLoop' )
    OEndOfLoopIndex = EndOfLoop.GetOutPort( 'Index' )
    OEndOfLoopMin = EndOfLoop.GetOutPort( 'Min' )
    OEndOfLoopMax = EndOfLoop.GetOutPort( 'Max' )
    OEndOfLoopGate = EndOfLoop.GetOutPort( 'Gate' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 12 , 276 )
    
    # Creation of Switch Nodes
    PySwitchOdd = []
    PySwitchOdd.append( 'from time import * ' )
    PySwitchOdd.append( 'def Switch(a) :   ' )
    PySwitchOdd.append( '    if ( a & 1 ) == 0 : ' )
    PySwitchOdd.append( '        sleep(1)    ' )
    PySwitchOdd.append( '    return a & 1,1-(a&1),a    ' )
    SwitchOdd,EndOfSwitchOdd = GraphLoopSwitchsNOTValid.SNode( 'Switch' , PySwitchOdd )
    EndOfSwitchOdd.SetName( 'EndOfSwitchOdd' )
    EndOfSwitchOdd.SetAuthor( '' )
    EndOfSwitchOdd.SetComment( 'Compute Node' )
    EndOfSwitchOdd.Coords( 711 , 161 )
    PyEndOfSwitchOdd = []
    EndOfSwitchOdd.SetPyFunction( '' , PyEndOfSwitchOdd )
    IEndOfSwitchOdda = EndOfSwitchOdd.InPort( 'a' , 'long' )
    IEndOfSwitchOddOdd = EndOfSwitchOdd.InPort( 'Odd' , 'boolean' )
    IEndOfSwitchOddDefault = EndOfSwitchOdd.GetInPort( 'Default' )
    OEndOfSwitchOdda = EndOfSwitchOdd.OutPort( 'a' , 'long' )
    OEndOfSwitchOddOdd = EndOfSwitchOdd.OutPort( 'Odd' , 'boolean' )
    OEndOfSwitchOddGate = EndOfSwitchOdd.GetOutPort( 'Gate' )
    SwitchOdd.SetName( 'SwitchOdd' )
    SwitchOdd.SetAuthor( '' )
    SwitchOdd.SetComment( 'Compute Node' )
    SwitchOdd.Coords( 240 , 141 )
    ISwitchOdda = SwitchOdd.InPort( 'a' , 'long' )
    ISwitchOddGate = SwitchOdd.GetInPort( 'Gate' )
    OSwitchOddOdd = SwitchOdd.OutPort( 'Odd' , 'long' )
    OSwitchOddEven = SwitchOdd.OutPort( 'Even' , 'int' )
    OSwitchOdda = SwitchOdd.OutPort( 'a' , 'int' )
    OSwitchOddDefault = SwitchOdd.GetOutPort( 'Default' )
    
    PySwitchEven = []
    PySwitchEven.append( 'from time import *  ' )
    PySwitchEven.append( 'def Switch(a) :    ' )
    PySwitchEven.append( '    if ( a & 1 ) == 0 :  ' )
    PySwitchEven.append( '        sleep(1)  ' )
    PySwitchEven.append( '    return a & 1,1-(a&1),a    ' )
    SwitchEven,EndOfSwitchEven = GraphLoopSwitchsNOTValid.SNode( 'Switch' , PySwitchEven )
    EndOfSwitchEven.SetName( 'EndOfSwitchEven' )
    EndOfSwitchEven.SetAuthor( '' )
    EndOfSwitchEven.SetComment( 'Compute Node' )
    EndOfSwitchEven.Coords( 718 , 361 )
    PyEndOfSwitchEven = []
    EndOfSwitchEven.SetPyFunction( '' , PyEndOfSwitchEven )
    IEndOfSwitchEvena = EndOfSwitchEven.InPort( 'a' , 'long' )
    IEndOfSwitchEvenEven = EndOfSwitchEven.InPort( 'Even' , 'boolean' )
    IEndOfSwitchEvenDefault = EndOfSwitchEven.GetInPort( 'Default' )
    OEndOfSwitchEvena = EndOfSwitchEven.OutPort( 'a' , 'long' )
    OEndOfSwitchEvenEven = EndOfSwitchEven.OutPort( 'Even' , 'boolean' )
    OEndOfSwitchEvenGate = EndOfSwitchEven.GetOutPort( 'Gate' )
    SwitchEven.SetName( 'SwitchEven' )
    SwitchEven.SetAuthor( '' )
    SwitchEven.SetComment( 'Compute Node' )
    SwitchEven.Coords( 235 , 386 )
    ISwitchEvena = SwitchEven.InPort( 'a' , 'long' )
    ISwitchEvenGate = SwitchEven.GetInPort( 'Gate' )
    OSwitchEvenOdd = SwitchEven.OutPort( 'Odd' , 'long' )
    OSwitchEvenEven = SwitchEven.OutPort( 'Even' , 'int' )
    OSwitchEvena = SwitchEven.OutPort( 'a' , 'int' )
    OSwitchEvenDefault = SwitchEven.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndOfSwitchOdda = GraphLoopSwitchsNOTValid.Link( OIsOdda , IEndOfSwitchOdda )
    
    LSwitchOddOddIsOddGate = GraphLoopSwitchsNOTValid.Link( OSwitchOddOdd , IIsOddGate )
    
    LSwitchOddOddEndOfSwitchOddOdd = GraphLoopSwitchsNOTValid.Link( OSwitchOddOdd , IEndOfSwitchOddOdd )
    
    LSwitchOddaIsOdda = GraphLoopSwitchsNOTValid.Link( OSwitchOdda , IIsOdda )
    LSwitchOddaIsOdda.AddCoord( 1 , 443 , 121 )
    LSwitchOddaIsOdda.AddCoord( 2 , 443 , 212 )
    
    LSwitchOddDefaultEndOfSwitchOddDefault = GraphLoopSwitchsNOTValid.Link( OSwitchOddDefault , IEndOfSwitchOddDefault )
    
    LEndOfSwitchOddaSwitchsCompareaOdd = GraphLoopSwitchsNOTValid.Link( OEndOfSwitchOdda , ISwitchsCompareaOdd )
    
    LEndOfSwitchOddOddSwitchsCompareOdd = GraphLoopSwitchsNOTValid.Link( OEndOfSwitchOddOdd , ISwitchsCompareOdd )
    
    LIsEvenaEndOfSwitchEvena = GraphLoopSwitchsNOTValid.Link( OIsEvena , IEndOfSwitchEvena )
    
    LLoopIndexSwitchEvena = GraphLoopSwitchsNOTValid.Link( OLoopIndex , ISwitchEvena )
    
    LLoopIndexSwitchOdda = GraphLoopSwitchsNOTValid.Link( OLoopIndex , ISwitchOdda )
    
    LLoopMinEndOfLoopMin = GraphLoopSwitchsNOTValid.Link( OLoopMin , IEndOfLoopMin )
    
    LLoopMaxEndOfLoopMax = GraphLoopSwitchsNOTValid.Link( OLoopMax , IEndOfLoopMax )
    
    LSwitchEvenOddIsEvenGate = GraphLoopSwitchsNOTValid.Link( OSwitchEvenOdd , IIsEvenGate )
    
    LSwitchEvenEvenEndOfSwitchEvenDefault = GraphLoopSwitchsNOTValid.Link( OSwitchEvenEven , IEndOfSwitchEvenDefault )
    
    LSwitchEvenaIsEvena = GraphLoopSwitchsNOTValid.Link( OSwitchEvena , IIsEvena )
    LSwitchEvenaIsEvena.AddCoord( 1 , 434 , 382 )
    LSwitchEvenaIsEvena.AddCoord( 2 , 434 , 457 )
    
    LEndOfSwitchEvenaSwitchsCompareaEven = GraphLoopSwitchsNOTValid.Link( OEndOfSwitchEvena , ISwitchsCompareaEven )
    
    LEndOfSwitchEvenEvenSwitchsCompareEven = GraphLoopSwitchsNOTValid.Link( OEndOfSwitchEvenEven , ISwitchsCompareEven )
    
    LSwitchsCompareaEndOfLoopIndex = GraphLoopSwitchsNOTValid.Link( OSwitchsComparea , IEndOfLoopIndex )
    
    # Input datas
    ILoopIndex.Input( 0 )
    ILoopMin.Input( 0 )
    ILoopMax.Input( 100 )
    
    # Input Ports of the graph
    #IEndOfSwitchEvenEven = EndOfSwitchEven.GetInPort( 'Even' )
    
    # Output Ports of the graph
    #OSwitchOddEven = SwitchOdd.GetOutPort( 'Even' )
    #OEndOfLoopIndex = EndOfLoop.GetOutPort( 'Index' )
    #OEndOfLoopMin = EndOfLoop.GetOutPort( 'Min' )
    #OEndOfLoopMax = EndOfLoop.GetOutPort( 'Max' )
    return GraphLoopSwitchsNOTValid


GraphLoopSwitchsNOTValid = DefGraphLoopSwitchsNOTValid()
