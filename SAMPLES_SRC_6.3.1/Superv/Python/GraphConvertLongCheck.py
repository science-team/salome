#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertLongCheck
#
from SuperV import *
# Graph creation 
GraphConvertLongCheck = Graph( 'GraphConvertLongCheck' )
GraphConvertLongCheck.SetName( 'GraphConvertLongCheck' )
GraphConvertLongCheck.SetAuthor( 'JR' )
GraphConvertLongCheck.SetComment( 'Check conversions of Long' )
GraphConvertLongCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertLongCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyLong = []
PyLong.append( 'def Long() :    ' )
PyLong.append( '    aLong = 2147483647 ' )
PyLong.append( '    print type( aLong ),"aLong",aLong  ' )
PyLong.append( '    return aLong    ' )
PyLong.append( ' ' )
Long = GraphConvertLongCheck.INode( 'Long' , PyLong )
Long.OutPort( 'OutLong' , 'long' )
Long.SetName( 'Long' )
Long.SetAuthor( 'JR' )
Long.SetComment( 'InLine Node' )
Long.Coords( 14 , 114 )

# Creation of Links
LongOutLong = Long.Port( 'OutLong' )
MiscTypesInString = GraphConvertLongCheck.Link( LongOutLong , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertLongCheck.Link( LongOutLong , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertLongCheck.Link( LongOutLong , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertLongCheck.Link( LongOutLong , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertLongCheck.Link( LongOutLong , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertLongCheck.Link( LongOutLong , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertLongCheck.Link( LongOutLong , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertLongCheck.Link( LongOutLong , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertLongCheck.Run()
GraphConvertLongCheck.DoneW()
GraphConvertLongCheck.PrintPorts()
