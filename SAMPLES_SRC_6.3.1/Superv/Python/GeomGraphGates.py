#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GeomGraph
#
from SuperV import *

# Graph creation of GeomGraph
def DefGeomGraph() :
    GeomGraph = Graph( 'GeomGraph' )
    GeomGraph.SetName( 'GeomGraph' )
    GeomGraph.SetAuthor( '' )
    GeomGraph.SetComment( '' )
    GeomGraph.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    SetStudyID = GeomGraph.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'SetStudyID' )
    SetStudyID.SetName( 'SetStudyID' )
    SetStudyID.SetAuthor( '' )
    SetStudyID.SetContainer( 'localhost/FactoryServer' )
    SetStudyID.SetComment( 'SetStudyID from GEOM_Superv' )
    SetStudyID.Coords( 14 , 241 )
    ISetStudyIDtheStudyID = SetStudyID.GetInPort( 'theStudyID' )
    ISetStudyIDGate = SetStudyID.GetInPort( 'Gate' )
    OSetStudyIDGate = SetStudyID.GetOutPort( 'Gate' )
    
    MakeBox = GeomGraph.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeBox' )
    MakeBox.SetName( 'MakeBox' )
    MakeBox.SetAuthor( '' )
    MakeBox.SetContainer( 'localhost/FactoryServer' )
    MakeBox.SetComment( 'MakeBox from GEOM_Superv' )
    MakeBox.Coords( 198 , 46 )
    IMakeBoxtheX1 = MakeBox.GetInPort( 'theX1' )
    IMakeBoxtheY1 = MakeBox.GetInPort( 'theY1' )
    IMakeBoxtheZ1 = MakeBox.GetInPort( 'theZ1' )
    IMakeBoxtheX2 = MakeBox.GetInPort( 'theX2' )
    IMakeBoxtheY2 = MakeBox.GetInPort( 'theY2' )
    IMakeBoxtheZ2 = MakeBox.GetInPort( 'theZ2' )
    IMakeBoxGate = MakeBox.GetInPort( 'Gate' )
    OMakeBoxreturn = MakeBox.GetOutPort( 'return' )
    OMakeBoxGate = MakeBox.GetOutPort( 'Gate' )
    
    MakeCopy = GeomGraph.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeCopy' )
    MakeCopy.SetName( 'MakeCopy' )
    MakeCopy.SetAuthor( '' )
    MakeCopy.SetContainer( 'localhost/FactoryServer' )
    MakeCopy.SetComment( 'MakeCopy from GEOM_Superv' )
    MakeCopy.Coords( 383 , 46 )
    IMakeCopytheOriginal = MakeCopy.GetInPort( 'theOriginal' )
    IMakeCopyGate = MakeCopy.GetInPort( 'Gate' )
    OMakeCopyreturn = MakeCopy.GetOutPort( 'return' )
    OMakeCopyGate = MakeCopy.GetOutPort( 'Gate' )
    
    TranslateDXDYDZ = GeomGraph.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'TranslateDXDYDZ' )
    TranslateDXDYDZ.SetName( 'TranslateDXDYDZ' )
    TranslateDXDYDZ.SetAuthor( '' )
    TranslateDXDYDZ.SetContainer( 'localhost/FactoryServer' )
    TranslateDXDYDZ.SetComment( 'TranslateDXDYDZ from GEOM_Superv' )
    TranslateDXDYDZ.Coords( 579 , 46 )
    ITranslateDXDYDZtheObject = TranslateDXDYDZ.GetInPort( 'theObject' )
    ITranslateDXDYDZtheDX = TranslateDXDYDZ.GetInPort( 'theDX' )
    ITranslateDXDYDZtheDY = TranslateDXDYDZ.GetInPort( 'theDY' )
    ITranslateDXDYDZtheDZ = TranslateDXDYDZ.GetInPort( 'theDZ' )
    ITranslateDXDYDZGate = TranslateDXDYDZ.GetInPort( 'Gate' )
    OTranslateDXDYDZreturn = TranslateDXDYDZ.GetOutPort( 'return' )
    OTranslateDXDYDZGate = TranslateDXDYDZ.GetOutPort( 'Gate' )
    
    MakeCopy_1 = GeomGraph.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeCopy' )
    MakeCopy_1.SetName( 'MakeCopy_1' )
    MakeCopy_1.SetAuthor( '' )
    MakeCopy_1.SetContainer( 'localhost/FactoryServer' )
    MakeCopy_1.SetComment( 'MakeCopy from GEOM_Superv' )
    MakeCopy_1.Coords( 384 , 168 )
    IMakeCopy_1theOriginal = MakeCopy_1.GetInPort( 'theOriginal' )
    IMakeCopy_1Gate = MakeCopy_1.GetInPort( 'Gate' )
    OMakeCopy_1return = MakeCopy_1.GetOutPort( 'return' )
    OMakeCopy_1Gate = MakeCopy_1.GetOutPort( 'Gate' )
    
    MakeSphere_1 = GeomGraph.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeSphere' )
    MakeSphere_1.SetName( 'MakeSphere_1' )
    MakeSphere_1.SetAuthor( '' )
    MakeSphere_1.SetContainer( 'localhost/FactoryServer' )
    MakeSphere_1.SetComment( 'MakeSphere from GEOM_Superv' )
    MakeSphere_1.Coords( 604 , 348 )
    IMakeSphere_1theX = MakeSphere_1.GetInPort( 'theX' )
    IMakeSphere_1theY = MakeSphere_1.GetInPort( 'theY' )
    IMakeSphere_1theZ = MakeSphere_1.GetInPort( 'theZ' )
    IMakeSphere_1theRadius = MakeSphere_1.GetInPort( 'theRadius' )
    IMakeSphere_1Gate = MakeSphere_1.GetInPort( 'Gate' )
    OMakeSphere_1return = MakeSphere_1.GetOutPort( 'return' )
    OMakeSphere_1Gate = MakeSphere_1.GetOutPort( 'Gate' )
    
    MakeFuse = GeomGraph.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeFuse' )
    MakeFuse.SetName( 'MakeFuse' )
    MakeFuse.SetAuthor( '' )
    MakeFuse.SetContainer( 'localhost/FactoryServer' )
    MakeFuse.SetComment( 'MakeFuse from GEOM_Superv' )
    MakeFuse.Coords( 790 , 148 )
    IMakeFusetheShape1 = MakeFuse.GetInPort( 'theShape1' )
    IMakeFusetheShape2 = MakeFuse.GetInPort( 'theShape2' )
    IMakeFuseGate = MakeFuse.GetInPort( 'Gate' )
    OMakeFusereturn = MakeFuse.GetOutPort( 'return' )
    OMakeFuseGate = MakeFuse.GetOutPort( 'Gate' )
    
    MakeFuse_1 = GeomGraph.FNode( 'GEOM_Superv' , 'GEOM_Superv' , 'MakeFuse' )
    MakeFuse_1.SetName( 'MakeFuse_1' )
    MakeFuse_1.SetAuthor( '' )
    MakeFuse_1.SetContainer( 'localhost/FactoryServer' )
    MakeFuse_1.SetComment( 'MakeFuse from GEOM_Superv' )
    MakeFuse_1.Coords( 993 , 312 )
    IMakeFuse_1theShape1 = MakeFuse_1.GetInPort( 'theShape1' )
    IMakeFuse_1theShape2 = MakeFuse_1.GetInPort( 'theShape2' )
    IMakeFuse_1Gate = MakeFuse_1.GetInPort( 'Gate' )
    OMakeFuse_1return = MakeFuse_1.GetOutPort( 'return' )
    OMakeFuse_1Gate = MakeFuse_1.GetOutPort( 'Gate' )
    
    # Creation of Links
    LSetStudyIDGateMakeBoxGate = GeomGraph.Link( OSetStudyIDGate , IMakeBoxGate )
    
    LMakeBoxreturnMakeCopytheOriginal = GeomGraph.Link( OMakeBoxreturn , IMakeCopytheOriginal )
    
    LMakeBoxreturnMakeCopy_1theOriginal = GeomGraph.Link( OMakeBoxreturn , IMakeCopy_1theOriginal )
    
    LMakeBoxGateMakeCopyGate = GeomGraph.Link( OMakeBoxGate , IMakeCopyGate )
    
    LMakeCopyreturnTranslateDXDYDZtheObject = GeomGraph.Link( OMakeCopyreturn , ITranslateDXDYDZtheObject )
    
    LMakeCopyGateMakeCopy_1Gate = GeomGraph.Link( OMakeCopyGate , IMakeCopy_1Gate )
    LMakeCopyGateMakeCopy_1Gate.AddCoord( 1 , 363 , 263 )
    LMakeCopyGateMakeCopy_1Gate.AddCoord( 2 , 564 , 141 )
    
    LTranslateDXDYDZreturnMakeFusetheShape1 = GeomGraph.Link( OTranslateDXDYDZreturn , IMakeFusetheShape1 )
    
    LTranslateDXDYDZGateMakeFuseGate = GeomGraph.Link( OTranslateDXDYDZGate , IMakeFuseGate )
    
    LMakeCopy_1returnMakeFusetheShape2 = GeomGraph.Link( OMakeCopy_1return , IMakeFusetheShape2 )
    
    LMakeCopy_1GateMakeSphere_1Gate = GeomGraph.Link( OMakeCopy_1Gate , IMakeSphere_1Gate )
    LMakeCopy_1GateMakeSphere_1Gate.AddCoord( 1 , 572 , 503 )
    LMakeCopy_1GateMakeSphere_1Gate.AddCoord( 2 , 572 , 263 )
    
    LMakeSphere_1returnMakeFuse_1theShape2 = GeomGraph.Link( OMakeSphere_1return , IMakeFuse_1theShape2 )
    
    LMakeSphere_1GateTranslateDXDYDZGate = GeomGraph.Link( OMakeSphere_1Gate , ITranslateDXDYDZGate )
    
    LMakeFusereturnMakeFuse_1theShape1 = GeomGraph.Link( OMakeFusereturn , IMakeFuse_1theShape1 )
    
    LMakeFuseGateMakeFuse_1Gate = GeomGraph.Link( OMakeFuseGate , IMakeFuse_1Gate )
    
    # Input datas
    ISetStudyIDtheStudyID.Input( 1 )
    IMakeBoxtheX1.Input( 0 )
    IMakeBoxtheY1.Input( 0 )
    IMakeBoxtheZ1.Input( 0 )
    IMakeBoxtheX2.Input( 50 )
    IMakeBoxtheY2.Input( 50 )
    IMakeBoxtheZ2.Input( 50 )
    ITranslateDXDYDZtheDX.Input( 10 )
    ITranslateDXDYDZtheDY.Input( 10 )
    ITranslateDXDYDZtheDZ.Input( 10 )
    IMakeSphere_1theX.Input( 0 )
    IMakeSphere_1theY.Input( 0 )
    IMakeSphere_1theZ.Input( 0 )
    IMakeSphere_1theRadius.Input( 12 )
    
    # Output Ports of the graph
    #OMakeFuse_1return = MakeFuse_1.GetOutPort( 'return' )
    return GeomGraph


GeomGraph = DefGeomGraph()
