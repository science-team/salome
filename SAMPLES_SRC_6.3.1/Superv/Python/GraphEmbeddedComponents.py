#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphEmbeddedComponents
#
from SuperV import *

# Graph creation of GraphEmbeddedComponents
def DefGraphEmbeddedComponents() :
    GraphEmbeddedComponents = Graph( 'GraphEmbeddedComponents' )
    GraphEmbeddedComponents.SetName( 'GraphEmbeddedComponents' )
    GraphEmbeddedComponents.SetAuthor( '' )
    GraphEmbeddedComponents.SetComment( '' )
    GraphEmbeddedComponents.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphEmbeddedComponents.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'localhost/SuperVisionContainer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 1 , 152 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    Sub = GraphEmbeddedComponents.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'localhost/SuperVisionContainer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 412 , 377 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    Mul = GraphEmbeddedComponents.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
    Mul.SetName( 'Mul' )
    Mul.SetAuthor( '' )
    Mul.SetContainer( 'localhost/SuperVisionContainer' )
    Mul.SetComment( 'Mul from MulComponent' )
    Mul.Coords( 412 , 152 )
    IMulx = Mul.GetInPort( 'x' )
    IMuly = Mul.GetInPort( 'y' )
    IMulGate = Mul.GetInPort( 'Gate' )
    OMulz = Mul.GetOutPort( 'z' )
    OMulGate = Mul.GetOutPort( 'Gate' )
    
    Div = GraphEmbeddedComponents.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
    Div.SetName( 'Div' )
    Div.SetAuthor( '' )
    Div.SetContainer( 'localhost/SuperVisionContainer' )
    Div.SetComment( 'Div from DivComponent' )
    Div.Coords( 622 , 124 )
    IDivx = Div.GetInPort( 'x' )
    IDivy = Div.GetInPort( 'y' )
    IDivGate = Div.GetInPort( 'Gate' )
    ODivz = Div.GetOutPort( 'z' )
    ODivGate = Div.GetOutPort( 'Gate' )
    
    Addition = GraphEmbeddedComponents.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
    Addition.SetName( 'Addition' )
    Addition.SetAuthor( '' )
    Addition.SetContainer( 'localhost/AdditionServer' )
    Addition.SetComment( 'Addition from AddComponent' )
    Addition.Coords( 0 , 0 )
    IAdditionGate = Addition.GetInPort( 'Gate' )
    OAdditionAdder = Addition.GetOutPort( 'Adder' )
    OAdditionGate = Addition.GetOutPort( 'Gate' )
    
    Addition_1 = GraphEmbeddedComponents.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
    Addition_1.SetName( 'Addition_1' )
    Addition_1.SetAuthor( '' )
    Addition_1.SetContainer( 'localhost/Addition_1Server' )
    Addition_1.SetComment( 'Addition from AddComponent' )
    Addition_1.Coords( 4 , 327 )
    IAddition_1Gate = Addition_1.GetInPort( 'Gate' )
    OAddition_1Adder = Addition_1.GetOutPort( 'Adder' )
    OAddition_1Gate = Addition_1.GetOutPort( 'Gate' )
    
    # Creation of Computing Nodes
    AddAndCompare_ServiceinParameter = []
    AddAndCompare_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'Adder' , 'Adder' ) )
    AddAndCompare_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'x' ) )
    AddAndCompare_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'y' ) )
    AddAndCompare_ServiceinParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'Adder' , 'anOtherAdder' ) )
    AddAndCompare_ServiceoutParameter = []
    AddAndCompare_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'FuncValue' ) )
    AddAndCompare_ServiceoutParameter.append( SALOME_ModuleCatalog.ServicesParameter( 'double' , 'z' ) )
    AddAndCompare_ServiceinStreamParameter = []
    AddAndCompare_ServiceoutStreamParameter = []
    AddAndCompare_Service = SALOME_ModuleCatalog.Service( 'AddAndCompare' , AddAndCompare_ServiceinParameter , AddAndCompare_ServiceoutParameter , AddAndCompare_ServiceinStreamParameter , AddAndCompare_ServiceoutStreamParameter , 0 , 0 )
    AddAndCompare = GraphEmbeddedComponents.CNode( AddAndCompare_Service )
    AddAndCompare.SetName( 'AddAndCompare' )
    AddAndCompare.SetAuthor( '' )
    AddAndCompare.SetComment( 'Python function' )
    AddAndCompare.Coords( 233 , 0 )
    IAddAndCompareAdder = AddAndCompare.GetInPort( 'Adder' )
    IAddAndComparex = AddAndCompare.GetInPort( 'x' )
    IAddAndComparey = AddAndCompare.GetInPort( 'y' )
    IAddAndCompareanOtherAdder = AddAndCompare.GetInPort( 'anOtherAdder' )
    IAddAndCompareGate = AddAndCompare.GetInPort( 'Gate' )
    OAddAndCompareFuncValue = AddAndCompare.GetOutPort( 'FuncValue' )
    OAddAndComparez = AddAndCompare.GetOutPort( 'z' )
    OAddAndCompareGate = AddAndCompare.GetOutPort( 'Gate' )
    
    # Creation of Links
    LAddFuncValueMulx = GraphEmbeddedComponents.Link( OAddFuncValue , IMulx )
    
    LAddFuncValueAddAndComparex = GraphEmbeddedComponents.Link( OAddFuncValue , IAddAndComparex )
    LAddFuncValueAddAndComparex.AddCoord( 1 , 195 , 108 )
    LAddFuncValueAddAndComparex.AddCoord( 2 , 195 , 233 )
    
    LAddzSubx = GraphEmbeddedComponents.Link( OAddz , ISubx )
    LAddzSubx.AddCoord( 1 , 187 , 459 )
    LAddzSubx.AddCoord( 2 , 186 , 262 )
    
    LAddzAddAndComparey = GraphEmbeddedComponents.Link( OAddz , IAddAndComparey )
    LAddzAddAndComparey.AddCoord( 1 , 187 , 139 )
    LAddzAddAndComparey.AddCoord( 2 , 186 , 261 )
    
    LSubzDivx = GraphEmbeddedComponents.Link( OSubz , IDivx )
    LSubzDivx.AddCoord( 1 , 598 , 203 )
    LSubzDivx.AddCoord( 2 , 598 , 457 )
    
    LMulzDivy = GraphEmbeddedComponents.Link( OMulz , IDivy )
    
    LAdditionAdderAddAndCompareAdder = GraphEmbeddedComponents.Link( OAdditionAdder , IAddAndCompareAdder )
    
    LAddition_1AdderAddAndCompareanOtherAdder = GraphEmbeddedComponents.Link( OAddition_1Adder , IAddAndCompareanOtherAdder )
    LAddition_1AdderAddAndCompareanOtherAdder.AddCoord( 1 , 215 , 168 )
    LAddition_1AdderAddAndCompareanOtherAdder.AddCoord( 2 , 214 , 407 )
    
    # Input datas
    IAddx.Input( 1 )
    IAddy.Input( 2 )
    ISuby.Input( 3 )
    IMuly.Input( 4 )
    
    # Output Ports of the graph
    #ODivz = Div.GetOutPort( 'z' )
    #OAddAndCompareFuncValue = AddAndCompare.GetOutPort( 'FuncValue' )
    #OAddAndComparez = AddAndCompare.GetOutPort( 'z' )
    return GraphEmbeddedComponents


GraphEmbeddedComponents = DefGraphEmbeddedComponents()

GraphEmbeddedComponents.Run()
GraphEmbeddedComponents.DoneW()
GraphEmbeddedComponents.PrintPorts()

