#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphTypesCheck
#
from SuperV import *
# Graph creation 
GraphTypesCheck = Graph( 'GraphTypesCheck' )
GraphTypesCheck.SetName( 'GraphTypesCheck' )
GraphTypesCheck.SetAuthor( 'JR' )
GraphTypesCheck.SetComment( '' )
GraphTypesCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

BoolCheck = GraphTypesCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'BoolCheck' )
BoolCheck.SetName( 'BoolCheck' )
BoolCheck.SetAuthor( '' )
BoolCheck.SetContainer( 'localhost/FactoryServer' )
BoolCheck.SetComment( 'BoolCheck from TypesCheck' )
BoolCheck.Coords( 202 , 108 )

CharCheck = GraphTypesCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'CharCheck' )
CharCheck.SetName( 'CharCheck' )
CharCheck.SetAuthor( '' )
CharCheck.SetContainer( 'localhost/FactoryServer' )
CharCheck.SetComment( 'CharCheck from TypesCheck' )
CharCheck.Coords( 13 , 166 )

ShortCheck = GraphTypesCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'ShortCheck' )
ShortCheck.SetName( 'ShortCheck' )
ShortCheck.SetAuthor( '' )
ShortCheck.SetContainer( 'localhost/FactoryServer' )
ShortCheck.SetComment( 'ShortCheck from TypesCheck' )
ShortCheck.Coords( 205 , 258 )

FloatCheck = GraphTypesCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'FloatCheck' )
FloatCheck.SetName( 'FloatCheck' )
FloatCheck.SetAuthor( '' )
FloatCheck.SetContainer( 'localhost/FactoryServer' )
FloatCheck.SetComment( 'FloatCheck from TypesCheck' )
FloatCheck.Coords( 204 , 409 )

Addition = GraphTypesCheck.FNode( 'AddComponent' , 'AddComponent' , 'Addition' )
Addition.SetName( 'Addition' )
Addition.SetAuthor( '' )
Addition.SetContainer( 'localhost/FactoryServer' )
Addition.SetComment( 'Addition from AddComponent' )
Addition.Coords( 16 , 459 )

LongCheck = GraphTypesCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'LongCheck' )
LongCheck.SetName( 'LongCheck' )
LongCheck.SetAuthor( '' )
LongCheck.SetContainer( 'localhost/FactoryServer' )
LongCheck.SetComment( 'LongCheck from TypesCheck' )
LongCheck.Coords( 17 , 318 )

MiscTypes = GraphTypesCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 431 , 170 )

StringCheck = GraphTypesCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'StringCheck' )
StringCheck.SetName( 'StringCheck' )
StringCheck.SetAuthor( '' )
StringCheck.SetContainer( 'localhost/FactoryServer' )
StringCheck.SetComment( 'StringCheck from TypesCheck' )
StringCheck.Coords( 13 , 10 )

# Creation of Links
BoolCheckOutBool = BoolCheck.Port( 'OutBool' )
MiscTypesInBool = GraphTypesCheck.Link( BoolCheckOutBool , MiscTypes.Port( 'InBool' ) )
MiscTypesInBool.AddCoord( 1 , 402 , 280 )
MiscTypesInBool.AddCoord( 2 , 401 , 188 )

CharCheckOutChar = CharCheck.Port( 'OutChar' )
MiscTypesInChar = GraphTypesCheck.Link( CharCheckOutChar , MiscTypes.Port( 'InChar' ) )
MiscTypesInChar.AddCoord( 1 , 382 , 307 )
MiscTypesInChar.AddCoord( 2 , 382 , 247 )

ShortCheckOutShort = ShortCheck.Port( 'OutShort' )
MiscTypesInShort = GraphTypesCheck.Link( ShortCheckOutShort , MiscTypes.Port( 'InShort' ) )

FloatCheckOutFloat = FloatCheck.Port( 'OutFloat' )
MiscTypesInFloat = GraphTypesCheck.Link( FloatCheckOutFloat , MiscTypes.Port( 'InFloat' ) )
MiscTypesInFloat.AddCoord( 1 , 400 , 395 )
MiscTypesInFloat.AddCoord( 2 , 399 , 490 )

AdditionAdder = Addition.Port( 'Adder' )
MiscTypesInObjRef = GraphTypesCheck.Link( AdditionAdder , MiscTypes.Port( 'InObjRef' ) )
MiscTypesInObjRef.AddCoord( 1 , 415 , 453 )
MiscTypesInObjRef.AddCoord( 2 , 415 , 540 )

LongCheckOutLong = LongCheck.Port( 'OutLong' )
MiscTypesInLong = GraphTypesCheck.Link( LongCheckOutLong , MiscTypes.Port( 'InLong' ) )
MiscTypesInLong.AddCoord( 1 , 383 , 367 )
MiscTypesInLong.AddCoord( 2 , 383 , 399 )

StringCheckOutString = StringCheck.Port( 'OutString' )
MiscTypesInString = GraphTypesCheck.Link( StringCheckOutString , MiscTypes.Port( 'InString' ) )
MiscTypesInString.AddCoord( 1 , 412 , 251 )
MiscTypesInString.AddCoord( 2 , 411 , 91 )

# Creation of Input datas
BoolCheckInBool = BoolCheck.Input( 'InBool' , 1)
CharCheckInChar = CharCheck.Input( 'InChar' , 255)
ShortCheckInShort = ShortCheck.Input( 'InShort' , 16383)
FloatCheckInFloat = FloatCheck.Input( 'InFloat' , 3.14159)
LongCheckInLong = LongCheck.Input( 'InLong' , 2147483647)
MiscTypesInDouble = MiscTypes.Input( 'InDouble' , 3.14159)
StringCheckInString = StringCheck.Input( 'InString' , 'aString')

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphTypesCheck.Run()
GraphTypesCheck.DoneW()
GraphTypesCheck.PrintPorts()
