#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphWrongPythonFunctions_1
#
from SuperV import *

# Graph creation of GraphWrongPythonFunctions_1
def DefGraphWrongPythonFunctions_1() :
    GraphWrongPythonFunctions_1 = Graph( 'GraphWrongPythonFunctions_1' )
    GraphWrongPythonFunctions_1.SetName( 'GraphWrongPythonFunctions_1' )
    GraphWrongPythonFunctions_1.SetAuthor( 'JR' )
    GraphWrongPythonFunctions_1.SetComment( '' )
    GraphWrongPythonFunctions_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    # Creation of InLine Nodes
    PyIsOdd = []
    PyIsOdd.append( 'from timed import *      ' )
    PyIsOdd.append( 'def IsOdd(a) :          ' )
    PyIsOdd.append( '    print a,"IsOdd (GraphLoopSwitch1)"         ' )
    PyIsOdd.append( '    sleep( 1 )      ' )
    PyIsOdd.append( '    return a        ' )
    IsOdd = GraphWrongPythonFunctions_1.INode( 'IsOdd' , PyIsOdd )
    IsOdd.SetName( 'IsOdd' )
    IsOdd.SetAuthor( '' )
    IsOdd.SetComment( 'Python function' )
    IsOdd.Coords( 397 , 1 )
    IIsOdda = IsOdd.InPort( 'a' , 'long' )
    IIsOddGate = IsOdd.GetInPort( 'Gate' )
    OIsOdda = IsOdd.OutPort( 'a' , 'long' )
    OIsOddGate = IsOdd.GetOutPort( 'Gate' )
    
    PyValEven = []
    PyValEven.append( 'from timed import *  ' )
    PyValEven.append( 'def ValEven(Even) :  ' )
    PyValEven.append( '    sleep(1)  ' )
    PyValEven.append( '    return Even    ' )
    PyValEven.append( '' )
    ValEven = GraphWrongPythonFunctions_1.INode( 'ValEven' , PyValEven )
    ValEven.SetName( 'ValEven' )
    ValEven.SetAuthor( '' )
    ValEven.SetComment( 'Compute Node' )
    ValEven.Coords( 396 , 222 )
    IValEvenEven = ValEven.InPort( 'Even' , 'long' )
    IValEvenGate = ValEven.GetInPort( 'Gate' )
    OValEvenEven = ValEven.OutPort( 'Even' , 'long' )
    OValEvenGate = ValEven.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInitLoop = []
    PyInitLoop.append( 'def InitLoop(Index,Min,Max)     ' )
    PyInitLoop.append( '	Index = Min      ' )
    PyInitLoop.append( '	return Index,Min,Max      ' )
    PyInitLoop.append( '   ' )
    PyMoreInitLoop = []
    PyMoreInitLoop.append( 'def More() ' )
    PyMoreInitLoop.append( '    return toto     ' )
    PyNextInitLoop = []
    PyNextInitLoop.append( 'def Next(...)    ' )
    PyNextInitLoop.append( '    return Ok     ' )
    InitLoop,EndOfInitLoop = GraphWrongPythonFunctions_1.LNode( 'InitLoop' , PyInitLoop , 'More' , PyMoreInitLoop , 'Next' , PyNextInitLoop )
    EndOfInitLoop.SetName( 'EndOfInitLoop' )
    EndOfInitLoop.SetAuthor( '' )
    EndOfInitLoop.SetComment( 'Compute Node' )
    EndOfInitLoop.Coords( 767 , 102 )
    PyEndOfInitLoop = []
    PyEndOfInitLoop.append( 'def EndOfInitLoop( DoLoop , Index , Min , Max ) : ' )
    PyEndOfInitLoop.append( '	Index = Index + 1      ' )
    PyEndOfInitLoop.append( '	if Index <= Max :    ' )
    PyEndOfInitLoop.append( '		DoLoop = 1      ' )
    PyEndOfInitLoop.append( '	else :      ' )
    PyEndOfInitLoop.append( '		DoLoop = 0      ' )
    PyEndOfInitLoop.append( '	return DoLoop,Index,Min,Max :  ' )
    EndOfInitLoop.SetPyFunction( 'EndOfInitLoop' , PyEndOfInitLoop )
    IInitLoopDoLoop = InitLoop.GetInPort( 'DoLoop' )
    IInitLoopIndex = InitLoop.InPort( 'Index' , 'long' )
    IInitLoopMin = InitLoop.InPort( 'Min' , 'long' )
    IInitLoopMax = InitLoop.InPort( 'Max' , 'long' )
    IInitLoopGate = InitLoop.GetInPort( 'Gate' )
    OInitLoopDoLoop = InitLoop.GetOutPort( 'DoLoop' )
    OInitLoopIndex = InitLoop.GetOutPort( 'Index' )
    OInitLoopMin = InitLoop.GetOutPort( 'Min' )
    OInitLoopMax = InitLoop.GetOutPort( 'Max' )
    IEndOfInitLoopDoLoop = EndOfInitLoop.GetInPort( 'DoLoop' )
    IEndOfInitLoopIndex = EndOfInitLoop.GetInPort( 'Index' )
    IEndOfInitLoopMin = EndOfInitLoop.GetInPort( 'Min' )
    IEndOfInitLoopMax = EndOfInitLoop.GetInPort( 'Max' )
    IEndOfInitLoopGate = EndOfInitLoop.GetInPort( 'Gate' )
    OEndOfInitLoopDoLoop = EndOfInitLoop.GetOutPort( 'DoLoop' )
    OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    OEndOfInitLoopGate = EndOfInitLoop.GetOutPort( 'Gate' )
    InitLoop.SetName( 'InitLoop' )
    InitLoop.SetAuthor( '' )
    InitLoop.SetComment( 'Compute Node' )
    InitLoop.Coords( 10 , 108 )
    
    # Creation of Switch Nodes
    PySwitch = []
    PySwitch.append( 'from time import *    ' )
    PySwitch.append( 'def Switch(a) ' )
    PySwitch.append( '    if a & 1 == 0 :    ' )
    PySwitch.append( '        sleep(1)    ' )
    PySwitch.append( '    return a & 1,1-(a&1),a      ' )
    Switch,EndSwitch = GraphWrongPythonFunctions_1.SNode( 'Switch' , PySwitch )
    EndSwitch.SetName( 'EndSwitch' )
    EndSwitch.SetAuthor( '' )
    EndSwitch.SetComment( 'Compute Node' )
    EndSwitch.Coords( 587 , 103 )
    PyEndSwitch = []
    PyEndSwitch.append( 'def EndSwitch(a,Even) ' )
    PyEndSwitch.append( '    sleep(1)       ' )
    PyEndSwitch.append( '    return a,      ' )
    EndSwitch.SetPyFunction( 'EndSwitch' , PyEndSwitch )
    IEndSwitcha = EndSwitch.InPort( 'a' , 'long' )
    IEndSwitchEven = EndSwitch.InPort( 'Even' , 'long' )
    IEndSwitchDefault = EndSwitch.GetInPort( 'Default' )
    OEndSwitcha = EndSwitch.OutPort( 'a' , 'long' )
    OEndSwitchGate = EndSwitch.GetOutPort( 'Gate' )
    Switch.SetName( 'Switch' )
    Switch.SetAuthor( '' )
    Switch.SetComment( 'Compute Node' )
    Switch.Coords( 194 , 109 )
    ISwitcha = Switch.InPort( 'a' , 'long' )
    ISwitchGate = Switch.GetInPort( 'Gate' )
    OSwitchOdd = Switch.OutPort( 'Odd' , 'long' )
    OSwitchEven = Switch.OutPort( 'Even' , 'int' )
    OSwitcha = Switch.OutPort( 'a' , 'int' )
    OSwitchDefault = Switch.GetOutPort( 'Default' )
    
    # Creation of Links
    LIsOddaEndSwitcha = GraphWrongPythonFunctions_1.Link( OIsOdda , IEndSwitcha )
    
    LInitLoopIndexSwitcha = GraphWrongPythonFunctions_1.Link( OInitLoopIndex , ISwitcha )
    
    LInitLoopMinEndOfInitLoopMin = GraphWrongPythonFunctions_1.Link( OInitLoopMin , IEndOfInitLoopMin )
    
    LInitLoopMaxEndOfInitLoopMax = GraphWrongPythonFunctions_1.Link( OInitLoopMax , IEndOfInitLoopMax )
    
    LSwitchOddIsOddGate = GraphWrongPythonFunctions_1.Link( OSwitchOdd , IIsOddGate )
    
    LSwitchEvenValEvenEven = GraphWrongPythonFunctions_1.Link( OSwitchEven , IValEvenEven )
    
    LSwitchaIsOdda = GraphWrongPythonFunctions_1.Link( OSwitcha , IIsOdda )
    
    LSwitchDefaultEndSwitchDefault = GraphWrongPythonFunctions_1.Link( OSwitchDefault , IEndSwitchDefault )
    
    LEndSwitchaEndOfInitLoopIndex = GraphWrongPythonFunctions_1.Link( OEndSwitcha , IEndOfInitLoopIndex )
    
    LValEvenEvenEndSwitchEven = GraphWrongPythonFunctions_1.Link( OValEvenEven , IEndSwitchEven )
    
    # Input datas
    IInitLoopIndex.Input( 0 )
    IInitLoopMin.Input( 5 )
    IInitLoopMax.Input( 23 )
    
    # Output Ports of the graph
    #OEndOfInitLoopIndex = EndOfInitLoop.GetOutPort( 'Index' )
    #OEndOfInitLoopMin = EndOfInitLoop.GetOutPort( 'Min' )
    #OEndOfInitLoopMax = EndOfInitLoop.GetOutPort( 'Max' )
    return GraphWrongPythonFunctions_1


GraphWrongPythonFunctions_1 = DefGraphWrongPythonFunctions_1()
