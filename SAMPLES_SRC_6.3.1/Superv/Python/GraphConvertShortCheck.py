#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertShortCheck
#
from SuperV import *
# Graph creation 
GraphConvertShortCheck = Graph( 'GraphConvertShortCheck' )
GraphConvertShortCheck.SetName( 'GraphConvertShortCheck' )
GraphConvertShortCheck.SetAuthor( 'JR' )
GraphConvertShortCheck.SetComment( 'Check conversions of Short' )
GraphConvertShortCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertShortCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PyShort = []
PyShort.append( 'def Short() :  ' )
PyShort.append( '    aShort = 32767 ' )
PyShort.append( '    print type( aShort ),"aShort",aShort ' )
PyShort.append( '    return aShort  ' )
PyShort.append( ' ' )
Short = GraphConvertShortCheck.INode( 'Short' , PyShort )
Short.OutPort( 'OutShort' , 'short' )
Short.SetName( 'Short' )
Short.SetAuthor( 'JR' )
Short.SetComment( 'InLine Node' )
Short.Coords( 14 , 114 )

# Creation of Links
ShortOutShort = Short.Port( 'OutShort' )
MiscTypesInString = GraphConvertShortCheck.Link( ShortOutShort , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertShortCheck.Link( ShortOutShort , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertShortCheck.Link( ShortOutShort , MiscTypes.Port( 'InChar' ) )

MiscTypesInShort = GraphConvertShortCheck.Link( ShortOutShort , MiscTypes.Port( 'InShort' ) )

MiscTypesInLong = GraphConvertShortCheck.Link( ShortOutShort , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertShortCheck.Link( ShortOutShort , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertShortCheck.Link( ShortOutShort , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertShortCheck.Link( ShortOutShort , MiscTypes.Port( 'InObjRef' ) )

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertShortCheck.Run()
GraphConvertShortCheck.DoneW()
GraphConvertShortCheck.PrintPorts()
