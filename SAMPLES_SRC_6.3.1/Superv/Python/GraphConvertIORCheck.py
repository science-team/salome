#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphConvertIORCheck
#
from SuperV import *
# Graph creation 
GraphConvertIORCheck = Graph( 'GraphConvertIORCheck' )
GraphConvertIORCheck.SetName( 'GraphConvertIORCheck' )
GraphConvertIORCheck.SetAuthor( 'JR' )
GraphConvertIORCheck.SetComment( 'Check conversions of IOR' )
GraphConvertIORCheck.Coords( 0 , 0 )

# Creation of Factory Nodes

MiscTypes = GraphConvertIORCheck.FNode( 'TypesCheck' , 'TypesCheck' , 'MiscTypes' )
MiscTypes.SetName( 'MiscTypes' )
MiscTypes.SetAuthor( '' )
MiscTypes.SetContainer( 'localhost/FactoryServer' )
MiscTypes.SetComment( 'MiscTypes from TypesCheck' )
MiscTypes.Coords( 284 , 28 )

# Creation of InLine Nodes
PySyrComponent = []
PySyrComponent.append( 'from LifeCycleCORBA import *       ' )
PySyrComponent.append( 'def SyrComponent( aContainer , aComponent ) :       ' )
PySyrComponent.append( '    print "SyrComponent(",aContainer,",",aComponent,")"       ' )
PySyrComponent.append( '    orb = CORBA.ORB_init([], CORBA.ORB_ID)       ' )
PySyrComponent.append( '    print "SyrComponent orb",orb     ' )
PySyrComponent.append( '    lcc = LifeCycleCORBA(orb)       ' )
PySyrComponent.append( '    print "SyrComponent lcc",lcc     ' )
PySyrComponent.append( '    print "SyrComponent(",aContainer,",",aComponent,")"       ' )
PySyrComponent.append( '    ComponentRef = lcc.FindOrLoadComponent( aContainer , aComponent )       ' )
PySyrComponent.append( '    print "SyrComponent(",aContainer,",",aComponent,") --> ",ComponentRef       ' )
PySyrComponent.append( '    IOR = orb.object_to_string( ComponentRef )  ' )
PySyrComponent.append( '    return IOR  ' )
PySyrComponent.append( ' ' )
SyrComponent = GraphConvertIORCheck.INode( 'SyrComponent' , PySyrComponent )
SyrComponent.InPort( 'aContainer' , 'string' )
SyrComponent.InPort( 'aComponent' , 'string' )
SyrComponent.OutPort( 'anIOR' , 'string' )
SyrComponent.SetName( 'SyrComponent' )
SyrComponent.SetAuthor( 'JR' )
SyrComponent.SetComment( 'InLine Node' )
SyrComponent.Coords( 14 , 114 )

# Creation of Links
SyrComponentanIOR = SyrComponent.Port( 'anIOR' )
MiscTypesInShort = GraphConvertIORCheck.Link( SyrComponentanIOR , MiscTypes.Port( 'InShort' ) )

MiscTypesInString = GraphConvertIORCheck.Link( SyrComponentanIOR , MiscTypes.Port( 'InString' ) )

MiscTypesInBool = GraphConvertIORCheck.Link( SyrComponentanIOR , MiscTypes.Port( 'InBool' ) )

MiscTypesInChar = GraphConvertIORCheck.Link( SyrComponentanIOR , MiscTypes.Port( 'InChar' ) )

MiscTypesInLong = GraphConvertIORCheck.Link( SyrComponentanIOR , MiscTypes.Port( 'InLong' ) )

MiscTypesInFloat = GraphConvertIORCheck.Link( SyrComponentanIOR , MiscTypes.Port( 'InFloat' ) )

MiscTypesInDouble = GraphConvertIORCheck.Link( SyrComponentanIOR , MiscTypes.Port( 'InDouble' ) )

MiscTypesInObjRef = GraphConvertIORCheck.Link( SyrComponentanIOR , MiscTypes.Port( 'InObjRef' ) )

# Creation of Input datas
SyrComponentaContainer = SyrComponent.Input( 'aContainer' , 'FactoryServerPy')
SyrComponentaComponent = SyrComponent.Input( 'aComponent' , 'SyrControlComponent')

# Creation of Output variables
MiscTypesOutString = MiscTypes.Port( 'OutString' )
MiscTypesOutBool = MiscTypes.Port( 'OutBool' )
MiscTypesOutChar = MiscTypes.Port( 'OutChar' )
MiscTypesOutShort = MiscTypes.Port( 'OutShort' )
MiscTypesOutLong = MiscTypes.Port( 'OutLong' )
MiscTypesOutFloat = MiscTypes.Port( 'OutFloat' )
MiscTypesOutDouble = MiscTypes.Port( 'OutDouble' )
MiscTypesOutObjRef = MiscTypes.Port( 'OutObjRef' )

GraphConvertIORCheck.Run()
GraphConvertIORCheck.DoneW()
GraphConvertIORCheck.PrintPorts()
