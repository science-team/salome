#  Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
#  Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
#  CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#
# Generated python file of Graph GraphLoopMacroNodesOutput
#
from SuperV import *

# Graph creation of GraphLoopMacroNodesOutput
def DefGraphLoopMacroNodesOutput() :
    GraphLoopMacroNodesOutput = Graph( 'GraphLoopMacroNodesOutput' )
    GraphLoopMacroNodesOutput.SetName( 'GraphLoopMacroNodesOutput' )
    GraphLoopMacroNodesOutput.SetAuthor( 'JR' )
    GraphLoopMacroNodesOutput.SetComment( '' )
    GraphLoopMacroNodesOutput.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphLoopMacroNodesOutput.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 205 , 238 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    Sub = GraphLoopMacroNodesOutput.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 394 , 146 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    Mul = GraphLoopMacroNodesOutput.FNode( 'MulComponent' , 'MulComponent' , 'Mul' )
    Mul.SetName( 'Mul' )
    Mul.SetAuthor( '' )
    Mul.SetContainer( 'FactoryServer' )
    Mul.SetComment( 'Mul from MulComponent' )
    Mul.Coords( 821 , 319 )
    IMulx = Mul.GetInPort( 'x' )
    IMuly = Mul.GetInPort( 'y' )
    IMulGate = Mul.GetInPort( 'Gate' )
    OMulz = Mul.GetOutPort( 'z' )
    OMulGate = Mul.GetOutPort( 'Gate' )
    
    Div = GraphLoopMacroNodesOutput.FNode( 'DivComponent' , 'DivComponent' , 'Div' )
    Div.SetName( 'Div' )
    Div.SetAuthor( '' )
    Div.SetContainer( 'FactoryServer' )
    Div.SetComment( 'Div from DivComponent' )
    Div.Coords( 825 , 133 )
    IDivx = Div.GetInPort( 'x' )
    IDivy = Div.GetInPort( 'y' )
    IDivGate = Div.GetInPort( 'Gate' )
    ODivz = Div.GetOutPort( 'z' )
    ODivGate = Div.GetOutPort( 'Gate' )
    
    # Creation of InLine Nodes
    PyResultsControl = []
    PyResultsControl.append( 'def ResultsControl(x,y,zDiv,zMul) :' )
    PyResultsControl.append( '    Add_FuncValue = x - y' )
    PyResultsControl.append( '    Add_z = x + y' )
    PyResultsControl.append( '    Sub_z = 1.5 - Add_z' )
    PyResultsControl.append( '    Macro_GraphAdd_Add_FuncValue = Sub_z - Add_FuncValue' )
    PyResultsControl.append( '    Macro_GraphAdd_Add_z = Sub_z + Add_FuncValue' )
    PyResultsControl.append( '    Macro_GraphSub_z = Add_FuncValue - Add_z' )
    PyResultsControl.append( '    Div_z = Macro_GraphAdd_Add_FuncValue/Macro_GraphAdd_Add_z' )
    PyResultsControl.append( '    Mul_z = Sub_z * Macro_GraphSub_z' )
    PyResultsControl.append( '    OK = \'Okay\'' )
    PyResultsControl.append( '    if Div_z != zDiv or Mul_z != zMul :' )
    PyResultsControl.append( '        OK = \'KO\'' )
    PyResultsControl.append( '    return zDiv,zMul,Div_z,Mul_z,OK' )
    PyResultsControl.append( '' )
    ResultsControl = GraphLoopMacroNodesOutput.INode( 'ResultsControl' , PyResultsControl )
    ResultsControl.SetName( 'ResultsControl' )
    ResultsControl.SetAuthor( '' )
    ResultsControl.SetComment( 'Compute Node' )
    ResultsControl.Coords( 1102 , 196 )
    IResultsControlx = ResultsControl.InPort( 'x' , 'double' )
    IResultsControly = ResultsControl.InPort( 'y' , 'double' )
    IResultsControlzDiv = ResultsControl.InPort( 'zDiv' , 'double' )
    IResultsControlzMul = ResultsControl.InPort( 'zMul' , 'double' )
    IResultsControlGate = ResultsControl.GetInPort( 'Gate' )
    OResultsControlzDiv = ResultsControl.OutPort( 'zDiv' , 'double' )
    OResultsControlzMul = ResultsControl.OutPort( 'zMul' , 'double' )
    OResultsControlDiv_z = ResultsControl.OutPort( 'Div_z' , 'double' )
    OResultsControlMul_z = ResultsControl.OutPort( 'Mul_z' , 'double' )
    OResultsControlOK = ResultsControl.OutPort( 'OK' , 'string' )
    OResultsControlGate = ResultsControl.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyLoop = []
    PyLoop.append( 'def Init(Index,Min,Max,Incr,zDiv,zMul,OK) :' )
    PyLoop.append( '        if Min <= Max :' )
    PyLoop.append( '            Index = Min' )
    PyLoop.append( '        else :' )
    PyLoop.append( '            Index = Max' )
    PyLoop.append( '        return Index,Min,Max,Incr,zDiv,zMul,OK' )
    PyLoop.append( '' )
    PyMoreLoop = []
    PyMoreLoop.append( 'def More(Index,Min,Max,Incr,zDiv,zMul,OK) :' )
    PyMoreLoop.append( '        if Index < Max :' )
    PyMoreLoop.append( '            DoLoop = 1' )
    PyMoreLoop.append( '        else :' )
    PyMoreLoop.append( '            DoLoop = 0' )
    PyMoreLoop.append( '        return DoLoop,Index,Min,Max,Incr,zDiv,zMul,OK' )
    PyMoreLoop.append( '' )
    PyNextLoop = []
    PyNextLoop.append( 'def Next(Index,Min,Max,Incr,zDiv,zMul,OK) :' )
    PyNextLoop.append( '        Index = Index + Incr' )
    PyNextLoop.append( '        return Index,Min,Max,Incr,zDiv,zMul,OK' )
    PyNextLoop.append( '' )
    Loop,EndLoop = GraphLoopMacroNodesOutput.LNode( 'Init' , PyLoop , 'More' , PyMoreLoop , 'Next' , PyNextLoop )
    EndLoop.SetName( 'EndLoop' )
    EndLoop.SetAuthor( '' )
    EndLoop.SetComment( 'Compute Node' )
    EndLoop.Coords( 1354 , 156 )
    PyEndLoop = []
    PyEndLoop.append( 'def EndLoop(DoLoop,Index,Min,Max,Incr,zDiv,zMul,OK) :' )
    PyEndLoop.append( '	print \'EndLoop\',DoLoop,Index,Min,Max,Incr,zDiv,zMul,OK' )
    PyEndLoop.append( '	if OK != \'Okay\' :' )
    PyEndLoop.append( '	    DoLoop = 0' )
    PyEndLoop.append( '	return DoLoop,Index,Min,Max,Incr ,zDiv,zMul,OK' )
    PyEndLoop.append( '' )
    EndLoop.SetPyFunction( 'EndLoop' , PyEndLoop )
    ILoopDoLoop = Loop.GetInPort( 'DoLoop' )
    ILoopIndex = Loop.InPort( 'Index' , 'long' )
    ILoopMin = Loop.InPort( 'Min' , 'long' )
    ILoopMax = Loop.InPort( 'Max' , 'long' )
    ILoopIncr = Loop.InPort( 'Incr' , 'long' )
    ILoopzDiv = Loop.InPort( 'zDiv' , 'double' )
    ILoopzMul = Loop.InPort( 'zMul' , 'double' )
    ILoopOK = Loop.InPort( 'OK' , 'string' )
    ILoopGate = Loop.GetInPort( 'Gate' )
    OLoopDoLoop = Loop.GetOutPort( 'DoLoop' )
    OLoopIndex = Loop.GetOutPort( 'Index' )
    OLoopMin = Loop.GetOutPort( 'Min' )
    OLoopMax = Loop.GetOutPort( 'Max' )
    OLoopIncr = Loop.GetOutPort( 'Incr' )
    OLoopzDiv = Loop.GetOutPort( 'zDiv' )
    OLoopzMul = Loop.GetOutPort( 'zMul' )
    OLoopOK = Loop.GetOutPort( 'OK' )
    OLoopGate = Loop.GetOutPort( 'Gate' )
    IEndLoopDoLoop = EndLoop.GetInPort( 'DoLoop' )
    IEndLoopIndex = EndLoop.GetInPort( 'Index' )
    IEndLoopMin = EndLoop.GetInPort( 'Min' )
    IEndLoopMax = EndLoop.GetInPort( 'Max' )
    IEndLoopIncr = EndLoop.GetInPort( 'Incr' )
    IEndLoopzDiv = EndLoop.GetInPort( 'zDiv' )
    IEndLoopzMul = EndLoop.GetInPort( 'zMul' )
    IEndLoopOK = EndLoop.GetInPort( 'OK' )
    IEndLoopGate = EndLoop.GetInPort( 'Gate' )
    OEndLoopDoLoop = EndLoop.GetOutPort( 'DoLoop' )
    OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    OEndLoopMin = EndLoop.GetOutPort( 'Min' )
    OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    OEndLoopIncr = EndLoop.GetOutPort( 'Incr' )
    OEndLoopzDiv = EndLoop.GetOutPort( 'zDiv' )
    OEndLoopzMul = EndLoop.GetOutPort( 'zMul' )
    OEndLoopOK = EndLoop.GetOutPort( 'OK' )
    OEndLoopGate = EndLoop.GetOutPort( 'Gate' )
    Loop.SetName( 'Loop' )
    Loop.SetAuthor( '' )
    Loop.SetComment( 'Compute Node' )
    Loop.Coords( 5 , 265 )
    
    # Creation of Macro Nodes
    GraphSub_1 = DefGraphSub_1()
    Macro_GraphSub = GraphLoopMacroNodesOutput.GraphMNode( GraphSub_1 )
    Macro_GraphSub.SetCoupled( 'GraphSub_1' )
    Macro_GraphSub.SetName( 'Macro_GraphSub' )
    Macro_GraphSub.SetAuthor( '' )
    Macro_GraphSub.SetComment( 'Macro Node' )
    Macro_GraphSub.Coords( 512 , 319 )
    IMacro_GraphSubSub__x = Macro_GraphSub.GetInPort( 'Sub__x' )
    IMacro_GraphSubSub__y = Macro_GraphSub.GetInPort( 'Sub__y' )
    IMacro_GraphSubGate = Macro_GraphSub.GetInPort( 'Gate' )
    OMacro_GraphSubSub__z = Macro_GraphSub.GetOutPort( 'Sub__z' )
    OMacro_GraphSubGate = Macro_GraphSub.GetOutPort( 'Gate' )
    
    GraphAddLoop = DefGraphAddLoop()
    Macro_GraphAddLoop = GraphLoopMacroNodesOutput.GraphMNode( GraphAddLoop )
    Macro_GraphAddLoop.SetCoupled( 'GraphAddLoop' )
    Macro_GraphAddLoop.SetName( 'Macro_GraphAddLoop' )
    Macro_GraphAddLoop.SetAuthor( '' )
    Macro_GraphAddLoop.SetComment( 'Macro Node' )
    Macro_GraphAddLoop.Coords( 628 , 14 )
    IMacro_GraphAddLoopInit_1__Index = Macro_GraphAddLoop.GetInPort( 'Init_1__Index' )
    IMacro_GraphAddLoopInit_1__Min = Macro_GraphAddLoop.GetInPort( 'Init_1__Min' )
    IMacro_GraphAddLoopInit_1__Max = Macro_GraphAddLoop.GetInPort( 'Init_1__Max' )
    IMacro_GraphAddLoopInit_1__Incr = Macro_GraphAddLoop.GetInPort( 'Init_1__Incr' )
    IMacro_GraphAddLoopInit_1__zDiv = Macro_GraphAddLoop.GetInPort( 'Init_1__zDiv' )
    IMacro_GraphAddLoopInit_1__zMul = Macro_GraphAddLoop.GetInPort( 'Init_1__zMul' )
    IMacro_GraphAddLoopInit_1__OK = Macro_GraphAddLoop.GetInPort( 'Init_1__OK' )
    IMacro_GraphAddLoopInit_1__x = Macro_GraphAddLoop.GetInPort( 'Init_1__x' )
    IMacro_GraphAddLoopInit_1__y = Macro_GraphAddLoop.GetInPort( 'Init_1__y' )
    IMacro_GraphAddLoopGate = Macro_GraphAddLoop.GetInPort( 'Gate' )
    OMacro_GraphAddLoopAdd__FuncValue = Macro_GraphAddLoop.GetOutPort( 'Add__FuncValue' )
    OMacro_GraphAddLoopAdd__z = Macro_GraphAddLoop.GetOutPort( 'Add__z' )
    OMacro_GraphAddLoopEndInit_1__Index = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__Index' )
    OMacro_GraphAddLoopEndInit_1__Min = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__Min' )
    OMacro_GraphAddLoopEndInit_1__Max = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__Max' )
    OMacro_GraphAddLoopEndInit_1__Incr = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__Incr' )
    OMacro_GraphAddLoopEndInit_1__zDiv = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__zDiv' )
    OMacro_GraphAddLoopEndInit_1__zMul = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__zMul' )
    OMacro_GraphAddLoopEndInit_1__OK = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__OK' )
    OMacro_GraphAddLoopEndInit_1__x = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__x' )
    OMacro_GraphAddLoopEndInit_1__y = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__y' )
    OMacro_GraphAddLoopGate = Macro_GraphAddLoop.GetOutPort( 'Gate' )
    
    # Creation of Links
    LAddFuncValueMacro_GraphSubSub__x = GraphLoopMacroNodesOutput.Link( OAddFuncValue , IMacro_GraphSubSub__x )
    
    LAddFuncValueMacro_GraphAddLoopInit_1__y = GraphLoopMacroNodesOutput.Link( OAddFuncValue , IMacro_GraphAddLoopInit_1__y )
    
    LAddzSuby = GraphLoopMacroNodesOutput.Link( OAddz , ISuby )
    
    LAddzMacro_GraphSubSub__y = GraphLoopMacroNodesOutput.Link( OAddz , IMacro_GraphSubSub__y )
    
    LSubzMulx = GraphLoopMacroNodesOutput.Link( OSubz , IMulx )
    LSubzMulx.AddCoord( 1 , 767 , 389 )
    LSubzMulx.AddCoord( 2 , 767 , 297 )
    LSubzMulx.AddCoord( 3 , 592 , 297 )
    LSubzMulx.AddCoord( 4 , 592 , 217 )
    
    LSubzMacro_GraphAddLoopInit_1__x = GraphLoopMacroNodesOutput.Link( OSubz , IMacro_GraphAddLoopInit_1__x )
    
    LSubzMacro_GraphAddLoopInit_1__zDiv = GraphLoopMacroNodesOutput.Link( OSubz , IMacro_GraphAddLoopInit_1__zDiv )
    
    LSubzMacro_GraphAddLoopInit_1__zMul = GraphLoopMacroNodesOutput.Link( OSubz , IMacro_GraphAddLoopInit_1__zMul )
    
    LSubzMacro_GraphAddLoopInit_1__OK = GraphLoopMacroNodesOutput.Link( OSubz , IMacro_GraphAddLoopInit_1__OK )
    
    LMulzResultsControlzMul = GraphLoopMacroNodesOutput.Link( OMulz , IResultsControlzMul )
    
    LDivzResultsControlzDiv = GraphLoopMacroNodesOutput.Link( ODivz , IResultsControlzDiv )
    
    LMacro_GraphSubSub__zMuly = GraphLoopMacroNodesOutput.Link( OMacro_GraphSubSub__z , IMuly )
    
    LMacro_GraphSubGateMulGate = GraphLoopMacroNodesOutput.Link( OMacro_GraphSubGate , IMulGate )
    
    LLoopIndexEndLoopIndex = GraphLoopMacroNodesOutput.Link( OLoopIndex , IEndLoopIndex )
    
    LLoopIndexAddx = GraphLoopMacroNodesOutput.Link( OLoopIndex , IAddx )
    
    LLoopIndexResultsControlx = GraphLoopMacroNodesOutput.Link( OLoopIndex , IResultsControlx )
    
    LLoopMinEndLoopMin = GraphLoopMacroNodesOutput.Link( OLoopMin , IEndLoopMin )
    
    LLoopMaxEndLoopMax = GraphLoopMacroNodesOutput.Link( OLoopMax , IEndLoopMax )
    
    LLoopMaxAddy = GraphLoopMacroNodesOutput.Link( OLoopMax , IAddy )
    
    LLoopMaxResultsControly = GraphLoopMacroNodesOutput.Link( OLoopMax , IResultsControly )
    
    LLoopIncrEndLoopIncr = GraphLoopMacroNodesOutput.Link( OLoopIncr , IEndLoopIncr )
    
    LResultsControlzDivEndLoopzDiv = GraphLoopMacroNodesOutput.Link( OResultsControlzDiv , IEndLoopzDiv )
    
    LResultsControlzMulEndLoopzMul = GraphLoopMacroNodesOutput.Link( OResultsControlzMul , IEndLoopzMul )
    
    LResultsControlOKEndLoopOK = GraphLoopMacroNodesOutput.Link( OResultsControlOK , IEndLoopOK )
    
    LMacro_GraphAddLoopAdd__FuncValueDivx = GraphLoopMacroNodesOutput.Link( OMacro_GraphAddLoopAdd__FuncValue , IDivx )
    
    LMacro_GraphAddLoopAdd__zDivy = GraphLoopMacroNodesOutput.Link( OMacro_GraphAddLoopAdd__z , IDivy )
    
    # Input datas
    ISubx.Input( 1.5 )
    ILoopIndex.Input( 0 )
    ILoopMin.Input( 7 )
    ILoopMax.Input( 17 )
    ILoopIncr.Input( 1 )
    ILoopzDiv.Input( 0 )
    ILoopzMul.Input( 0 )
    ILoopOK.Input( 'Okay' )
    IMacro_GraphAddLoopInit_1__Index.Input( 0 )
    IMacro_GraphAddLoopInit_1__Min.Input( 5 )
    IMacro_GraphAddLoopInit_1__Max.Input( 11 )
    IMacro_GraphAddLoopInit_1__Incr.Input( 1 )
    
    # Output Ports of the graph
    #OEndLoopIndex = EndLoop.GetOutPort( 'Index' )
    #OEndLoopMin = EndLoop.GetOutPort( 'Min' )
    #OEndLoopMax = EndLoop.GetOutPort( 'Max' )
    #OEndLoopIncr = EndLoop.GetOutPort( 'Incr' )
    #OEndLoopzDiv = EndLoop.GetOutPort( 'zDiv' )
    #OEndLoopzMul = EndLoop.GetOutPort( 'zMul' )
    #OEndLoopOK = EndLoop.GetOutPort( 'OK' )
    #OResultsControlDiv_z = ResultsControl.GetOutPort( 'Div_z' )
    #OResultsControlMul_z = ResultsControl.GetOutPort( 'Mul_z' )
    #OMacro_GraphAddLoopEndInit_1__Index = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__Index' )
    #OMacro_GraphAddLoopEndInit_1__Min = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__Min' )
    #OMacro_GraphAddLoopEndInit_1__Max = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__Max' )
    #OMacro_GraphAddLoopEndInit_1__Incr = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__Incr' )
    #OMacro_GraphAddLoopEndInit_1__zDiv = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__zDiv' )
    #OMacro_GraphAddLoopEndInit_1__zMul = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__zMul' )
    #OMacro_GraphAddLoopEndInit_1__OK = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__OK' )
    #OMacro_GraphAddLoopEndInit_1__x = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__x' )
    #OMacro_GraphAddLoopEndInit_1__y = Macro_GraphAddLoop.GetOutPort( 'EndInit_1__y' )
    return GraphLoopMacroNodesOutput

# Graph creation of GraphSub_1
def DefGraphSub_1() :
    GraphSub_1 = Graph( 'GraphSub_1' )
    GraphSub_1.SetCoupled( 'Macro_GraphSub' )
    GraphSub_1.SetName( 'GraphSub_1' )
    GraphSub_1.SetAuthor( '' )
    GraphSub_1.SetComment( '' )
    GraphSub_1.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Sub = GraphSub_1.FNode( 'SubComponent' , 'SubComponent' , 'Sub' )
    Sub.SetName( 'Sub' )
    Sub.SetAuthor( '' )
    Sub.SetContainer( 'localhost/FactoryServer' )
    Sub.SetComment( 'Sub from SubComponent' )
    Sub.Coords( 55 , 71 )
    ISubx = Sub.GetInPort( 'x' )
    ISuby = Sub.GetInPort( 'y' )
    ISubGate = Sub.GetInPort( 'Gate' )
    OSubz = Sub.GetOutPort( 'z' )
    OSubGate = Sub.GetOutPort( 'Gate' )
    
    # Input Ports of the graph
    #ISubx = Sub.GetInPort( 'x' )
    #ISuby = Sub.GetInPort( 'y' )
    
    # Output Ports of the graph
    #OSubz = Sub.GetOutPort( 'z' )
    return GraphSub_1

# Graph creation of GraphAddLoop
def DefGraphAddLoop() :
    GraphAddLoop = Graph( 'GraphAddLoop' )
    GraphAddLoop.SetCoupled( 'Macro_GraphAddLoop' )
    GraphAddLoop.SetName( 'GraphAddLoop' )
    GraphAddLoop.SetAuthor( '' )
    GraphAddLoop.SetComment( '' )
    GraphAddLoop.Coords( 0 , 0 )
    
    # Creation of Factory Nodes
    
    Add = GraphAddLoop.FNode( 'AddComponent' , 'AddComponent' , 'Add' )
    Add.SetName( 'Add' )
    Add.SetAuthor( '' )
    Add.SetContainer( 'localhost/FactoryServer' )
    Add.SetComment( 'Add from AddComponent' )
    Add.Coords( 248 , 247 )
    IAddx = Add.GetInPort( 'x' )
    IAddy = Add.GetInPort( 'y' )
    IAddGate = Add.GetInPort( 'Gate' )
    OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    OAddz = Add.GetOutPort( 'z' )
    OAddGate = Add.GetOutPort( 'Gate' )
    
    # Creation of Loop Nodes
    PyInit_1 = []
    PyInit_1.append( 'def Init_1(Index,Min,Max,Incr,zDiv,zMul,OK,x,y) :' )
    PyInit_1.append( '        if Min <= Max :' )
    PyInit_1.append( '            Index = Min' )
    PyInit_1.append( '        else :' )
    PyInit_1.append( '            Index = Max' )
    PyInit_1.append( '        return Index,Min,Max,Incr,zDiv,zMul,OK,x,y' )
    PyInit_1.append( '' )
    PyMoreInit_1 = []
    PyMoreInit_1.append( 'def More_1(Index,Min,Max,Incr,zDiv,zMul,OK,x,y) :' )
    PyMoreInit_1.append( '        if Index < Max :' )
    PyMoreInit_1.append( '            DoLoop = 1' )
    PyMoreInit_1.append( '        else :' )
    PyMoreInit_1.append( '            DoLoop = 0' )
    PyMoreInit_1.append( '        return DoLoop,Index,Min,Max,Incr,zDiv,zMul,OK,x,y' )
    PyMoreInit_1.append( '' )
    PyNextInit_1 = []
    PyNextInit_1.append( 'def Next_1(Index,Min,Max,Incr,zDiv,zMul,OK,x,y) :' )
    PyNextInit_1.append( '        Index = Index + Incr' )
    PyNextInit_1.append( '        return Index,Min,Max,Incr,zDiv,zMul,OK,x,y' )
    PyNextInit_1.append( '' )
    Init_1,EndInit_1 = GraphAddLoop.LNode( 'Init_1' , PyInit_1 , 'More_1' , PyMoreInit_1 , 'Next_1' , PyNextInit_1 )
    EndInit_1.SetName( 'EndInit_1' )
    EndInit_1.SetAuthor( '' )
    EndInit_1.SetComment( 'Compute Node' )
    EndInit_1.Coords( 460 , 146 )
    PyEndInit_1 = []
    EndInit_1.SetPyFunction( 'EndInit_1' , PyEndInit_1 )
    IInit_1DoLoop = Init_1.GetInPort( 'DoLoop' )
    IInit_1Index = Init_1.InPort( 'Index' , 'long' )
    IInit_1Min = Init_1.InPort( 'Min' , 'long' )
    IInit_1Max = Init_1.InPort( 'Max' , 'long' )
    IInit_1Incr = Init_1.InPort( 'Incr' , 'long' )
    IInit_1zDiv = Init_1.InPort( 'zDiv' , 'double' )
    IInit_1zMul = Init_1.InPort( 'zMul' , 'double' )
    IInit_1OK = Init_1.InPort( 'OK' , 'string' )
    IInit_1x = Init_1.InPort( 'x' , 'double' )
    IInit_1y = Init_1.InPort( 'y' , 'double' )
    IInit_1Gate = Init_1.GetInPort( 'Gate' )
    OInit_1DoLoop = Init_1.GetOutPort( 'DoLoop' )
    OInit_1Index = Init_1.GetOutPort( 'Index' )
    OInit_1Min = Init_1.GetOutPort( 'Min' )
    OInit_1Max = Init_1.GetOutPort( 'Max' )
    OInit_1Incr = Init_1.GetOutPort( 'Incr' )
    OInit_1zDiv = Init_1.GetOutPort( 'zDiv' )
    OInit_1zMul = Init_1.GetOutPort( 'zMul' )
    OInit_1OK = Init_1.GetOutPort( 'OK' )
    OInit_1x = Init_1.GetOutPort( 'x' )
    OInit_1y = Init_1.GetOutPort( 'y' )
    OInit_1Gate = Init_1.GetOutPort( 'Gate' )
    IEndInit_1DoLoop = EndInit_1.GetInPort( 'DoLoop' )
    IEndInit_1Index = EndInit_1.GetInPort( 'Index' )
    IEndInit_1Min = EndInit_1.GetInPort( 'Min' )
    IEndInit_1Max = EndInit_1.GetInPort( 'Max' )
    IEndInit_1Incr = EndInit_1.GetInPort( 'Incr' )
    IEndInit_1zDiv = EndInit_1.GetInPort( 'zDiv' )
    IEndInit_1zMul = EndInit_1.GetInPort( 'zMul' )
    IEndInit_1OK = EndInit_1.GetInPort( 'OK' )
    IEndInit_1x = EndInit_1.GetInPort( 'x' )
    IEndInit_1y = EndInit_1.GetInPort( 'y' )
    IEndInit_1Gate = EndInit_1.GetInPort( 'Gate' )
    OEndInit_1DoLoop = EndInit_1.GetOutPort( 'DoLoop' )
    OEndInit_1Index = EndInit_1.GetOutPort( 'Index' )
    OEndInit_1Min = EndInit_1.GetOutPort( 'Min' )
    OEndInit_1Max = EndInit_1.GetOutPort( 'Max' )
    OEndInit_1Incr = EndInit_1.GetOutPort( 'Incr' )
    OEndInit_1zDiv = EndInit_1.GetOutPort( 'zDiv' )
    OEndInit_1zMul = EndInit_1.GetOutPort( 'zMul' )
    OEndInit_1OK = EndInit_1.GetOutPort( 'OK' )
    OEndInit_1x = EndInit_1.GetOutPort( 'x' )
    OEndInit_1y = EndInit_1.GetOutPort( 'y' )
    OEndInit_1Gate = EndInit_1.GetOutPort( 'Gate' )
    Init_1.SetName( 'Init_1' )
    Init_1.SetAuthor( '' )
    Init_1.SetComment( 'Compute Node' )
    Init_1.Coords( 37 , 147 )
    
    # Creation of Links
    LAddGateEndInit_1Gate = GraphAddLoop.Link( OAddGate , IEndInit_1Gate )
    
    LInit_1IndexEndInit_1Index = GraphAddLoop.Link( OInit_1Index , IEndInit_1Index )
    
    LInit_1MinEndInit_1Min = GraphAddLoop.Link( OInit_1Min , IEndInit_1Min )
    
    LInit_1MaxEndInit_1Max = GraphAddLoop.Link( OInit_1Max , IEndInit_1Max )
    
    LInit_1IncrEndInit_1Incr = GraphAddLoop.Link( OInit_1Incr , IEndInit_1Incr )
    
    LInit_1zDivEndInit_1zDiv = GraphAddLoop.Link( OInit_1zDiv , IEndInit_1zDiv )
    
    LInit_1zMulEndInit_1zMul = GraphAddLoop.Link( OInit_1zMul , IEndInit_1zMul )
    
    LInit_1OKEndInit_1OK = GraphAddLoop.Link( OInit_1OK , IEndInit_1OK )
    
    LInit_1xEndInit_1x = GraphAddLoop.Link( OInit_1x , IEndInit_1x )
    
    LInit_1xAddx = GraphAddLoop.Link( OInit_1x , IAddx )
    
    LInit_1yEndInit_1y = GraphAddLoop.Link( OInit_1y , IEndInit_1y )
    
    LInit_1yAddy = GraphAddLoop.Link( OInit_1y , IAddy )
    
    # Input Ports of the graph
    #IInit_1Index = Init_1.GetInPort( 'Index' )
    #IInit_1Min = Init_1.GetInPort( 'Min' )
    #IInit_1Max = Init_1.GetInPort( 'Max' )
    #IInit_1Incr = Init_1.GetInPort( 'Incr' )
    #IInit_1zDiv = Init_1.GetInPort( 'zDiv' )
    #IInit_1zMul = Init_1.GetInPort( 'zMul' )
    #IInit_1OK = Init_1.GetInPort( 'OK' )
    #IInit_1x = Init_1.GetInPort( 'x' )
    #IInit_1y = Init_1.GetInPort( 'y' )
    
    # Output Ports of the graph
    #OAddFuncValue = Add.GetOutPort( 'FuncValue' )
    #OAddz = Add.GetOutPort( 'z' )
    #OEndInit_1Index = EndInit_1.GetOutPort( 'Index' )
    #OEndInit_1Min = EndInit_1.GetOutPort( 'Min' )
    #OEndInit_1Max = EndInit_1.GetOutPort( 'Max' )
    #OEndInit_1Incr = EndInit_1.GetOutPort( 'Incr' )
    #OEndInit_1zDiv = EndInit_1.GetOutPort( 'zDiv' )
    #OEndInit_1zMul = EndInit_1.GetOutPort( 'zMul' )
    #OEndInit_1OK = EndInit_1.GetOutPort( 'OK' )
    #OEndInit_1x = EndInit_1.GetOutPort( 'x' )
    #OEndInit_1y = EndInit_1.GetOutPort( 'y' )
    return GraphAddLoop


GraphLoopMacroNodesOutput = DefGraphLoopMacroNodesOutput()
