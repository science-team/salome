#!/bin/sh
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# ====================================================================
# This script is just here to illustrate the procedure for preparing
# the configure process. It creates configuration files (ex:
# ltmain.sh) using libtoolize, the aclocal macro, the configure script
# using autoconf and some scripts used in building process (ex:
# install-sh) using automake. Automake is used here to creates the
# files Makefile.in from the files Makefile.am.
# ====================================================================
# CONF_DIR is the path containing the present script
#
CONF_DIR=`echo $0 | sed -e "s,[^/]*$,,;s,/$,,;s,^$,.,"`
cd ${CONF_DIR}

# ____________________________________________________________________
# aclocal creates the aclocal.m4 file from the standard macro and the
# custom macro embedded in the directory salome_adm/unix/config_files.
# output:
#   aclocal.m4
#   autom4te.cache (directory)
echo "====================================================== aclocal"

aclocal -I salome_adm/unix/config_files || exit 1


# ____________________________________________________________________
# libtoolize creates some configuration files (ltmain.sh,
# config.guess and config.sub). It only depends on the libtool
# version. The files are created in the directory specified with the
# AC_CONFIG_AUX_DIR(<mydir>) tag (see configure.ac).
# output:
#   salome_adm/unix/config_files/config.guess
#   salome_adm/unix/config_files/config.sub
#   salome_adm/unix/config_files/ltmain.sh
echo "====================================================== libtoolize"

libtoolize --force --copy --automake || exit 1


# ____________________________________________________________________
# autoconf creates the configure script from the file configure.ac (or
# configure.in if configure.ac doesn't exist)
# output:
#   configure
echo "====================================================== autoconf"

autoconf

# ____________________________________________________________________
# autoheader creates config.h.in
# output:
#   configure
#echo "====================================================== autoheader"

#autoheader

# ____________________________________________________________________
# automake creates some scripts used in building process
# (install-sh, missing, ...). It only depends on the automake
# version. The files are created in the directory specified with the
# AC_CONFIG_AUX_DIR(<mydir>) tag (see configure.ac). This step also
# creates the Makefile.in files from the Makefile.am files.
# output:
#   salome_adm/unix/config_files/compile
#   salome_adm/unix/config_files/depcomp
#   salome_adm/unix/config_files/install-sh
#   salome_adm/unix/config_files/missing
#   salome_adm/unix/config_files/py-compile
#   Makefile.in (from Makefile.am)
echo "====================================================== automake"

automake --add-missing --copy --gnu


# ____________________________________________________________________
#
# Note that automake could be executed at last after autoconf. The
# order doesn't matter.
#
# When modifying a Makefile.am file, after a first step of
# configure+make, you just have to :
# - go to the top source directory and run automake, to rebuild the Makefile.in,
# - go to the top build directory and run config.status to rebuild the
#   Makefile from the Makefile.in,
# - go to the source directory being modified, and then run make.
#
