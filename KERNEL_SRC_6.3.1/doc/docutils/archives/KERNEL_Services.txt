=================================================================
KERNEL Services for end user (Python interface)
=================================================================

*html version of this document is produced with docutils*::

  rst2html < doc.txt > doc.html

This document corresponds to SALOME2 3.2.0

.. contents::
.. sectnum::

+-------------------------------------------+
| **WORK in PROGRESS, INCOMPLETE DOCUMENT** |
+-------------------------------------------+

-------------------------------------------------------------------------------

In a SALOME application, distributed components, servers and clients use
the CORBA middleware for comunication. CORBA interfaces are defined via idl
files. All the different CORBA interfaces are available for users in Python,
see CORBA interfaces below.

For some general purpose services, CORBA interfaces have been encapsulated
in order to provide a simple interface (encapsulation is generally done in
C++ classes, and a Python SWIG interface is also generated from C++, to 
ensure a consistent behavior between C++ modules and Python modules or user
script).

General purpose services
========================

SALOME services access from a Python shell
------------------------------------------
See SALOME_Application_ for detailed instructions to launch a Python
interpreter with full acces to the SALOME environment and services.

.. _SALOME_Application: ./SALOME_Application.txt

You can use the embedded Python interpreter in Grahic User Interface, or an
external interpreter, with::

   ./runSession
   python

In either cases, SALOME services access is done with::

   import salome
   salome.salome_init()

In the embedded interpreter, it is already done, but there is no problem to
do it several times, so it is preferable to add these instructions
systematically in your scripts, to allow them to work in all configurations.

Container and component instanciation
-------------------------------------

See LifeCycleCORBA_ for the C++ interface (Python interface obtained with SWIG
is very similar).

.. _LifeCycleCORBA: ./tui/KERNEL/classSALOME__LifeCycleCORBA.html


In the following example, a test component provided in KERNEL is launched
in the local container, "FactoryServer", created when SALOME starts::

   import salome
   salome.salome_init()
   
   import LifeCycleCORBA
   lcc = LifeCycleCORBA.LifeCycleCORBA()
   obj=lcc.FindOrLoad_Component("FactoryServer","SalomeTestComponent")
   
   import Engines
   comp=obj._narrow(Engines.TestComponent)
   
   comp.Coucou(1)

The answer is something like::

   'TestComponent_i : L = 1'
 
The _narrow() instruction is not always mandatory in Python, but sometimes 
useful to be sure you have got the right type of object. Here, Testcomponent_
interface is defined in CORBA module Engines. With this example, it works also
without the _narrow() instruction::

   obj.Coucou(1)

.. _Testcomponent: ./tui/KERNEL/interfaceEngines_1_1TestComponent.html


In the next example, a component instance is created in a specific Container
defined by it's computer hostname and it's name. Here we use the local
computer. Note that in Utils_Identity_, getShortHostName() gives the short
hostname of the computer, without domain suffixes, which is used in SALOME.
The container process is created here if it does not exists, and a new
component instance is created::

   import salome
   salome.salome_init()
   import LifeCycleCORBA
   lcc = LifeCycleCORBA.LifeCycleCORBA()

   import Utils_Identity
   host  = Utils_Identity.getShortHostName()

   import Engines
   params={}
   params['hostname']=host
   params['container_name']='myContainer'
   comp=lcc.LoadComponent(params,'SalomeTestComponent')
   comp.Coucou(1)

.. _Utils_Identity: ./tui/KERNEL/namespaceUtils__Identity.html

If you want to get a list of containers and component instances, client object
from orbmodule_ provides a list::

   import orbmodule
   clt=orbmodule.client()
   clt.showNS()

.. _orbmodule: ./tui/KERNEL/classorbmodule_1_1client.html

The list looks like::

   Logger.
   ContainerManager.object
   Containers.dir
     cli70ac.dir
       FactoryServerPy.object
       SuperVisionContainer.object
       FactoryServer.object
       FactoryServer.dir
         SalomeTestComponent_inst_1.object
       myContainer.object
       myContainer.dir
         SalomeTestComponent_inst_1.object
         SalomeTestComponent_inst_2.object
   Registry.object
   Kernel.dir
     ModulCatalog.object
     Session.object
   Study.dir
     Study2.object
     extStudy_1.object
     extStudy_2.object
     extStudy_3.object
   myStudyManager.object
   SalomeAppEngine.object


File transfer service
---------------------

See FileTransferCORBA_ for the C++ interface (Python interface obtained with
SWIG is very similar).

.. _FileTransferCORBA: ./tui/KERNEL/classSALOME__FileTransferCORBA.html

The following example shows how to tranfer a file from a remote host to the
client computer. Remote hostname is 'cli76cc', we would like to copy
'tkcvs_8_0_3.tar.gz' from remote to local computer. A full pathname is
required. A container is created on remote computer if it does not exist,
to handle the file transfer::

   import salome
   salome.salome_init()

   import LifeCycleCORBA
   remotefile="/home/prascle/tkcvs_8_0_3.tar.gz"
   aFileTransfer=LifeCycleCORBA.SALOME_FileTransferCORBA('cli76cc',remotefile)
   localFile=aFileTransfer.getLocalFile()


CORBA Naming service access
---------------------------

See SALOME_NamingService_ for the C++ interface. The Python interface
SALOME_NamingServicePy_ is not yet derived from the C++ interface and offers
only the most useful functions.

.. _SALOME_NamingService: ./tui/KERNEL/classSALOME__NamingService.html
.. _SALOME_NamingServicePy: ./tui/KERNEL/classSALOME__NamingServicePy_1_1SALOME__NamingServicePy__i.html

Batch services
--------------

See Batch_ documentation (in french only).

.. _Batch: ./Batch.html/index.html

All IDL Interfaces
==================

Containers and component life cycle, File transfer service
----------------------------------------------------------

+-----------------------------+-----------------------------------------------+
| Engines_                    | Engines CORBA module.                         |
+=============================+===============================================+
| Component_                  | Generic component interface.                  |
|                             | All SALOME components inherit this interface  |
+-----------------------------+-----------------------------------------------+
| Container_                  | Container: host for C++ and Python components |
|                             | components instances                          |
+-----------------------------+-----------------------------------------------+
| FileTransfer_               | Agent for file transfer created by a container|
|                             | copy a local file to a distent client         |
+-----------------------------+-----------------------------------------------+
| FileRef_                    | Reference to a file, used by a container for  |
|                             | file transfers                                |
+-----------------------------+-----------------------------------------------+
| ContainerManager_           | Unique instance, in charge of container       |
|                             | creation on remote computers                  |
+-----------------------------+-----------------------------------------------+
| MPIContainer_               | An exemple of parallel implementation for     |
|                             | containers and components                     |
+-----------------------------+-----------------------------------------------+
| MPIObject_                  |                                               |
+-----------------------------+-----------------------------------------------+

Study management
----------------

+-----------------------------+-----------------------------------------------+
| SALOMEDS_                   | SALOMEDS CORBA module                         |
+=============================+===============================================+
| SALOMEDSidl_                |                                               |
+-----------------------------+-----------------------------------------------+
| SALOMEDS_Attributes_        |                                               |
+-----------------------------+-----------------------------------------------+

High speed transfer, object life cycle, exceptions, GUI interface...
--------------------------------------------------------------------

+-----------------------------+-----------------------------------------------+
| SALOME_                     | SALOME CORBA module                           |
+=============================+===============================================+
| SALOME_Comm_                |                                               |
+-----------------------------+-----------------------------------------------+
| SALOME_GenericObj_          |                                               |
+-----------------------------+-----------------------------------------------+
| SALOME_Exception_           |                                               |
+-----------------------------+-----------------------------------------------+
| SALOME_Session_             |                                               |
+-----------------------------+-----------------------------------------------+

Miscelleanous
-------------
+-----------------------------+-----------------------------------------------+
|                             | other CORBA modules                           |
+=============================+===============================================+
| SALOME_ModuleCatalog_       |                                               |
+-----------------------------+-----------------------------------------------+
| SALOME_RessourcesCatalog_   |                                               |
+-----------------------------+-----------------------------------------------+
| SALOME_Registry_            |                                               |
+-----------------------------+-----------------------------------------------+
| Logger_                     |                                               |
+-----------------------------+-----------------------------------------------+

**Other idl for test purposes**
nstest.idl
SALOME_TestComponent.idl
SALOME_TestModuleCatalog.idl
SALOME_TestMPIComponent.idl
TestNotif.idl

.. _Engines:           ./tui/KERNEL/namespaceEngines.html
.. _Component:         ./tui/KERNEL/interfaceEngines_1_1Component.html
.. _Container:         ./tui/KERNEL/interfaceEngines_1_1Container.html
.. _fileTransfer:      ./tui/KERNEL/interfaceEngines_1_1fileTransfer.html
.. _fileRef:           ./tui/KERNEL/interfaceEngines_1_1fileRef.html
.. _ContainerManager:  ./tui/KERNEL/interfaceEngines_1_1ContainerManager.html
.. _MPIContainer:      ./tui/KERNEL/interfaceEngines_1_1MPIContainer.html
.. _MPIObject:         ./tui/KERNEL/interfaceEngines_1_1MPIObject.html
.. _SALOME:                   ./tui/KERNEL/namespaceSALOME.html
.. _SALOMEDS:                 ./tui/KERNEL/namespaceSALOMEDS.html
.. _SALOME_Component:         ./tui/KERNEL/SALOME__Component_8idl.html
.. _SALOME_ContainerManager:  ./tui/KERNEL/SALOME__ContainerManager_8idl.html
.. _SALOMEDSidl:              ./tui/KERNEL/SALOMEDS_8idl.html
.. _SALOMEDS_Attributes:      ./tui/KERNEL/SALOMEDS__Attributes_8idl.html
.. _SALOME_ModuleCatalog:     ./tui/KERNEL/SALOME__ModuleCatalog_8idl.html
.. _SALOME_RessourcesCatalog: ./tui/KERNEL/SALOME__RessourcesCatalog_8idl.html
.. _SALOME_Registry:          ./tui/KERNEL/SALOME__Registry_8idl.html
.. _Logger:                   ./tui/KERNEL/Logger_8idl.html
.. _SALOME_Comm:              ./tui/KERNEL/SALOME__Comm_8idl.html
.. _SALOME_GenericObj:        ./tui/KERNEL/SALOME__GenericObj_8idl.html
.. _SALOME_Exception:         ./tui/KERNEL/SALOME__Exception_8idl.html
.. _SALOME_Session:           ./tui/KERNEL/SALOME__Session_8idl.html
.. _SALOME_MPIContainer:      ./tui/KERNEL/SALOME__MPIContainer_8idl.html
.. _SALOME_MPIObject:         ./tui/KERNEL/SALOME__MPIObject_8idl.html


-------------------------------------------------------------------------------

+----------------------------------+------------------------------------------+
| `General KERNEL documentation`_  | `End User KERNEL Doxygen documentation`_ |
+----------------------------------+------------------------------------------+

.. _`General KERNEL documentation`:           ./index.html
.. _`End User KERNEL Doxygen documentation`:  ./tui/KERNEL/index.html
