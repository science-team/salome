#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QTextStream>
#include <QtGui/QApplication>
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>

#include <cstdlib>

#include "hxx2salome.h"

// VSR: uncomment for debug mode
// #define __DEBUG__

static QString quote( const QString& val )
{
  QString v = val;
  if ( !v.startsWith( "\"" ) ) v.prepend( "\"" );
  if ( !v.endsWith( "\"" ) ) v.append( "\"" );
  return v;
}

static QString unquote( const QString& val )
{
  QString v = val;
  if ( v.startsWith( "\"" ) ) v = v.remove( 0, 1 );
  if ( v.endsWith( "\"" ) ) v = v.remove( v.length()-1, 1 );
  return v;
}

static QString relFileNameFromDir( const QString& dir, const QString& filename )
{
#ifdef __DEBUG__
  printf( "0. dir: %s, filename: %s\n", qPrintable( dir ), qPrintable( filename ) );
#endif
  if ( !filename.isEmpty() ) {
    QString dirpath  = QFileInfo( dir.isEmpty() ? QDir::currentPath() : dir ).absoluteFilePath();
    QString filepath = QFileInfo( filename ).absoluteFilePath();
#ifdef __DEBUG__
    printf( "1. dirpath: %s, filepath: %s\n", qPrintable( dirpath ), qPrintable( filepath ) );
#endif
    if ( filepath.startsWith( dirpath ) ) {
      QString fpath = filepath.mid( dirpath.length() );
      if ( fpath.startsWith( "/" ) ) fpath.remove( 0, 1 );
#ifdef __DEBUG__
      printf( "2. fpath: %s\n", qPrintable( fpath ) );
#endif
      return fpath;
    }
  }
  return filename;
}

HXX2Salome::HXX2Salome() : QDialog()
{
  setupUi( this );
  retrieve();
}

HXX2Salome::~HXX2Salome()
{
  dump();
}

void HXX2Salome::retrieve()
{
  QFile file( QDir::home().absoluteFilePath( ".hxx2salome" ) );
  if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
    QTextStream in( &file );
    while ( !in.atEnd() ) {
      QString line = in.readLine();
      QRegExp re( "^(.*)\\s+(.*)$" );
      if ( re.exactMatch( line ) ) {
	QString var = re.cap( 1 ).trimmed();
	QString val = unquote( re.cap( 2 ).trimmed() );
	if ( !var.isEmpty() && !val.isEmpty() ) {
	  if ( var == "CppDir" )
	    SourceTreeText->setText( val );
	  else if ( var == "CppInc" )
	    IncludeText->setText( val );
	  else if ( var == "CppLib" )
	    LibraryText->setText( val );
	  else if ( var == "EnvFile" )
	    EnvFileText->setText( val );
	  else if ( var == "SalomeDir" )
	    OutputTreeText->setText( val );
	  else if ( var == "Shell" )
	    ShellChoice->setCurrentIndex( val == "csh" ? 1 : 0 );
	}
      }
    }
    file.close();
  }
}

void HXX2Salome::dump()
{
  QFile file( QDir::home().absoluteFilePath( ".hxx2salome" ) );
  if ( file.open( QIODevice::WriteOnly | QIODevice::Text ) ) {
    file.write( QString( "CppDir %1\n" ).arg( quote( SourceTreeText->text() ) ).toLatin1() );
    file.write( QString( "CppInc %1\n" ).arg( quote( IncludeText->text() ) ).toLatin1() );
    file.write( QString( "CppLib %1\n" ).arg( quote( LibraryText->text() ) ).toLatin1() );
    file.write( QString( "SalomeDir %1\n" ).arg( quote( OutputTreeText->text() ) ).toLatin1() );
    file.write( QString( "EnvFile %1\n" ).arg( quote( EnvFileText->text() ) ).toLatin1() );
    file.write( QString( "Shell %1\n" ).arg( ShellChoice->currentIndex() == 1 ? "csh" : "bash" ).toLatin1() );
  }
  file.close();
}

void HXX2Salome::on_CloseButton_clicked()
{
  close();
}

void HXX2Salome::on_SourceTreeButton_clicked()
{
  QString s = QFileDialog::getExistingDirectory( this,
						 tr( "Get Existing directory" ),
						 SourceTreeText->text() ); 
  if ( !s.isEmpty() ) SourceTreeText->setText( s );
}

void HXX2Salome::on_IncludeButton_clicked()
{
  QString s = QFileDialog::getOpenFileName( this,
					    tr( "Choose a file to open" ),
					    IncludeText->text(),
					    tr( "Include files (*.h *.hh *.hxx *.hpp)" ) );
  if ( !s.isEmpty() ) {
    IncludeText->setText( relFileNameFromDir( SourceTreeText->text().trimmed(), s ) );
  }
}

void HXX2Salome::on_LibraryButton_clicked()
{
  QString s = QFileDialog::getOpenFileName( this,
					    tr( "Choose a file to open" ),
					    LibraryText->text(),
					    tr( "Shared Libraries (*.so *.dll)" ) );
  if ( !s.isEmpty() ) {
    LibraryText->setText( relFileNameFromDir( SourceTreeText->text().trimmed(), s ) );
  }
}

void HXX2Salome::on_EnvFileButton_clicked()
{
  QString s = QFileDialog::getOpenFileName( this,
					    tr( "Choose a script file to open" ),
					    EnvFileText->text(),
					    tr( "Environment files (*.csh *.sh)" ) );
  if ( !s.isEmpty() ) EnvFileText->setText( s );
}

void HXX2Salome::on_OutputTreeButton_clicked()
{
  QString s = QFileDialog::getExistingDirectory( this,
						 tr( "Choose a directory" ),
						 OutputTreeText->text() ); 
  if ( !s.isEmpty() ) OutputTreeText->setText( s );
}

void HXX2Salome::on_GenerateButton_clicked()
{
  // check input validity
  QString CppDir = SourceTreeText->text().trimmed();
  QFileInfo fid( CppDir.isEmpty() ? QString( "." ) : CppDir );

  if ( CppDir.isEmpty() ) {
    QMessageBox::StandardButton btn =
      QMessageBox::warning( this,
			    tr( "Warning" ),
			    tr( "You are about to use the current directory for the C++ component tree!\nContinue?" ),
			    QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes );
    if ( btn != QMessageBox::Yes ) return;
  }

  QString CppInc = IncludeText->text().trimmed();
  if ( CppInc.isEmpty() ) {
    QMessageBox::critical( this,
			   tr( "Error" ),
			   tr( "Component C++ include file is not specified!" ) );
    return;
  }
  if ( QFileInfo( CppInc ).isAbsolute() ) CppInc = relFileNameFromDir( CppDir, CppInc );
  if ( QFileInfo( CppInc ).isAbsolute() ) {
    QMessageBox::critical( this,
			   tr( "Error" ),
			   tr( "Component C++ include file is specified in directory other than\n%1!" ).arg( CppDir ) );
    return;
  }
  CppInc = QFileInfo( CppInc ).fileName();

  QString CppLib = LibraryText->text().trimmed();
  if ( CppLib.isEmpty() ) {
    QMessageBox::critical( this,
			   tr( "Error" ),
			   tr( "Component shared library is not specified!" ) );
    return;
  }
  if ( QFileInfo( CppLib ).isAbsolute() ) CppLib = relFileNameFromDir( CppDir, CppLib );
  if ( QFileInfo( CppLib ).isAbsolute() ) {
    QMessageBox::critical( this,
			   tr( "Error" ),
			   tr( "Component shared library is specified in directory other than\n%1!" ).arg( CppDir ) );
    return;
  }
  CppLib = QFileInfo( CppLib ).fileName();

  QString SalomeDir = OutputTreeText->text().trimmed();
  QFileInfo fis( SalomeDir.isEmpty() ? QString( "." ) : SalomeDir );

  if ( SalomeDir.isEmpty() ) {
    QMessageBox::StandardButton btn =
      QMessageBox::warning( this,
			    tr( "Warning" ),
			    tr( "You are about to use the current directory as the Salome component tree!\nContinue?" ),
			    QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes );
    if ( btn != QMessageBox::Yes ) return;
  }

  QString EnvFile = EnvFileText->text().trimmed();
  QFileInfo fienv( EnvFile );

  // generate command line
  QStringList cmdlist;
  cmdlist << "${HXX2SALOME_ROOT_DIR}/hxx2salome";
  if ( MakeGUI->isChecked() )
    cmdlist << "-g";
  if ( Compile->isChecked() )
    cmdlist << "-c";
  if ( ShellChoice->currentIndex() == 1 )
    cmdlist << "-s csh";
  if ( !EnvFile.isEmpty() ) {
    cmdlist << "-e";
    cmdlist << quote( fienv.absoluteFilePath() );
  }
  cmdlist << quote( fid.absoluteFilePath() );
  cmdlist << quote( CppInc );
  cmdlist << quote( CppLib );
  cmdlist << quote( fis.absoluteFilePath() );
  QString command = cmdlist.join( " " );

  // execute command
#ifdef __DEBUG__
  printf( "command: %s\n", qPrintable( command ) );
#endif
  QApplication::setOverrideCursor( Qt::WaitCursor );
  std::system( command.toLatin1().constData() );
  QApplication::restoreOverrideCursor();
}
