Audit files of Salome SIERPINSKY module
Number of files: 39

Statements are found in audit_C_STATEMENTS_gerber.txt

==== BINARI:
./resources/*.png

==== EMPTY:
./NEWS
./AUTHORS
./ChangeLog

==== NO LICENCE:
./bin/VERSION.in
./README 
./INSTALL

==== GPL2: 

==== Files not mentioned above contain LGPL 2.1 statement:
./bin/
./idl/
./src/
./build_configure
./adm_local/
./configure.ac
./Makefile.am 
./SIERPINSKY_version.h.in
./resources/
./COPYING LGPL 2.1 Licence
./clean_configure
