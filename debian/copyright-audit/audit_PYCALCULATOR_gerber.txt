Audit files of Salome PYCALCULATOR module
Number of files: 36

Statements are found in audit_C_STATEMENTS_gerber.txt

==== BINARI:
./doc/salome/tui/images/*.gif
./resources/*.png

==== EMPTY:
./NEWS
./README
./AUTHORS
./ChangeLog

==== NO LICENCE:
./bin/VERSION.in
./doc/salome/tui/static/myheader.html
./doc/salome/tui/static/doxygen.css
./doc/salome/tui/static/footer.html
./adm_local/unix/config_files/README 
./INSTALL

==== STATEMENT-1:

==== GPL2:

==== Files not mentioned above contain LGPL 2.1 statement:
./bin/*
./doc/*
./idl/*
./src/*
./build_configure
./adm_local/*
./configure.ac
./Makefile.am
./resources/*
./COPYING
./clean_configure
