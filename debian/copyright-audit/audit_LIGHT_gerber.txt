Audit files of Salome LIGHT module
Number of files: 49

Statements are found in audit_C_STATEMENTS_gerber.txt

==== BINARI:
./resources/*.png

==== EMPTY:
./AUTHORS
./ChangeLog
./NEWS

==== NO LICENCE:
./INSTALL
./README
./resources/queen.txt
./bin/VERSION.in

==== Files not mentioned above contain LGPL 2.1 statement:
./LIGHT_version.h.in
./adm_local/*
./Makefile.am
./src/*
./clean_configure
./build_configure
./COPYINGL LGPL 2.1 License
./configure.ac
./resources/*
./bin/Makefile.am
