<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>ICON_QXGRAPH_RESET</source>
        <translation>qx_view_reset.png</translation>
    </message>
    <message>
        <source>ICON_QXGRAPH_ZOOM</source>
        <translation>qx_view_zoom.png</translation>
    </message>
    <message>
        <source>ICON_QXGRAPH_GLOBALPAN</source>
        <translation>qx_view_glpan.png</translation>
    </message>
    <message>
        <source>ICON_QXGRAPH_PAN</source>
        <translation>qx_view_pan.png</translation>
    </message>
    <message>
        <source>ICON_QXGRAPH_FITALL</source>
        <translation>qx_view_fitall.png</translation>
    </message>
    <message>
        <source>ICON_QXGRAPH_FITAREA</source>
        <translation>qx_view_fitarea.png</translation>
    </message>
</context>
</TS>
