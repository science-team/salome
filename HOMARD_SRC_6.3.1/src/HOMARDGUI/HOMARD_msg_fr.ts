<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>@default</name>
    <message>
        <source>HOM_MEN_HOMARD</source>
        <translation>HOMARD</translation>
    </message>
    <message>
        <source>HOM_MEN_NEW_CASE</source>
        <translation>Nouveau cas</translation>
    </message>
    <message>
        <source>HOM_MEN_NEW_ITERATION</source>
        <translation>Nouvelle itération</translation>
    </message>
    <message>
        <source>HOM_MEN_COMPUTE</source>
        <translation>Calculer</translation>
    </message>
    <message>
        <source>HOM_MEN_EDIT_CASE</source>
        <translation>Editer le cas</translation>
    </message>
    <message>
        <source>HOM_MEN_EDIT_ITERATION</source>
        <translation>Editer l&apos;itération</translation>
    </message>
    <message>
        <source>HOM_MEN_EDIT_HYPO</source>
        <translation>Editer l&apos;hypothèse</translation>
    </message>
    <message>
        <source>HOM_MEN_EDIT_ZONE</source>
        <translation>Editer la zone</translation>
    </message>
    <message>
        <source>HOM_MEN_EDIT_BOUNDARY</source>
        <translation>Editer la frontière</translation>
    </message>
    <message>
        <source>HOM_MEN_EDIT_MESS_FILE</source>
        <translation>Afficher le fichier</translation>
    </message>
    <message>
        <source>HOM_TOP_HOMARD</source>
        <translation>HOMARD</translation>
    </message>
    <message>
        <source>HOM_TOP_NEW_CASE</source>
        <translation>Nouveau cas</translation>
    </message>
    <message>
        <source>HOM_TOP_NEW_ITERATION</source>
        <translation>Nouvelle itération</translation>
    </message>
    <message>
        <source>HOM_TOP_COMPUTE</source>
        <translation>Calculer</translation>
    </message>
    <message>
        <source>HOM_TOP_EDIT_CASE</source>
        <translation>Editer le cas</translation>
    </message>
    <message>
        <source>HOM_TOP_EDIT_ITERATION</source>
        <translation>Editer l&apos;itération</translation>
    </message>
    <message>
        <source>HOM_TOP_EDIT_HYPO</source>
        <translation>Editer l&apos;hypothèse</translation>
    </message>
    <message>
        <source>HOM_TOP_EDIT_ZONE</source>
        <translation>Editer la zone</translation>
    </message>
    <message>
        <source>HOM_TOP_EDIT_BOUNDARY</source>
        <translation>Editer la frontière</translation>
    </message>
    <message>
        <source>HOM_TOP_EDIT_MESS_FILE</source>
        <translation>Afficher le fichier</translation>
    </message>
    <message>
        <source>HOM_STB_HOMARD</source>
        <translation>HOMARD</translation>
    </message>
    <message>
        <source>HOM_STB_NEW_CASE</source>
        <translation>Nouveau cas</translation>
    </message>
    <message>
        <source>HOM_STB_NEW_ITERATION</source>
        <translation>Nouvelle itération</translation>
    </message>
    <message>
        <source>HOM_STB_COMPUTE</source>
        <translation>Calculer</translation>
    </message>
    <message>
        <source>HOM_STB_EDIT_CASE</source>
        <translation>Editer le cas</translation>
    </message>
    <message>
        <source>HOM_STB_EDIT_ITERATION</source>
        <translation>Editer l&apos;itération</translation>
    </message>
    <message>
        <source>HOM_STB_EDIT_HYPO</source>
        <translation>Editer l&apos;hypothèse</translation>
    </message>
    <message>
        <source>HOM_STB_EDIT_ZONE</source>
        <translation>Editer la zone</translation>
    </message>
    <message>
        <source>HOM_STB_EDIT_BOUNDARY</source>
        <translation>Editer la frontière</translation>
    </message>
    <message>
        <source>HOM_STB_EDIT_MESS_FILE</source>
        <translation>Afficher le fichier</translation>
    </message>
    <message>
        <source>HOM_WARNING</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <source>HOM_ERROR</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>HOM_INACTIVE_BUTTON</source>
        <translation>Bouton inactif</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>Appliquer et fermer</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation>Répertoire</translation>
    </message>
    <message>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation>Sélection</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Tout</translation>
    </message>
    <message>
        <source>HOM_SELECT_OBJECT_1</source>
        <translation>Selectionner un objet.</translation>
    </message>
    <message>
        <source>HOM_SELECT_OBJECT_2</source>
        <translation>Selectionner un seul objet.</translation>
    </message>
    <message>
        <source>HOM_SELECT_OBJECT_3</source>
        <translation>Selectionner un objet de type %1.</translation>
    </message>
    <message>
        <source>HOM_SELECT_FILE_1</source>
        <translation>Selectionner un fichier.</translation>
    </message>
    <message>
        <source>HOM_SELECT_FILE_2</source>
        <translation>Selectionner un seul fichier.</translation>
    </message>
    <message>
        <source>HOM_MED_FILE_1</source>
        <translation>Ce fichier MED est illisible.</translation>
    </message>
    <message>
        <source>HOM_MED_FILE_2</source>
        <translation>Ce fichier MED ne contient aucun maillage.</translation>
    </message>
    <message>
        <source>HOM_MED_FILE_3</source>
        <translation>Ce fichier MED contient plus d&apos;un maillage.</translation>
    </message>
    <message>
        <source>HOM_MED_FILE_4</source>
        <translation>Impossible de lire le maillage de ce fichier MED.</translation>
    </message>
    <message>
        <source>HOM_MED_FILE_5</source>
        <translation>Ce fichier MED ne contient aucun champ.</translation>
    </message>
    <message>
        <source>HOM_MED_FILE_6</source>
        <translation>Impossible de lire le(s) champ(s) de ce fichier MED.</translation>
    </message>
    <message>
        <source>HOM_SELECT_STUDY</source>
        <translation>Selectionner une étude avec un fichier MED associé\n ou sélectionner un fichier MED.</translation>
    </message>
    <message>
        <source>Create a case</source>
        <translation>Création d&apos;un cas</translation>
    </message>
    <message>
        <source>HOM_CASE_NAME</source>
        <translation>Il faut donner un nom au cas.</translation>
    </message>
    <message>
        <source>HOM_CASE_DIRECTORY_1</source>
        <translation>Il faut choisir un répertoire de travail pour le cas.</translation>
    </message>
    <message>
        <source>HOM_CASE_DIRECTORY_2</source>
        <translation>Ce répertoire est déjà utilisé.</translation>
    </message>
    <message>
        <source>HOM_CASE_DIRECTORY_3</source>
        <translation>Un répertoire valide doit être choisi.</translation>
    </message>
    <message>
        <source>HOM_CASE_MESH</source>
        <translation>Il faut choisir le maillage initial.</translation>
    </message>
    <message>
        <source>HOM_CASE_GROUP</source>
        <translation>Le groupe &quot;%1&quot; ne peut pas être attribué à plus d&apos;une frontière.</translation>
    </message>
    <message>
        <source>HOM_CASE_EDIT_WINDOW_TITLE</source>
        <translation>Edition d&apos;un cas</translation>
    </message>
    <message>
        <source>HOM_ITER_NAME</source>
        <translation>Il faut donner un nom à l&apos;itération.</translation>
    </message>
    <message>
        <source>HOM_ITER_STARTING_POINT</source>
        <translation>Il faut désigner l&apos;itération précédente.</translation>
    </message>
    <message>
        <source>HOM_ITER_MESH</source>
        <translation>Donner le nom du maillage final.</translation>
    </message>
    <message>
        <source>HOM_ITER_HYPO</source>
        <translation>Choisir une hypothèse.</translation>
    </message>
    <message>
        <source>HOM_ITER_FIELD_FILE</source>
        <translation>Avec cette hypothese, il faut fournir le fichier du champ.</translation>
    </message>
    <message>
        <source>HOM_ITER_STARTING_POINT_0</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <source>HOM_ITER_STARTING_POINT_1</source>
        <translation>Itération initiale du cas</translation>
    </message>
    <message>
        <source>HOM_ITER_EDIT_WINDOW_TITLE</source>
        <translation>Edition d&apos;une itération</translation>
    </message>
    <message>
        <source>Discrete boundary</source>
        <translation>Frontière discrète</translation>
    </message>
    <message>
        <source>Analytical boundary</source>
        <translation>Frontière analytique</translation>
    </message>
    <message>
        <source>Conformity type</source>
        <translation>Type de conformité</translation>
    </message>
    <message>
        <source>Conformal</source>
        <translation>Conforme</translation>
    </message>
    <message>
        <source>Non conformal</source>
        <translation>Non conforme</translation>
    </message>
    <message>
        <source>Non conformal option</source>
        <translation>Option de non conformité</translation>
    </message>
    <message>
        <source>Free</source>
        <translation>Libre</translation>
    </message>
    <message>
        <source>1 hanging node per mesh</source>
        <translation>1 noeud pendant par maille</translation>
    </message>
    <message>
        <source>1 node per edge</source>
        <translation>1 noeud pendant par arête</translation>
    </message>
    <message>
        <source>Create an iteration</source>
        <translation>Création d&apos;une itération</translation>
    </message>
    <message>
        <source>Iteration Name</source>
        <translation>Nom de l&apos;itération</translation>
    </message>
    <message>
        <source>Previous iteration</source>
        <translation>Itération précédente</translation>
    </message>
    <message>
        <source>Mesh n</source>
        <translation>Maillage n</translation>
    </message>
    <message>
        <source>Mesh n+1</source>
        <translation>Maillage n+1</translation>
    </message>
    <message>
        <source>Field information</source>
        <translation>Information sur les champs</translation>
    </message>
    <message>
        <source>Field file</source>
        <translation>Fichier des champs</translation>
    </message>
    <message>
        <source>No time step</source>
        <translation>Sans pas de temps</translation>
    </message>
    <message>
        <source>Last time step</source>
        <translation>Dernier pas de temps</translation>
    </message>
    <message>
        <source>Chosen time step</source>
        <translation>Pas de temps choisi</translation>
    </message>
    <message>
        <source>Time step</source>
        <translation>Pas de temps</translation>
    </message>
    <message>
        <source>Rank</source>
        <translation>Numéro d&apos;ordre</translation>
    </message>
    <message>
        <source>Hypothesis</source>
        <translation>Hypothèse</translation>
    </message>
    <message>
        <source>Create an hypothesis</source>
        <translation>Création d&apos;une hypothèse</translation>
    </message>
    <message>
        <source>HOM_HYPO_NAME</source>
        <translation>Il faut donner un nom à l&apos;hypothèse.</translation>
    </message>
    <message>
        <source>HOM_HYPO_FIELD_FILE</source>
        <translation>Il faut fournir le fichier du champ.</translation>
    </message>
    <message>
        <source>HOM_HYPO_ZONE_1</source>
        <translation>Choisir une zone.</translation>
    </message>
    <message>
        <source>HOM_HYPO_ZONE_2</source>
        <translation>Il faut choisir au moins une zone.</translation>
    </message>
    <message>
        <source>HOM_HYPO_COMP</source>
        <translation>Il faut choisir au moins une composante.</translation>
    </message>
    <message>
        <source>HOM_HYPO_NORM_L2</source>
        <translation>Norme L2</translation>
    </message>
    <message>
        <source>HOM_HYPO_NORM_INF</source>
        <translation>Norme infinie</translation>
    </message>
    <message>
        <source>HOM_HYPO_NORM_ABS</source>
        <translation>Absolu</translation>
    </message>
    <message>
        <source>HOM_HYPO_NORM_REL</source>
        <translation>Relatif</translation>
    </message>
    <message>
        <source>HOM_HYPO_EDIT_WINDOW_TITLE</source>
        <translation>Edition d&apos;une hypothèse</translation>
    </message>
    <message>
        <source>Type of adaptation</source>
        <translation>Type d&apos;adaptation</translation>
    </message>
    <message>
        <source>Uniform</source>
        <translation>Uniforme</translation>
    </message>
    <message>
        <source>Driven by a field</source>
        <translation>Pilotage par un champ</translation>
    </message>
    <message>
        <source>With geometrical zones</source>
        <translation>Selon des zones géométriques</translation>
    </message>
    <message>
        <source>Uniform adaptation</source>
        <translation>Adaptation uniforme</translation>
    </message>
    <message>
        <source>Coarsening</source>
        <translation>Déraffinement</translation>
    </message>
    <message>
        <source>Refinement</source>
        <translation>Raffinement</translation>
    </message>
    <message>
        <source>File of the fields</source>
        <translation>Fichier des champs</translation>
    </message>
    <message>
        <source>Governing field for the adaptation</source>
        <translation>Champ pilotant l&apos;adaptation</translation>
    </message>
    <message>
        <source>Field name</source>
        <translation>Nom du champ</translation>
    </message>
    <message>
        <source>Component</source>
        <translation>Composante</translation>
    </message>
    <message>
        <source>Refinement threshold</source>
        <translation>Seuil de raffinement</translation>
    </message>
    <message>
        <source>Coarsening threshold</source>
        <translation>Seuil de déraffinement</translation>
    </message>
    <message>
        <source>Percentage of meshes</source>
        <translation>Pourcentage de mailles</translation>
    </message>
    <message>
        <source>No refinement</source>
        <translation>Sans raffinement</translation>
    </message>
    <message>
        <source>No coarsening</source>
        <translation>Sans déraffinement</translation>
    </message>
    <message>
        <source>Zone management</source>
        <translation>Gestion des zones</translation>
    </message>
    <message>
        <source>Zone name</source>
        <translation>Nom de la zone</translation>
    </message>
    <message>
        <source>Field Interpolation</source>
        <translation>Interpolation des champs</translation>
    </message>
    <message>
        <source>Chosen</source>
        <translation>Choisi</translation>
    </message>
    <message>
        <source>Create a zone</source>
        <translation>Création d&apos;une zone</translation>
    </message>
    <message>
        <source>HOM_ZONE_NAME</source>
        <translation>Il faut donner un nom à la zone.</translation>
    </message>
    <message>
        <source>HOM_ZONE_LIMIT</source>
        <translation>%1 maxi doit être plus grand que %1 mini.</translation>
    </message>
    <message>
        <source>HOM_ZONE_EDIT_WINDOW_TITLE</source>
        <translation>Edition d&apos;une zone</translation>
    </message>
    <message>
        <source>Type of zone</source>
        <translation>Type de la zone</translation>
    </message>
    <message>
        <source>Coordinates</source>
        <translation>Coordonnées</translation>
    </message>
    <message>
        <source>Create an analytical boundary</source>
        <translation>Création d&apos;une frontière analytique</translation>
    </message>
    <message>
        <source>Create a discrete boundary</source>
        <translation>Création d&apos;une frontière discrète</translation>
    </message>
    <message>
        <source>Type of boundary</source>
        <translation>Type de la frontière</translation>
    </message>
    <message>
        <source>Coordinates of the cylindre</source>
        <translation>Coordonnées du cylindre</translation>
    </message>
    <message>
        <source>Coordinates of the sphere</source>
        <translation>Coordonnées de la sphère</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation>Rayon</translation>
    </message>
    <message>
        <source>X axis</source>
        <translation>X axe</translation>
    </message>
    <message>
        <source>Y axis</source>
        <translation>Y axe</translation>
    </message>
    <message>
        <source>Z axis</source>
        <translation>Z axe</translation>
    </message>
    <message>
        <source>HOM_BOUN_NAME</source>
        <translation>Il faut donner un nom à la frontière.</translation>
    </message>
    <message>
        <source>HOM_BOUN_MESH</source>
        <translation>Il faut choisir le maillage qui contient la frontière.</translation>
    </message>
    <message>
        <source>HOM_BOUN_CASE</source>
        <translation>Le fichier du maillage du cas est inconnu.</translation>
    </message>
    <message>
        <source>HOM_BOUN_A_EDIT_WINDOW_TITLE</source>
        <translation>Edition d&apos;une frontière analytique</translation>
    </message>
    <message>
        <source>HOM_BOUN_D_EDIT_WINDOW_TITLE</source>
        <translation>Edition d&apos;une frontière discrète</translation>
    </message>
    <message>
        <source>Filtering with groups</source>
        <translation>Filtrage par les groupes</translation>
    </message>
    <message>
        <source>Selection of groups</source>
        <translation>Choix des groupes</translation>
    </message>
    <message>
        <source>Selected groups</source>
        <translation>Groupes choisis</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Edit a file</source>
        <translation>Affichage d&apos;un fichier</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
</context>
</TS>
