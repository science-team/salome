<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>@default</name>
    <message>
        <source>GHS3D_ADV_ARGS</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source>GHS3D_HYPOTHESIS</source>
        <translation>GHS3D</translation>
    </message>
    <message>
        <source>GHS3D_OPTIMIZATIOL_LEVEL</source>
        <translation>Niveau d&apos;optimisation</translation>
    </message>
    <message>
        <source>GHS3D_PERMISSION_DENIED</source>
        <translation>Il n&apos;est pas possible d&apos;écrire dans le répertoire</translation>
    </message>
    <message>
        <source>GHS3D_STD_ARGS</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>GHS3D_TITLE</source>
        <translation>Construction de l&apos;hypothèse </translation>
    </message>
    <message>
        <source>GHS3D_TO_MESH_HOLES</source>
        <translation>Mailler les trous</translation>
    </message>
    <message>
        <source>INIT_MEMORY_SIZE</source>
        <translation>Taille initiale de la mémoire</translation>
    </message>
    <message>
        <source>KEEP_WORKING_FILES</source>
        <translation>Conserver les fichiers temporaires</translation>
    </message>
    <message>
        <source>LEVEL_NONE</source>
        <translation>Zéro</translation>
    </message>
    <message>
        <source>LEVEL_LIGHT</source>
        <translation>Léger</translation>
    </message>
    <message>
        <source>LEVEL_MEDIUM</source>
        <translation>Moyen (standard)</translation>
    </message>
    <message>
        <source>LEVEL_STANDARDPLUS</source>
        <translation>Standard+</translation>
    </message>
    <message>
        <source>LEVEL_STRONG</source>
        <translation>Fort</translation>
    </message>
    <message>
        <source>MAX_MEMORY_SIZE</source>
        <translation>Taille maximale de la mémoire</translation>
    </message>
    <message>
        <source>MEGABYTE</source>
        <translation>Megabytes</translation>
    </message>
    <message>
        <source>NO_INITIAL_CENTRAL_POINT</source>
        <translation>Supprimer le point central initial</translation>
    </message>
    <message>
        <source>RECOVERY_VERSION</source>
        <translation>Utiliser la version de restauration des frontières</translation>
    </message>
    <message>
        <source>FEM_CORRECTION</source>
        <translation>Utiliser correction FEM</translation>
    </message>
    <message>
        <source>SELECT_DIR</source>
        <translation>...</translation>
    </message>
    <message>
        <source>TEXT_OPTION</source>
        <translation>Option comme texte</translation>
    </message>
    <message>
        <source>TO_ADD_NODES</source>
        <translation>Créer de nouveaux nœuds</translation>
    </message>
    <message>
        <source>VERBOSE_LEVEL</source>
        <translation>Niveau de verbosité</translation>
    </message>
    <message>
        <source>WORKING_DIR</source>
        <translation>Répertoire de travail</translation>
    </message>
    <message>
        <source>GHS3D_ENFORCED_VERTICES</source>
        <translation>Points de passage</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_X_COLUMN</source>
        <translation>X</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_Y_COLUMN</source>
        <translation>Y</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_Z_COLUMN</source>
        <translation>Z</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_SIZE_COLUMN</source>
        <translation>Taille</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_X_LABEL</source>
        <translation>X:</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_Y_LABEL</source>
        <translation>Y:</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_Z_LABEL</source>
        <translation>Z:</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_SIZE_LABEL</source>
        <translation>Taille:</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_VERTEX</source>
        <translation>Ajouter un point de passage</translation>
    </message>
    <message>
        <source>GHS3D_ENF_VER_REMOVE</source>
        <translation>Supprimer un point</translation>
    </message>
</context>
</TS>
