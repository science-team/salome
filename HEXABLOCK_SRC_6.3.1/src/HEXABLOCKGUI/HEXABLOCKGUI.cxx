//  Copyright (C) 2009-2011  CEA/DEN, EDF R&D
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
//  See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#define _DEVDEBUG_

// #include <Python.h>

#include <cassert>


// #include "klinkitemselectionmodel.h"


#include <QInputDialog>
#include <QIcon>


#include <QtxTreeView.h>
#include <SUIT_MessageBox.h>
#include <SUIT_Session.h>
#include <SUIT_ResourceMgr.h>
#include <SUIT_Desktop.h>
#include <SUIT_ViewManager.h>
#include <SUIT_ViewWindow.h>
#include <SUIT_DataObject.h>
#include <SUIT_DataBrowser.h>


// #include <QxScene_ViewManager.h>
#include <SVTK_ViewManager.h>
#include <SVTK_ViewModel.h>
#include <SVTK_ViewWindow.h>


#include <SalomeApp_DataObject.h>
#include <SalomeApp_Study.h>
#include <SalomeApp_Module.h>
#include <SalomeApp_Application.h>
#include <SalomeApp_DataModel.h>


#include <SALOME_ListIO.hxx>
#include <SALOME_ListIteratorOfListIO.hxx>


#include <SALOME_LifeCycleCORBA.hxx>

#include "Resource.hxx"
// #include "QtGuiContext.hxx"

#include "HEXABLOCKGUI.hxx"
#include "HEXABLOCKGUI_Export.hxx"
#include "HEXABLOCKGUI_Trace.hxx"
#include "HEXABLOCKGUI_Resource.hxx"
#include "HEXABLOCKGUI_DataModel.hxx"
#include "HEXABLOCKGUI_DocumentGraphicView.hxx"
#include "HEXABLOCKGUI_DocumentModel.hxx"
#include "HEXABLOCKGUI_DocumentSelectionModel.hxx"
#include "HEXABLOCKGUI_DocumentDelegate.hxx"
#include "HEXABLOCKGUI_DocumentPanel.hxx"


#define DW_MINIMUM_WIDTH       150
#define DWINPUT_MINIMUM_HEIGHT 150

using namespace std;
using namespace HEXABLOCK::GUI;

int  HEXABLOCKGUI::_oldStudyId = -1;



HEXABLOCKGUI::HEXABLOCKGUI() :
  SalomeApp_Module( "HEXABLOCK" ), // default name
  LightApp_Module( "HEXABLOCK" ),
  _menuId(190),
  _dwPattern(0),
  _dwAssociation(0),
  _dwGroups(0),
  _dwMesh(0),
  _dwObjectBrowser(0),
  _dwInputPanel(0),
  _currentModel(0),
  _patternDataModel(0),
  _patternBuilderModel(0),
  _patternDataTreeView(0),
  _patternBuilderTreeView(0),
  _associationTreeView(0),
  _groupsTreeView(0),
  _meshTreeView(0),
  _currentGraphicView(0),
  _treeViewDelegate(0),
  _patternDataSelectionModel(0),
  _patternBuilderSelectionModel(0),
  _documentCnt(0),
  _isSaved( false ),
  _suitVM(0),
  _selectFromTree( false )
{
  DEBTRACE("HEXABLOCKGUI::HEXABLOCKGUI");
//   _studyContextMap.clear();
}

HEXABLOCKGUI::~HEXABLOCKGUI()
{
//   if ( getApp() )
//     disconnect( getApp(), SIGNAL(studyClosed()), _genericGui, SLOT  (onCleanOnExit()));
}


SalomeApp_Study* HEXABLOCKGUI::activeStudy()
{
  SUIT_Application* app = SUIT_Session::session()->activeApplication();
  if( app )
    return dynamic_cast<SalomeApp_Study*>( app->activeStudy() );
  else
    return NULL;
}


// Gets an reference to the module's engine
HEXABLOCK_ORB::HEXABLOCK_Gen_ptr HEXABLOCKGUI::InitHEXABLOCKGen( SalomeApp_Application* app )
{
  Engines::EngineComponent_var comp = app->lcc()->FindOrLoad_Component( "FactoryServer","HEXABLOCK" );
  HEXABLOCK_ORB::HEXABLOCK_Gen_ptr clr = HEXABLOCK_ORB::HEXABLOCK_Gen::_narrow(comp);
  ASSERT(!CORBA::is_nil(clr));
  return clr;
}

void HEXABLOCKGUI::initialize( CAM_Application* app )
{
  DEBTRACE("HEXABLOCKGUI::initialize");
  SalomeApp_Module::initialize( app );
//   LightApp_Module::initialize( app );

  InitHEXABLOCKGen( dynamic_cast<SalomeApp_Application*>( app ) );

  QWidget* aParent = application()->desktop();
  DEBTRACE(app << "  " << application() << " " << application()->desktop() << " " << aParent);

  SUIT_ResourceMgr* aResourceMgr = app->resourceMgr();
  setResource(aResourceMgr);


//   LightApp_SelectionMgr* mgr = HEXABLOCKGUI::selectionMgr();
//   connect( mgr, SIGNAL( currentSelectionChanged() ),
//                this, SLOT( printVTK() ) );

  if ( app && app->desktop() ){
      connect( app->desktop(), SIGNAL( windowActivated( SUIT_ViewWindow* ) ),
               this, SLOT(onWindowActivated( SUIT_ViewWindow* )) );
      connect( getApp()->objectBrowser()->treeView(),SIGNAL( clicked(const QModelIndex&) ),
               this, SLOT( onClick(const QModelIndex&) ) );
//       connect( getApp()->objectBrowser()->treeView(),SIGNAL( doubleClicked(const QModelIndex&) ),
//                this, SLOT( onDblClick(const QModelIndex&) ) );
//       connect( getApp(),   SIGNAL(studyClosed()),
//                _genericGui,SLOT  (onCleanOnExit()));
  }

// // TEST
//   QGraphicsScene* scene = new QGraphicsScene;
//   scene->addText("Hello, philou!");
//   QGraphicsView* view = new QGraphicsView(scene);
//   view->show();
//   app->desktop()->setCentralWidget(view);
// // TEST
  createAndFillDockWidget();
  createActions();
  createMenus();
  createTools();
  studyActivated();

//   if (createSComponent()) updateObjBrowser();


//   SalomeApp_Study* activestudy = activeStudy();
//   activestudy->addComponent( dataModel() );
 
}

void HEXABLOCKGUI::viewManagers( QStringList& list ) const
{
  DEBTRACE("HEXABLOCKGUI::viewManagers");
//   list.append( QxScene_Viewer::Type() );
  list.append( SVTK_Viewer::Type() );
}


bool HEXABLOCKGUI::activateModule( SUIT_Study* theStudy )
{
  DEBTRACE("HEXABLOCKGUI::activateModule");
  bool bOk = SalomeApp_Module::activateModule( theStudy );
//   bool bOk = LightApp_Module::activateModule( theStudy );
  if ( !bOk ) return false;

  setMenuShown( true );
  setToolShown( true );

//   showDockWidgets(false);
  showDockWidgets(true);

//   // import Python module that manages HEXABLOCK plugins (need to be here because SalomePyQt API uses active module)
//   PyGILState_STATE gstate = PyGILState_Ensure();
//   PyObject* pluginsmanager=PyImport_ImportModule((char*)"salome_pluginsmanager");
//   if(pluginsmanager==NULL)
//     PyErr_Print();
//   else
//     {
//       PyObject* result=PyObject_CallMethod( pluginsmanager, (char*)"initialize", (char*)"isss",1,"hexablock","HEXABLOCK","Plugins");
//       if(result==NULL)
//         PyErr_Print();
//       Py_XDECREF(result);
//     }
//   PyGILState_Release(gstate);
//   // end of HEXABLOCK plugins loading


  return bOk;
}

bool HEXABLOCKGUI::deactivateModule( SUIT_Study* theStudy )
{
  DEBTRACE("HEXABLOCKGUI::deactivateModule");

  setMenuShown( false );
  setToolShown( false );
  showDockWidgets( false );
//   QtGuiContext *context = QtGuiContext::getQtCurrent();
//   _studyContextMap[theStudy->id()] = context;
//   DEBTRACE("_studyContextMap[theStudy] " << theStudy << " " << context);
  return SalomeApp_Module::deactivateModule( theStudy );
//   return LightApp_Module::deactivateModule( theStudy );
}

// --- Default windows

void HEXABLOCKGUI::windows( QMap<int, int>& theMap ) const
{
  DEBTRACE("HEXABLOCKGUI::windows");
  theMap.clear();
  theMap.insert( SalomeApp_Application::WT_ObjectBrowser, Qt::LeftDockWidgetArea );
//   theMap.insert( SalomeApp_Application::WT_PyConsole,     Qt::BottomDockWidgetArea );
}

// LightApp_Displayer* HEXABLOCKGUI::displayer()
// {
//   DEBTRACE("HEXABLOCKGUI::displayer");
//   return _currentGraphicView;
// }



QString  HEXABLOCKGUI::engineIOR() const
{
  DEBTRACE("HEXABLOCKGUI::engineIOR");
//   return getApp()->defaultEngineIOR();
}






void HEXABLOCKGUI::onClick(const QModelIndex& index) //CS_TODO
{
  DEBTRACE("HEXABLOCKGUI::onClick !!!!!!!!!!!!!!!!!!!!! ");
  DataObjectList dol =getApp()->objectBrowser()->getSelected();
  DEBTRACE("HEXABLOCKGUI::onClick 1 ");
  if (dol.isEmpty()) return;
  DEBTRACE("HEXABLOCKGUI::onClick 2 ");

  LightApp_DataObject* item = dynamic_cast<LightApp_DataObject*>(dol[0]);
  if (!item) return;

  DEBTRACE(item->name().toStdString());
  DEBTRACE(item->entry().toStdString());

  HEXABLOCKGUI_DataModel *model = dynamic_cast<HEXABLOCKGUI_DataModel*>(dataModel());
  if (!model) return;
  DEBTRACE("HEXABLOCKGUI::onClick 3 ");
  QWidget * viewWindow = model->getViewWindow( item->entry() );
  if (!viewWindow) return;
  DEBTRACE("HEXABLOCKGUI::onClick 4 ");
  DEBTRACE("--- " << viewWindow << " "  << item->entry().toStdString());
  if (getApp()->activeModule()->moduleName().compare("HEXABLOCK") != 0)
    getApp()->activateModule("HEXABLOCK");

  _selectFromTree = true;
  viewWindow->setFocus();
  _selectFromTree = false;
}




// void HEXABLOCKGUI::onDblClick(const QModelIndex& index) //CS_TODO
// {
//   DEBTRACE("HEXABLOCKGUI::onDblClick");
// //   DataObjectList dol =getApp()->objectBrowser()->getSelected();
// //   if (dol.isEmpty()) return;
// // 
// //   SalomeApp_DataObject* item = dynamic_cast<SalomeApp_DataObject*>(dol[0]);
// //   if (!item) return;
// // 
// //   DEBTRACE(item->name().toStdString());
// //   SalomeWrap_DataModel *model = dynamic_cast<SalomeWrap_DataModel*>(dataModel());
// //   if (!model) return;
// //   DEBTRACE(item->entry().toStdString());
// //   QWidget * viewWindow = model->getViewWindow(item->entry().toStdString());
// //   if (!viewWindow) return;
// //   DEBTRACE("--- " << viewWindow << " "  << item->entry().toStdString());
// //   if (getApp()->activeModule()->moduleName().compare("HEXABLOCK") != 0)
// //     getApp()->activateModule("HEXABLOCK");
// // 
// //   _selectFromTree = true;
// //   viewWindow->setFocus();
// //   _selectFromTree = false;
// }

void HEXABLOCKGUI::onWindowActivated( SUIT_ViewWindow* svw)
{
  DEBTRACE("HEXABLOCKGUI::onWindowActivated");
  SVTK_ViewWindow* viewWindow = dynamic_cast<SVTK_ViewWindow*>(svw);
  if (!viewWindow) return;
  if (getApp()->activeModule() && getApp()->activeModule()->moduleName().compare("HEXABLOCK") != 0)
    getApp()->activateModule("HEXABLOCK");

  switchModel( viewWindow );

  if (_selectFromTree) return;
  HEXABLOCKGUI_DataModel *model = dynamic_cast<HEXABLOCKGUI_DataModel*>( dataModel() );
  if (!model) return;
  model->setSelected(svw);

}
// {
//   std::cout << "CS_BP onWindowActivated onWindowActivated onWindowActivated onWindowActivated "<<std::endl;

//   SVTK_ViewWindow* viewWindow = dynamic_cast<SVTK_ViewWindow*>(svw);
//   if (!viewWindow) return;
//   DEBTRACE("viewWindow " << viewWindow);
//   DEBTRACE("activeModule()->moduleName() " << (getApp()->activeModule() ? getApp()->activeModule()->moduleName().toStdString() : "") );
//   if (getApp()->activeModule() && getApp()->activeModule()->moduleName().compare("HEXABLOCK") != 0)
//     getApp()->activateModule("HEXABLOCK");


//   disconnect(viewWindow, SIGNAL( tryClose( bool&, QxScene_ViewWindow* ) ),
//              this, SLOT(onTryClose( bool&, QxScene_ViewWindow* )) );
//   disconnect(viewWindow->getViewManager(), SIGNAL( deleteView( SUIT_ViewWindow* ) ),
//              this, SLOT(onWindowClosed( SUIT_ViewWindow* )) );
//   connect(viewWindow, SIGNAL( tryClose( bool&, QxScene_ViewWindow* ) ),
//           this, SLOT(onTryClose( bool&, QxScene_ViewWindow* )) );
//   connect(viewWindow->getViewManager(), SIGNAL( deleteView( SUIT_ViewWindow* ) ),
//           this, SLOT(onWindowClosed( SUIT_ViewWindow* )) );

//   switchModel( viewWindow ); CS_TODO dans la vue

//   _studyContextMap[getApp()->activeStudy()->id()] = QtGuiContext::getQtCurrent();
  
//   if (_selectFromTree) return;
//   SalomeWrap_DataModel *model = dynamic_cast<SalomeWrap_DataModel*>(dataModel());
//   if (!model) return;
//   model->setSelected(svw);
// }

void HEXABLOCKGUI::onWindowClosed( SUIT_ViewWindow* svw) 
{
  DEBTRACE("HEXABLOCKGUI::onWindowClosed");
}

// void HEXABLOCKGUI::onTryClose(bool &isClosed, QxScene_ViewWindow* window) //CS_TODO
// {
//   DEBTRACE("HEXABLOCKGUI::onTryClose");
//   isClosed = _genericGui->closeContext(window);
// }

CAM_DataModel* HEXABLOCKGUI::createDataModel()
{
  return new HEXABLOCKGUI_DataModel(this);
}

bool HEXABLOCKGUI::createSComponent() //addComponent
{
  DEBTRACE("HEXABLOCKGUI::createSComponent");
  _PTR(Study)            aStudy = (( SalomeApp_Study* )(getApp()->activeStudy()))->studyDS();
  _PTR(StudyBuilder)     aBuilder (aStudy->NewBuilder());
  _PTR(GenericAttribute) anAttr;
  _PTR(AttributeName)    aName;

  // --- Find or create "HEXABLOCK" SComponent in the study
  _PTR(SComponent) aComponent = aStudy->FindComponent("HEXABLOCK");
  if ( !aComponent )
    {
      aComponent = aBuilder->NewComponent("HEXABLOCK");
      anAttr = aBuilder->FindOrCreateAttribute(aComponent, "AttributeName");
      aName = _PTR(AttributeName) (anAttr);
      aName->SetValue(getApp()->moduleTitle("HEXABLOCK").toStdString());
      
      anAttr = aBuilder->FindOrCreateAttribute(aComponent, "AttributePixMap");
      _PTR(AttributePixMap) aPixmap(anAttr);
      aPixmap->SetPixMap("share/salome/resources/hexablock/ModuleHexablock.png");
      
      aBuilder->DefineComponentInstance(aComponent, getApp()->defaultEngineIOR().toStdString());
//       SalomeApp_DataModel::synchronize( aComponent, HEXABLOCKGUI::activeStudy() );
      return true;
    }
  return false;
}

void HEXABLOCKGUI::setResource(SUIT_ResourceMgr* r) 
{
  DEBTRACE("HEXABLOCKGUI::setResource");
  _myresource = new HEXABLOCKGUI_Resource(r);
  _myresource->preferencesChanged();
}

void HEXABLOCKGUI::createPreferences() 
{
  DEBTRACE("HEXABLOCKGUI::createPreferences");
  _myresource->createPreferences(this);
}

void HEXABLOCKGUI::preferencesChanged( const QString& sect, const QString& name ) 
{
  DEBTRACE("HEXABLOCKGUI::preferencesChanged");
  _myresource->preferencesChanged(sect, name);
  if(name=="userCatalog")
    {
//       _genericGui->getCatalogWidget()->addCatalogFromFile(Resource::userCatalog.toStdString());
    }
}

void HEXABLOCKGUI::studyActivated() //CS_TODO
{
  int newStudyId = getApp()->activeStudy()->id();
  DEBTRACE("HEXABLOCKGUI::studyActivated " << _oldStudyId << " " << newStudyId);
  
  if (_oldStudyId != -1)
    {
//       _studyContextMap[_oldStudyId] = QtGuiContext::getQtCurrent();      
//       if (_studyContextMap.count(newStudyId))
//         {
//           DEBTRACE("switch to valid context " << QtGuiContext::getQtCurrent() << " " << _studyContextMap[newStudyId]);
//           QtGuiContext::setQtCurrent(_studyContextMap[newStudyId]);
//         }
//       else
//         {
//           DEBTRACE("no switch to null context");
//         }
    }
  _oldStudyId = newStudyId;
}




void HEXABLOCKGUI::createAndFillDockWidget() 
{
  QMainWindow *aParent = application()->desktop();

  // Create dock widget (3)
  //1) *********** user input panel ( contain user's edit dialog box )
  _dwInputPanel = new QDockWidget(aParent);
  _dwInputPanel->setVisible(false);
  _dwInputPanel->setWindowTitle("Input Panel");

  _dwInputPanel->setMinimumHeight(DWINPUT_MINIMUM_HEIGHT);
  _dwInputPanel->setMinimumWidth(DW_MINIMUM_WIDTH); // --- force a minimum until display

//   _dialogView = new DialogView(_dwInputPanel);
//   _dialogView = new SingleItemView(_dwInputPanel);
//   _dialogView->setMinimumHeight(400);
//   _dialogView->setItemDelegate(_treeViewDelegate);
//   _dwInputPanel->setWidget(_dialogView);
//   _dialogView->show();
//   _dwInputPanel->setWidget(new VertexDialog(aParent));
  _dwInputPanel->raise();

  


  //2) ************* document data ( Pattern, Association, Mesh ) in treeview representation
  _treeViewDelegate = new DocumentDelegate(_dwInputPanel);
  
  //      Pattern
  _dwPattern = new QDockWidget(aParent);
//   _dwPattern->installEventFilter(this);
  connect( _dwPattern, SIGNAL( visibilityChanged(bool) ), this, SLOT( showPatternMenus(bool) ) );
  std::cout <<" CONNECT ========> " << _dwPattern << std::endl;
  _dwPattern->setVisible(false);
  _dwPattern->setWindowTitle("Model");
  _dwPattern->setMinimumWidth(DW_MINIMUM_WIDTH); // --- force a minimum until display
  

  


//     OB_Browser* o = new OB_Browser;
//   _patternDataTreeView = o->treeView();//new QTreeView(_dwPattern);
//   _patternDataTreeView = new OB_Browser;

  QFrame*      patternFrame  = new QFrame(_dwPattern);
  QVBoxLayout* patternLayout = new QVBoxLayout(patternFrame);
  _patternDataTreeView    = new QTreeView(patternFrame); //_dwPattern);
  _patternBuilderTreeView = new QTreeView(patternFrame);
  patternLayout->addWidget(_patternDataTreeView );
  patternLayout->addWidget(_patternBuilderTreeView );
//   _patternDataTreeView->setMinimumHeight(DW_MINIMUM_WIDTH); 


  _patternDataTreeView->setEditTriggers(QAbstractItemView::AllEditTriggers); 
  _patternDataTreeView->setSelectionMode(QAbstractItemView::SingleSelection); //CS_TEST

  _patternBuilderTreeView->setEditTriggers(QAbstractItemView::AllEditTriggers);
  //QAbstractItemView::DoubleClicked, QAbstractItemView::SelectedClicked)

  _patternDataTreeView->setItemDelegate(_treeViewDelegate);
  _patternBuilderTreeView->setItemDelegate(_treeViewDelegate);
  _dwPattern->setWidget(patternFrame);
  patternFrame->show();
  //_dwPattern->raise();

  //      Association
  _dwAssociation = new QDockWidget(aParent);
//   _dwAssociation->installEventFilter(this);
  connect( _dwAssociation, SIGNAL( visibilityChanged(bool) ), this, SLOT( showAssociationMenus(bool) ) );
  _dwAssociation->setVisible(false);
  _dwAssociation->setWindowTitle("Association");
  _dwAssociation->setMinimumWidth(DW_MINIMUM_WIDTH); // --- force a minimum until display
  _associationTreeView = new QTreeView(_dwAssociation);
  //   _associationTreeView->setMinimumHeight(DW_MINIMUM_WIDTH);
  _associationTreeView->setEditTriggers(QAbstractItemView::AllEditTriggers);//QAbstractItemView::SelectedClicked); 
  _associationTreeView->setItemDelegate(_treeViewDelegate);
  _dwAssociation->setWidget(_associationTreeView);
  _associationTreeView->show();
  //   _dwAssociation->raise();

  //      Groups
  _dwGroups = new QDockWidget(aParent);
//   _dwGroups->installEventFilter(this);
  connect( _dwGroups, SIGNAL( visibilityChanged(bool) ), this, SLOT( showGroupsMenus(bool) ) );
  _dwGroups->setVisible(false);
  _dwGroups->setWindowTitle("Groups");
  _dwGroups->setMinimumWidth(DW_MINIMUM_WIDTH); // --- force a minimum until display
  _groupsTreeView = new QTreeView(_dwGroups);
//   _associationTreeView->setMinimumHeight(DW_MINIMUM_WIDTH); 
  _groupsTreeView->setEditTriggers(QAbstractItemView::AllEditTriggers);
  _groupsTreeView->setItemDelegate(_treeViewDelegate);
  _dwGroups->setWidget(_groupsTreeView);
  _groupsTreeView->show();

  //      Mesh
  _dwMesh = new QDockWidget(aParent);
//   _dwMesh->installEventFilter(this);
  connect( _dwMesh, SIGNAL( visibilityChanged(bool) ), this, SLOT( showMeshMenus(bool) ) );
  _dwMesh->setVisible(false);
  _dwMesh->setWindowTitle("Mesh");
  _dwMesh->setMinimumWidth(DW_MINIMUM_WIDTH); // --- force a minimum until display
  _meshTreeView = new QTreeView(_dwMesh);
  //   _meshTreeView->setMinimumHeight(DW_MINIMUM_WIDTH);
  _meshTreeView->setEditTriggers(QAbstractItemView::AllEditTriggers);
  _meshTreeView->setItemDelegate(_treeViewDelegate);
  _dwMesh->setWidget(_meshTreeView);
  _meshTreeView->show();
  //   _dwMesh->raise();



  //3) ************* documents ( salome objectbrowser )
  QDockWidget *_dwObjectBrowser = 0;
  QWidget* wid = getApp()->objectBrowser()->treeView();
  //   QWidget *wid = application()->objectBrowser()->treeView();
  QWidget *w   = wid->parentWidget();
  while ( w && !_dwObjectBrowser ) {
    _dwObjectBrowser = ::qobject_cast<QDockWidget*>( w );
    w = w->parentWidget();
  }
//   _dwObjectBrowser->installEventFilter(this);
//   connect( _dwObjectBrowser, SIGNAL( visibilityChanged(bool) ), this, SLOT( showObjectBrowserMenus(bool) ) );
//   _dwObjectBrowser->setVisible(false);
  _dwObjectBrowser->setMinimumWidth(DW_MINIMUM_WIDTH); // --- force a minimum until display
  _dwObjectBrowser->setWindowTitle("Study");


  // dock widget position
  //   aParent->addDockWidget(Qt::LeftDockWidgetArea,  _dwPattern);
  //   aParent->addDockWidget(Qt::RightDockWidgetArea, _dwInputPanel);
  //   aParent->addDockWidget( Qt::LeftDockWidgetArea, _dwPattern ); //static_cast<Qt::DockWidgetArea>(1)
  aParent->addDockWidget( Qt::LeftDockWidgetArea, _dwObjectBrowser ); //static_cast<Qt::DockWidgetArea>(1)
  aParent->addDockWidget( Qt::LeftDockWidgetArea, _dwInputPanel );

  aParent->tabifyDockWidget( _dwObjectBrowser, _dwPattern );
  aParent->tabifyDockWidget( _dwPattern, _dwAssociation );
  aParent->tabifyDockWidget( _dwAssociation, _dwGroups );
  aParent->tabifyDockWidget( _dwGroups, _dwMesh );



#if QT_VERSION >= 0x040500
  aParent->setTabPosition(Qt::AllDockWidgetAreas, Resource::tabPanelsUp? QTabWidget::North: QTabWidget::South);
#endif



}




void HEXABLOCKGUI::createActions()
{
  QMainWindow *aParent = application()->desktop();
  SUIT_ResourceMgr* resMgr = SUIT_Session::session()->resourceMgr();

  // Document 
  _newAct = createAction( _menuId++, tr("Create a new document"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_NEW_DOCUMENT" ) ),
                                     tr("New document"),  tr("Create a new document"),
                                     0, aParent, false, this,  SLOT(newDocument()) );
  _newAct->setShortcut( Qt::CTRL + Qt::SHIFT + Qt::Key_N ); // --- QKeySequence::New ambiguous in SALOME

  _importAct = createAction( _menuId++, tr("Import a document"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_IMPORT_DOCUMENT" ) ),
                                            tr("Import document"), tr("Import a document"),
                                            0, aParent, false, this,  SLOT(importDocument()) );
  _importAct->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_O); // --- QKeySequence::Open ambiguous in SALOME


  // Pattern Data creation
  _addVertex = createAction( _menuId++, tr("Create a vertex"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_ADD_VERTEX" ) ),
                                            tr("Add vertex"),  tr("Create a new vertex"),
                                            0, aParent, false, this,  SLOT(addVertex()) );

  _addEdge = createAction( _menuId++, tr("Create an edge"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_ADD_EDGE" ) ),
                                            tr("Add edge"),  tr("Create a new edge"),
                                            0, aParent, false, this,  SLOT(addEdge()) );

  _addQuad = createAction( _menuId++, tr("Create a quad"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_ADD_QUAD" ) ),
                                            tr("Add quad"),  tr("Create a new quad"),
                                            0, aParent, false, this,  SLOT(addQuad()) );

  _addHexa = createAction( _menuId++, tr("Create an hexa"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_ADD_HEXA" ) ),
                                            tr("Add hexa"),  tr("Create a new hexa"),
                                            0, aParent, false, this,  SLOT(addHexa()) );




  // Builder Data creation
  _addVector    = createAction( _menuId++, tr("Create a vector"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_ADD_VECTOR" ) ),
                                            tr("Add vector"),  tr("Create a new vector"),
                                            0, aParent, false, this,  SLOT(addVector()) );

  _addCylinder  = createAction( _menuId++, tr("Create a cylinder"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_ADD_CYLINDER" ) ),
                                            tr("Add cylinder"),  tr("Create a new cylinder"),
                                            0, aParent, false, this,  SLOT(addCylinder()) );

  _addPipe      = createAction( _menuId++, tr("Create a pipe"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_ADD_PIPE" ) ),
                                            tr("Add pipe"),  tr("Create a new pipe"),
                                            0, aParent, false, this,  SLOT(addPipe()) );

  _makeGrid     = createAction( _menuId++, tr("Make a grid"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_MAKE_GRID" ) ),
                                            tr("Make grid"),  tr("Make a grid"),
                                            0, aParent, false, this,  SLOT(makeGrid()) );

  _makeCylinder     = createAction( _menuId++, tr("Make a cylinder"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_MAKE_CYLINDER" ) ),
                                            tr("Make cylinder"),  tr("Make a cylinder"),
                                            0, aParent, false, this,  SLOT(makeCylinder()) );

  _makePipe     = createAction( _menuId++, tr("Make a pipe"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_MAKE_PIPE" ) ),
                                            tr("Make pipe"),  tr("Make a pipe"),
                                            0, aParent, false, this,  SLOT(makePipe()) );

  _makeCylinders     = createAction( _menuId++, tr("Make cylinders"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_MAKE_CYLINDERS" ) ),
                                            tr("Make cylinders"),  tr("Make cylinders"),
                                            0, aParent, false, this,  SLOT(makeCylinders()) );

  _makePipes     = createAction( _menuId++, tr("Make pipes"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_MAKE_PIPES" ) ),
                                            tr("Make pipes"),  tr("Make pipes"),
                                            0, aParent, false, this,  SLOT(makePipes()) );



  // Pattern Data edition
  _removeHexa     = createAction( _menuId++, tr("Remove hexa"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_REMOVE_HEXA" ) ),
                                            tr("Remove hexa"),  tr("Remove hexa"),
                                            0, aParent, false, this,  SLOT(removeHexa()) );

  _prismQuad     = createAction( _menuId++, tr("Prism quad"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_PRISM_QUAD" ) ),
                                            tr("Prism quad"),  tr("Prism quad"),
                                            0, aParent, false, this,  SLOT(prismQuad()) );

  _joinQuad     = createAction( _menuId++, tr("join quad"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_JOIN_QUAD" ) ),
                                            tr("join quad"),  tr("Join quad"),
                                            0, aParent, false, this,  SLOT(joinQuad()) );

  _merge     = createAction( _menuId++, tr("Merge"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_MERGE" ) ),
                                            tr("Merge"),  tr("Merge"),
                                            0, aParent, false, this,  SLOT(merge()) );

  _disconnect     = createAction( _menuId++, tr("Disconnect"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_DISCONNECT" ) ),
                                            tr("Disconnect"),  tr("Disconnect"),
                                            0, aParent, false, this,  SLOT(disconnectElts()) );

  _cutEdge     = createAction( _menuId++, tr("Cut edge"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_CUT_EDGE" ) ),
                                            tr("Cut edge"),  tr("Cut edge"),
                                            0, aParent, false, this,  SLOT(cutEdge()) );


  _makeTransformation     = createAction( _menuId++, tr("Make transformation"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_MAKE_TRANSFORMATION" ) ),
                                            tr("Make transformation"),  tr("Make transformation"),
                                            0, aParent, false, this,  SLOT(makeTransformation()) );

  _makeSymmetry     = createAction( _menuId++, tr("Make symmetry"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_MAKE_SYMMETRY" ) ),
                                            tr("Make symmetry"),  tr("Make symmetry"),
                                            0, aParent, false, this,  SLOT(makeSymmetry()) );

  _performTransformation     = createAction( _menuId++, tr("Perform transformation"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_PERFORM_TRANSFORMATION" ) ), tr("Perform transformation"),  tr("Perform transformation"),
                                            0, aParent, false, this,  SLOT(performTransformation()) );

  _performSymmetry     = createAction( _menuId++, tr("Perform Symmetry"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_PERFORM_SYMMETRY" ) ),
                                            tr("Perform Symmetry"),  tr("Perform Symmetry"),
                                            0, aParent, false, this,  SLOT(performSymmetry()) );

  _addGroup     = createAction( _menuId++, tr("Add group"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_ADD_GROUP" ) ),
                                            tr("Add group"),  tr("Add group"),
                                            0, aParent, false, this,  SLOT(addGroup()) );
  _removeGroup     = createAction( _menuId++, tr("Remove group"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_REMOVE_GROUP" ) ),
                                            tr("Remove group"),  tr("Remove group"),
                                            0, aParent, false, this,  SLOT(removeGroup()) );

  _addLaw     = createAction( _menuId++, tr("Add law"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_ADD_LAW" ) ),
                                            tr("Add law"),  tr("Add law"),
                                            0, aParent, false, this,  SLOT(addLaw()) );

  _removeLaw     = createAction( _menuId++, tr("Remove law"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_REMOVE_LAW" ) ),
                                            tr("Remove law"),  tr("Remove law"),
                                            0, aParent, false, this,  SLOT(removeLaw()) );


  _setPropagation = createAction( _menuId++, tr("Set Propagation"), resMgr->loadPixmap( "HEXABLOCK", tr( "ICON_SET_PROPAGATION" ) ),tr("Set Propagation"),  tr("Set Propagation"),
                                            0, aParent, false, this,  SLOT(_setPropagation()) );



//   _newAct->setShortcut( Qt::CTRL + Qt::SHIFT + Qt::Key_N ); // --- QKeySequence::New ambiguous in SALOME

  //       QAction* createAction(const int id,
  //                             const QString& toolTip,
  //                             const QIcon& icon,
  //                             const QString& menu,
  //                             const QString& status,
  //                             const int shortCut,
  //                             QObject* parent =0,
  //                             bool checkable = false,
  //                             QObject* receiver =0,
  //                             const char* member =0);
}

void HEXABLOCKGUI::createMenus()
{
  int aMenuId;
  aMenuId = createMenu( "File", -1, -1 );
  createMenu( separator(), aMenuId, -1, 10 );
  aMenuId = createMenu( "HEXABLOCK" , aMenuId, -1, 10 );
  createMenu( _newAct, aMenuId );
  createMenu( _importAct, aMenuId );

//   aMenuId = createMenu( tr( "HEXABLOCK" ), -1, -1, 30 );
  aMenuId = createMenu( tr( "Object Browser" ), -1, -1, 30 );
  createMenu( _newAct, aMenuId );//, 10
  createMenu( _importAct, aMenuId );

  aMenuId = createMenu( tr("Model"), -1, -1, 30 );
  createMenu( _addVertex, aMenuId );
  createMenu( _addEdge,   aMenuId );
  createMenu( _addQuad,   aMenuId );
  createMenu( _addHexa,   aMenuId );
  createMenu( separator(), aMenuId);
  // Pattern Builder
  createMenu( _addVector, aMenuId );
  createMenu( _addCylinder, aMenuId );
  createMenu( _addPipe, aMenuId );
  createMenu( _makeGrid,  aMenuId ); //Cartesian, Cylindrical, Spherical
  createMenu( _makeCylinder, aMenuId );
  createMenu( _makePipe,     aMenuId );
  createMenu( _makeCylinders,aMenuId );
  createMenu( _makePipes,    aMenuId );
  createMenu( separator(), aMenuId);

  // Pattern Data Edition
  createMenu( _removeHexa, aMenuId );
  createMenu( _prismQuad,  aMenuId );
  createMenu( _joinQuad,   aMenuId );
  createMenu( _merge,      aMenuId );//   createMenu( _mergeVertices,   aMenuId ); //   createMenu( _mergeEdges,   aMenuId );
  createMenu( _disconnect, aMenuId );
  createMenu( _cutEdge,    aMenuId );
  createMenu( _makeTransformation, aMenuId ); //   createMenu( _makeTranslation,   aMenuId );
  createMenu( _makeSymmetry,       aMenuId );
  createMenu( _performTransformation,   aMenuId );
  createMenu( _performSymmetry,         aMenuId );

// // Association
//   aMenuId = createMenu( tr("Association"), -1, -1, 30 );
//   createMenu( _newAct, aMenuId );
//   createMenu( _importAct, aMenuId );

  // Group
  aMenuId = createMenu( tr("Groups"), -1, -1, 30 );
  createMenu( _addGroup,    aMenuId );
  createMenu( _removeGroup, aMenuId ); //CS_TODO

  // Law
  aMenuId = createMenu( tr("Mesh"), -1, -1, 30 );
  createMenu( _addLaw,    aMenuId );
  createMenu( _removeLaw, aMenuId );
  createMenu( _setPropagation, aMenuId );

}


void HEXABLOCKGUI::createTools()
{
  int aToolId = createTool ( tr( "HEXABLOCK Toolbar" ) );

  createTool( _newAct, aToolId );
  createTool( _importAct, aToolId );
  createTool( separator(), aToolId );

  // Pattern Data
  createTool( _addVertex, aToolId );
  createTool( _addEdge, aToolId );
  createTool( _addQuad, aToolId );
  createTool( _addHexa, aToolId );
  createTool( separator(), aToolId );

  // Pattern Builder
  createTool( _addVector, aToolId );
  createTool( _addCylinder, aToolId );
  createTool( _addPipe, aToolId );
  createTool( _makeGrid,  aToolId );
  createTool( _makeCylinder, aToolId );
  createTool( _makePipe,     aToolId );
  createTool( _makeCylinders,aToolId );
  createTool( _makePipes,    aToolId );
  createTool( separator(), aToolId );

  // Pattern Data Edition
  createTool( _removeHexa, aToolId );
  createTool( _prismQuad,  aToolId );
  createTool( _joinQuad,   aToolId );
  createTool( _merge,      aToolId ); 
  createTool( _disconnect, aToolId );
  createTool( _cutEdge,    aToolId );
  createTool( _makeTransformation, aToolId );
  createTool( _makeSymmetry,       aToolId );
  createTool( _performTransformation,   aToolId );
  createTool( _performSymmetry,         aToolId );
  createTool( separator(), aToolId );

  // Association
  createTool( separator(), aToolId );

  // Group
  createTool( _addGroup,    aToolId );
  createTool( _removeGroup, aToolId ); //CS_TODO
  createTool( separator(), aToolId );

  // Law
  createTool( _addLaw,    aToolId );
  createTool( _removeLaw, aToolId );
  createTool( _setPropagation, aToolId );

}

void HEXABLOCKGUI::initialMenus()
{
  showObjectBrowserMenus( true );
  showPatternMenus( false );
  showAssociationMenus( false );
  showGroupsMenus( false );
  showMeshMenus( false );
}


void HEXABLOCKGUI::showObjectBrowserMenus(bool show)
{
  DEBTRACE("HEXABLOCKGUI::showObjectBrowserMenus " << show);
  show = true; // 
  setMenuShown(_newAct, show);
  setToolShown(_newAct, show);
  setMenuShown(_importAct, show);
  setToolShown(_importAct, show);
}

void HEXABLOCKGUI::showPatternMenus(bool show)
{
  DEBTRACE("HEXABLOCKGUI::showPatternMenus " << show);
  setMenuShown(_addVertex, show );//true);
  setToolShown(_addVertex, show);
  setMenuShown(_addEdge,  show );//true);
  setToolShown(_addEdge, show);
  setMenuShown(_addQuad,  show );//true);
  setToolShown(_addQuad, show);
  setMenuShown(_addHexa,  show );//true);
  setToolShown(_addHexa, show);


  setMenuShown( _addVector,  show );//true);
  setToolShown( _addVector, show);
  setMenuShown( _addCylinder,  show );//true);
  setToolShown( _addCylinder, show);
  setMenuShown( _addPipe,  show );//true);
  setToolShown( _addPipe, show);
  setMenuShown( _makeGrid,  show );//true);
  setToolShown( _makeGrid, show);
  setMenuShown( _makeCylinder,  show );//true);
  setToolShown( _makeCylinder, show);
  setMenuShown( _makePipe,  show );//true);
  setToolShown( _makePipe, show);
  setMenuShown( _makeCylinders,  show );//true);
  setToolShown( _makeCylinders, show);
  setMenuShown( _makePipes,  show );//true);
  setToolShown( _makePipes, show);

  // Pattern Data Edition
  setMenuShown( _removeHexa,  show );//true);
  setToolShown( _removeHexa, show);
  setMenuShown( _prismQuad,  show );//true);
  setToolShown( _prismQuad, show);
  setMenuShown( _joinQuad,  show );//true);
  setToolShown( _joinQuad, show);
  setMenuShown( _merge,  show );//true);
  setToolShown( _merge, show);
  setMenuShown( _disconnect,  show );//true);
  setToolShown( _disconnect, show);
  setMenuShown( _cutEdge,  show );//true);
  setToolShown( _cutEdge, show);
  setMenuShown( _makeTransformation,  show );//true);
  setToolShown( _makeTransformation, show);
  setMenuShown( _makeSymmetry,  show );//true);
  setToolShown( _makeSymmetry, show);
  setMenuShown( _performTransformation,  show );//true);
  setToolShown( _performTransformation, show);
  setMenuShown( _performSymmetry,  show );//true);
  setToolShown( _performSymmetry, show);
}


void HEXABLOCKGUI::showAssociationMenus(bool show)
{
  DEBTRACE("HEXABLOCKGUI::showAssociationMenus" << show);

}

void HEXABLOCKGUI::showGroupsMenus(bool show)
{
  DEBTRACE("HEXABLOCKGUI::showGroupsMenus" << show);
  setMenuShown( _addGroup,  show );//true);
  setToolShown( _addGroup, show);
  setMenuShown( _removeGroup ,  show );//true);
  setToolShown( _removeGroup , show);  
}

void HEXABLOCKGUI::showMeshMenus(bool show)
{
  DEBTRACE("HEXABLOCKGUI::showMeshMenus" << show);
  setMenuShown( _addLaw,  show );//true);
  setToolShown( _addLaw, show);
  setMenuShown( _removeLaw,  show );//true);
  setToolShown( _removeLaw, show);;
  setMenuShown( _setPropagation,  show );//true);
  setToolShown( _setPropagation, show);
}






void HEXABLOCKGUI::switchModel(SUIT_ViewWindow *view)
{
  DEBTRACE("HEXABLOCKGUI::switchModel " << view);
  if ( _mapViewModel.count(view) ){
    if ( _currentModel != _mapViewModel[view] ){

      if (_dwInputPanel){
        QWidget* w = _dwInputPanel->widget();
        if (w) w->close();
      }

      // model
      _currentModel = _mapViewModel[view];
      _patternDataModel->setSourceModel(_currentModel);
      _patternBuilderModel->setSourceModel(_currentModel);
      _associationsModel->setSourceModel(_currentModel);
      _groupsModel->setSourceModel(_currentModel);
      _meshModel->setSourceModel(_currentModel);
  
      // setting model ( in view )
      _currentGraphicView->setModel(_patternDataModel);
      connect( _currentModel, SIGNAL(patternDataChanged() ), _currentGraphicView,  SLOT ( onPatternDatachanged() ) );
      _patternDataTreeView->setModel(_patternDataModel); //_currentModel
      _patternBuilderTreeView->setModel(_patternBuilderModel);//_currentModel
      _associationTreeView->setModel(_associationsModel);;
      _groupsTreeView->setModel(_groupsModel);
      _meshTreeView->setModel(_meshModel);

      // setting selection ( in view )
      _patternDataSelectionModel    = new PatternDataSelectionModel( _patternDataModel );
      _patternBuilderSelectionModel = new PatternBuilderSelectionModel( _patternBuilderModel, _patternDataSelectionModel );
  
      _currentGraphicView->setSelectionModel(_patternDataSelectionModel);
      _patternDataTreeView->setSelectionModel(_patternDataSelectionModel);
//       _patternDataTreeView->setSelectionMode( QAbstractItemView::MultiSelection );//CS_TEST
//       _patternDataTreeView->setSelectionMode(QAbstractItemView::SingleSelection); //CS_TEST
      _patternDataTreeView->setEditTriggers(QAbstractItemView::AllEditTriggers);
      _patternBuilderTreeView->setSelectionModel(_patternBuilderSelectionModel);
      _patternBuilderTreeView->setEditTriggers(QAbstractItemView::AllEditTriggers);


      _treeViewDelegate->setDocumentModel( _currentModel );
      _treeViewDelegate->setPatternDataSelectionModel( _patternDataSelectionModel );
      _treeViewDelegate->setPatternBuilderSelectionModel( _patternBuilderSelectionModel );
      _treeViewDelegate->setGroupSelectionModel( _groupsTreeView->selectionModel() );
      _treeViewDelegate->setMeshSelectionModel( _meshTreeView->selectionModel() );


    }
    showPatternMenus(true);
  } else {
    DEBTRACE("HEXABLOCKGUI::switchModel : no model found, cannot switch");
    initialMenus();
  }
}


// bool HEXABLOCKGUI::closeContext(QWidget *view)
// {
//   DEBTRACE("HEXABLOCKGUI::closeContext");
//   if (! _mapViewContext.count(view))
//     return true;
//   QtGuiContext* context = _mapViewContext[view];
//   switchModel(view);
// 
//   bool tryToSave = false;
// 
//   if (QtGuiContext::getQtCurrent()->isEdition())
//     {
//       if (!QtGuiContext::getQtCurrent()->_setOfModifiedSubjects.empty())
//         {
//           QMessageBox msgBox;
//           msgBox.setText("Some elements are modified and not taken into account.");
//           string info = "do you want to apply your changes ?\n";
//           info += " - Save    : do not take into account edition in progress,\n";
//           info += "             but if there are other modifications, select a file name for save\n";
//           info += " - Discard : discard all modifications and close the schema\n";
//           info += " - Cancel  : do not close the schema, return to edition";
//           msgBox.setInformativeText(info.c_str());
//           msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
//           msgBox.setDefaultButton(QMessageBox::Cancel);
//           int ret = msgBox.exec();
//           switch (ret)
//             {
//             case QMessageBox::Save:
//               tryToSave = true;
//               break;
//             case QMessageBox::Discard:
//               tryToSave = false;
//               break;
//             case QMessageBox::Cancel:
//             default:
//               return false;
//               break;
//             }
//         }
//       else
//         if (QtGuiContext::getQtCurrent()->isNotSaved())
//           {
//             QMessageBox msgBox;
//             msgBox.setText("The schema has been modified");
//             string info = "do you want to save the schema ?\n";
//             info += " - Save    : select a file name for save\n";
//             info += " - Discard : discard all modifications and close the schema\n";
//             info += " - Cancel  : do not close the schema, return to edition";
//             msgBox.setInformativeText(info.c_str());
//             msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
//             msgBox.setDefaultButton(QMessageBox::Cancel);
//             int ret = msgBox.exec();
//             switch (ret)
//               {
//               case QMessageBox::Save:
//                 tryToSave = true;
//                 break;
//               case QMessageBox::Discard:
//                 tryToSave = false;
//                 break;
//               case QMessageBox::Cancel:
//               default:
//                 return false;
//                 break;
//               }
//           }
// 
//       if (tryToSave)
//         {
//           onExportSchemaAs();
//           if (! _isSaved) // --- probably, user has cancelled the save dialog. Do not close
//             return false;
//         }
//     }
// 
//   map<QWidget*, HEXABLOCK::HMI::QtGuiContext*>::iterator it = _mapViewContext.begin();
//   QtGuiContext* newContext = 0;
//   QWidget* newView = 0;
//   for (; it != _mapViewContext.end(); ++it)
//     {
//       if ((*it).second != context)
//         {
//           newView = (*it).first;
//           newContext = (*it).second;
//           break;
//         }
//     }
//   int studyId = _wrapper->activeStudyId();
//   if (context->getStudyId() == studyId)
//     {
//       _wrapper->deleteSchema(view);
//       DEBTRACE("delete context");
//       if (GuiExecutor* exec = context->getGuiExecutor())
//         {
//           exec->closeContext();
//         }
//       delete context;
//       _mapViewContext.erase(view);
//       switchModel(newView);
//     }
//   return true;
// }

void HEXABLOCKGUI::showDockWidgets(bool isVisible)
{
  DEBTRACE("HEXABLOCKGUI::showDockWidgets " << isVisible);

  if (_dwObjectBrowser) _dwObjectBrowser->setVisible(isVisible);
  if (_dwObjectBrowser) _dwObjectBrowser->toggleViewAction()->setVisible(isVisible);

  if (_dwPattern) _dwPattern->setVisible(isVisible);
  if (_dwPattern) _dwPattern->toggleViewAction()->setVisible(isVisible);

  if (_dwAssociation) _dwAssociation->setVisible(isVisible);
  if (_dwAssociation) _dwAssociation->toggleViewAction()->setVisible(isVisible);

  if (_dwMesh) _dwMesh->setVisible(isVisible);
  if (_dwMesh) _dwMesh->toggleViewAction()->setVisible(isVisible);

  if (_dwGroups) _dwGroups->setVisible(isVisible);
  if (_dwGroups) _dwGroups->toggleViewAction()->setVisible(isVisible);

  if (_dwInputPanel) _dwInputPanel->setVisible(isVisible);
  if (_dwInputPanel) _dwInputPanel->toggleViewAction()->setVisible(isVisible);

//   if ( isVisible ) _dwObjectBrowser->raise();//_dwPattern->raise();
}


// void HEXABLOCKGUI::raiseStacked()
// {
// //   if (_dwStacked) _dwStacked->raise();
// }


// DocumentGraphicView* HEXABLOCKGUI::newGraphicView()
// {
//   DocumentGraphicView *newGView = 0;
// 
//   SUIT_ViewManager* suitVM = getApp()->getViewManager(SVTK_Viewer::Type(), true);
//   if ( suitVM ){
//     DEBTRACE("HEXABLOCKGUI::newGraphicView suitVM ");
//     //   SVTK_ViewWindow *suitVW  = 0;
//     SUIT_ViewWindow *suitVW = suitVM->createViewWindow();
//     if ( suitVW ){
//       DEBTRACE("HEXABLOCKGUI::newGraphicView suitVW");
//       newGView = new DocumentGraphicView(getApp(), suitVW, application()->desktop());
//     }
//   }
//   return newGView;
// }


DocumentGraphicView* HEXABLOCKGUI::newGraphicView()
{
  DocumentGraphicView *newGView = 0;

  SUIT_ViewWindow *suitVW = NULL;
  if ( !_suitVM ){
    _suitVM = getApp()->getViewManager(SVTK_Viewer::Type(), true);
    suitVW = _suitVM->getActiveView();
  } else {
    suitVW = _suitVM->createViewWindow();
  }
  if ( suitVW )
    newGView = new DocumentGraphicView(getApp(), suitVW, application()->desktop());

  return newGView;
}










void HEXABLOCKGUI::testDocument()
{
  //CS_TEST
  // ----------
  QStandardItem *parentItem = _currentModel->invisibleRootItem();
  QStandardItem *myItem =  new QStandardItem("MyItem");
  parentItem->appendRow(myItem);
// //   ----------
//   QModelIndex v0 = _currentModel->addVertex(0., 0., 0.);
//   QModelIndex v1 = _currentModel->addVertex(5., 0., 0.);
//   QModelIndex v2 = _currentModel->addVertex(5., 5., 0.);
//   QModelIndex v3 = _currentModel->addVertex(0., 5., 0.);
//   QModelIndex v4 = _currentModel->addVertex(0., 0., 5.);
//   QModelIndex v5 = _currentModel->addVertex(5., 0., 5.);
//   QModelIndex v6 = _currentModel->addVertex(5., 5., 5.);
//   QModelIndex v7 = _currentModel->addVertex(0., 5., 5.);
// // 
// // 
//   QModelIndex q0 = _currentModel->addQuadVertices( v0, v1, v2, v3 );
//   QModelIndex q1 = _currentModel->addQuadVertices( v4, v5, v6, v7 );
//   QModelIndex q2 = _currentModel->addQuadVertices( v0, v3, v7, v4 );
//   QModelIndex q3 = _currentModel->addQuadVertices( v1, v2, v6, v5 );
//   QModelIndex q4 = _currentModel->addQuadVertices( v0, v1, v5, v4 );
//   QModelIndex q5 = _currentModel->addQuadVertices( v3, v2, v6, v7 );
// 
//   QModelIndex h0 = _currentModel->addHexaQuad( q0, q1, q2, q3, q4, q5 );
//   QModelIndex vx = _currentModel->addVector(1., 0., 0.);
//   QModelIndex vy = _currentModel->addVector(0., 1., 0.);
//   QModelIndex vz = _currentModel->addVector(0., 0., 1.);
// 
//   QModelIndex orig1 = _currentModel->addVertex (0, 0,0);
//   QModelIndex orig2 = _currentModel->addVertex (50,0,0);
//   QModelIndex vz    = _currentModel->addVector (0,0,1);
//   QModelIndex vx    = _currentModel->addVector (1,0,0);
// 
//   int nr  = 4;
//   int nri = 3;
//   int nre = nr;
//   int na = 9;
//   int nl = 5;
// 
//   QModelIndex  cyl  = _currentModel->addCylinder   (orig1, vz, nr, nl);
//   QModelIndex  pipe = _currentModel->addPipe       (orig2, vz, nri, nre, nl);
// 
//   _currentModel->makeCylinder (cyl,  vx, nr, na, nl);
//   _currentModel->makePipe(pipe, vx, nr, na, nl);

}


void HEXABLOCKGUI::newDocument()
{
  DEBTRACE("HEXABLOCKGUI::newDocument");

  SUIT_ViewWindow *suitVW = NULL;

  std::stringstream name;
  name << "newDoc_" << ++_documentCnt;

  QString fileName = name.str().c_str();

  QMainWindow *aParent = application()->desktop();
  QWidget *central = aParent->centralWidget();
  if (central)
    central->setFocus();
  else
    DEBTRACE("No Central Widget");

  // --- new Doc Model
  _currentModel     = new DocumentModel(this); //CS_TOCHECK this
//   _currentModel->setHeaderData(0, Qt::Horizontal, tr("HELLOH0"));
//   _currentModel->setHeaderData(1, Qt::Horizontal, tr("HELLOH1"));
//   _currentModel->setHeaderData(0, Qt::Vertical, tr("HELLOV0"));
  _patternDataModel    = new PatternDataModel(this);
  _patternBuilderModel = new PatternBuilderModel(this);
  _associationsModel   = new AssociationsModel(this);
  _groupsModel     = new GroupsModel(this);
  _meshModel       = new MeshModel(this);

  _patternDataModel->setSourceModel(_currentModel);
  _patternBuilderModel->setSourceModel(_currentModel);
  _associationsModel->setSourceModel(_currentModel);
  _groupsModel->setSourceModel(_currentModel);
  _meshModel->setSourceModel(_currentModel);

  // --- new Graphic view ( SVTK )
  _currentGraphicView  = newGraphicView();

  // --- setting model
  _currentGraphicView->setModel(_patternDataModel);
  connect( _currentModel, SIGNAL(patternDataChanged() ), _currentGraphicView,  SLOT ( onPatternDatachanged() ) );
  _patternDataTreeView->setModel(_patternDataModel);//_currentModel;
//   _patternDataTreeView->setModel(_currentModel);//;
  _patternBuilderTreeView->setModel(_patternBuilderModel);//_currentModel;
  _associationTreeView->setModel(_associationsModel);
  _groupsTreeView->setModel(_groupsModel);
  _meshTreeView->setModel(_meshModel);


  // --- setting selection model
  _patternDataSelectionModel    = new PatternDataSelectionModel(_patternDataModel); //_currentModel
  _patternBuilderSelectionModel = new PatternBuilderSelectionModel( _patternBuilderModel, _patternDataSelectionModel );

  _currentGraphicView->setSelectionModel(_patternDataSelectionModel);
  _patternDataTreeView->setSelectionModel(_patternDataSelectionModel);
//   _patternDataTreeView->setSelectionMode(QAbstractItemView::SingleSelection); QAbstractItemView::MultiSelection //CS_TEST
  _patternBuilderTreeView->setSelectionModel(_patternBuilderSelectionModel);

  _treeViewDelegate->setDocumentModel( _currentModel );
  _treeViewDelegate->setPatternDataSelectionModel( _patternDataSelectionModel );
  _treeViewDelegate->setPatternBuilderSelectionModel( _patternBuilderSelectionModel );
  _treeViewDelegate->setGroupSelectionModel( _groupsTreeView->selectionModel() );
  _treeViewDelegate->setMeshSelectionModel( _meshTreeView->selectionModel() );


  // Salome Data Model
  suitVW = _currentGraphicView->get_SUIT_ViewWindow();
   HEXABLOCKGUI_DataModel* dm = dynamic_cast<HEXABLOCKGUI_DataModel*>( dataModel() );
   if ( dm ) {
      dm->createDocument( _currentModel->documentImpl(), suitVW );
      getApp()->updateObjectBrowser();
   }

  _mapViewModel[suitVW] = _currentModel;

//   showDockWidgets(true);
//   showPatternMenus
//   _dwPattern->setVisible(true);
//   _dwPattern->toggleViewAction()->setVisible(true);
//   _dwPattern->raise();

//   showPatternMenus( true );
//   showAssociationMenus( true );
//   showGroupsMenus( true );
//   showMeshMenus( true );

  _dwPattern->raise();
//     testDocument();
}



void HEXABLOCKGUI::importDocument( const QString &inFile )
{
  DEBTRACE("HEXABLOCKGUI::importDocument");
  QMainWindow *aParent = application()->desktop();
  QString selectedFile;

  if ( inFile.isNull() ){
    QFileDialog dialog( aParent, "Choose a filename to load" ,
                        QString::null, tr( "XML-Files (*.xml);;All Files (*)" ) );
    dialog.setHistory( getQuickDirList() );
    if (dialog.exec()){
      QStringList selectedFiles = dialog.selectedFiles();
      if (!selectedFiles.isEmpty())
        selectedFile = selectedFiles.first();
    }
  } else {
    selectedFile = inFile;
  }

  if (! selectedFile.isEmpty()){
    newDocument();
    _currentModel->load(selectedFile);
//     _currentGraphicView->loadVTK( "/tmp/load.vtk" ); //CS_TEST
//     _currentGraphicView->loadVTK( "/export/home/bphetsar/V6_main/lorraine.vtk" ); //CS_TEST
  }

//   showPatternMenus( true );
//   showAssociationMenus( true );
//   showGroupsMenus( true );
//   showMeshMenus( true );

}


void HEXABLOCKGUI::addVertex()
{
    if (!_dwInputPanel) return;

    VertexDialog* diag = new VertexDialog(_dwInputPanel);
    diag->setDocumentModel( _currentModel );
    diag->setPatternDataSelectionModel(_patternDataSelectionModel);
    diag->setFocus();
//     std::cout<< "_patternDataSelectionModel  -> " <<_patternDataSelectionModel <<std::endl;
//     std::cout<< "_patternDataTreeView->selectionModel() -> " <<_patternDataTreeView->selectionModel() <<std::endl;
//     diag->setPatternDataSelectionModel(static_patternDataTreeView->selectionModel());
    _dwInputPanel->setWidget(diag);
    _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::addEdge()
{
    if (!_dwInputPanel) return;

    EdgeDialog* diag = new EdgeDialog(_dwInputPanel, true);
    diag->setDocumentModel( _currentModel );
    diag->setPatternDataSelectionModel(_patternDataSelectionModel);
    diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
    diag->setFocus();
    _dwInputPanel->setWidget(diag);
    _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
//     _patternDataTreeView->setFocusPolicy(Qt::NoFocus);
//     _patternDataTreeView->setFocusProxy(diag);
//     diag->show();
//     diag->setFocus();
//     diag->raise();
//     diag->activateWindow();
//     diag->exec();
}


void HEXABLOCKGUI::addQuad()
{
  if (!_dwInputPanel) return;

  QuadDialog* diag = new QuadDialog(_dwInputPanel, true);
  diag->setDocumentModel( _currentModel );
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
  //   diag->show();
}


void HEXABLOCKGUI::addHexa()
{
  if (!_dwInputPanel) return;

  HexaDialog* diag = new HexaDialog(_dwInputPanel, true);
  diag->setDocumentModel( _currentModel );
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
  //   diag->show();
}


void HEXABLOCKGUI::addVector()
{
  if (!_dwInputPanel) return;

  VectorDialog* diag = new VectorDialog(_dwInputPanel, true);
  diag->setDocumentModel( _currentModel );
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
  //   diag->raise();
  //   diag->activateWindow();
  //   diag->show();
}


void HEXABLOCKGUI::addCylinder()
{
  CylinderDialog* diag = new CylinderDialog(_dwInputPanel, true);
  diag->setDocumentModel( _currentModel );
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}


void HEXABLOCKGUI::addPipe()
{
  PipeDialog* diag = new PipeDialog(_dwInputPanel, true);
  diag->setDocumentModel( _currentModel );
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}


void HEXABLOCKGUI::makeGrid()
{
  if (!_dwInputPanel) return;

  MakeGridDialog* diag = new MakeGridDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
  //   diag->show();
}

void HEXABLOCKGUI::makeCylinder()
{
  if (!_dwInputPanel) return;

  MakeCylinderDialog* diag = new MakeCylinderDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::makePipe()
{
  if (!_dwInputPanel) return;

  MakePipeDialog* diag = new MakePipeDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::makeCylinders()
{
  if (!_dwInputPanel) return;

  MakeCylindersDialog* diag = new MakeCylindersDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::makePipes()
{
  if (!_dwInputPanel) return;

  MakePipesDialog* diag = new MakePipesDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}



void HEXABLOCKGUI::removeHexa()
{
  if (!_dwInputPanel) return;

  RemoveHexaDialog* diag = new RemoveHexaDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
  //   diag->show();
}

void HEXABLOCKGUI::prismQuad()
{
 if (!_dwInputPanel) return;

  PrismQuadDialog* diag = new PrismQuadDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::joinQuad()
{
  if (!_dwInputPanel) return;

  JoinQuadDialog* diag = new JoinQuadDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}


void HEXABLOCKGUI::merge()
{
  if (!_dwInputPanel) return;

  MergeDialog* diag = new MergeDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}


void HEXABLOCKGUI::disconnectElts()
{
  if (!_dwInputPanel) return;

  DisconnectDialog* diag = new DisconnectDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::cutEdge()
{
  if (!_dwInputPanel) return;

  CutEdgeDialog* diag = new CutEdgeDialog(_dwInputPanel, true);
  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}


void HEXABLOCKGUI::makeTransformation()
{
  if (!_dwInputPanel) return;

  MakeTransformationDialog* diag = new MakeTransformationDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
//   diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
  //   diag->show();
}


void HEXABLOCKGUI::makeSymmetry()
{
  if (!_dwInputPanel) return;

  MakeSymmetryDialog* diag = new MakeSymmetryDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::performTransformation()
{
  if (!_dwInputPanel) return;

  PerformTransformationDialog* diag = new PerformTransformationDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::performSymmetry()
{
  if (!_dwInputPanel) return;

  PerformSymmetryDialog* diag = new PerformSymmetryDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setPatternDataSelectionModel(_patternDataSelectionModel);
  diag->setPatternBuilderSelectionModel(_patternBuilderSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}


void HEXABLOCKGUI::addGroup()
{
  if (!_dwInputPanel) return;

  GroupDialog* diag = new GroupDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::removeGroup() 
{
  bool removeGroupOK = false;

  QItemSelectionModel *groupsSelectionModel = _groupsTreeView->selectionModel();

  QModelIndexList l = groupsSelectionModel->selectedIndexes();
  foreach( const QModelIndex& selected, l ){
    if ( selected.data(HEXA_TREE_ROLE) == GROUP_TREE ){
      QModelIndex selected = _groupsModel->mapToSource( selected );
      Q_ASSERT(selected.isValid());
      bool removed = _currentModel->removeGroup(selected);
      if ( removed == true )
        removeGroupOK = true;
      else
        SUIT_MessageBox::critical( 0, tr( "ERR_ERROR" ), tr( "CANNOT REMOVE %1" ).arg(selected.data().toString()) );
    }
  }

  if ( removeGroupOK == true ){
    SUIT_MessageBox::information( 0, tr( "HEXA_INFO" ), tr( "GROUP REMOVED" ) );
  } else {
    SUIT_MessageBox::critical( 0, tr( "ERR_ERROR" ), tr( "CANNOT REMOVE GROUP" ) );
  }

}

void HEXABLOCKGUI::addLaw()
{
  if (!_dwInputPanel) return;

  QItemSelectionModel *meshSelectionModel = _meshTreeView->selectionModel();

  LawDialog* diag = new LawDialog(_dwInputPanel, true);

  diag->setDocumentModel(_currentModel);
  diag->setMeshSelectionModel(meshSelectionModel);
  diag->setFocus();
  _dwInputPanel->setWidget(diag);
  _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
}

void HEXABLOCKGUI::removeLaw()
{
  bool removeLawOK = false;

  QItemSelectionModel *meshSelectionModel = _meshTreeView->selectionModel();

  QModelIndexList l = meshSelectionModel->selectedIndexes();
  foreach( const QModelIndex& selected, l ){
    if ( selected.data(HEXA_TREE_ROLE) == LAW_TREE ){
      QModelIndex selected = _meshModel->mapToSource( selected );
      Q_ASSERT(selected.isValid());
      bool removed = _currentModel->removeLaw(selected);
      if ( removed == true )
        removeLawOK = true;
      else
        SUIT_MessageBox::critical( 0, tr( "ERR_ERROR" ), tr( "CANNOT REMOVE %1" ).arg(selected.data().toString()) );
    }
  }

  if ( removeLawOK == true ){
    SUIT_MessageBox::information( 0, tr( "HEXA_INFO" ), tr( "LAW REMOVED" ) );
  } else {
    SUIT_MessageBox::critical( 0, tr( "ERR_ERROR" ), tr( "CANNOT REMOVE LAW" ) );
  }

}



void HEXABLOCKGUI::setPropagation()
{
  if (!_dwInputPanel) return;

  QItemSelectionModel *meshSelectionModel = _meshTreeView->selectionModel();
  QModelIndexList l = meshSelectionModel->selectedIndexes();
  foreach( const QModelIndex& selected, l ){
    if ( selected.data(HEXA_TREE_ROLE) == PROPAGATION_TREE ){
      QModelIndex selected = _meshModel->mapToSource( selected );
      Q_ASSERT(selected.isValid());
      HEXA_NS::Propagation* p = selected.data(HEXA_DATA_ROLE).value<HEXA_NS::Propagation *>();
      PropagationDialog* diag = new PropagationDialog(_dwInputPanel, true);
      diag->setDocumentModel(_currentModel);
      diag->setMeshSelectionModel(meshSelectionModel);
      diag->setValue(p);
      diag->setFocus();
      _dwInputPanel->setWidget(diag);
      _dwInputPanel->setWindowTitle( tr("INPUT PANEL : %1").arg(diag->windowTitle()) );
      return;
    }
  }

  SUIT_MessageBox::critical( 0, tr( "ERR_ERROR" ), tr( "PLEASE SELECT A PROPAGATION" ) );
}





LightApp_SelectionMgr* HEXABLOCKGUI::selectionMgr()
{
  SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>( SUIT_Session::session()->activeApplication() );
  if( anApp )
    return dynamic_cast<LightApp_SelectionMgr*>( anApp->selectionMgr() );
  else
    return 0;
}






// bool HEXABLOCKGUI::eventFilter(QObject *obj, QEvent *event)
// {
//     if ( event->type() == QEvent::Enter ){//QEvent::Show ){ //QEvent::KeyPress) { 
//         showObjectBrowserMenus( false );
//         showPatternMenus( false );
//         showAssociationMenus( false );
//         showGroupsMenus( false );
//         showMeshMenus( false );
//         if ( obj == _dwObjectBrowser ) {
//           showObjectBrowserMenus( true );
//         } else if ( obj == _dwPattern  ) {
//           showPatternMenus( true );
//         } else if ( obj == _dwAssociation ) {
//           showAssociationMenus( true );
//         } else if ( obj == _dwGroups ) {
//           showGroupsMenus( true );
//         } else if ( obj == _dwMesh ) {
// 
//           showMeshMenus( true );
//         }
//         return false;
//     } else {
//          // standard event processing
//          return QObject::eventFilter(obj, event);
//     }
// }




QStringList HEXABLOCKGUI::getQuickDirList()
{
  QStringList dirList;
  SUIT_ResourceMgr* resMgr = SUIT_Session::session()->resourceMgr();
  if ( resMgr )
    dirList = resMgr->stringValue( "FileDlg", "QuickDirList" ).split( ';', QString::SkipEmptyParts );
  return dirList;
}



// --- Export the module

extern "C"
{
  HEXABLOCKGUI_EXPORT CAM_Module* createModule()
  {
    return new HEXABLOCKGUI();
  }
}






