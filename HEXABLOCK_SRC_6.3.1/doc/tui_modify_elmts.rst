:tocdepth: 3

.. _tuimodifyelements:


========================================
Modify elements by transforming elements
========================================

Translate and Rotate for any kind of elements::

 	doc.performTranslation(elements, vec)
 	doc.performRotation(elements, ver, vec, angle)

GUI command: :ref:`guimodifyelements`
