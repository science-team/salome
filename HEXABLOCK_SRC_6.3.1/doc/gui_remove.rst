:tocdepth: 3

.. _guiremove:

================
Remove hexahedra
================


To remove an **Hexaedron** in the **Main Menu** select **Model -> Remove hexa** 

**Arguments:** 1 hexaedra + select if including connected hexa or not


The dialogue box for removing hexahedra:

**todo refaire les copies d'ecran avec les hexaedres selectionnes**

.. image:: _static/dialogbox_remove.PNG
   :align: center

.. centered::
   Remove hexahedra

The selection can be made either in the list of hexahedra or in the graphic zone.

.. image:: _static/remove3.PNG
   :align: center

TUI command: :ref:`tuiremove`
