:tocdepth: 3

.. _tuimergeelements:

============== 
Merge elements
==============


Merge 2 quadrangles::
 	
	l = doc.mergeQuads(qa, qb, va1, vb1, va2, vb2)
 
Merge 2 edges::

	 l = doc.mergeEdges(e1, e2, v1, v2)

Merge 2 vertices::

	 l = doc.mergeVertices(ver1, ver2)


GUI command: :ref:`guimergeelements`
