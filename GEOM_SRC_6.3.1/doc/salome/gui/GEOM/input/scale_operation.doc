/*!

\page scale_operation_page Scale Transform

\n To produce a <b>Scale Transform</b> in the <b>Main Menu</b> select
<b>Operations - > Transformation - > Scale Transform</b>

\n This operation creates a scaled shape basing on the initial
shape. For this, you need to define the \b Shape to be scaled, the
<b>Central Point</b> of scale and the Scale Factor(s).
\n The \b Result will be a \b GEOM_Object.
\n <b>Advanced option:</b>
   \ref restore_presentation_parameters_page "Set presentation parameters and subshapes from arguments".

\image html transformation10.png
\n <b>TUI Command:</b> <em>geompy.MakeScaleTransform(Shape, CenterOfScale, Factor)</em>
\n <b>Arguments:</b> Name + 1 shape(s) + 1 vertex + 1 Scale Factor.

\image html transformation10a.png
\n <b>TUI Command:</b> <em>geompy.MakeScaleAlongAxes(Shape, CenterOfScale, FactorX, FactorY, FactorZ)</em>
\n <b>Arguments:</b> Name + 1 shape(s) + 1 vertex + 3 Scale Factors.

\note If the <b>Central Point</b> is not defined, the scaling will be
performed relatively the origin of the global coordinate system.

\note Scaling by one factor is a simple transformation, it does not modify the
geometry of the shape, while scaling by several different factors along axes
is a general transformation, which can modify the geometry, for example, a
circle can be transformed into an ellipse.

\n <b>Example of simple scaling:</b>

\image html scale_transformsn1.png "The initial object"

\image html scale_transformsn2.png "The resulting object (resized)"

\n <b>Example of scaling by different factors along axes:</b>

\image html scale_transformsn3.png "The initial object"

\image html scale_transformsn4.png "The resulting object (resized and distorted)"

Our <b>TUI Scripts</b> provide you with useful examples of the use of
\ref tui_scale "Scale Transformation" and of \ref swig_scale "Scale Along Axes Transformation"

*/
