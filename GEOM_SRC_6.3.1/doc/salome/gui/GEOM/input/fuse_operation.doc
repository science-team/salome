/*!

\page fuse_operation_page Fuse

For detail description of the Boolean operations please refer to
<a href="SALOME_BOA_PA.pdf">this document</a>. 
It provides a general review of the Partition and Boolean
operations algorithms, describes the usage methodology and highlighs
major limitations of these operations.

To produce a \b Fuse operation in the <b>Main Menu</b> select
<b>Operations - > Boolean - > Fuse</b>

This operation creates a shape from two shapes.

The \b Result will be any \b GEOM_Object.
<b>TUI Command:</b> <em>geompy.MakeFuse(s1, s2)</em>
<b>Arguments:</b> Name + 2 shapes.
<b>Advanced option:</b>
\ref restore_presentation_parameters_page "Set presentation parameters and subshapes from arguments".

\image html bool1.png

<b>Example:</b>

\image html fusesn1.png "The initial shapes"

\image html fusesn2.png "The resulting fuse"

Our <b>TUI Scripts</b> provide you with useful examples of the use of 
\ref tui_fuse "Boolean Operations".

*/
