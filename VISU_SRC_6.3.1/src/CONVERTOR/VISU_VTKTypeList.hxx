// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_VTKTypeList.hxx
//  Author : Oleg UVAROV
//  Module : VISU
//
#ifndef VISU_VTKTypeList_HeaderFile
#define VISU_VTKTypeList_HeaderFile

#include "VISU_TypeList.hxx"

#include <vtkCharArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkShortArray.h>
#include <vtkUnsignedShortArray.h>
#include <vtkIntArray.h>
#include <vtkUnsignedIntArray.h>
#include <vtkLongArray.h>
#include <vtkUnsignedLongArray.h> 
#include <vtkFloatArray.h> 
#include <vtkDoubleArray.h> 


//----------------------------------------------------------------------------
namespace VISU
{
  //----------------------------------------------------------------------------
  namespace TL
  {
    //----------------------------------------------------------------------------
    typedef TSequence< char, 
                       unsigned char, 
                       short, 
                       unsigned short, 
                       int, 
                       unsigned int, 
                       long, 
                       unsigned long, 
                       float, 
                       double >::TResult TVTKBasicTypeList;


    //----------------------------------------------------------------------------
    typedef TSequence< vtkCharArray, 
                       vtkUnsignedCharArray, 
                       vtkShortArray, 
                       vtkUnsignedShortArray, 
                       vtkIntArray, 
                       vtkUnsignedIntArray, 
                       vtkLongArray, 
                       vtkUnsignedLongArray, 
                       vtkFloatArray, 
                       vtkDoubleArray >::TResult TVTKArrayTypeList;


    //----------------------------------------------------------------------------
    typedef TSequence< TInt2Type< VTK_CHAR >, 
                       TInt2Type< VTK_UNSIGNED_CHAR >, 
                       TInt2Type< VTK_SHORT >, 
                       TInt2Type< VTK_UNSIGNED_SHORT >, 
                       TInt2Type< VTK_INT >, 
                       TInt2Type< VTK_UNSIGNED_INT >, 
                       TInt2Type< VTK_LONG >, 
                       TInt2Type< VTK_UNSIGNED_LONG >, 
                       TInt2Type< VTK_FLOAT >, 
                       TInt2Type< VTK_DOUBLE > >::TResult TVTKBasicEnumList;
    
    
    //----------------------------------------------------------------------------
    template< unsigned int type_enum >
    struct TEnum2VTKBasicType
    {
      typedef typename TTypeAt< TVTKBasicTypeList, TIndexOf< TVTKBasicEnumList, TInt2Type< type_enum > >::value >::TResult TResult;
    };

    
    //----------------------------------------------------------------------------
    template< unsigned int type_enum >
    struct TEnum2VTKArrayType
    {
      typedef typename TTypeAt< TVTKArrayTypeList, TIndexOf< TVTKBasicEnumList, TInt2Type< type_enum > >::value >::TResult TResult;
    };
    

    //----------------------------------------------------------------------------
    template< class T >
    struct TVTKBasicType2Enum
    {
      typedef typename TTypeAt< TVTKBasicEnumList, TIndexOf< TVTKBasicTypeList, T >::value >::TResult TResult;
    };
    

    //----------------------------------------------------------------------------
  }


  //----------------------------------------------------------------------------
}

#endif
