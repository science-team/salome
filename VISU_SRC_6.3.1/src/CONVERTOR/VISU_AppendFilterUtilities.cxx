// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME OBJECT : kernel of SALOME component
//  File   : VISU_GeometryFilter.cxx
//  Author : 
//  Module : SALOME
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/CONVERTOR/VISU_AppendFilterUtilities.cxx,v 1.3.2.1.6.1.8.1 2011-06-02 06:00:16 vsr Exp $
//
#include "VISU_AppendFilterUtilities.hxx"
#include "VISU_ConvertorUtils.hxx"

#include <vtkCell.h>
#include <vtkCellData.h>
#include <vtkPointData.h>

#include <vtkDataSetCollection.h>
#include <vtkObjectFactory.h>

#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>

#include <vtkInformationVector.h>
#include <vtkInformation.h>
#include <vtkExecutive.h>

#include <vtkPoints.h>
#include <vtkIntArray.h>

#include <algorithm>
#include <vector>
#include <map>

namespace
{
  //---------------------------------------------------------------
  typedef vtkIdType TCellId;
  typedef vtkIdType TInputId;
  typedef std::pair<TInputId, TCellId> TInputCellId;

  typedef std::pair<vtkIdType, vtkIdType> TObjectId; 
  typedef std::map<TObjectId, TInputCellId> TObject2InputIdMap;
  

  //---------------------------------------------------------------
  void
  DoMergingInputs(vtkCellData *theCellData, 
                  TInputId theInputId,
                  TObject2InputIdMap& theResult)
  {
    if(vtkDataArray *aDataArray = theCellData->GetArray("VISU_CELLS_MAPPER")){
      if(vtkIntArray *anIntArray = dynamic_cast<vtkIntArray*>(aDataArray)){
        int *aPointer = anIntArray->GetPointer(0);
        int aNbCells = anIntArray->GetNumberOfTuples();
        for(vtkIdType aCellId = 0; aCellId < aNbCells; aCellId++){
          int aObjId = *aPointer++;
          int anEntity = *aPointer++;
          TObjectId anObjectId(aObjId, anEntity);
          TObject2InputIdMap::iterator anIter = theResult.find(anObjectId);
          if(anIter != theResult.end())
            continue;
          TInputCellId anInputCellId(theInputId, aCellId);
          theResult.insert(anIter, TObject2InputIdMap::value_type(anObjectId, anInputCellId));
        }
      }
    }
  }


  //---------------------------------------------------------------
  struct TFillFieldList
  {
    vtkDataSetAttributes::FieldList myFieldList;
    bool myIsFirstCellData;

    TFillFieldList(vtkIdType theNbInputs):
      myFieldList(theNbInputs),
      myIsFirstCellData(true)
    {}

    void
    operator()(TInputId theInputId, vtkDataSet* theDataSet)
    {
      vtkCellData *aCellData = theDataSet->GetCellData();
      if(myIsFirstCellData){
        myFieldList.InitializeFieldList(aCellData);
        myIsFirstCellData = false;
      }else{
        myFieldList.IntersectFieldList(aCellData);
      }
    }
    
    virtual
    vtkIdType
    GetNbCells() const = 0;
  };
  

  //---------------------------------------------------------------
  struct TCellCounter: TFillFieldList
  {
    vtkIdType myNbCells;
    
    TCellCounter(vtkIdType theNbInputs):
      TFillFieldList(theNbInputs),
      myNbCells(0)
    {}

    void
    operator()(TInputId theInputId, vtkDataSet* theDataSet)
    {
      TFillFieldList::operator()(theInputId, theDataSet);
      myNbCells += theDataSet->GetNumberOfCells();
    }

    virtual
    vtkIdType
    GetNbCells() const
    {
      return myNbCells;
    }
  };


  //---------------------------------------------------------------
  struct TCellIdMerger: TFillFieldList
  {
    TObject2InputIdMap myObject2InputIdMap;

    TCellIdMerger(vtkIdType theNbInputs):
      TFillFieldList(theNbInputs)
    {}

    void
    operator()(TInputId theInputId, vtkDataSet* theDataSet)
    {
      TFillFieldList::operator()(theInputId, theDataSet);
      vtkCellData *aCellData = theDataSet->GetCellData();
      DoMergingInputs(aCellData, theInputId, myObject2InputIdMap);
    }

    virtual
    vtkIdType
    GetNbCells() const
    {
      return myObject2InputIdMap.size();
    }
  };


  //---------------------------------------------------------------
  template<class TFunctor>
  void
  ForEachInput(vtkInformationVector **theInputVector, 
               vtkIdType theNumberOfInputConnections,
               TFunctor& theFunctor)
  {
    for(vtkIdType anInputId = 0; anInputId < theNumberOfInputConnections; anInputId++)
      if(vtkDataSet *aDataSet = VISU::GetInput(theInputVector, anInputId))
        if(aDataSet->GetNumberOfPoints() > 0 && aDataSet->GetNumberOfCells() > 0)
          theFunctor(anInputId, aDataSet);
  }


  //---------------------------------------------------------------
  template<class TDataSet>
  bool
  RequestData(vtkInformationVector **theInputVector,
              vtkIdType theNumberOfInputConnections,
              vtkInformationVector *theOutputVector,
              vtkPointSet* theSharedPointSet,
              bool theIsMergingInputs,
              bool theIsMappingInputs)
  {
    if ( theNumberOfInputConnections == 1 ) {
      // get the input and ouptut
      vtkDataSet *anInput = VISU::GetInput( theInputVector, 0 );
      vtkDataSet* anOutput = VISU::GetOutput( theOutputVector );

      if ( anInput->GetDataObjectType() != anOutput->GetDataObjectType() )
        return false;

      // This has to be here because it initialized all field datas.
      anOutput->CopyStructure( anInput );
  
      // Pass all. (data object's field data is passed by the
      // superclass after this method)
      anOutput->GetPointData()->PassData( anInput->GetPointData() );
      anOutput->GetCellData()->PassData( anInput->GetCellData() );
      
      return true;
    }

    if ( theSharedPointSet ) {
      vtkPoints* aPoints = theSharedPointSet->GetPoints();
      if(aPoints->GetNumberOfPoints() < 1)
        return true;
  
      TDataSet* anOutput = TDataSet::SafeDownCast(VISU::GetOutput(theOutputVector));
      vtkIdType anNbInputs = theNumberOfInputConnections;
      if ( theIsMergingInputs ) {
        TCellIdMerger aFunctor(anNbInputs);
        ForEachInput<TCellIdMerger>(theInputVector, anNbInputs, aFunctor);

        vtkDataSetAttributes::FieldList& aFieldList = aFunctor.myFieldList;
        TObject2InputIdMap& anObject2InputIdMap = aFunctor.myObject2InputIdMap;
        vtkIdType aNbCells = aFunctor.GetNbCells();
        if(aNbCells < 1)
          return true;
    
        // Now can allocate memory
        anOutput->Allocate(aNbCells); 
        vtkCellData *anOutputCellData = anOutput->GetCellData();
        anOutputCellData->CopyAllocate(aFieldList, aNbCells);
      
        // Append each input dataset together
        // 1.points
        anOutput->SetPoints(theSharedPointSet->GetPoints());
        anOutput->GetPointData()->PassData(theSharedPointSet->GetPointData());
      
        // 2.cells
        vtkIdList *anIdList = vtkIdList::New(); 
        anIdList->Allocate(VTK_CELL_SIZE);
        TObject2InputIdMap::const_iterator anIter = anObject2InputIdMap.begin();
        TObject2InputIdMap::const_iterator anEndIter = anObject2InputIdMap.end();
        for(; anIter != anEndIter; anIter++){
          //TObjectId anObjectId = anIter->first;
          const TInputCellId& anInputCellId = anIter->second;
          TInputId anInputId = anInputCellId.first;
          if(vtkDataSet *aDataSet = VISU::GetInput(theInputVector, anInputId)){
            TCellId aCellId = anInputCellId.second;
            aDataSet->GetCellPoints(aCellId, anIdList);
            
            vtkIdType aCellType = aDataSet->GetCellType(aCellId);
            vtkIdType aNewCellId = anOutput->InsertNextCell(aCellType, anIdList);
          
            vtkCellData *aCellData = aDataSet->GetCellData();
            anOutputCellData->CopyData(aFieldList, aCellData, anInputId, aCellId, aNewCellId);
          }
        }
        anIdList->Delete();

        if(theIsMappingInputs){
          vtkIntArray *aDataArray = vtkIntArray::New();
          aDataArray->SetName("VISU_INPUTS_MAPPER");
          aDataArray->SetNumberOfComponents(2);
          aDataArray->SetNumberOfTuples(aNbCells);

          vtkIdType aTupleId = 0;
          TObject2InputIdMap::const_iterator anIter = anObject2InputIdMap.begin();
          TObject2InputIdMap::const_iterator anEndIter = anObject2InputIdMap.end();
          for(vtkIdType aCellId = 0; anIter != anEndIter; anIter++, aCellId++){
            const TInputCellId& anInputCellId = anIter->second;
            TInputId anInputId = anInputCellId.first;
            /*TCellId*/ aCellId = anInputCellId.second;
            aDataArray->SetValue(aTupleId++, anInputId);
            aDataArray->SetValue(aTupleId++, aCellId);
          }

          anOutputCellData->AddArray(aDataArray);
          aDataArray->Delete();
        }

        return true;
      }else{
        TCellCounter aFunctor(anNbInputs);
        ForEachInput<TCellCounter>(theInputVector, anNbInputs, aFunctor);
        
        vtkDataSetAttributes::FieldList& aFieldList = aFunctor.myFieldList;
        vtkIdType aNbCells = aFunctor.GetNbCells();
        if(aNbCells < 1)
          return true;
        
        // Now can allocate memory
        anOutput->Allocate(aNbCells); 
        vtkCellData *anOutputCellData = anOutput->GetCellData();
        anOutputCellData->CopyAllocate(aFieldList, aNbCells);
        
        // Append each input dataset together
        // 1.points
        anOutput->SetPoints(theSharedPointSet->GetPoints());
        anOutput->GetPointData()->PassData(theSharedPointSet->GetPointData());
        
        // 2.cells
        vtkIdList *anIdList = vtkIdList::New(); 
        anIdList->Allocate(VTK_CELL_SIZE);
        for(vtkIdType anInputId = 0; anInputId < anNbInputs; anInputId++){
          if(vtkDataSet *aDataSet = VISU::GetInput(theInputVector, anInputId)){
            vtkIdType aNbCells = aDataSet->GetNumberOfCells(); 
            vtkCellData *aCellData = aDataSet->GetCellData();
            // copy cell and cell data
            for(vtkIdType aCellId = 0; aCellId < aNbCells; aCellId++){
              aDataSet->GetCellPoints(aCellId, anIdList);

              vtkIdType aCellType = aDataSet->GetCellType(aCellId);
              vtkIdType aNewCellId = anOutput->InsertNextCell(aCellType, anIdList);

              anOutputCellData->CopyData(aFieldList, aCellData, anInputId, aCellId, aNewCellId);
            }
          }
        }
        anIdList->Delete();

        if(theIsMappingInputs){
          vtkIntArray *aDataArray = vtkIntArray::New();
          aDataArray->SetName("VISU_INPUTS_MAPPER");
          aDataArray->SetNumberOfComponents(2);
          aDataArray->SetNumberOfTuples(aNbCells);

          vtkIdType aTupleId = 0;
          for(vtkIdType anInputId = 0; anInputId < anNbInputs; anInputId++){
            if(vtkDataSet *aDataSet = VISU::GetInput(theInputVector, anInputId)){
              vtkIdType aNbCells = aDataSet->GetNumberOfCells(); 
              for(vtkIdType aCellId = 0; aCellId < aNbCells; aCellId++){
                aDataArray->SetValue(aTupleId++, aCellId);
                aDataArray->SetValue(aTupleId++, anInputId);
              }
            }
          }

          anOutputCellData->AddArray(aDataArray);
          aDataArray->Delete();
        }
        return true;
      }
    }

    return false;
  }


  //---------------------------------------------------------------
}


namespace VISU
{
  //---------------------------------------------------------------
  TAppendFilterHelper
  ::TAppendFilterHelper(vtkObject* theParent):
    myIsMergingInputs(false),
    myIsMappingInputs(false),
    myParent(*theParent)
  {}


  //---------------------------------------------------------------
  void
  TAppendFilterHelper
  ::SetSharedPointSet(vtkPointSet* thePointSet)
  {
    if(GetSharedPointSet() == thePointSet)
      return;
    
    mySharedPointSet = thePointSet;
    
    myParent.Modified();
  }


  //---------------------------------------------------------------
  vtkPointSet*
  TAppendFilterHelper
  ::GetSharedPointSet()
  {
    return mySharedPointSet.GetPointer();
  }
  

  //---------------------------------------------------------------
  void
  TAppendFilterHelper
  ::SetMappingInputs(bool theIsMappingInputs)
  {
    if(myIsMappingInputs == theIsMappingInputs)
      return;
    
    myIsMappingInputs = theIsMappingInputs;
    myParent.Modified();
  }
  
  
  //---------------------------------------------------------------
  bool
  TAppendFilterHelper
  ::IsMappingInputs()
  {
    return myIsMappingInputs;
  }
  

  //---------------------------------------------------------------
  void
  TAppendFilterHelper
  ::SetMergingInputs(bool theIsMergingInputs)
  {
    if(myIsMergingInputs == theIsMergingInputs)
      return;
    
    myIsMergingInputs = theIsMergingInputs;
    myParent.Modified();
  }
  
  
  //---------------------------------------------------------------
  bool
  TAppendFilterHelper
  ::IsMergingInputs()
  {
    return myIsMergingInputs;
  }
  

  //---------------------------------------------------------------
  bool
  UnstructuredGridRequestData(vtkInformationVector **theInputVector,
                              vtkIdType theNumberOfInputConnections,
                              vtkInformationVector *theOutputVector,
                              vtkPointSet* theSharedPointSet,
                              bool theIsMergingInputs,
                              bool theIsMappingInputs)
  {
    return RequestData<vtkUnstructuredGrid>(theInputVector,
                                            theNumberOfInputConnections,
                                            theOutputVector,
                                            theSharedPointSet,
                                            theIsMergingInputs,
                                            theIsMappingInputs);
  }


  //---------------------------------------------------------------
  bool
  PolyDataRequestData(vtkInformationVector **theInputVector,
                      vtkIdType theNumberOfInputConnections,
                      vtkInformationVector *theOutputVector,
                      vtkPointSet* theSharedPointSet,
                      bool theIsMergingInputs,
                      bool theIsMappingInputs)
  {
    return RequestData<vtkPolyData>(theInputVector,
                                    theNumberOfInputConnections,
                                    theOutputVector,
                                    theSharedPointSet,
                                    theIsMergingInputs,
                                    theIsMappingInputs);
  }


  //---------------------------------------------------------------
}
