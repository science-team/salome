// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File:
//  Author:
//  Module : VISU
//
#include "VISU_TableReader.hxx"

#include <QFileInfo>
#include <QString>
#include <QRegExp>
#include <QFile>
#include <QStringList>

#include <fstream>
#include <iostream>
#include <sstream>

#include <vtkPoints.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkPolyData.h>

#include <vtkStructuredGrid.h>
#include <vtkStructuredGridGeometryFilter.h>

#include <Basics_Utils.hxx>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif


//---------------------------------------------------------------
int
VISU::TTable2D
::Check()
{
  if ( myRows.empty() ) 
    return 0;

  int iEnd = myRows[0].myValues.size();
  if ( iEnd == 0 )
    return 0;

  if ( myColumnTitles.size() != iEnd ) 
    myColumnTitles.resize( iEnd );

  if ( myColumnUnits.size() != iEnd )
    myColumnUnits.resize( iEnd );

  int jEnd = myRows.size();
  for ( int j = 0; j < jEnd; j++ )
    if ( myRows[j].myValues.size() != iEnd )
      return 0;

  return 1;
}


//---------------------------------------------------------------
void
VISU::TTable2D
::getColumns(VISU::TTable2D& theTable2D) const
{
  TRows& aRows = theTable2D.myRows;
  aRows.clear();
  if ( myRows.empty() )
    return;

  int jEnd = myRows.size();

  //Define Titles & Units
  theTable2D.myColumnTitles.resize(jEnd);
  theTable2D.myColumnUnits.resize(jEnd);
  for ( int j = 0; j < jEnd; j++ ) {
    theTable2D.myColumnTitles[j] = myRows[j].myTitle;
    theTable2D.myColumnUnits[j] = myRows[j].myUnit;
  }

  //Define Rows
  int iEnd = myRows[0].myValues.size();
  for ( int i = 0; i < iEnd; i++ ) {
    TRow aNewRow;
    aNewRow.myTitle = myColumnTitles[i];
    aNewRow.myUnit = myColumnUnits[i];
    aNewRow.myValues.resize(jEnd);
    for ( int j = 0; j < jEnd; j++ ) {
      aNewRow.myValues[j] = myRows[j].myValues[i];
    }
    aRows.push_back(aNewRow);
  }
}


//---------------------------------------------------------------
namespace
{
  int getLine( std::ifstream& theStmIn, QString& theString )
  {
    char tmp;
    std::ostringstream aStrOut;

    while ( theStmIn.get( tmp ) ) {
      aStrOut<<tmp;
      if ( tmp == '\n' ) 
        break;
    }

    aStrOut<<std::ends;
    theString = aStrOut.str().c_str();

    return !theStmIn.eof();
  }

  //=======================================================================
  //function : findNextCell
  //purpose  : auxilary for ImportCSVTable
  //=======================================================================
  bool findNextCell(std::ifstream& aStmIn, QString& aStr,
                   QString& aCell, const char theSeparator)
  {
    aCell = "";
    int index, tmpind = 0;
    if( aStr.at(0) == theSeparator ) {
      aStr.remove(0,1);
      aStr = aStr.trimmed();
      if(aStr.size()==0) return true;
    }
    QString aTmp = aStr;
    if( aTmp.at(0) == '"' ) {
      // find closed "
      while( !aStmIn.eof() ) {
        tmpind = aTmp.indexOf('"',1);
        if( tmpind < 0 ) {
          while( !aStmIn.eof() ) {
            aCell.push_back(aTmp);
            aCell.push_back('\n');
            getLine( aStmIn, aTmp );
            tmpind = aTmp.indexOf('"',1);
            if( tmpind >= 0 ) {
              break;
            }
          }
        }
        if(tmpind < 0)
          return false;
        // read next symbol
        if( aTmp.at(tmpind+1) == '"' ) {
          // "" is found => need to continue
          aCell.push_back(aTmp.left(tmpind+1));
          aTmp = aTmp.mid(tmpind+2);
        }
        else if( aTmp.at(tmpind+1) == theSeparator ) {
          index = tmpind+1;
          break;
        }
        else {
          return false;
        }
      }
    }
    else {
      // find index for separator
      index = aTmp.indexOf( theSeparator );
    }
    if( index < 0 ) {
      aCell += aTmp;
      aStr = "";
    }
    else {
      if(index>0) aCell += aTmp.left(index);
      aStr = aTmp.mid(index).trimmed();
    }
    if( aCell.size()>0 && aCell.at(0) == '"' ) {
      // remove first and last symbols
      int last = aCell.size()-1;
      aCell.remove(last,1);
      aCell.remove(0,1);
    }
    // replace "" to " in aCell
    QChar ctmp = '"';
    QString tmp(ctmp);
    tmp += ctmp;
    index = aCell.indexOf(tmp);
    while(index>=0) {
      aCell.remove(index,1);
      index = aCell.indexOf(tmp);
    }
    return true;
  }

}


//---------------------------------------------------------------
void 
VISU::ImportTables( const char* theFileName, TTableContainer& theContainer,
                    bool theFirstStrAsTitle)
{
  std::ifstream aStmIn;
  QFileInfo aFileInfo( theFileName );
  if( !aFileInfo.isFile() || !aFileInfo.isReadable() || !aFileInfo.size() )
    return;

  QString tmp(theFileName);
  tmp = tmp.trimmed();
  tmp = tmp.right(3).trimmed();

  if( tmp == QString("csv") ) {
    const char separator = ',';
    ImportCSVTable(theFileName, theContainer, theFirstStrAsTitle, separator);
    return;
  }

  aStmIn.open( theFileName );
  QString aTmp;
  do {
    // find beginning of table (tables are separated by empty lines)
    while( getLine( aStmIn, aTmp ) && aTmp.trimmed() == "" );

    PTableIDMapper aTableIDMapper( new TTableIDMapper() );
    TTable2D& aTable2D = *aTableIDMapper;
    if(MYDEBUG) std::cout << "New table is found" << std::endl;

    bool IsFirst = true;
    while( !aStmIn.eof() && aTmp.trimmed() != "" ){
      QString data = aTmp.trimmed();
      QString cmt = "";
      QString keyword = "";
      // split string to data and comment (comment starts from '#' symbol)
      int index = aTmp.indexOf( "#" );
      if ( index >= 0 ) {
        data = aTmp.left( index ).trimmed();
        cmt = aTmp.mid( index+1 ).trimmed();
      }
      // if comment is not empty, try to get keyword from it (separated by ':' symbol)
      if ( !cmt.isEmpty() ) {
        int index1 = cmt.indexOf( ":" );
        if ( index1 >= 0 ) {
          QString tmpstr = cmt.left( index1 ).trimmed();
          if ( tmpstr == QString( "TITLE" ) ||
               tmpstr == QString( "COLUMN_TITLES" ) ||
               tmpstr == QString( "COLUMN_UNITS" ) ||
               tmpstr == QString( "COMMENT" ) ) {
            keyword = tmpstr;
            cmt = cmt.mid( index1+1 ).trimmed();
          }
        }
      }
      // if data is empty, process only comment
      if ( data.isEmpty() ) {
        // if keyword is found, try to process it
        // elsewise it is a simple comment, just ignore it
        if ( !keyword.isEmpty() ) {
          if ( keyword == QString( "TITLE" ) ) {
            QString title = cmt;
            if ( aTable2D.myTitle != "" )
              title = QString( aTable2D.myTitle.c_str() ) + QString( " " ) + title;
            if(MYDEBUG) std::cout << "...Table TITLE is: " << title.toLatin1().constData() << std::endl;
            aTable2D.myTitle = title.toLatin1().constData();
          }
          else if ( keyword == QString( "COLUMN_TITLES" ) ) {
            // comment may contain column headers
            QStringList aStrList = cmt.split( "|", QString::SkipEmptyParts );
            if(MYDEBUG) std::cout << "...Column TITLES are: ";
            for ( int i = 0; i < aStrList.count(); i++ ) {
              QString tmpstr = aStrList[ i ].trimmed();
              if(MYDEBUG) std::cout << tmpstr.toLatin1().constData() << " ";
              aTable2D.myColumnTitles.push_back( tmpstr.toLatin1().constData() );
            }
            if(MYDEBUG) std::cout << std::endl;
          }
          else if ( keyword == QString( "COLUMN_UNITS" ) ) {
            // comment may contain column units
            QStringList aStrList = cmt.split( " ", QString::SkipEmptyParts );
            if(MYDEBUG) std::cout << "...Column UNITS are: ";
            for ( int i = 0; i < aStrList.count(); i++ ) {
              QString tmpstr = aStrList[ i ].trimmed();
              if(MYDEBUG) std::cout << tmpstr.toLatin1().constData() << " ";
              aTable2D.myColumnUnits.push_back( tmpstr.toLatin1().constData() );
            }
            if(MYDEBUG) std::cout << std::endl;
          }
          else if ( keyword == QString( "COMMENT" ) ) {
            // keyword 'COMMENT' processing can be here
            // currently it is ignored
            if(MYDEBUG) std::cout << "...COMMENT: " << cmt.toLatin1().constData() << std::endl;
          }
        }
        else {
          if(MYDEBUG) std::cout << "...comment: " << cmt.toLatin1().constData() << std::endl;
          // simple comment processing can be here
          // currently it is ignored
        }
      }
      // if data is not empty, try to process it
      else {
        TTable2D::TRow aRow;
        if(MYDEBUG) std::cout << "...New row is found: " << std::endl;
        QString datar1 = data.replace(QRegExp("\t"), " ");
        QStringList aValList = datar1.split( " ", QString::SkipEmptyParts );
        if( aTable2D.myColumnTitles.size()==0 && IsFirst && theFirstStrAsTitle ) {
          for ( int i = 0; i < aValList.count(); i++ ) {
            QString tmpstr = aValList[ i ].trimmed();
            aTable2D.myColumnTitles.push_back( tmpstr.toLatin1().constData() );
          }
        }
        else {
          if ( !cmt.isEmpty() ) {
            aRow.myTitle = cmt.toLatin1().constData();
            if(MYDEBUG) std::cout << "......ROW TITLE is: " << cmt.toLatin1().constData() << std::endl;
          }
          //QString datar1 = data.replace(QRegExp("\t"), " ");
          //QStringList aValList = datar1.split( " ", QString::SkipEmptyParts );
          for ( int i = 0; i < aValList.count(); i++ ) {
            if ( aValList[i].trimmed() != "" ) {
              TTable2D::TValue aVal = aValList[i].trimmed().toLatin1().constData();
              aRow.myValues.push_back( aVal );
            }
          }
          if( aRow.myValues.size() > 0 )
            aTable2D.myRows.push_back( aRow );
        }
        IsFirst = false;
        // ************** OLD CODE ******************
        /*
        TValue aVal;
        istringstream aStream( data );
        aStream.precision( STRPRECISION );
        while( aStream >> aVal ) {
          aRow.myValues.push_back( aVal );
        }
        if( aRow.myValues.size() > 0 )
          aTable2D.myRows.push_back( aRow );
        */
        // ************** OLD CODE ******************
      }
      getLine( aStmIn, aTmp );
    }
    if( aTable2D.Check() ) {
      if(MYDEBUG) std::cout << "aTable2D is checked OK " << aTable2D.myTitle << std::endl;
      theContainer.push_back( aTableIDMapper );
    }
  } while ( !aStmIn.eof() );
  aStmIn.close();

  if(MYDEBUG) std::cout << "After close" << std::endl;
}


//=======================================================================
//function : ImportCSVTable
//purpose  : 
//=======================================================================
void VISU::ImportCSVTable(const char* theFileName, TTableContainer& theContainer,
                          bool theFirstStrAsTitle, const char theSeparator)
{
  std::ifstream aStmIn;
  QFileInfo aFileInfo( theFileName );
  if( !aFileInfo.isFile() || !aFileInfo.isReadable() || !aFileInfo.size() )
    return;
  aStmIn.open( theFileName );
  QString aTmp;
  do {
    // find beginning of table (tables are separated by empty lines)
    while( getLine( aStmIn, aTmp ) && aTmp.trimmed() == "" );

    PTableIDMapper aTableIDMapper( new TTableIDMapper() );
    TTable2D& aTable2D = *aTableIDMapper;
    if(MYDEBUG) std::cout << "New table is found" << std::endl;

    bool IsFirst = true;
    QStringList aValList;

    while( !aStmIn.eof() ) {
      QString aCell = "";
      if( !( findNextCell(aStmIn, aTmp, aCell, theSeparator)) ) {
        return;
      }
      if( aTmp.size()==0 ) {
        // make table row
        aValList.push_back(aCell);
        if( IsFirst && theFirstStrAsTitle ) {
          for ( int i = 0; i < aValList.count(); i++ ) {
            aTable2D.myColumnTitles.push_back( aValList[i].trimmed().toLatin1().constData() );
          }
        }
        else {
          TTable2D::TRow aRow;
          for ( int i = 0; i < aValList.count(); i++ ) {
            if ( aValList[i].trimmed() != "" ) {
              TTable2D::TValue aVal = aValList[i].trimmed().toLatin1().constData();
              aRow.myValues.push_back( aVal );
            }
            else {
              aRow.myValues.push_back( "Empty" );
            }
          }
          if( aRow.myValues.size() > 0 ) {
            aTable2D.myRows.push_back( aRow );
          }
        }
        // clear list of values and read next string
        aValList.clear();
        getLine( aStmIn, aTmp );
        IsFirst = false;
      }
      else {
        // put value to table cell
        aValList.push_back(aCell);
      }
    }

    if( aTable2D.Check() ) {
      if(MYDEBUG) std::cout << "aTable2D is checked OK " << aTable2D.myTitle << std::endl;
      theContainer.push_back( aTableIDMapper );
    }

  } while ( !aStmIn.eof() );
  aStmIn.close();

  if(MYDEBUG) std::cout << "After close" << std::endl;
}


//---------------------------------------------------------------
VISU::TTableIDMapper
::TTableIDMapper():
  myOutput( vtkPolyData::New() ),
  myXAxisPosition( -1 )
{}

VISU::TTableIDMapper
::~TTableIDMapper()
{
  myOutput->Delete();
}

vtkPolyData*
VISU::TTableIDMapper
::GetPolyDataOutput()
{
  if ( myXAxisPosition == -1 )
    SetXAxisPosition( 0 );

  return myOutput;
}

long unsigned int
VISU::TTableIDMapper
::GetMemorySize()
{
  return myOutput->GetActualMemorySize() * 1024;
}

void
VISU::TTableIDMapper
::SetXAxisPosition( vtkIdType theAxisPosition )
{
  // Set "C" numeric locale to import numbers correctly
  Kernel_Utils::Localizer loc;

  if ( myXAxisPosition == theAxisPosition || !Check() )
    return;

  myOutput->Initialize();

  if ( !Check() )
    return;

  TTable2D aTable2D;
  getColumns( aTable2D );
  
  vtkIdType aXSize = aTable2D.myRows[0].myValues.size();

  // It is necessary to decrease the size at 1 take intoa account X axis
  vtkIdType anYSize = aTable2D.myRows.size() - 1; 

  vtkIdType aNbPoints = aXSize * anYSize;

  std::vector<double> anXAxis(aXSize);
  const TTable2D::TValues& aValues = aTable2D.myRows[theAxisPosition].myValues;
  for ( vtkIdType aX = 0; aX < aXSize; aX++ )
    anXAxis[aX] = atof( aValues[aX].c_str() );

  double aXRange = anXAxis[aXSize - 1] - anXAxis[0];
  double anYDelta = aXRange / anYSize;
  std::vector<double> anYAxis(anYSize);
  for ( vtkIdType anY = 0; anY < anYSize; anY++ )
    anYAxis[anY] = anY * anYDelta;

  vtkPoints* aPoints = vtkPoints::New();
  aPoints->SetNumberOfPoints( aNbPoints );

  vtkIntArray *aPointsIDMapper = vtkIntArray::New();
  aPointsIDMapper->SetName("VISU_POINTS_MAPPER");
  aPointsIDMapper->SetNumberOfComponents(2);
  aPointsIDMapper->SetNumberOfTuples(aNbPoints);
  int *aPointsIDMapperPtr = aPointsIDMapper->GetPointer(0);

  //vtkIntArray *aCellIDMapper = vtkIntArray::New();
  //aCellIDMapper->SetName("VISU_POINTS_MAPPER");
  //aCellIDMapper->SetNumberOfComponents(2);
  //aCellIDMapper->SetNumberOfTuples(aNbPoints);
  //int *aCellIDMapperPtr = aCellIDMapper->GetPointer(0);

  for ( vtkIdType aY = 0, aPntId = 0; aY < anYSize; aY++ ) {
    for ( vtkIdType aX = 0; aX < aXSize; aX++, aPntId++ ) {
      aPoints->SetPoint( aPntId, anXAxis[aX], anYAxis[aY], 0.0 );

      *aPointsIDMapperPtr++ = aPntId;
      *aPointsIDMapperPtr++ = 0;

      //*aCellIDMapperPtr++ = aPntId;
      //*aCellIDMapperPtr++ = 0;
    }
  }

  std::vector<TValues> anYData;
  for ( vtkIdType anY = 0; anY < anYSize + 1; anY++ ) {
    if ( anY == theAxisPosition )
      continue;
    anYData.push_back( aTable2D.myRows[anY].myValues );
  }

  vtkDoubleArray* aScalars = vtkDoubleArray::New();
  aScalars->SetNumberOfComponents( 1 );
  aScalars->SetNumberOfTuples( aNbPoints );
  double *aScalarsPtr = aScalars->GetPointer(0);
  for ( vtkIdType anY = 0; anY < anYSize; anY++ ) {
    const TTable2D::TValues& aValues = anYData[anY];
    for ( vtkIdType aX = 0; aX < aXSize; aX++ ) {
      double aValue = atof( aValues[aX].c_str() );
      *aScalarsPtr++ = aValue;
    }
  }

  vtkStructuredGrid* aStructuredGrid = vtkStructuredGrid::New();
  aStructuredGrid->SetPoints( aPoints );
  aPoints->Delete();

  aStructuredGrid->SetDimensions( aXSize, anYSize, 1 );

  aStructuredGrid->GetPointData()->AddArray( aPointsIDMapper );
  aPointsIDMapper->Delete();

  //aStructuredGrid->GetCellData()->AddArray( aCellIDMapper );
  //aCellIDMapper->Delete();

  aStructuredGrid->GetPointData()->SetScalars( aScalars );
  aScalars->Delete();

  vtkStructuredGridGeometryFilter* aFilter = vtkStructuredGridGeometryFilter::New();
  aFilter->SetInput( aStructuredGrid );
  aFilter->Update();
  myOutput->ShallowCopy( aFilter->GetOutput() );
  aFilter->Delete();
}

