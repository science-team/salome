// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Convertor_impl.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_Convertor_impl.hxx"
#include "VISU_Structures_impl.hxx"
#include "VISU_PointCoords.hxx"
#include "VISU_MeshValue.hxx"

#include "VISU_AppendFilter.hxx"
#include "VISU_AppendPolyData.hxx"
#include "VTKViewer_CellLocationsArray.h"
#include "VISU_CommonCellsFilter.hxx"

#include "VISU_ConvertorUtils.hxx"

#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>

#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellData.h>

#include <vtkIdList.h>
#include <vtkCellType.h>
#include <vtkCellArray.h>
#include <vtkCellLinks.h>
#include <vtkUnsignedCharArray.h>

#include <QString>
#include <QFileInfo>

#include <memory>

static vtkFloatingPointType ERR_SIZE_CALC = 1.00;

static int MYVTKDEBUG = 0;

#ifdef _DEBUG_
static int MYDEBUG = 0;
static int MYDEBUGWITHFILES = 0;
//#define _DEXCEPT_
#else
static int MYDEBUG = 0;
static int MYDEBUGWITHFILES = 0;
#endif


namespace
{
  //---------------------------------------------------------------
  template<class T> 
  std::string 
  dtos(const std::string& fmt, T val)
  {
    static QString aString;
    aString.sprintf(fmt.c_str(),val);
    return (const char*)aString.toLatin1();
  }


  //---------------------------------------------------------------
  inline
  void
  PrintCells( vtkCellArray* theConnectivity, 
              const VISU::TConnect& theVector)
  {
    theConnectivity->InsertNextCell( theVector.size(), &theVector[ 0 ] );
  }


  //---------------------------------------------------------------
  void
  GetCellsOnSubMesh(const VISU::PUnstructuredGrid& theSource,
                    const VISU::PMeshOnEntityImpl& theMeshOnEntity, 
                    const VISU::PSubMeshImpl& theSubMesh,
                    const vtkIdType theGeom) 
  {
    VISU::TTimerLog aTimerLog(MYDEBUG,"GetCellsOnSubMesh");
    const VISU::TCell2Connect& anArray = theSubMesh->myCell2Connect;
    vtkIdType aCellsSize = theSubMesh->myCellsSize;
    vtkIdType aNbCells = theSubMesh->myNbCells;
    INITMSG(MYDEBUG,"GetCellsOnSubMesh "<<
            "- theGeom = "<<theGeom<<
            "; aNbCells = "<<aNbCells<<
            endl);


    vtkCellArray* aConnectivity = vtkCellArray::New();
    aConnectivity->Allocate(aCellsSize,0);
    vtkUnsignedCharArray* aCellTypesArray = vtkUnsignedCharArray::New();
    aCellTypesArray->SetNumberOfComponents(1);
    aCellTypesArray->SetNumberOfTuples(aNbCells);

    for(vtkIdType anID = 0; anID < aNbCells; anID++){
      PrintCells( aConnectivity, anArray[ anID ] );
      aCellTypesArray->SetValue(anID,(unsigned char)theGeom);
    }

    {
      int aNbTuples = aNbCells;
      int anEntity = int(theMeshOnEntity->myEntity);
      vtkIntArray *aDataArray = vtkIntArray::New();
      aDataArray->SetName("VISU_CELLS_MAPPER");
      aDataArray->SetNumberOfComponents(2);
      aDataArray->SetNumberOfTuples(aNbTuples);
      int *aDataArrayPtr = aDataArray->GetPointer(0);
      for(int aTupleId = 0; aTupleId < aNbTuples; aTupleId++){
        int anObjID = theSubMesh->GetElemObjID(aTupleId);
        *aDataArrayPtr++ = anObjID;
        *aDataArrayPtr++ = anEntity;
      }
      theSource->GetCellData()->AddArray(aDataArray);
      aDataArray->Delete();
    }

    vtkIdType *pts = 0, npts = 0;
    VTKViewer_CellLocationsArray* aCellLocationsArray = VTKViewer_CellLocationsArray::New();
    aCellLocationsArray->SetNumberOfComponents(1);
    aCellLocationsArray->SetNumberOfTuples(aNbCells);
    aConnectivity->InitTraversal();
    for(int i=0; aConnectivity->GetNextCell(npts,pts); i++)
      aCellLocationsArray->SetValue(i,aConnectivity->GetTraversalLocation(npts));
    theSource->SetCells(aCellTypesArray,aCellLocationsArray,aConnectivity);

    if(MYVTKDEBUG) aConnectivity->DebugOn();

    aCellLocationsArray->Delete();
    aCellTypesArray->Delete();
    aConnectivity->Delete();
  } 
  
  
  //---------------------------------------------------------------
  void
  GetCellsOnFamily(const VISU::PUnstructuredGrid& theSource,
                   const VISU::PMeshOnEntityImpl& theMeshOnEntity, 
                   const VISU::PFamilyImpl& theFamily) 
  {
    INITMSG(MYDEBUG,"GetCellsOnFamily"<<endl);

    vtkIdType aNbCells = theFamily->myNbCells;
    vtkIdType aCellsSize = theFamily->myCellsSize;

    vtkCellArray* aConnectivity = vtkCellArray::New();
    aConnectivity->Allocate(aCellsSize,0);
    vtkUnsignedCharArray* aCellTypesArray = vtkUnsignedCharArray::New();
    aCellTypesArray->SetNumberOfComponents(1);
    aCellTypesArray->SetNumberOfTuples(aNbCells);

    VISU::TSubMeshID& aMeshID = theFamily->myMeshID;
    aMeshID.resize(aNbCells);

    vtkIntArray *aDataArray = vtkIntArray::New();
    int anEntity = int(theMeshOnEntity->myEntity);
    aDataArray->SetName("VISU_CELLS_MAPPER");
    aDataArray->SetNumberOfComponents(2);
    aDataArray->SetNumberOfTuples(aNbCells);
    int *aDataArrayPtr = aDataArray->GetPointer(0);

    VISU::TID2ID& anElemObj2VTKID = theFamily->myElemObj2VTKID;

    const VISU::TGeom2SubMesh& aGeom2SubMesh = theMeshOnEntity->myGeom2SubMesh;
    VISU::TGeom2SubMesh::const_iterator anIter = aGeom2SubMesh.begin();
    for(vtkIdType aCellId = 0; anIter != aGeom2SubMesh.end(); anIter++){
      VISU::EGeometry aEGeom = anIter->first;
      vtkIdType aVGeom = VISUGeom2VTK(aEGeom);

      const VISU::TSubMeshImpl& aSubMesh = anIter->second;
      const VISU::TCell2Connect& anArray = aSubMesh.myCell2Connect;

      const VISU::TGeom2SubMeshID& aGeom2SubMeshID = theFamily->myGeom2SubMeshID;
      if(aGeom2SubMeshID.empty()) 
        EXCEPTION(std::runtime_error,"GetCells >> There is no elements on the family !!!");

      VISU::TGeom2SubMeshID::const_iterator aGeom2SubMeshIDIter = aGeom2SubMeshID.find(aEGeom);
      if(aGeom2SubMeshIDIter == aGeom2SubMeshID.end()) 
        continue;

      const VISU::TSubMeshID& aSubMeshID = aGeom2SubMeshIDIter->second;

      INITMSG(MYDEBUG,
              " - aEGeom = "<<aEGeom<<
              "; aVGeom = "<<aVGeom<<
              "; aSubMeshID.size() = "<<aSubMeshID.size()<<
              endl);

      VISU::TSubMeshID::const_iterator aSubMeshIDIter = aSubMeshID.begin();
      for(; aSubMeshIDIter != aSubMeshID.end(); aSubMeshIDIter++, aCellId++){
        vtkIdType anID = *aSubMeshIDIter;
        PrintCells( aConnectivity, anArray[ anID ] );
        aCellTypesArray->SetValue(aCellId, (unsigned char)aVGeom);
        vtkIdType anObjID = aSubMesh.GetElemObjID(anID);
        anElemObj2VTKID[anObjID] = aCellId;
        aMeshID[aCellId] = anObjID;
        *aDataArrayPtr++ = anObjID;
        *aDataArrayPtr++ = anEntity;
      }
    }

    theSource->GetCellData()->AddArray(aDataArray);
    aDataArray->Delete();

    vtkIdType *pts = 0, npts = 0;
    VTKViewer_CellLocationsArray* aCellLocationsArray = VTKViewer_CellLocationsArray::New();
    aCellLocationsArray->SetNumberOfComponents(1);
    aCellLocationsArray->SetNumberOfTuples(aNbCells);
    aConnectivity->InitTraversal();
    for(int i=0; aConnectivity->GetNextCell(npts,pts); i++)
      aCellLocationsArray->SetValue(i,aConnectivity->GetTraversalLocation(npts));
    theSource->SetCells(aCellTypesArray,aCellLocationsArray,aConnectivity);

    if(MYVTKDEBUG) aConnectivity->DebugOn();

    aCellLocationsArray->Delete();
    aCellTypesArray->Delete();
    aConnectivity->Delete();
  }
  
  
  //---------------------------------------------------------------
  void
  GetCells(const VISU::PUnstructuredGrid& theSource,
           const VISU::PSubProfileImpl& theSubProfile,
           const VISU::PProfileImpl& theProfile,
           const VISU::PMeshOnEntityImpl& theMeshOnEntity)
  {
    vtkIdType aNbCells = theSubProfile->myNbCells;
    vtkIdType aCellsSize = theSubProfile->myCellsSize;
    VISU::EGeometry aEGeom = theSubProfile->myGeom;
    vtkIdType aNbNodes = VISUGeom2NbNodes(aEGeom);
    vtkIdType aVGeom = VISUGeom2VTK(aEGeom);

    INITMSG(MYDEBUG,"GetCells - aVGeom = "<<aVGeom<<endl);

    const VISU::TSubMeshID& aSubMeshID = theSubProfile->mySubMeshID;

    const VISU::TGeom2SubMesh& aGeom2SubMesh = theMeshOnEntity->myGeom2SubMesh;
    VISU::TGeom2SubMesh::const_iterator anIter = aGeom2SubMesh.find(aEGeom);
    if(anIter == aGeom2SubMesh.end())
      EXCEPTION(std::runtime_error,"GetCells >> There is no elements for the GEOM("<<aEGeom<<")");
    
    const VISU::TSubMeshImpl& aSubMesh = anIter->second;
    const VISU::TCell2Connect& aCell2Connect = aSubMesh.myCell2Connect;
    
    vtkCellArray* aConnectivity = vtkCellArray::New();
    aConnectivity->Allocate(aCellsSize,0);
    vtkUnsignedCharArray* aCellTypesArray = vtkUnsignedCharArray::New();
    aCellTypesArray->SetNumberOfComponents(1);
    aCellTypesArray->SetNumberOfTuples(aNbCells);
    
    if(theSubProfile->myStatus == VISU::eAddAll){
      VISU::TCell2Connect::const_iterator anIter = aCell2Connect.begin();
      for(vtkIdType anId = 0, aConnId = 0; anIter != aCell2Connect.end(); anIter++){
        const VISU::TConnect& anArray = aCell2Connect[anId];
        PrintCells( aConnectivity, anArray );
        aCellTypesArray->SetValue(anId,(unsigned char)aVGeom);
        aConnId += aNbNodes;
        anId++;
      }
    }else{
      VISU::TSubMeshID::const_iterator anIter = aSubMeshID.begin();
      for(vtkIdType anId = 0, aConnId = 0; anIter != aSubMeshID.end(); anIter++){
        vtkIdType aSubId = *anIter;
        const VISU::TConnect& anArray = aCell2Connect[aSubId];
        PrintCells( aConnectivity, anArray );
        aCellTypesArray->SetValue(anId,(unsigned char)aVGeom);
        aConnId += aNbNodes;
        anId++;
      }
    }
    
    vtkIdType *pts = 0, npts = 0;
    VTKViewer_CellLocationsArray* aCellLocationsArray = VTKViewer_CellLocationsArray::New();
    
    aCellLocationsArray->SetNumberOfComponents(1);
    aCellLocationsArray->SetNumberOfTuples(aNbCells);
    aConnectivity->InitTraversal();
    for(int i=0; aConnectivity->GetNextCell(npts,pts); i++)
      aCellLocationsArray->SetValue(i,aConnectivity->GetTraversalLocation(npts));
    theSource->SetCells(aCellTypesArray,aCellLocationsArray,aConnectivity);
    
    {
      int aNbTuples = aNbCells;
      int anEntity = int(theMeshOnEntity->myEntity);
      vtkIntArray *aDataArray = vtkIntArray::New();
      aDataArray->SetName("VISU_CELLS_MAPPER");
      aDataArray->SetNumberOfComponents(2);
      aDataArray->SetNumberOfTuples(aNbTuples);
      int *aDataArrayPtr = aDataArray->GetPointer(0);
      for(int aTupleId = 0; aTupleId < aNbTuples; aTupleId++){
        int anObjID = theSubProfile->GetElemObjID(aTupleId);
        *aDataArrayPtr++ = anObjID;
        *aDataArrayPtr++ = anEntity;
      }
      theSource->GetCellData()->AddArray(aDataArray);
      aDataArray->Delete();
    }
    
    aCellLocationsArray->Delete();
    aCellTypesArray->Delete();
    aConnectivity->Delete();
  }
  
  
  //---------------------------------------------------------------
  void
  GetMeshOnSubProfile(const VISU::PMeshImpl& theMesh,
                      const VISU::PMeshOnEntityImpl& theMeshOnEntity,
                      const VISU::PProfileImpl& theProfile,
                      const VISU::PSubProfileImpl& theSubProfile)
  {
    INITMSG(MYDEBUG,"GetMeshOnSubProfile - aEGeom = "<<theSubProfile->myGeom<<endl);

    //rnv to fix the: 21040: [CEA 428] Bug of visualization of node field on profile
    //throw exception in case if the profile on node.
    //This exception catch in tne VISU_Convertor_impl::GetTimeStampOnMesh(...) function.
    if( theMeshOnEntity->myEntity == VISU::CELL_ENTITY && 
	theSubProfile->myGeom == VISU::ePOINT1 &&
	!theSubProfile->isDefault() ) 
      EXCEPTION(std::runtime_error,"theMeshOnEntity->myEntity == VISU::CELL_ENTITY && theSubProfile->myGeom == VISU::ePOINT1 && theSubProfile->isDefault()");
    
    const VISU::PUnstructuredGrid& aSource = theSubProfile->GetSource();
    if(theSubProfile->myIsVTKDone)
      return;
    
    aSource->ShallowCopy(theMesh->GetPointSet());
    INITMSGA(MYDEBUG,0,"GetNumberOfPoints - "<<aSource->GetNumberOfPoints()<<endl);
    GetCells(aSource,theSubProfile,theProfile,theMeshOnEntity);
    BEGMSG(MYDEBUG,"GetNumberOfCells - "<<aSource->GetNumberOfCells()<<endl);
    
    theSubProfile->myIsVTKDone = true;
  }
  
  
  //---------------------------------------------------------------
  bool
  GetMeshOnProfile(const VISU::PMeshImpl& theMesh,
                   const VISU::PMeshOnEntityImpl& theMeshOnEntity,
                   const VISU::PProfileImpl& theProfile)
  {
    //rnv fix for bug IPAL18514 4x (CRASH after trying to build of presentation):
    // throw exection in case if pointer on profile =0
    if(!theProfile.get())
      EXCEPTION(std::runtime_error,"GetMeshOnProfile: theProfile.get() == NULL");

    // rnv fix for issue 19999:
    // Throw exception in case if mesh on entity from profile is not equal
    // input mesh on entity. This exception catch in tne VISU_Convertor_impl::GetTimeStampOnMesh
    // function.
    if(theProfile->myMeshOnEntity && theProfile->myMeshOnEntity != theMeshOnEntity.get())
      EXCEPTION(std::runtime_error,"GetMeshOnProfile >> theProfile->myMeshOnEntity != theMeshOnEntity.get()");

    if(theProfile->myIsVTKDone)
      return true;
   
    //    if(theProfile->myMeshOnEntity && theProfile->myMeshOnEntity != theMeshOnEntity.get())
    //      return false;
      
    VISU::TTimerLog aTimerLog(MYDEBUG,"GetMeshOnProfile");
    INITMSG(MYDEBUG,"GetMeshOnProfile - anEntity = "<<theMeshOnEntity->myEntity<<std::endl);

    const VISU::PAppendFilter& anAppendFilter = theProfile->GetFilter();
    anAppendFilter->SetSharedPointSet(theMesh->GetPointSet());

    if(theProfile->myIsAll){
      vtkUnstructuredGrid* aDataSet = theMeshOnEntity->GetUnstructuredGridOutput();
      anAppendFilter->AddInput(aDataSet);
    }else{
      const VISU::TGeom2SubProfile& aGeom2SubProfile = theProfile->myGeom2SubProfile;

      VISU::TID2ID& anElemObj2VTKID = theProfile->myElemObj2VTKID;

      VISU::TSubProfileArr& aSubProfileArr = theProfile->mySubProfileArr;
      aSubProfileArr.resize(aGeom2SubProfile.size());

      VISU::TGeom2SubProfile::const_iterator anIter = aGeom2SubProfile.begin();
      for(vtkIdType anInputID = 0, aCellID = 0; anIter != aGeom2SubProfile.end(); anIter++){
        VISU::PSubProfileImpl aSubProfile = anIter->second;
        if(aSubProfile->myStatus == VISU::eRemoveAll)
          continue;
        
        GetMeshOnSubProfile(theMesh,
                            theMeshOnEntity,
                            theProfile,
                            aSubProfile);
        
        const VISU::PUnstructuredGrid& aSource = aSubProfile->GetSource();
        anAppendFilter->AddInput(aSource.GetPointer());

        vtkIdType aNbCells = aSource->GetNumberOfCells();
        for(vtkIdType aCell = 0; aCell < aNbCells; aCell++, aCellID++){
          vtkIdType anObjID = aSubProfile->GetElemObjID(aCell);
          anElemObj2VTKID[anObjID] = aCellID;
        }

        aSubProfileArr[anInputID++] = aSubProfile;
      }
    }
    anAppendFilter->Update(); // Fix on VTK
    theProfile->myMeshOnEntity = theMeshOnEntity.get();
    theProfile->myNamedPointCoords = theMesh->myNamedPointCoords;
    
    theProfile->myIsVTKDone = true;
    return true;
  }
  
  
  //---------------------------------------------------------------
  void
  GetGaussSubMeshSource(const VISU::PPolyData& theSource,
                        const VISU::PGaussSubMeshImpl& theGaussSubMesh,
                        const VISU::PMeshOnEntityImpl& theMeshOnEntity)
  {
    vtkCellArray* aConnectivity = vtkCellArray::New();
    vtkIdType aCellsSize = theGaussSubMesh->myCellsSize;
    aConnectivity->Allocate(aCellsSize, 0);
    
    vtkIdList *anIdList = vtkIdList::New();
    anIdList->SetNumberOfIds(1);

    const VISU::TPointCoords& aCoords = theGaussSubMesh->myPointCoords;
    vtkIdType aNbPoints = aCoords.GetNbPoints();
    for(vtkIdType aPointId = 0; aPointId < aNbPoints; aPointId++){
      anIdList->SetId(0, aPointId);
      aConnectivity->InsertNextCell(anIdList);
    }
    anIdList->Delete();
    
    const VISU::PPolyData& aSource = theGaussSubMesh->GetSource();
    aSource->ShallowCopy(aCoords.GetPointSet());
    aSource->SetVerts(aConnectivity);
    
    aConnectivity->Delete();

    {
      vtkIdType aNbTuples = aNbPoints;
      vtkIntArray *aDataArray = vtkIntArray::New();
      aDataArray->SetName("VISU_POINTS_MAPPER");
      aDataArray->SetNumberOfComponents(2);
      aDataArray->SetNumberOfTuples(aNbTuples);
      int *aDataArrayPtr = aDataArray->GetPointer(0);
      for(vtkIdType aTupleId = 0; aTupleId < aNbTuples; aTupleId++){
        vtkIdType aGlobalID = theGaussSubMesh->GetGlobalID(aTupleId);
        *aDataArrayPtr++ = aGlobalID;
        *aDataArrayPtr++ = 0;
      }
      aSource->GetPointData()->AddArray(aDataArray);
      aDataArray->Delete();
    }

    {
      vtkIdType aNbTuples = aNbPoints;
      vtkIntArray *aDataArray = vtkIntArray::New();
      aDataArray->SetName("VISU_CELLS_MAPPER");
      aDataArray->SetNumberOfComponents(2);
      aDataArray->SetNumberOfTuples(aNbTuples);
      int *aDataArrayPtr = aDataArray->GetPointer(0);
      for(vtkIdType aTupleId = 0; aTupleId < aNbTuples; aTupleId++){
        VISU::TGaussPointID aGaussPointID = theGaussSubMesh->GetObjID(aTupleId);
        *aDataArrayPtr++ = aGaussPointID.first;
        *aDataArrayPtr++ = aGaussPointID.second;
      }
      aSource->GetCellData()->AddArray(aDataArray);
      aDataArray->Delete();
    }
  }
  
  
  //---------------------------------------------------------------
  void
  GetGaussSubMesh(const VISU::PMeshImpl& theMesh,
                  const VISU::PMeshOnEntityImpl& theMeshOnEntity,
                  const VISU::PGaussMeshImpl& theGaussMesh,
                  const VISU::PGaussSubMeshImpl& theGaussSubMesh)
  {
    VISU::PGaussImpl aGauss = theGaussSubMesh->myGauss;
    
    if(!theGaussSubMesh->myIsDone)
      return;
    
    if(theGaussSubMesh->myIsVTKDone)
      return;
    
    VISU::TTimerLog aTimerLog(MYDEBUG,"GetGaussSubMesh");
    INITMSG(MYDEBUG,"GetGaussSubMesh - aVGeom = "<<aGauss->myGeom<<endl);

    const VISU::PPolyData& aSource = theGaussSubMesh->GetSource();
    GetGaussSubMeshSource(aSource, theGaussSubMesh, theMeshOnEntity);

    INITMSGA(MYDEBUG,0,"GetNumberOfPoints - "<<aSource->GetNumberOfPoints()<<endl);
    BEGMSG(MYDEBUG,"GetNumberOfCells - "<<aSource->GetNumberOfCells()<<endl);
    
    theGaussSubMesh->myIsVTKDone = true;
  }
  

  //---------------------------------------------------------------
  void
  BuildGaussMesh(const VISU::PMeshImpl& theMesh,
                 const VISU::PMeshOnEntityImpl& theMeshOnEntity,
                 const VISU::PGaussMeshImpl& theGaussMesh)
  {
    if(theGaussMesh->myIsVTKDone)
      return;

    VISU::TTimerLog aTimerLog(MYDEBUG,"BuildGaussMesh");
    const VISU::PAppendPolyData& anAppendFilter = theGaussMesh->GetFilter();
    const VISU::TGeom2GaussSubMesh& aGeom2GaussSubMesh = theGaussMesh->myGeom2GaussSubMesh;
    VISU::TGeom2GaussSubMesh::const_iterator anIter = aGeom2GaussSubMesh.begin();
    for(vtkIdType aStartID = 0; anIter != aGeom2GaussSubMesh.end(); anIter++){
      VISU::PGaussSubMeshImpl aGaussSubMesh = anIter->second;
      if(aGaussSubMesh->myStatus == VISU::eRemoveAll)
        continue;

      aGaussSubMesh->myStartID = aStartID;

      GetGaussSubMesh(theMesh,
                      theMeshOnEntity,
                      theGaussMesh,
                      aGaussSubMesh);
      
      const VISU::PPolyData& aSource = aGaussSubMesh->GetSource();
      aStartID += aSource->GetNumberOfCells();

      anAppendFilter->AddInput(aSource.GetPointer());
    }
    anAppendFilter->Update(); // Fix on VTK

    theMeshOnEntity->GetOutput()->Update();

    vtkDataSet* aSource = anAppendFilter->GetOutput();
    INITMSGA(MYDEBUG,0,"aNbPoints - "<<aSource->GetNumberOfPoints()<<endl);
    BEGMSG(MYDEBUG,"aNbCells - "<<aSource->GetNumberOfCells()<<endl);
    
    theGaussMesh->myIsVTKDone = true;
  }


  //---------------------------------------------------------------
  void
  PrintMemorySize(vtkUnstructuredGrid* theDataSet)
  {
    theDataSet->Update();
    BEGMSG(1,"GetPoints() = "<<vtkFloatingPointType(theDataSet->GetPoints()->GetActualMemorySize()*1000)<<endl);
    BEGMSG(1,"GetCells() = "<<vtkFloatingPointType(theDataSet->GetCells()->GetActualMemorySize()*1000)<<endl);
    BEGMSG(1,"GetCellTypesArray() = "<<vtkFloatingPointType(theDataSet->GetCellTypesArray()->GetActualMemorySize()*1000)<<endl);
    BEGMSG(1,"GetCellLocationsArray() = "<<vtkFloatingPointType(theDataSet->GetCellLocationsArray()->GetActualMemorySize()*1000)<<endl);
    theDataSet->BuildLinks();
    BEGMSG(1,"GetCellLinks() = "<<vtkFloatingPointType(theDataSet->GetCellLinks()->GetActualMemorySize()*1000)<<endl);
    BEGMSG(1,"GetPointData() = "<<vtkFloatingPointType(theDataSet->GetPointData()->GetActualMemorySize()*1000)<<endl);
    BEGMSG(1,"GetCellData() = "<<vtkFloatingPointType(theDataSet->GetCellData()->GetActualMemorySize()*1000)<<endl);
    BEGMSG(1,"GetActualMemorySize() = "<<vtkFloatingPointType(theDataSet->GetActualMemorySize()*1000)<<endl);
  }
}


//---------------------------------------------------------------
VISU_Convertor_impl
::VISU_Convertor_impl()
{}


//---------------------------------------------------------------
VISU_Convertor_impl
::~VISU_Convertor_impl() 
{}


//---------------------------------------------------------------
VISU_Convertor* 
VISU_Convertor_impl
::Build() 
{ 
  if(!myIsDone){ 
    myIsDone = true;  
    BuildEntities();
    BuildFields();
    BuildMinMax();
    BuildGroups();
  }
  return this;
}

VISU_Convertor* 
VISU_Convertor_impl
::BuildEntities() 
{ 
  return this;
}

VISU_Convertor* 
VISU_Convertor_impl
::BuildFields() 
{ 
  BuildEntities();
  return this;
}

VISU_Convertor* 
VISU_Convertor_impl
::BuildMinMax() 
{ 
  BuildFields();
  return this;
}

VISU_Convertor* 
VISU_Convertor_impl
::BuildGroups() 
{ 
  return this;
}


//---------------------------------------------------------------
VISU::PNamedIDMapper 
VISU_Convertor_impl
::GetMeshOnEntity(const std::string& theMeshName, 
                  const VISU::TEntity& theEntity)
{
  INITMSG(MYDEBUG,"GetMeshOnEntity"<<
          "; theMeshName = '"<<theMeshName<<"'"<<
          "; theEntity = "<<theEntity<<
          endl);

  //Cheching possibility do the query
  TFindMeshOnEntity aFindMeshOnEntity = 
    FindMeshOnEntity(theMeshName,theEntity);
  
  VISU::PMeshImpl aMesh = boost::get<0>(aFindMeshOnEntity);;
  VISU::PMeshOnEntityImpl aMeshOnEntity = boost::get<1>(aFindMeshOnEntity);
  
  //Main part of code
#ifndef _DEXCEPT_
  try{
#endif
    if(!aMeshOnEntity->myIsVTKDone){
      VISU::TTimerLog aTimerLog(MYDEBUG,"VISU_Convertor_impl::GetMeshOnEntity");
      const VISU::PAppendFilter& anAppendFilter = aMeshOnEntity->GetFilter();
      if(MYVTKDEBUG) anAppendFilter->DebugOn();

      LoadMeshOnEntity(aMesh,aMeshOnEntity);
      anAppendFilter->SetSharedPointSet(aMesh->GetPointSet());
      
      const VISU::TGeom2SubMesh& aGeom2SubMesh = aMeshOnEntity->myGeom2SubMesh;
      VISU::TGeom2SubMesh::const_iterator anIter = aGeom2SubMesh.begin();

      VISU::TID2ID& anElemObj2VTKID = aMeshOnEntity->myElemObj2VTKID;
      VISU::TSubMeshArr& aSubMeshArr = aMeshOnEntity->mySubMeshArr;
      aSubMeshArr.resize(aGeom2SubMesh.size());

      for(vtkIdType anID = 0, aCellID = 0; anIter != aGeom2SubMesh.end(); anIter++, anID++){
        VISU::EGeometry aEGeom = anIter->first;
        vtkIdType aVGeom = VISUGeom2VTK(aEGeom);
        VISU::PSubMeshImpl aSubMesh = anIter->second;

        aSubMesh->CopyStructure( aMesh );

        aSubMesh->myStartID = aCellID;

        const VISU::PUnstructuredGrid& aSource = aSubMesh->GetSource();
        GetCellsOnSubMesh(aSource, aMeshOnEntity, aSubMesh, aVGeom);
        anAppendFilter->AddInput(aSource.GetPointer());
        
        vtkIdType aNbCells = aSource->GetNumberOfCells();
        for(vtkIdType aCell = 0; aCell < aNbCells; aCell++, aCellID++){
          vtkIdType anObjID = aSubMesh->GetElemObjID(aCell);
          anElemObj2VTKID[anObjID] = aCellID;
        }

        aSubMeshArr[anID] = aSubMesh;
      }
      
      aMeshOnEntity->CopyStructure( aMesh );

      aMeshOnEntity->myIsVTKDone = true;

      if(MYDEBUGWITHFILES){
        std::string aMeshName = (const char*)QString(theMeshName.c_str()).simplified().toLatin1();
        std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-";
        aFileName += aMeshName + dtos("-%d-",int(theEntity)) + "-MeshOnEntity.vtk";
        VISU::WriteToFile(anAppendFilter->GetOutput(),aFileName);
      }

      if(MYVTKDEBUG){
        GetMeshOnEntitySize(theMeshName,theEntity);
        PrintMemorySize(anAppendFilter->GetOutput());
      }
    }

#ifndef _DEXCEPT_
  }catch(...){
    throw;
  }
#endif

  return aMeshOnEntity;
}


//---------------------------------------------------------------
VISU::PUnstructuredGridIDMapper 
VISU_Convertor_impl
::GetFamilyOnEntity(const std::string& theMeshName, 
                    const VISU::TEntity& theEntity,
                    const std::string& theFamilyName)
{
  INITMSG(MYDEBUG,"GetFamilyOnEntity"<<
          "; theMeshName = '"<<theMeshName<<"'"<<
          "; theEntity = "<<theEntity<<
          "; theFamilyName = '"<<theFamilyName<<"'"<<
          endl);

  //Cheching possibility do the query
  TFindFamilyOnEntity aFindFamilyOnEntity = 
    FindFamilyOnEntity(theMeshName,theEntity,theFamilyName);

  VISU::PMeshImpl aMesh = boost::get<0>(aFindFamilyOnEntity);;
  VISU::PMeshOnEntityImpl aMeshOnEntity = boost::get<1>(aFindFamilyOnEntity);
  VISU::PFamilyImpl aFamily = boost::get<2>(aFindFamilyOnEntity);

  //Main part of code
#ifndef _DEXCEPT_
  try{
#endif
    if ( !aFamily->myIsVTKDone ) {
      GetMeshOnEntity( theMeshName, theEntity );

      LoadFamilyOnEntity( aMesh, aMeshOnEntity, aFamily );

      aFamily->CopyStructure( aMesh );

      const VISU::PUnstructuredGrid& aSource = aFamily->GetSource();
      GetCellsOnFamily( aSource, aMeshOnEntity, aFamily );

      aFamily->myIsVTKDone = true;

      if(MYDEBUGWITHFILES){
        std::string aMeshName = (const char*)QString(theMeshName.c_str()).simplified().toLatin1();
        std::string aFamilyName = (const char*)QString(theFamilyName.c_str()).simplified().toLatin1();
        std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-";
        aFileName += aMeshName + dtos("-%d-",int(theEntity)) + aFamilyName + "-FamilyOnEntity.vtk";
        VISU::WriteToFile(aSource.GetPointer(),aFileName);
      }

      if(MYVTKDEBUG){
        GetFamilyOnEntitySize(theMeshName,theEntity,theFamilyName);
        PrintMemorySize(aSource.GetPointer());
      }
    }

#ifndef _DEXCEPT_
  }catch(...){
    throw;
  }
#endif

  return aFamily;
}


//---------------------------------------------------------------
VISU::PUnstructuredGridIDMapper 
VISU_Convertor_impl
::GetMeshOnGroup(const std::string& theMeshName, 
                 const std::string& theGroupName)
{
  INITMSG(MYDEBUG,"GetMeshOnGroup\n");
  INITMSGA(MYDEBUG,0,
           "- theMeshName = '"<<theMeshName<<
           "'; theGroupName = '"<<theGroupName<<"'"<<
           endl);

  //Cheching possibility do the query
  TFindMeshOnGroup aFindMeshOnGroup = FindMeshOnGroup(theMeshName,theGroupName);
  VISU::PMeshImpl aMesh = boost::get<0>(aFindMeshOnGroup);
  VISU::PGroupImpl aGroup = boost::get<1>(aFindMeshOnGroup);

  //Main part of code
#ifndef _DEXCEPT_
  try{
#endif
    if(!aGroup->myIsVTKDone){
      const VISU::PAppendFilter& anAppendFilter = aGroup->GetFilter();
      const VISU::TFamilySet& aFamilySet = aGroup->myFamilySet;

      LoadMeshOnGroup(aMesh,aFamilySet);
      anAppendFilter->SetSharedPointSet(aMesh->GetPointSet());

      VISU::TFamilySet::const_iterator anIter = aFamilySet.begin();

      VISU::TID2ID& anElemObj2VTKID = aGroup->myElemObj2VTKID;
      VISU::TFamilyArr& aFamilyArr = aGroup->myFamilyArr;
      aFamilyArr.resize(aFamilySet.size());

      for(vtkIdType anID = 0; anIter != aFamilySet.end(); anIter++, anID++){
        VISU::PFamilyImpl aFamily = (*anIter).second;
        const std::string& aFamilyName = aFamily->myName;
        const VISU::TEntity& anEntity = aFamily->myEntity;

        VISU::PIDMapper anIDMapper = GetFamilyOnEntity(theMeshName,anEntity,aFamilyName);
        vtkDataSet* anOutput = anIDMapper->GetOutput();
        anAppendFilter->AddInput(anOutput);

        vtkIdType aStartID = anElemObj2VTKID.size();
        vtkIdType aNbCells = anOutput->GetNumberOfCells();
        for(vtkIdType aCellID = 0; aCellID < aNbCells; aCellID++){
          anElemObj2VTKID[aFamily->GetElemObjID(aCellID)] = aStartID + aCellID;
        }
        aFamilyArr[anID] = aFamily;
      }

      aGroup->CopyStructure( aMesh );

      aGroup->myIsVTKDone = true;

      if(MYDEBUGWITHFILES){
        std::string aMeshName = (const char*)QString(theMeshName.c_str()).simplified().toLatin1();
        std::string aGroupName = (const char*)QString(theGroupName.c_str()).simplified().toLatin1();
        std::string aFileName = std::string(getenv("HOME"))+"/"+getenv("USER")+"-";
        aFileName += aMeshName + "-" + aGroupName + "-MeshOnGroup.vtk";
        VISU::WriteToFile(anAppendFilter->GetOutput(),aFileName);
      }
    }
#ifndef _DEXCEPT_
  }catch(...){
    throw;
  }
#endif

  return aGroup;
}


//---------------------------------------------------------------
vtkUnstructuredGrid*
VISU_Convertor_impl
::GetTimeStampOnProfile( const VISU::PMeshImpl& theMesh,
                         const VISU::PMeshOnEntityImpl& theMeshOnEntity,
                         const VISU::PFieldImpl& theField,
                         const VISU::PValForTimeImpl& theValForTime,
                         const VISU::PUnstructuredGridIDMapperImpl& theUnstructuredGridIDMapper,
                         const VISU::PProfileImpl& theProfile,
                         const VISU::TEntity& theEntity )
{
  LoadMeshOnEntity( theMesh, theMeshOnEntity );
  GetMeshOnEntity( theMeshOnEntity->myMeshName, theMeshOnEntity->myEntity );
  GetMeshOnProfile( theMesh, theMeshOnEntity, theProfile );
  
  theUnstructuredGridIDMapper->myIDMapper = theProfile;

  if ( theMeshOnEntity->myEntity == VISU::NODE_ENTITY ) {
    // add geometry elements to output,
    // if timestamp on NODE_ENTITY and
    // on profiles with status eAddPart
    const VISU::TGeom2SubProfile& aGeom2SubProfile = theProfile->myGeom2SubProfile;
    VISU::TGeom2SubProfile::const_iterator aSubProfileIter = aGeom2SubProfile.begin();
    for ( ; aSubProfileIter != aGeom2SubProfile.end(); aSubProfileIter++ ) {
      const VISU::EGeometry& aGeom = aSubProfileIter->first;
      const VISU::PSubProfileImpl& aSubProfile = aSubProfileIter->second;
      if ( aSubProfile->myStatus == VISU::eAddPart && aGeom == VISU::ePOINT1 ) {
        const VISU::TMeshOnEntityMap& aMeshOnEntityMap = theMesh->myMeshOnEntityMap;
        VISU::TMeshOnEntityMap::const_reverse_iterator aMeshOnEntityIter = aMeshOnEntityMap.rbegin();
        for( ; aMeshOnEntityIter != aMeshOnEntityMap.rend(); aMeshOnEntityIter++ ) {
          VISU::TEntity anEntity = aMeshOnEntityIter->first;
          if ( anEntity == VISU::NODE_ENTITY )
            continue;
          VISU::PNamedIDMapper aNamedIDMapper = GetMeshOnEntity( theMesh->myName, anEntity );
          if( aNamedIDMapper ) {
            theUnstructuredGridIDMapper->SetReferencedMesh( aNamedIDMapper );
            VISU::PUnstructuredGrid aSource = theUnstructuredGridIDMapper->GetSource();
            VISU::GetTimeStampOnProfile( aSource, theField, theValForTime, theEntity );
            
            return theUnstructuredGridIDMapper->GetUnstructuredGridOutput();
          }
        }
      }
    }
  }

  VISU::PUnstructuredGrid aSource = theUnstructuredGridIDMapper->GetSource();
  VISU::GetTimeStampOnProfile( aSource, theField, theValForTime, theEntity );

  return theUnstructuredGridIDMapper->GetUnstructuredGridOutput();
}


//---------------------------------------------------------------
VISU::PUnstructuredGridIDMapper 
VISU_Convertor_impl
::GetTimeStampOnMesh( const std::string& theMeshName, 
                      const VISU::TEntity& theEntity,
                      const std::string& theFieldName,
                      int theStampsNum )
{
  INITMSG(MYDEBUG,"GetTimeStampOnMesh"<<
          "; theMeshName = '"<<theMeshName<<"'"<<
          "; theEntity = "<<theEntity<<
          "; theFieldName = '"<<theFieldName<<"'"<<
          "; theStampsNum = "<<theStampsNum<<
          endl);

  //Cheching possibility do the query
  TFindTimeStamp aFindTimeStamp = FindTimeStamp(theMeshName,
                                                theEntity,
                                                theFieldName,
                                                theStampsNum);

  VISU::PMeshImpl aMesh = boost::get<0>(aFindTimeStamp);
  VISU::PMeshOnEntityImpl aMeshOnEntity = boost::get<1>(aFindTimeStamp);
  VISU::PMeshOnEntityImpl aVTKMeshOnEntity = boost::get<2>(aFindTimeStamp);
  VISU::PValForTimeImpl aValForTime = boost::get<4>(aFindTimeStamp);
  VISU::PFieldImpl aField = boost::get<3>(aFindTimeStamp);

  //Main part of code
  VISU::PUnstructuredGridIDMapperImpl anUnstructuredGridIDMapper = aValForTime->myUnstructuredGridIDMapper;
#ifndef _DEXCEPT_
  try{
#endif
    if(!anUnstructuredGridIDMapper->myIsVTKDone){
      VISU::TTimerLog aTimerLog(MYDEBUG,"VISU_Convertor_impl::GetTimeStampOnMesh");
      LoadValForTimeOnMesh(aMesh, aMeshOnEntity, aField, aValForTime);

      vtkUnstructuredGrid* anOutput = NULL;
      try{
        anOutput = GetTimeStampOnProfile(aMesh,
                                         aVTKMeshOnEntity,
                                         aField,
                                         aValForTime,
                                         anUnstructuredGridIDMapper,
                                         aValForTime->myProfile,
                                         aMeshOnEntity->myEntity);
      }catch(std::exception& exc){
        MSG(MYDEBUG,"Follow exception was occured :\n"<<exc.what());
        anOutput = GetTimeStampOnProfile(aMesh,
                                         aMeshOnEntity,
                                         aField,
                                         aValForTime,
                                         anUnstructuredGridIDMapper,
                                         aValForTime->myProfile,
                                         aVTKMeshOnEntity->myEntity);
      }

      anUnstructuredGridIDMapper->CopyStructure( aMesh );

      anUnstructuredGridIDMapper->myIsVTKDone = true;

      if(MYDEBUGWITHFILES){
        std::string aMeshName = (const char*)QString(theMeshName.c_str()).simplified().toLatin1();
        std::string aFieldName = (const char*)QString(theFieldName.c_str()).simplified().toLatin1();
        std::string aPrefix = std::string(getenv("HOME"))+"/"+getenv("USER")+"-";
        std::string aFileName = aPrefix + aMeshName + dtos("-%d-",int(theEntity)) + 
          aFieldName + dtos("-%d", theStampsNum) + "-TimeStampOnMesh.vtk";
        VISU::WriteToFile(anOutput,aFileName);
      }
      if(MYVTKDEBUG){
        GetTimeStampSize(theMeshName, theEntity, theFieldName, theStampsNum);
        anOutput->Update();
        if(theEntity == VISU::NODE_ENTITY)
          BEGMSG(MYVTKDEBUG,"GetPointData() = "<<vtkFloatingPointType(anOutput->GetPointData()->GetActualMemorySize()*1000)<<endl);
        else
          BEGMSG(MYVTKDEBUG,"GetCellData() = "<<vtkFloatingPointType(anOutput->GetCellData()->GetActualMemorySize()*1000)<<endl);
        BEGMSG(MYVTKDEBUG,"GetActualMemorySize() = "<<vtkFloatingPointType(anOutput->GetActualMemorySize()*1000)<<endl);
      }
      }
#ifndef _DEXCEPT_
    }catch(std::exception& exc){
    throw;
  }catch(...){
    throw;
  }
#endif

  return anUnstructuredGridIDMapper;
}


//---------------------------------------------------------------
VISU::PGaussPtsIDMapper 
VISU_Convertor_impl
::GetTimeStampOnGaussPts(const std::string& theMeshName, 
                         const VISU::TEntity& theEntity,
                         const std::string& theFieldName,
                         int theStampsNum)
{
  INITMSG(MYDEBUG,"GetTimeStampOnGaussPts"<<
          "; theMeshName = '"<<theMeshName<<"'"<<
          "; theEntity = "<<theEntity<<
          "; theFieldName = '"<<theFieldName<<"'"<<
          "; theStampsNum = "<<theStampsNum<<
          endl);

  if(theEntity == VISU::NODE_ENTITY)
    EXCEPTION(std::runtime_error, "It is impossible to reate Gauss Points on NODE_ENTITY !!!");

  //Cheching possibility do the query
  TFindTimeStamp aFindTimeStamp = FindTimeStamp(theMeshName,
                                                theEntity,
                                                theFieldName,
                                                theStampsNum);
  
  VISU::PMeshImpl aMesh = boost::get<0>(aFindTimeStamp);
  VISU::PMeshOnEntityImpl aMeshOnEntity = boost::get<1>(aFindTimeStamp);
  VISU::PMeshOnEntityImpl aVTKMeshOnEntity = aMeshOnEntity;
  VISU::PValForTimeImpl aValForTime = boost::get<4>(aFindTimeStamp);
  VISU::PFieldImpl aField = boost::get<3>(aFindTimeStamp);

  //Main part of code
  VISU::PGaussPtsIDFilter aGaussPtsIDFilter = aValForTime->myGaussPtsIDFilter;
#ifndef _DEXCEPT_
  try{
#endif
    if(!aGaussPtsIDFilter->myIsVTKDone){
      VISU::TTimerLog aTimerLog(MYDEBUG,"VISU_Convertor_impl::GetTimeStampOnGaussPts");
      LoadValForTimeOnGaussPts(aMesh, aMeshOnEntity, aField, aValForTime);
      
      GetMeshOnEntity(aVTKMeshOnEntity->myMeshName, aVTKMeshOnEntity->myEntity);
      
      VISU::PProfileImpl aProfile = aValForTime->myProfile;
      GetMeshOnProfile(aMesh, aVTKMeshOnEntity, aProfile);

      VISU::PGaussMeshImpl aGaussMesh = aValForTime->myGaussMesh;
      if(!aGaussMesh->myIsVTKDone){
        BuildGaussMesh(aMesh, aVTKMeshOnEntity, aGaussMesh);
        aGaussMesh->myParent = aProfile.get();
        aGaussMesh->myIsVTKDone = true;
      }

      aGaussPtsIDFilter->myIDMapper = aGaussMesh;
      aGaussPtsIDFilter->myGaussPtsIDMapper = aGaussMesh;
      VISU::PPolyData aSource = aGaussPtsIDFilter->GetSource();
      VISU::GetTimeStampOnGaussMesh(aSource, aField, aValForTime);
      vtkPolyData* anOutput = aGaussPtsIDFilter->GetPolyDataOutput();

      aGaussPtsIDFilter->myIsVTKDone = true;

      if(MYDEBUGWITHFILES){
        std::string aMeshName = (const char*)QString(theMeshName.c_str()).simplified().toLatin1();
        std::string aFieldName = (const char*)QString(theFieldName.c_str()).simplified().toLatin1();
        std::string aPrefix = std::string(getenv("HOME"))+"/"+getenv("USER")+"-";
        std::string aFileName = aPrefix + aMeshName + dtos("-%d-",int(theEntity)) + 
          aFieldName + dtos("-%d",theStampsNum) + "-TimeStampOnGaussPts.vtk";
        VISU::WriteToFile(anOutput, aFileName);
      }
      if(MYVTKDEBUG){
        GetTimeStampSize(theMeshName, theEntity, theFieldName, theStampsNum);
        anOutput->Update();
        if(theEntity == VISU::NODE_ENTITY)
          BEGMSG(MYVTKDEBUG,"GetPointData() = "<<vtkFloatingPointType(anOutput->GetPointData()->GetActualMemorySize()*1000)<<endl);
        else
          BEGMSG(MYVTKDEBUG,"GetCellData() = "<<vtkFloatingPointType(anOutput->GetCellData()->GetActualMemorySize()*1000)<<endl);
        BEGMSG(MYVTKDEBUG,"GetActualMemorySize() = "<<vtkFloatingPointType(anOutput->GetActualMemorySize()*1000)<<endl);
      }
    }
#ifndef _DEXCEPT_
  }catch(std::exception& exc){
    throw;
  }catch(...){
    throw;
  }
#endif

  return aGaussPtsIDFilter;
}

//---------------------------------------------------------------
VISU::PMeshImpl 
VISU_Convertor_impl
::FindMesh(const std::string& theMeshName)
{
  GetMeshMap();
  VISU::TMeshMap::iterator aMeshMapIter = myMeshMap.find(theMeshName);
  if(aMeshMapIter == myMeshMap.end())
    EXCEPTION(std::runtime_error,"FindMesh >> There is no mesh with the name - '"<<theMeshName<<"'!!!");

  VISU::PMeshImpl aMesh = aMeshMapIter->second;
  return aMesh;
}


//---------------------------------------------------------------
VISU_Convertor_impl::TFindMeshOnEntity
VISU_Convertor_impl
::FindMeshOnEntity(const std::string& theMeshName,
                   const VISU::TEntity& theEntity)
{
  VISU::PMeshImpl aMesh = FindMesh(theMeshName);
  VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  VISU::TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter = aMeshOnEntityMap.find(theEntity);
  if(aMeshOnEntityMapIter == aMeshOnEntityMap.end())
    EXCEPTION(std::runtime_error,"FindMeshOnEntity >> There is no mesh on the entity - "<<theEntity<<"!!!");

  VISU::PMeshOnEntityImpl aMeshOnEntity = aMeshOnEntityMapIter->second;
  
  return TFindMeshOnEntity(aMesh,
                           aMeshOnEntity);
}


//---------------------------------------------------------------
VISU_Convertor_impl::TFindFamilyOnEntity
VISU_Convertor_impl
::FindFamilyOnEntity(const std::string& theMeshName,
                     const VISU::TEntity& theEntity,
                     const std::string& theFamilyName)
{
  if(theFamilyName != ""){
    VISU::PMeshImpl aMesh = FindMesh(theMeshName);
    VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
    VISU::TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter = aMeshOnEntityMap.find(theEntity);
    if(aMeshOnEntityMapIter == aMeshOnEntityMap.end())
      EXCEPTION(std::runtime_error,"FindFamilyOnEntity >> There is no mesh on the entity - "<<theEntity<<"!!!");

    VISU::PMeshOnEntityImpl aMeshOnEntity = aMeshOnEntityMapIter->second;

    VISU::TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
    VISU::TFamilyMap::iterator aFamilyMapIter = aFamilyMap.find(theFamilyName);
    if(aFamilyMapIter != aFamilyMap.end()){
      const VISU::PFamily& aFamily = aFamilyMapIter->second;
      return TFindFamilyOnEntity(aMesh,
                                 aMeshOnEntity,
                                 aFamily);
    }
  }
  return TFindFamilyOnEntity();
}


//---------------------------------------------------------------
size_t
VISU_Convertor_impl
::GetSize() 
{
  size_t aResult = 0;
  const VISU::TMeshMap& aMeshMap = GetMeshMap();
  VISU::TMeshMap::const_iterator aMeshMapIter = aMeshMap.begin();
  for(; aMeshMapIter != aMeshMap.end(); aMeshMapIter++){
    const std::string& aMeshName = aMeshMapIter->first;
    const VISU::PMesh aMesh = aMeshMapIter->second;
    const VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
    VISU::TMeshOnEntityMap::const_iterator aMeshOnEntityMapIter;
    //Import fields
    aMeshOnEntityMapIter = aMeshOnEntityMap.begin();
    for(; aMeshOnEntityMapIter != aMeshOnEntityMap.end(); aMeshOnEntityMapIter++){
      const VISU::TEntity& anEntity = aMeshOnEntityMapIter->first;
      const VISU::PMeshOnEntity aMeshOnEntity = aMeshOnEntityMapIter->second;
      const VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
      VISU::TFieldMap::const_iterator aFieldMapIter = aFieldMap.begin();
      for(; aFieldMapIter != aFieldMap.end(); aFieldMapIter++){
        const std::string& aFieldName = aFieldMapIter->first;
        const VISU::PField aField = aFieldMapIter->second;
        const VISU::TValField& aValField = aField->myValField;
        VISU::TValField::const_iterator aValFieldIter = aValField.begin();
        for(; aValFieldIter != aValField.end(); aValFieldIter++){
          int aTimeStamp = aValFieldIter->first;
          aResult += GetTimeStampSize(aMeshName,anEntity,aFieldName,aTimeStamp);
        }
      }
      //Importing groups
      const VISU::TGroupMap& aGroupMap = aMesh->myGroupMap;
      VISU::TGroupMap::const_iterator aGroupMapIter = aGroupMap.begin();
      for(; aGroupMapIter != aGroupMap.end(); aGroupMapIter++){
        const std::string& aGroupName = aGroupMapIter->first;
        aResult += GetMeshOnGroupSize(aMeshName,aGroupName);
      }
      //Import families
      const VISU::TFamilyMap& aFamilyMap = aMeshOnEntity->myFamilyMap;
      VISU::TFamilyMap::const_iterator aFamilyMapIter = aFamilyMap.begin();
      for(; aFamilyMapIter != aFamilyMap.end(); aFamilyMapIter++){
        const std::string& aFamilyName = aFamilyMapIter->first;
        aResult += GetFamilyOnEntitySize(aMeshName,anEntity,aFamilyName);
      }
      //Import mesh on entity
      aResult += GetMeshOnEntitySize(aMeshName,anEntity);
    }
  }
  MSG(MYDEBUG,"GetSize - aResult = "<<vtkFloatingPointType(aResult));
  return aResult;
}


//---------------------------------------------------------------
size_t
VISU_Convertor_impl
::GetMeshOnEntitySize(const std::string& theMeshName, 
                      const VISU::TEntity& theEntity)
{
  TFindMeshOnEntity aFindMeshOnEntity = 
    FindMeshOnEntity(theMeshName, theEntity);

  VISU::PMeshImpl aMesh = boost::get<0>(aFindMeshOnEntity);
  VISU::PMeshOnEntityImpl aMeshOnEntity = boost::get<1>(aFindMeshOnEntity);

  size_t aPointsSize = 3*aMesh->GetNbPoints()*sizeof(VISU::TCoord);
  size_t aNbCells = aMeshOnEntity->myNbCells;
  size_t aCellsSize = aMeshOnEntity->myCellsSize;

  size_t aConnectivitySize = aCellsSize*sizeof(vtkIdType);
  size_t aTypesSize = aNbCells*sizeof(char);
  size_t aLocationsSize = aNbCells*sizeof(int);
  vtkFloatingPointType aNbCellsPerPoint = aCellsSize / aNbCells - 1;
  size_t aLinksSize = aMesh->GetNbPoints() * 
    (vtkIdType(sizeof(vtkIdType)*aNbCellsPerPoint) + sizeof(vtkCellLinks::Link));
  aLinksSize = 0;
  size_t aResult = aPointsSize + aConnectivitySize + aTypesSize + aLocationsSize + aLinksSize;

  MSG(MYDEBUG,"GetMeshOnEntitySize "<<
      "- aResult = "<<vtkFloatingPointType(aResult)<<
      "; theMeshName = '"<<theMeshName<<"'"<<
      "; theEntity = "<<theEntity);
  if(MYDEBUG){
    INITMSG(MYVTKDEBUG,"- aPointsSize = "<<vtkFloatingPointType(aPointsSize)<<"\n");
    BEGMSG(MYVTKDEBUG,"- aConnectivitySize = "<<vtkFloatingPointType(aConnectivitySize)<<"\n");
    BEGMSG(MYVTKDEBUG,"- aTypesSize = "<<vtkFloatingPointType(aTypesSize)<<"\n");
    BEGMSG(MYVTKDEBUG,"- aLocationsSize = "<<vtkFloatingPointType(aLocationsSize)<<"\n");
    BEGMSG(MYVTKDEBUG,"- aLinksSize = "<<vtkFloatingPointType(aLinksSize)<<"\n");
  }

  aResult = size_t(aResult*ERR_SIZE_CALC);
  return aResult;
}


//---------------------------------------------------------------
size_t
VISU_Convertor_impl
::GetFamilyOnEntitySize(const std::string& theMeshName, 
                        const VISU::TEntity& theEntity,
                        const std::string& theFamilyName)
{
  TFindFamilyOnEntity aFindFamilyOnEntity = 
    FindFamilyOnEntity(theMeshName,theEntity,theFamilyName);
  VISU::PMeshImpl aMesh = boost::get<0>(aFindFamilyOnEntity);
  VISU::PFamilyImpl aFamily = boost::get<2>(aFindFamilyOnEntity);
  VISU::PMeshOnEntityImpl aMeshOnEntity = boost::get<1>(aFindFamilyOnEntity);

  size_t aPointsSize = 3*aMesh->GetNbPoints()*sizeof(VISU::TCoord);
  size_t aNbCells = aFamily->myNbCells;
  size_t aCellsSize = aFamily->myCellsSize;

  size_t aConnectivitySize = aCellsSize*sizeof(vtkIdType);
  size_t aTypesSize = aNbCells*sizeof(char);
  size_t aLocationsSize = aNbCells*sizeof(int);
  vtkFloatingPointType aNbCellsPerPoint = aCellsSize / aNbCells - 1;
  size_t aLinksSize = aMesh->GetNbPoints() * 
    (vtkIdType(sizeof(vtkIdType)*aNbCellsPerPoint) + sizeof(vtkCellLinks::Link));
  aLinksSize = 0;
  size_t aResult = aPointsSize + aConnectivitySize + aTypesSize + aLocationsSize + aLinksSize;

  MSG(MYDEBUG,"GetFamilyOnEntitySize "<<
      "- aResult = "<<vtkFloatingPointType(aResult)<<
      "; theMeshName = '"<<theMeshName<<"'"<<
      "; theEntity = "<<theEntity<<
      "; theFamilyName = '"<<theFamilyName<<"'");
  if(MYDEBUG){
    INITMSG(MYVTKDEBUG,"- aPointsSize = "<<vtkFloatingPointType(aPointsSize)<<"\n");
    BEGMSG(MYVTKDEBUG,"- aConnectivitySize = "<<vtkFloatingPointType(aConnectivitySize)<<"\n");
    BEGMSG(MYVTKDEBUG,"- aTypesSize = "<<vtkFloatingPointType(aTypesSize)<<"\n");
    BEGMSG(MYVTKDEBUG,"- aLocationsSize = "<<vtkFloatingPointType(aLocationsSize)<<"\n");
    BEGMSG(MYVTKDEBUG,"- aLinksSize = "<<vtkFloatingPointType(aLinksSize)<<"\n");
  }

  aResult = size_t(aResult*ERR_SIZE_CALC);
  return aResult;
}


//---------------------------------------------------------------
VISU_Convertor_impl::TFindMeshOnGroup
VISU_Convertor_impl
::FindMeshOnGroup(const std::string& theMeshName, 
                  const std::string& theGroupName)
{
  VISU::PMeshImpl aMesh = FindMesh(theMeshName);
  VISU::TGroupMap& aGroupMap = aMesh->myGroupMap;
  VISU::TGroupMap::iterator aGroupMapIter = aGroupMap.find(theGroupName);
  if(aGroupMapIter == aGroupMap.end())
    EXCEPTION(std::runtime_error,"FindMesh >> There is no the group in the mesh!!! - '"<<theGroupName<<"'");

  VISU::PGroupImpl aGroup = aGroupMapIter->second;
  return TFindMeshOnGroup(aMesh,aGroup);
}


size_t
VISU_Convertor_impl
::GetMeshOnGroupSize(const std::string& theMeshName, 
                     const std::string& theGroupName)
{
  TFindMeshOnGroup aFindMeshOnGroup = FindMeshOnGroup(theMeshName,theGroupName);
  VISU::PMeshImpl aMesh = boost::get<0>(aFindMeshOnGroup);
  VISU::PGroupImpl aGroup = boost::get<1>(aFindMeshOnGroup);

  size_t aPointsSize = 3*aMesh->GetNbPoints()*sizeof(VISU::TCoord);
  VISU::TNbASizeCells aNbASizeCells = aGroup->GetNbASizeCells();
  size_t aNbCells = aNbASizeCells.first;
  size_t aCellsSize = aNbASizeCells.second;
  size_t aConnectivityAndTypesSize = aCellsSize*sizeof(vtkIdType);
  size_t aLocationsSize = aNbCells*sizeof(int);
  vtkFloatingPointType aNbCellsPerPoint = aCellsSize / aNbCells - 1;
  size_t aLinksSize = aMesh->GetNbPoints() * 
    (vtkIdType(sizeof(vtkIdType)*aNbCellsPerPoint) + sizeof(short));
  aLinksSize = 0;
  size_t aResult = aPointsSize + aConnectivityAndTypesSize + aLocationsSize + aLinksSize;
  if(MYDEBUG){
    MSG(MYVTKDEBUG,"GetMeshOnGroupSize - aPointsSize = "<<vtkFloatingPointType(aPointsSize));
    MSG(MYVTKDEBUG,"GetMeshOnGroupSize - aConnectivityAndTypesSize = "<<vtkFloatingPointType(aConnectivityAndTypesSize));
    MSG(MYVTKDEBUG,"GetMeshOnGroupSize - aLocationsSize = "<<vtkFloatingPointType(aLocationsSize));
    MSG(MYVTKDEBUG,"GetMeshOnGroupSize - aLinksSize = "<<vtkFloatingPointType(aLinksSize));
  }
  MSG(MYDEBUG,"GetMeshOnGroupSize - aResult = "<<vtkFloatingPointType(aResult)<<"; theMeshName = '"
      <<theMeshName<<"'; theGroupName = '"<<theGroupName<<"'");

  aResult = size_t(aResult*ERR_SIZE_CALC);
  return aResult;
}


VISU_Convertor_impl::TFindField
VISU_Convertor_impl
::FindField(const std::string& theMeshName, 
            const VISU::TEntity& theEntity, 
            const std::string& theFieldName)
{
  TFindMeshOnEntity aFindMeshOnEntity = 
    FindMeshOnEntity(theMeshName,theEntity);

  VISU::PMeshImpl aMesh = boost::get<0>(aFindMeshOnEntity);;
  VISU::PMeshOnEntityImpl aMeshOnEntity = boost::get<1>(aFindMeshOnEntity);

  VISU::TMeshOnEntityMap& aMeshOnEntityMap = aMesh->myMeshOnEntityMap;
  VISU::PMeshOnEntityImpl aVTKMeshOnEntity = aMeshOnEntity;
  if ( theEntity == VISU::NODE_ENTITY ) {
    if(aMeshOnEntityMap.find( VISU::CELL_ENTITY ) != aMeshOnEntityMap.end())
      aVTKMeshOnEntity = aMeshOnEntityMap[ VISU::CELL_ENTITY ];
    else if (aMeshOnEntityMap.find( VISU::FACE_ENTITY ) != aMeshOnEntityMap.end() )
      aVTKMeshOnEntity = aMeshOnEntityMap[ VISU::FACE_ENTITY ];
    else if (aMeshOnEntityMap.find( VISU::EDGE_ENTITY ) != aMeshOnEntityMap.end() )
      aVTKMeshOnEntity = aMeshOnEntityMap[ VISU::EDGE_ENTITY ];
    else if ( aMeshOnEntityMap.find( VISU::NODE_ENTITY ) != aMeshOnEntityMap.end() )
      aVTKMeshOnEntity = aMeshOnEntityMap[ VISU::NODE_ENTITY ];
  }else
    aVTKMeshOnEntity = aMeshOnEntity;
  
  VISU::TFieldMap& aFieldMap = aMeshOnEntity->myFieldMap;
  VISU::TFieldMap::const_iterator aFieldIter= aFieldMap.find( theFieldName );
  if(aFieldIter == aFieldMap.end())
    EXCEPTION(std::runtime_error,"FindField >> There is no field on the mesh!!!");
  
  VISU::PFieldImpl aField = aFieldIter->second;

  return TFindField( aMesh,
                     aMeshOnEntity,
                     aVTKMeshOnEntity,
                     aField );
}


size_t
VISU_Convertor_impl
::GetFieldOnMeshSize(const std::string& theMeshName, 
                     const VISU::TEntity& theEntity,
                     const std::string& theFieldName)
{
  TFindField aFindField = FindField(theMeshName,theEntity,theFieldName);
  VISU::PMeshOnEntityImpl aVTKMeshOnEntity = boost::get<2>(aFindField);
  VISU::PFieldImpl aField = boost::get<3>(aFindField);

  size_t aMeshSize = GetMeshOnEntitySize(theMeshName,aVTKMeshOnEntity->myEntity);
  size_t aFieldOnMeshSize = size_t(aField->myDataSize*sizeof(vtkFloatingPointType)*aField->myValField.size()*ERR_SIZE_CALC);
  size_t aResult = aMeshSize + aFieldOnMeshSize;
  if(MYDEBUG)
    MSG(MYVTKDEBUG,"GetFieldOnMeshSize - aFieldOnMeshSize = "<<vtkFloatingPointType(aFieldOnMeshSize));
  MSG(MYDEBUG,"GetFieldOnMeshSize - aResult = "<<vtkFloatingPointType(aResult)<<"; theMeshName = '"<<theMeshName<<
      "'; theEntity = "<<theEntity<<"; theFieldName = '"<<theFieldName<<"'");

  return aResult;
}


VISU_Convertor_impl::TFindTimeStamp
VISU_Convertor_impl
::FindTimeStamp(const std::string& theMeshName, 
                const VISU::TEntity& theEntity, 
                const std::string& theFieldName, 
                int theStampsNum)
{
  TFindField aFindField = FindField(theMeshName,theEntity,theFieldName);
  VISU::PField aField = boost::get<3>(aFindField);

  VISU::TValField& aValField = aField->myValField;
  VISU::TValField::const_iterator aValFieldIter= aValField.find(theStampsNum);
  if(aValFieldIter == aValField.end())
    EXCEPTION(std::runtime_error,"FindTimeStamp >> There is no field with the timestamp!!!");
  
  VISU::PMeshImpl aMesh = boost::get<0>(aFindField);
  VISU::PMeshOnEntityImpl aMeshOnEntity = boost::get<1>(aFindField);
  VISU::PMeshOnEntityImpl aVTKMeshOnEntity = boost::get<2>(aFindField);
  VISU::PValForTimeImpl aValForTime = aValFieldIter->second;

  return TFindTimeStamp(aMesh,
                        aMeshOnEntity,
                        aVTKMeshOnEntity,
                        aField,
                        aValForTime);
}


size_t
VISU_Convertor_impl
::GetTimeStampSize(const std::string& theMeshName, 
                   const VISU::TEntity& theEntity,
                   const std::string& theFieldName,
                   int theStampsNum)
{
  TFindTimeStamp aFindTimeStamp = 
    FindTimeStamp(theMeshName,theEntity,theFieldName,theStampsNum);
  VISU::PMeshOnEntityImpl aVTKMeshOnEntity = boost::get<2>(aFindTimeStamp);
  VISU::PFieldImpl aField = boost::get<3>(aFindTimeStamp);
  
  size_t aMeshSize = GetMeshOnEntitySize(theMeshName, aVTKMeshOnEntity->myEntity);
  size_t aTimeStampSize = size_t(aField->myDataSize*sizeof(vtkFloatingPointType) * ERR_SIZE_CALC);
  size_t aResult = aMeshSize + aTimeStampSize;

  MSG(MYDEBUG && MYVTKDEBUG,"GetTimeStampSize - aTimeStampSize = "<<vtkFloatingPointType(aTimeStampSize));
  MSG(MYDEBUG,"GetTimeStampSize - aResult = "<<vtkFloatingPointType(aResult)<<
      "; theMeshName = '"<<theMeshName<<"'; theEntity = "<<theEntity<<
      "; theFieldName = '"<<theFieldName<<"'; theStampsNum = "<<theStampsNum);

  return aResult;
}


size_t
VISU_Convertor_impl
::GetTimeStampOnMeshSize(const std::string& theMeshName, 
                         const VISU::TEntity& theEntity,
                         const std::string& theFieldName,
                         int theTimeStampNumber,
                         bool& theIsEstimated)
{
  size_t aSize = 0;

  //Cheching possibility do the query
  TFindTimeStamp aFindTimeStamp = FindTimeStamp(theMeshName,
                                                theEntity,
                                                theFieldName,
                                                theTimeStampNumber);

  VISU::PValForTimeImpl aValForTime = boost::get<4>(aFindTimeStamp);
  VISU::PUnstructuredGridIDMapperImpl anUnstructuredGridIDMapper = aValForTime->myUnstructuredGridIDMapper;
  if(anUnstructuredGridIDMapper->myIsVTKDone){
    VISU::PIDMapper anIDMapper = GetTimeStampOnMesh(theMeshName, 
                                                    theEntity, 
                                                    theFieldName, 
                                                    theTimeStampNumber);
    anIDMapper->GetOutput();
    aSize += anIDMapper->GetMemorySize();
  }else
    aSize += GetTimeStampSize(theMeshName, theEntity, theFieldName, theTimeStampNumber);

  theIsEstimated = !(anUnstructuredGridIDMapper->myIsVTKDone);

  //cout<<"VISU_Convertor_impl::GetTimeStampOnMeshSize - "<<aSize<<"; "<<(anIDMapperFilter->myIsVTKDone)<<endl;
  return aSize;
}


size_t
VISU_Convertor_impl
::GetTimeStampOnGaussPtsSize(const std::string& theMeshName, 
                             const VISU::TEntity& theEntity,
                             const std::string& theFieldName,
                             int theTimeStampNumber,
                             bool& theIsEstimated)
{
  size_t aSize = 0;

  //Cheching possibility do the query
  TFindTimeStamp aFindTimeStamp = FindTimeStamp(theMeshName,
                                                theEntity,
                                                theFieldName,
                                                theTimeStampNumber);

  VISU::PValForTimeImpl aValForTime = boost::get<4>(aFindTimeStamp);
  VISU::PGaussPtsIDFilter aGaussPtsIDFilter = aValForTime->myGaussPtsIDFilter;
  if(aGaussPtsIDFilter->myIsVTKDone){
    VISU::PGaussPtsIDMapper aGaussPtsIDMapper = GetTimeStampOnGaussPts(theMeshName, 
                                                                       theEntity, 
                                                                       theFieldName, 
                                                                       theTimeStampNumber);
    aGaussPtsIDMapper->GetOutput();
    aSize += aGaussPtsIDMapper->GetMemorySize();
  }else
    aSize += GetTimeStampSize(theMeshName, theEntity, theFieldName, theTimeStampNumber);

  theIsEstimated = !(aGaussPtsIDFilter->myIsVTKDone);

  //cout<<"VISU_Convertor_impl::GetTimeStampOnGaussPtsSize - "<<aSize<<"; "<<(aGaussPtsIDFilter->myIsVTKDone)<<endl;
  return aSize;
}


const VISU::PField
VISU_Convertor_impl
::GetField(const std::string& theMeshName, 
           VISU::TEntity theEntity, 
           const std::string& theFieldName) 
{
  TFindField aFindField = FindField(theMeshName,theEntity,theFieldName);
  VISU::PField aField = boost::get<3>(aFindField);
  return aField;
}


const VISU::PValForTime 
VISU_Convertor_impl
::GetTimeStamp(const std::string& theMeshName, 
               const VISU::TEntity& theEntity,
               const std::string& theFieldName,
               int theStampsNum)
{
  TFindTimeStamp aFindTimeStamp = 
    FindTimeStamp(theMeshName,theEntity,theFieldName,theStampsNum);
  VISU::PValForTime aValForTime = boost::get<4>(aFindTimeStamp);
  return aValForTime;
}
