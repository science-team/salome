// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   : 
//  Author : 
//  Module : SALOME
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/CONVERTOR/VISU_MergeFilter.cxx,v 1.5.2.1.6.1.8.1 2011-06-02 06:00:16 vsr Exp $
//
#include "VISU_MergeFilter.hxx"
#include "VISU_MergeFilterUtilities.hxx"

#include <vtkObjectFactory.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>

#include <vtkExecutive.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkStreamingDemandDrivenPipeline.h>


//------------------------------------------------------------------------------
vtkStandardNewMacro(VISU_MergeFilter);

//------------------------------------------------------------------------------
VISU_MergeFilter
::VISU_MergeFilter():
  myIsMergingInputs(false)
{
  this->FieldList = new VISU::TFieldList;
  this->SetNumberOfInputPorts(6);
}

//------------------------------------------------------------------------------
VISU_MergeFilter::~VISU_MergeFilter()
{
  delete this->FieldList;
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetGeometry(vtkDataSet *input)
{
  this->Superclass::SetInput(input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_MergeFilter::GetGeometry()
{
  if (this->GetNumberOfInputConnections(0) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(0, 0));
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetScalars(vtkDataSet *input)
{
  this->SetInput(1, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_MergeFilter::GetScalars()
{
  if (this->GetNumberOfInputConnections(1) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(1, 0));
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetVectors(vtkDataSet *input)
{
  this->SetInput(2, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_MergeFilter::GetVectors()
{
  if (this->GetNumberOfInputConnections(2) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(2, 0));
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetNormals(vtkDataSet *input)
{
  this->SetInput(3, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_MergeFilter::GetNormals()
{
  if (this->GetNumberOfInputConnections(3) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(3, 0));
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetTCoords(vtkDataSet *input)
{
  this->SetInput(4, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_MergeFilter::GetTCoords()
{
  if (this->GetNumberOfInputConnections(4) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(4, 0));
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::SetTensors(vtkDataSet *input)
{
  this->SetInput(5, input);
}

//------------------------------------------------------------------------------
vtkDataSet *VISU_MergeFilter::GetTensors()
{
  if (this->GetNumberOfInputConnections(5) < 1)
    {
    return NULL;
    }
  return vtkDataSet::SafeDownCast(
    this->GetExecutive()->GetInputData(5, 0));
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::AddField(const char* name, vtkDataSet* input)
{
  this->FieldList->Add(name, input);
}

//------------------------------------------------------------------------------
void VISU_MergeFilter::RemoveFields()
{
  delete this->FieldList;
  this->FieldList = new VISU::TFieldList;
}


//---------------------------------------------------------------
void
VISU_MergeFilter
::SetMergingInputs(bool theIsMergingInputs)
{
  if(myIsMergingInputs == theIsMergingInputs)
    return;

  myIsMergingInputs = theIsMergingInputs;
  Modified();
}

  
//---------------------------------------------------------------
bool
VISU_MergeFilter
::IsMergingInputs()
{
  return myIsMergingInputs;
}
  

//---------------------------------------------------------------
int
VISU_MergeFilter
::RequestData(vtkInformation *theRequest,
              vtkInformationVector **theInputVector,
              vtkInformationVector *theOutputVector)
{
  if(vtkUnstructuredGrid *anInput = dynamic_cast<vtkUnstructuredGrid*>(this->GetInput())){
    vtkUnstructuredGrid *anOutput = dynamic_cast<vtkUnstructuredGrid*>(this->GetOutput());
    return VISU::Execute(anInput,
                         anOutput,
                         this->GetScalars(),
                         this->GetVectors(),
                         this->GetNormals(),
                         this->GetTCoords(),
                         this->GetTensors(),
                         this->FieldList,
                         IsMergingInputs());
  }else if(vtkPolyData *anInput = dynamic_cast<vtkPolyData*>(this->GetInput())){
    vtkPolyData *anOutput = dynamic_cast<vtkPolyData*>(this->GetOutput());
    return VISU::Execute(anInput,
                         anOutput,
                         this->GetScalars(),
                         this->GetVectors(),
                         this->GetNormals(),
                         this->GetTCoords(),
                         this->GetTensors(),
                         this->FieldList,
                         IsMergingInputs());
  }

  return Superclass::RequestData(theRequest,
                                 theInputVector,
                                 theOutputVector);
}

//----------------------------------------------------------------------------
//  Trick:  Abstract data types that may or may not be the same type
// (structured/unstructured), but the points/cells match up.
// Output/Geometry may be structured while ScalarInput may be 
// unstructured (but really have same triagulation/topology as geometry).
// Just request all the input. Always generate all of the output (todo).
int
VISU_MergeFilter
::RequestUpdateExtent(vtkInformation *vtkNotUsed(request),
                      vtkInformationVector **inputVector,
                      vtkInformationVector *vtkNotUsed(outputVector))
{
  vtkInformation *inputInfo;
  int idx;
  
  for (idx = 0; idx < 6; ++idx)
    {
    inputInfo = inputVector[idx]->GetInformationObject(0);
    if (inputInfo)
      {
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(),
                     0);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(),
                     1);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS(),
                     0);
      inputInfo->Set(vtkStreamingDemandDrivenPipeline::EXACT_EXTENT(), 1);
      }
    }
  return 1;
}


//----------------------------------------------------------------------------
int
VISU_MergeFilter
::FillInputPortInformation(int port, vtkInformation *info)
{
  int retval = this->Superclass::FillInputPortInformation(port, info);
  if (port > 0)
    {
    info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
    }
  return retval;
}

