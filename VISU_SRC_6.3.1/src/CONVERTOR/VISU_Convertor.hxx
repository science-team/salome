// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU CONVERTOR :
//  File   : VISU_Convertor.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_Convertor_HeaderFile
#define VISU_Convertor_HeaderFile

/*! 
  \file VISU_Convertor.hxx
  \brief The file represents definition of basic interface of the VISU CONVERTER package
*/

#include "VISU_Structures.hxx"


//---------------------------------------------------------------
//! This class defines interface to read VTK interpretation of MED entities
/*!
  It is the main class of the VISU CONVERTER package.
  Following MED entities can be retrived from any MED file:
  - mesh from corresponding MED ENTITIES;
  - MED TIMESTAMPS;
  - MED FAMILIES;
  - MED GROUPS.
  The class produce its work in two main steps:
  1. Perfrom parsing of MED file to get known what MED entities are pressent in it
  2. Get VTK representation for any existing MED entity
  Also, it can perform some additional work to calculate expected amount of memory to build defined VTK representation
*/
class VISU_CONVERTOR_EXPORT VISU_Convertor
{
public:
  //! Get brief name of the corresponding source MED file
  virtual
  const std::string& 
  GetName();

  //! Let known whether the source MED file parsed or not
  virtual
  int
  IsDone() const;

  //! This method perform first parsing of MED file to get known what MED entities are pressent in it
  virtual
  VISU_Convertor* 
  Build() = 0;

  //! This method perform first parsing of MED file to get known what MED mesh entities are pressent in it
  virtual
  VISU_Convertor* 
  BuildEntities() = 0;

  //! This method perform first parsing of MED file to get known what MED fields are pressent in it
  virtual
  VISU_Convertor* 
  BuildFields() = 0;

  //! This min /max calculation over existing MED fields
  virtual
  VISU_Convertor* 
  BuildMinMax() = 0;

  //! This method perform first parsing of MED file to get known what MED groups are pressent in it
  virtual
  VISU_Convertor* 
  BuildGroups() = 0;

  //! This method allow to get known what MED entities are present is the MED file
  virtual
  const VISU::TMeshMap& 
  GetMeshMap();

  //! Get amount of memory to build VTK representations for all existing MED entities
  virtual
  size_t
  GetSize() = 0;

  //! Get mesh for corresponding MED ENTITY
  virtual
  VISU::PNamedIDMapper 
  GetMeshOnEntity(const std::string& theMeshName, 
                  const VISU::TEntity& theEntity) = 0;
  
  //! Get amount of memory to build mesh for corresponding MED ENTITY
  virtual
  size_t
  GetMeshOnEntitySize(const std::string& theMeshName, 
                      const VISU::TEntity& theEntity) = 0;
  
  //! Get mesh for corresponding MED FAMILY
  virtual 
  VISU::PUnstructuredGridIDMapper 
  GetFamilyOnEntity(const std::string& theMeshName, 
                    const VISU::TEntity& theEntity,
                    const std::string& theFamilyName) = 0;

  //! Get amount of memory to build mesh for corresponding MED FAMILY
  virtual 
  size_t 
  GetFamilyOnEntitySize(const std::string& theMeshName, 
                        const VISU::TEntity& theEntity,
                        const std::string& theFamilyName) = 0;

  //! Get mesh for corresponding MED GROUP
  virtual
  VISU::PUnstructuredGridIDMapper
  GetMeshOnGroup(const std::string& theMeshName, 
                 const std::string& theGroupName) = 0;
  
  //! Get amount of memory to build mesh for corresponding MED GROUP
  virtual
  size_t
  GetMeshOnGroupSize(const std::string& theMeshName, 
                     const std::string& theGroupName) = 0;
  
  //! Get mesh with attached values for corresponding MED TIMESTAMP
  virtual
  VISU::PUnstructuredGridIDMapper 
  GetTimeStampOnMesh(const std::string& theMeshName, 
                     const VISU::TEntity& theEntity,
                     const std::string& theFieldName,
                     int theTimeStampNumber) = 0;

  //! Get Gauss Points mesh with attached values for corresponding MED TIMESTAMP
  virtual
  VISU::PGaussPtsIDMapper 
  GetTimeStampOnGaussPts(const std::string& theMeshName, 
                         const VISU::TEntity& theEntity,
                         const std::string& theFieldName,
                         int theTimeStampNumber) = 0;
   
  //! Get amount of memory to build vtkDataSet for corresponding MED TIMESTAMP on mesh
  virtual 
  size_t
  GetTimeStampOnMeshSize(const std::string& theMeshName, 
                         const VISU::TEntity& theEntity,
                         const std::string& theFieldName,
                         int theTimeStampNumber,
                         bool& theIsEstimated) = 0;
    
  //! Get amount of memory to build vtkDataSet for corresponding MED TIMESTAMP on Gauss Points
  virtual 
  size_t
  GetTimeStampOnGaussPtsSize(const std::string& theMeshName, 
                             const VISU::TEntity& theEntity,
                             const std::string& theFieldName,
                             int theTimeStampNumber,
                             bool& theIsEstimated) = 0;
    
  //! Get amount of memory to build all MED TIMESTAMPS for corresponding MED FIELD
  virtual 
  size_t
  GetFieldOnMeshSize(const std::string& theMeshName, 
                     const VISU::TEntity& theEntity,
                     const std::string& theFieldName) = 0;
  
  //! Find MED FIELD container
  virtual
  const VISU::PField 
  GetField(const std::string& theMeshName, 
           VISU::TEntity theEntity, 
           const std::string& theFieldName) = 0;
  
  //! Find MED TIMESTAMP container
  virtual
  const VISU::PValForTime 
  GetTimeStamp(const std::string& theMeshName, 
               const VISU::TEntity& theEntity,
               const std::string& theFieldName,
               int theTimeStampNumber) = 0;
    
  //! Allow to generate pretty name for MED TIMESTAMP
  static 
  std::string 
  GenerateName(const VISU::TTime& aTime);

  static 
  std::string 
  GenerateName(const std::string& theName, unsigned int theTimeId);

protected:
  VISU_Convertor();
  
  std::string myName;
  VISU::TMeshMap myMeshMap;
  int myIsDone;
};

extern "C"
{
  //! Instatiate proper VISU_Convertor subclass
  VISU_CONVERTOR_EXPORT
  VISU_Convertor*
  CreateConvertor(const std::string& theFileName);
};


#endif
