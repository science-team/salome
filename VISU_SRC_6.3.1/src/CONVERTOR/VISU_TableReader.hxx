// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU CONVERTOR :
//  File   : VISU_TableReader.hxx
//  Author : Alexey PETROV
//  Module : VISU
//
#ifndef VISU_TableReader_HeaderFile
#define VISU_TableReader_HeaderFile

/*! 
  \file VISU_TableReader.hxx
  \brief The file contains definition of the Table reading functionlity
*/

#include "VISUConvertor.hxx"

#include "VISU_IDMapper.hxx"

#include <string>
#include <vector>

namespace VISU
{
  //---------------------------------------------------------------
  struct VISU_CONVERTOR_EXPORT TTable2D 
  {
    typedef std::string TValue;
    typedef std::vector<TValue> TValues;
    
    struct TRow
    {
      std::string myTitle;
      std::string myUnit;
      TValues myValues;
    };
    
    std::string myTitle;
    std::vector<std::string> myColumnUnits;
    std::vector<std::string> myColumnTitles;
    
    typedef std::vector<TRow> TRows;
    TRows myRows;
    
    int 
    Check();

    void
    getColumns( TTable2D& theTable2D ) const;
  };


  //---------------------------------------------------------------
  class VISU_CONVERTOR_EXPORT TTableIDMapper: 
    public virtual TPolyDataIDMapper,
    public virtual TTable2D
  {
  public:
    TTableIDMapper();
    ~TTableIDMapper();
    
    virtual
    vtkPolyData*
    GetPolyDataOutput();

    virtual
    long unsigned int
    GetMemorySize();

    void
    SetXAxisPosition( vtkIdType theAxisPosition );

    vtkIdType
    GetXAxisPosition();

  protected:
    vtkIdType myXAxisPosition;
    vtkPolyData* myOutput;
  };
  typedef MED::SharedPtr<TTableIDMapper> PTableIDMapper;
  

  //---------------------------------------------------------------
  typedef std::vector<PTableIDMapper> TTableContainer;
  VISU_CONVERTOR_EXPORT 
    void ImportTables( const char* theFileName, TTableContainer& theContainer,
                       bool theFirstStrAsTitle = false);

  void ImportCSVTable(const char* theFileName,
                      TTableContainer& theContainer,
                      bool theFirstStrAsTitle,
                      const char theSeparator);

  //---------------------------------------------------------------
}

#endif
