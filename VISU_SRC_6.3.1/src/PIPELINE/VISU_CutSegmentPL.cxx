// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// File:    VISU_CutSegmentPL.hxx
// Author:  Oleg UVAROV
// Module : VISU
//
#include "VISU_CutSegmentPL.hxx"
#include "VISU_PipeLineUtils.hxx"

#include <vtkAppendPolyData.h>

//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_CutSegmentPL);


//----------------------------------------------------------------------------
VISU_CutSegmentPL
::VISU_CutSegmentPL()
{
}

//----------------------------------------------------------------------------
void
VISU_CutSegmentPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_CutSegmentPL *aPipeLine = dynamic_cast<VISU_CutSegmentPL*>(thePipeLine)){
    vtkFloatingPointType x, y, z;
    aPipeLine->GetPoint1(x, y, z);
    SetPoint1(x, y, z);
    aPipeLine->GetPoint2(x, y, z);
    SetPoint2(x, y, z);
  }
}

//----------------------------------------------------------------------------
void
VISU_CutSegmentPL
::Init()
{
  Superclass::Init();

  vtkFloatingPointType aBounds[6];
  GetMergedInput()->GetBounds(aBounds);

  for( int i = 0; i < 3; i++ ) {
    vtkFloatingPointType min = aBounds[ 2*i ];
    vtkFloatingPointType max = aBounds[ 2*i+1 ];
    myPoint1[ i ] = i == 1 ? min : ( min + max ) / 2;
    myPoint2[ i ] = i == 1 ? max : ( min + max ) / 2;
  }
}

//----------------------------------------------------------------------------
void
VISU_CutSegmentPL
::SetPoint1(vtkFloatingPointType theX,
            vtkFloatingPointType theY,
            vtkFloatingPointType theZ )
{
  myPoint1[0] = theX;
  myPoint1[1] = theY;
  myPoint1[2] = theZ;
}

//----------------------------------------------------------------------------
void
VISU_CutSegmentPL
::GetPoint1(vtkFloatingPointType& theX,
            vtkFloatingPointType& theY,
            vtkFloatingPointType& theZ )
{
  theX = myPoint1[0];
  theY = myPoint1[1];
  theZ = myPoint1[2];
}

//----------------------------------------------------------------------------
void
VISU_CutSegmentPL
::SetPoint2(vtkFloatingPointType theX,
            vtkFloatingPointType theY,
            vtkFloatingPointType theZ )
{
  myPoint2[0] = theX;
  myPoint2[1] = theY;
  myPoint2[2] = theZ;
}

//----------------------------------------------------------------------------
void
VISU_CutSegmentPL
::GetPoint2(vtkFloatingPointType& theX,
            vtkFloatingPointType& theY,
            vtkFloatingPointType& theZ )
{
  theX = myPoint2[0];
  theY = myPoint2[1];
  theZ = myPoint2[2];
}

//----------------------------------------------------------------------------
vtkDataSet* 
VISU_CutSegmentPL
::InsertCustomPL()
{
  return myAppendPolyData->GetOutput();
}

//----------------------------------------------------------------------------
void
VISU_CutSegmentPL
::Update()
{
  vtkDataSet* aMergedInput = GetMergedInput();
  if(VISU::IsQuadraticData(aMergedInput)) // Bug 0020123, note 0005343
    throw std::runtime_error("Impossible to build presentation");

  vtkFloatingPointType aVector12[3], aVector21[3];
  VISU::Sub( myPoint2, myPoint1, aVector12 );
  VISU::Sub( myPoint1, myPoint2, aVector21 );

  double aPrecision = 1.0 / VTK_LARGE_FLOAT;
  double aNorm = vtkMath::Normalize( aVector12 );
  if( aNorm < aPrecision )
    return;

  // compute two vectors which are orthogonal to the line between the input points
  // these vectors could be used as normals of two planes, intersected exactly at this line
  // origin of these planes should be places at one of the input points
  vtkFloatingPointType aVector1[3], aVector2[3];
  vtkMath::Perpendiculars( aVector12, aVector1, aVector2, 0 );

  ClearAppendPolyData(myAppendPolyData);

  SetPartPosition(1);

  vtkFloatingPointType aBounds[6];
  GetMergedInput()->GetBounds(aBounds);

  // check if the input is planar -  in this case one cut plane will be enough
  // (besides, the second cut corrupts the resulting output, splitting it to points)
  bool isPlanar = true;
  vtkFloatingPointType aNormal[3] = { 0.0, 0.0, 0.0 };
  if( fabs( aBounds[0] - aBounds[1] ) < aPrecision )
    aNormal[0] = 1.0;
  else if( fabs( aBounds[2] - aBounds[3] ) < aPrecision )
    aNormal[1] = 1.0;
  else if( fabs( aBounds[4] - aBounds[5] ) < aPrecision )
    aNormal[2] = 1.0;
  else
    isPlanar = false;

  if( isPlanar ) {
    // choose a vector which is not collinear with normal of the plane
    vtkFloatingPointType aCross[3];
    vtkMath::Cross( aVector1, aNormal, aCross );
    bool isFirst = vtkMath::Norm( aCross ) > aPrecision;
    VISU_CutPlanesPL::CutWithPlane(myAppendPolyData, GetMergedInput(), isFirst ? aVector1 : aVector2, myPoint1);
    myAppendPolyData->Update();
  }
  else {
    vtkAppendPolyData *anAppendPolyData = vtkAppendPolyData::New();

    VISU_CutPlanesPL::CutWithPlane(anAppendPolyData, GetMergedInput(), aVector1, myPoint1);
    vtkDataSet *aDataSet = anAppendPolyData->GetOutput();
    aDataSet->Update();

    VISU_CutPlanesPL::CutWithPlane(myAppendPolyData, aDataSet, aVector2, myPoint1);
    myAppendPolyData->Update();

    anAppendPolyData->Delete();
  }

  // calculate values for building of table
  for (int i = 0; i<3 ; i++) {
    myRealDirLn[i] = myDirLn[i] = aVector12[i];
    if(myDirLn[i] < 0.0) 
      myDirLn[i] = -1.0*myDirLn[i]; //enk:: correction of bug Bug PAL10401
  }

  GetBoundProject(myBoundPrjLn, 
                  aBounds, 
                  myDirLn);

  VISU::Mul(myDirLn,
            myBoundPrjLn[0],
            myBasePnt);
  
  CorrectPnt(myBasePnt,
             aBounds);

  VISU_ScalarMapPL::Update();
}
