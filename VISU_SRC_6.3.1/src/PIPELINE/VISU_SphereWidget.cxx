// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VISU_SphereWidget.hxx"

#include <vtkActor.h>
#include <vtkAssemblyNode.h>
#include <vtkAssemblyPath.h>
#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkCellPicker.h>
#include <vtkDoubleArray.h>
#include <vtkMath.h>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphere.h>
#include <vtkSphereSource.h>
#include <vtkPoints.h>
#include <vtkSphere.h>
#include <vtkImplicitSum.h>
#include <vtkImplicitFunction.h>

vtkCxxRevisionMacro(VISU_SphereWidget, "$Revision: 1.5.2.1.6.1.8.1 $");
vtkStandardNewMacro(VISU_SphereWidget);
//====================================================================
// function:
// purpose:
//====================================================================
VISU_SphereWidget::VISU_SphereWidget()
{
  myState = VISU_SphereWidget::Start;
  this->EventCallbackCommand->SetCallback(VISU_SphereWidget::ProcessEvents);
  
  //Build the representation of the widget
  mySphereSource = vtkSphereSource::New();
  mySphereSource->SetThetaResolution(16);
  mySphereSource->SetPhiResolution(15);
  mySphereSource->LatLongTessellationOn();
  mySphereMapper = vtkPolyDataMapper::New();
  mySphereMapper->SetInput(mySphereSource->GetOutput());
  mySphereActor = vtkActor::New();
  mySphereActor->SetMapper(mySphereMapper);
  //
  // Define the point coordinates
  vtkFloatingPointType bounds[6];
  for(int i = 0; i < 6; i += 2){
    bounds[i]=-.5;
    bounds[i+1]=-bounds[i];
  }
  // Initial creation of the widget, serves to initialize it
  PlaceWidget(bounds);

  //Manage the picking stuff
  myPicker = vtkCellPicker::New();
  myPicker->SetTolerance(0.005); //need some fluff
  myPicker->AddPickList(mySphereActor);
  myPicker->PickFromListOn();
  
  // Set up the initial properties
  mySphereProperty = NULL;
  mySelectedSphereProperty = NULL;
  CreateDefaultProperties();
  myRmin=1.e-7;

  mySphere=vtkSphere::New();
  myImplicitSum=vtkImplicitSum::New();
  myImplicitSum->AddFunction(mySphere,-1.0);
  
  myRatio = 2.0;
}
//====================================================================
// function: ~
// purpose:
//====================================================================
VISU_SphereWidget::~VISU_SphereWidget()
{
  mySphereActor->Delete();
  mySphereMapper->Delete();
  mySphereSource->Delete();

  myPicker->Delete();

  if ( mySphereProperty ) {
    mySphereProperty->Delete();
  }
  if ( mySelectedSphereProperty ) {
    mySelectedSphereProperty->Delete();
  }
  mySphere->Delete();
  myImplicitSum->Delete();
}
//====================================================================
// function: SetThetaResolution
// purpose :
//====================================================================
void VISU_SphereWidget::SetThetaResolution(int r) 
{ 
  mySphereSource->SetThetaResolution(r); 
}
//====================================================================
// function: GetThetaResolution
// purpose :
//====================================================================
int VISU_SphereWidget::GetThetaResolution() 
{ 
  return mySphereSource->GetThetaResolution(); 
}
//====================================================================
// function: SetPhiResolution
// purpose :
//====================================================================
void VISU_SphereWidget::SetPhiResolution(int r)
{ 
  mySphereSource->SetPhiResolution(r); 
}
//====================================================================
// function: SetPhiResolution
// purpose :
//====================================================================
int VISU_SphereWidget::GetPhiResolution()   
{ 
  return mySphereSource->GetPhiResolution(); 
}
//====================================================================
// function: SetRadius
// purpose :
//====================================================================
void VISU_SphereWidget::SetRadius(vtkFloatingPointType theRadius) 
{
  if ( theRadius <= myRmin ) {
    theRadius = myRmin;
  }
  mySphereSource->SetRadius(theRadius); 
  mySphere->SetRadius(theRadius);
}
//====================================================================
// function: GetRadius
// purpose :
//====================================================================
vtkFloatingPointType VISU_SphereWidget::GetRadius()
{ 
  return mySphereSource->GetRadius(); 
}
//====================================================================
// function: SetCenter
// purpose :
//====================================================================
void VISU_SphereWidget::SetCenter(vtkFloatingPointType theCenter[3]) 
{
  mySphereSource->SetCenter(theCenter);
  mySphere->SetCenter(theCenter);
}
//====================================================================
// function: SetCenter
// purpose :
//====================================================================
void VISU_SphereWidget::SetCenter(vtkFloatingPointType theX, vtkFloatingPointType theY, vtkFloatingPointType theZ) 
{
  vtkFloatingPointType aCenter[3] = {theX, theY, theZ};
  SetCenter(aCenter);
}

//====================================================================
// function: GetCenter
// purpose :
//====================================================================
vtkFloatingPointType*  VISU_SphereWidget::GetCenter() 
{
  return mySphereSource->GetCenter();
}
//====================================================================
// function: GetCenter
// purpose :
//====================================================================
void  VISU_SphereWidget::GetCenter(vtkFloatingPointType theCenter[3]) 
{
  mySphereSource->GetCenter(theCenter);
}
//====================================================================
// function: GetSphereProperty
// purpose :
//====================================================================
vtkProperty*  VISU_SphereWidget::GetSphereProperty ()
{
  return mySphereProperty;
}
//====================================================================
// function: GetSelectedSphereProperty
// purpose :
//====================================================================
vtkProperty*  VISU_SphereWidget::GetSelectedSphereProperty ()
{
  return mySelectedSphereProperty;
}
//====================================================================
// function: ImplicitFunction
// purpose :
//====================================================================
vtkImplicitFunction* VISU_SphereWidget::ImplicitFunction()
{
  return myImplicitSum;
}
//====================================================================
// function: SetEnabled
// purpose :
//====================================================================
void VISU_SphereWidget::SetEnabled(int enabling)
{
  if ( !Interactor )    {
    vtkErrorMacro(<<"The interactor must be set prior to enabling/disabling widget");
    return;
  }

  if ( enabling )  {
    vtkDebugMacro(<<"Enabling sphere widget");
    if ( Enabled ) {//already enabled, just return
      return;
    }
    
    if ( ! CurrentRenderer )    {
      int aPos[2];
      Interactor->GetLastEventPosition(aPos);
      CurrentRenderer=Interactor->FindPokedRenderer(aPos[0], aPos[1]);
      if (!CurrentRenderer) {
        return;
      }
    }

    Enabled = 1;

    // listen for the following events
    Interactor->AddObserver(vtkCommand::MouseMoveEvent, 
                            EventCallbackCommand, 
                            Priority);
    Interactor->AddObserver(vtkCommand::LeftButtonPressEvent, 
                            EventCallbackCommand, 
                            Priority);
    Interactor->AddObserver(vtkCommand::LeftButtonReleaseEvent, 
                            EventCallbackCommand,
                            Priority);
    Interactor->AddObserver(vtkCommand::MiddleButtonPressEvent, 
                            EventCallbackCommand,
                            Priority);
    Interactor->AddObserver(vtkCommand::MiddleButtonReleaseEvent, 
                            EventCallbackCommand,
                            Priority);

    // Add the sphere
    CurrentRenderer->AddActor(mySphereActor);
    mySphereActor->SetProperty(mySphereProperty);
    mySphere->SetCenter(mySphereSource->GetCenter());
    mySphere->SetRadius(mySphereSource->GetRadius());
    
    InvokeEvent(vtkCommand::EnableEvent,NULL); //!!!see what will be done
  }
  //disabling----------------------------------------------------------
  else {
    vtkDebugMacro(<<"Disabling sphere widget");

    if ( !Enabled ){ //already disabled, just return
      return;
    }
    
    Enabled = 0;

    // don't listen for events any more
    Interactor->RemoveObserver(EventCallbackCommand);

    // turn off the sphere
    CurrentRenderer->RemoveActor(mySphereActor);
    InvokeEvent(vtkCommand::DisableEvent,NULL);
    CurrentRenderer = NULL;
  }

  Interactor->Render();
}
//====================================================================
// function:ProcessEvents
// purpose:
//====================================================================
void VISU_SphereWidget::ProcessEvents(vtkObject* vtkNotUsed(object), 
                                    unsigned long event,
                                    void* clientdata, 
                                    void* vtkNotUsed(calldata))
{
  VISU_SphereWidget* self = reinterpret_cast<VISU_SphereWidget *>( clientdata );

  switch(event)  {
    case vtkCommand::LeftButtonPressEvent:
      self->OnLeftButtonDown();
      break;
    case vtkCommand::LeftButtonReleaseEvent:
      self->OnLeftButtonUp();
      break;
    case vtkCommand::MiddleButtonPressEvent:
      self->OnMiddleButtonDown();
      break;
    case vtkCommand::MiddleButtonReleaseEvent:
      self->OnMiddleButtonUp();
      break;
    case vtkCommand::MouseMoveEvent:
      self->OnMouseMove();
      break;
    default:
      break;
  }
}
//====================================================================
// function:OnLeftButtonDown
// purpose:
//====================================================================
void VISU_SphereWidget::OnLeftButtonDown()
{
  int X = Interactor->GetEventPosition()[0];
  int Y = Interactor->GetEventPosition()[1];

  // Okay, make sure that the pick is in the current renderer
  vtkRenderer *aRenderer = Interactor->FindPokedRenderer(X,Y);
  //
  if (aRenderer != CurrentRenderer) {
    myState = VISU_SphereWidget::Outside;
    return;
  }
  // Okay, we can process this. Try to pick handles first;
  // if no places picked, then try to pick the sphere.
  myPicker->Pick(X, Y, 0., CurrentRenderer);
  if(vtkAssemblyPath *aPath = myPicker->GetPath()){
    if(aPath->GetFirstNode()->GetProp() == mySphereActor){
      myState = VISU_SphereWidget::Moving;
      HighlightSphere(1);
    }
  }else{
    myState = VISU_SphereWidget::Outside;
    return;
  }
  //
  EventCallbackCommand->SetAbortFlag(1);
  StartInteraction();
  InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
  Interactor->Render();
}
//====================================================================
// function:OnMouseMove
// purpose:
//====================================================================
void VISU_SphereWidget::OnMouseMove()
{
  // See whether we're active
  if ( myState == VISU_SphereWidget::Outside || 
       myState == VISU_SphereWidget::Start )    {
    return;
  }
  
  int X = Interactor->GetEventPosition()[0];
  int Y = Interactor->GetEventPosition()[1];

  // Do different things depending on state
  // Calculations everybody does
  double focalPoint[4], pickPoint[4], prevPickPoint[4], z;

  vtkRenderer *aRenderer=Interactor->FindPokedRenderer(X, Y);
  vtkCamera *aCamera=aRenderer->GetActiveCamera();
  if (!aCamera ) {
    return;
  }
  // Compute the two points defining the motion vector
  aCamera->GetFocalPoint(focalPoint);
  ComputeWorldToDisplay(focalPoint[0], 
                        focalPoint[1],
                        focalPoint[2], 
                        focalPoint);
  z = focalPoint[2];
  ComputeDisplayToWorld(double(Interactor->GetLastEventPosition()[0]),
                        double(Interactor->GetLastEventPosition()[1]),
                        z, 
                        prevPickPoint);
  ComputeDisplayToWorld(double(X), double(Y), z, pickPoint);

  // Process the motion
  if ( myState == VISU_SphereWidget::Moving ) {
    Translate(prevPickPoint, pickPoint);
  }
  else if ( myState == VISU_SphereWidget::Scaling ) {
    Scale(prevPickPoint, pickPoint, X, Y);
  }
  // Interact, if desired
  EventCallbackCommand->SetAbortFlag(1);
  InvokeEvent(vtkCommand::InteractionEvent,NULL);
  //
  Interactor->Render();
}
//====================================================================
// function:OnLeftButtonUp
// purpose:
//====================================================================
void VISU_SphereWidget::OnLeftButtonUp()
{
  if ( myState == VISU_SphereWidget::Outside ) {
    return;
  }

  myState = VISU_SphereWidget::Start;
  HighlightSphere(0);

  EventCallbackCommand->SetAbortFlag(1);
  EndInteraction();
  InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
  
  Interactor->Render();
}
//====================================================================
// function:OnMiddleButtonDown
// purpose:
//====================================================================
void VISU_SphereWidget::OnMiddleButtonDown()
{
  myState = VISU_SphereWidget::Scaling;

  int X = Interactor->GetEventPosition()[0];
  int Y = Interactor->GetEventPosition()[1];

  // Okay, make sure that the pick is in the current renderer
  vtkRenderer *aRenderer = Interactor->FindPokedRenderer(X,Y);
  if (aRenderer!=CurrentRenderer) {
    myState = VISU_SphereWidget::Outside;
    return;
  }
  
  // Okay, we can process this. Try to pick handles first;
  // if no handles picked, then pick the bounding box.
  myPicker->Pick(X, Y, 0., CurrentRenderer);
  vtkAssemblyPath *aPath = myPicker->GetPath();
  if ( !aPath ) {
    myState=VISU_SphereWidget::Outside;
    HighlightSphere(0);
    return;
  }
  
  HighlightSphere(1);

  EventCallbackCommand->SetAbortFlag(1);
  StartInteraction();
  InvokeEvent(vtkCommand::StartInteractionEvent,NULL);
  //
  Interactor->Render();
}
//====================================================================
// function:OnMiddleButtonUp
// purpose:
//====================================================================
void VISU_SphereWidget::OnMiddleButtonUp()
{
  if ( myState == VISU_SphereWidget::Outside ) {
    return;
  }
  myState = VISU_SphereWidget::Start;
  HighlightSphere(0);
  
  EventCallbackCommand->SetAbortFlag(1);
  EndInteraction();
  InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
  //
  Interactor->Render();
}
//====================================================================
// function:Translate
// purpose:
//====================================================================
void VISU_SphereWidget::Translate(double *p1, double *p2)
{
  vtkFloatingPointType v[3], aC[3], aC1[3];
  //
  v[0] = p2[0] - p1[0];
  v[1] = p2[1] - p1[1];
  v[2] = p2[2] - p1[2];
  //  
  mySphereSource->GetCenter(aC);
  aC1[0] = aC[0] + v[0];
  aC1[1] = aC[1] + v[1];
  aC1[2] = aC[2] + v[2];
  mySphereSource->SetCenter(aC1);
  mySphere->SetCenter(mySphereSource->GetCenter());
  mySphere->SetRadius(mySphereSource->GetRadius());
}
//====================================================================
// function:Scale
// purpose:
//====================================================================
void VISU_SphereWidget::Scale(double *p1, double *p2, 
                              int aX, int aY)
{
  double v[3];
  v[0] = p2[0] - p1[0];
  v[1] = p2[1] - p1[1];
  v[2] = p2[2] - p1[2];
  //
  vtkFloatingPointType aC[3], aR, sf, aR1;
  aR=mySphereSource->GetRadius();
  mySphereSource->GetCenter(aC);
  sf=vtkMath::Norm(v)/aR;
  int aCoordLast[2], iDX, iDY, iSign;
  Interactor->GetLastEventPosition(aCoordLast);
  //
  iDX=aX-aCoordLast[0];
  iDY=aCoordLast[1]-aY; 
  iSign=(iDX+iDY>0)? 1 : -1;
  sf=1.+iSign*sf;
  aR1=sf*aR;
  if (aR1<myRmin){
    aR1=myRmin;
  }
  mySphereSource->SetRadius(aR1);
  mySphere->SetCenter(mySphereSource->GetCenter());
  mySphere->SetRadius(mySphereSource->GetRadius());
}
//====================================================================
// function:GetSphere
// purpose:
//====================================================================
void VISU_SphereWidget::GetSphere(vtkSphere *sphere)
{
  sphere->SetRadius(mySphereSource->GetRadius());
  sphere->SetCenter(mySphereSource->GetCenter());
}
//====================================================================
// function:HighlightSphere
// purpose:
//====================================================================
void VISU_SphereWidget::HighlightSphere(int highlight)
{
  if ( highlight )  {
    this->ValidPick = 1;
    myPicker->GetPickPosition(this->LastPickPosition);// -> def in vtk3DWidget
    mySphereActor->SetProperty(mySelectedSphereProperty);
  }
  else {
    mySphereActor->SetProperty(mySphereProperty);
  }
}
//====================================================================
// function:CreateDefaultProperties
// purpose:
//====================================================================
void VISU_SphereWidget::CreateDefaultProperties()
{
  if (!mySphereProperty)    {
    mySphereProperty = vtkProperty::New();
    mySphereProperty->SetColor(0.,.5, .7);
    mySphereProperty->SetSpecular(0.5);
    mySphereProperty->SetRepresentationToWireframe();
  }
  if (!mySelectedSphereProperty)    {
    mySelectedSphereProperty = vtkProperty::New();
    mySelectedSphereProperty->SetColor(0.5, 0.5, 0.);
    mySelectedSphereProperty->SetSpecular(1.);
    mySelectedSphereProperty->SetRepresentationToWireframe();
  }
}
//====================================================================
// function:PlaceWidget
// purpose:
//====================================================================
void VISU_SphereWidget::PlaceWidget(vtkFloatingPointType bds[6])
{
  vtkFloatingPointType bounds[6], center[3], radius;

  this->AdjustBounds(bds, bounds, center);
  vtkFloatingPointType dX, dY, dZ;
  //
  dX=bounds[1]-bounds[0];
  dY=bounds[3]-bounds[2];
  dZ=bounds[5]-bounds[4];
  radius = dX;
  if (radius>dY){
    radius = dY;
  }
  if (radius>dZ)   {
    radius=dZ;
  }
  radius*=0.5;

  mySphereSource->SetCenter(center);
  mySphereSource->SetRadius(radius);
  mySphereSource->Update();
  //
  for (int i=0; i<6; i++) {
    InitialBounds[i]=bounds[i];
  }
  InitialLength = sqrt((bounds[1]-bounds[0])*(bounds[1]-bounds[0]) +
                       (bounds[3]-bounds[2])*(bounds[3]-bounds[2]) +
                       (bounds[5]-bounds[4])*(bounds[5]-bounds[4]));

  static vtkFloatingPointType EPS = 1.0E-1;
  myRmin = EPS*InitialLength;

}

//====================================================================
// function:ChangeRadius
// purpose:
//====================================================================
void VISU_SphereWidget::ChangeRadius(bool up)
{
  SetRadius( GetRadius() * ( up ? myRatio : 1 / myRatio ) );
}
//====================================================================
// function:GetPolyData
// purpose:
//====================================================================
void VISU_SphereWidget::GetPolyData(vtkPolyData *pd)
{ 
  pd->ShallowCopy(mySphereSource->GetOutput()); 
}
//====================================================================
// function:PrintSelf
// purpose:
//====================================================================
void VISU_SphereWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
