// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

// File:    VISU_CutSegmentPL.hxx
// Author:  Oleg UVAROV
// Module : VISU
//
#ifndef VISU_CutSegmentPL_HeaderFile
#define VISU_CutSegmentPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_CutLinesBasePL.hxx"


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_CutSegmentPL : public VISU_CutLinesBasePL
{
public:
  vtkTypeMacro(VISU_CutSegmentPL,VISU_CutLinesBasePL);

  static 
  VISU_CutSegmentPL* 
  New();

  virtual 
  void
  SetPoint1(vtkFloatingPointType theX,
            vtkFloatingPointType theY,
            vtkFloatingPointType theZ);

  virtual 
  void
  GetPoint1(vtkFloatingPointType& theX,
            vtkFloatingPointType& theY,
            vtkFloatingPointType& theZ);

  virtual 
  void
  SetPoint2(vtkFloatingPointType theX,
            vtkFloatingPointType theY,
            vtkFloatingPointType theZ);

  virtual 
  void
  GetPoint2(vtkFloatingPointType& theX,
            vtkFloatingPointType& theY,
            vtkFloatingPointType& theZ);

public:
  virtual
  void
  Init();

  vtkDataSet*
  InsertCustomPL();

  virtual
  void
  Update();

protected:
  VISU_CutSegmentPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  vtkFloatingPointType myPoint1[3];
  vtkFloatingPointType myPoint2[3];

private:
  VISU_CutSegmentPL(const VISU_CutSegmentPL&);  // Not implemented.
  void operator=(const VISU_CutSegmentPL&);  // Not implemented.
};


#endif
