// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_CutPlanesPL.hxx
// Author:  Alexey PETROV
// Module : VISU
//
#ifndef VISU_CutPlanesPL_HeaderFile
#define VISU_CutPlanesPL_HeaderFile

#include "VISUPipeline.hxx"
#include "VISU_ScalarMapPL.hxx"
#include "VISU_OptionalDeformationPL.hxx"
#include "VISU_MapperHolder.hxx"

#include <vector>

class vtkAppendPolyData;


//----------------------------------------------------------------------------
class VISU_PIPELINE_EXPORT VISU_CutPlanesPL : public VISU_ScalarMapPL,
                                              public VISU_OptionalDeformationPL
{
public:
  vtkTypeMacro(VISU_CutPlanesPL, VISU_ScalarMapPL);

  static 
  VISU_CutPlanesPL* 
  New();

  virtual
  unsigned long int 
  GetMTime();

  //----------------------------------------------------------------------------
  enum PlaneOrientation {XY, YZ, ZX};

  virtual 
  void
  SetOrientation(const VISU_CutPlanesPL::PlaneOrientation& theOrient,
                 vtkFloatingPointType theXAng, 
                 vtkFloatingPointType theYAng, 
                 int theNum = 0);
  
  virtual 
  const PlaneOrientation& 
  GetPlaneOrientation(int theNum = 0);

  virtual
  vtkFloatingPointType 
  GetRotateX(int theNum = 0);

  virtual
  vtkFloatingPointType
  GetRotateY(int theNum = 0);

  virtual
  vtkFloatingPointType 
  GetDisplacement(int theNum = 0);

  virtual
  void
  SetDisplacement(vtkFloatingPointType theDisp, 
                  int theNum = 0);

  virtual
  void
  SetPartPosition(int thePartNumber, 
                  vtkFloatingPointType thePartPosition);

  virtual
  vtkFloatingPointType 
  GetPartPosition(int thePartNumber, 
                  int theNum = 0);

  virtual 
  void
  SetPartDefault(int thePartNumber);

  virtual
  int
  IsPartDefault(int thePartNumber);

  virtual
  void
  SetNbParts(int theNb);

  virtual
  int
  GetNbParts();

public:
  virtual
  void
  Init();

  virtual
  void
  Update();

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();

  virtual
  vtkAppendPolyData* 
  GetAppendPolyData() 
  { 
    return myAppendPolyData; 
  }

public:
  static
  vtkFloatingPointType* 
  GetRx(vtkFloatingPointType theRx[3][3], 
        vtkFloatingPointType thaAng);

  static
  vtkFloatingPointType* 
  GetRy(vtkFloatingPointType theRy[3][3], 
        vtkFloatingPointType thaAng);

  static
  vtkFloatingPointType* 
  GetRz(vtkFloatingPointType theRz[3][3], 
        vtkFloatingPointType thaAng);

  static
  void
  CorrectPnt(vtkFloatingPointType thePnt[3], 
             const vtkFloatingPointType BoundPrj[6]);

  static
  void
  GetBoundProject(vtkFloatingPointType BoundPrj[3], 
                  const vtkFloatingPointType BoundBox[6], 
                  const vtkFloatingPointType Dir[3]);

  static
  void
  GetDir(vtkFloatingPointType theDir[3],
         const vtkFloatingPointType theAng[3],
         const PlaneOrientation& theBasePlane);

  static 
  void
  ClearAppendPolyData(vtkAppendPolyData *theAppendPolyData);

  static 
  void
  CutWithPlane(vtkAppendPolyData* theAppendPolyData, 
               vtkDataSet* theDataSet,
               vtkFloatingPointType theDir[3], 
               vtkFloatingPointType theOrig[3]);

  static
  void
  CutWithPlanes(vtkAppendPolyData* theAppendPolyData, 
                vtkDataSet* theDataSet,
                int theNbPlanes, 
                vtkFloatingPointType theDir[3], 
                vtkFloatingPointType theBounds[6],
                const std::vector<vtkFloatingPointType>& thePlanePosition,
                const std::vector<int>& thePlaneCondition,
                vtkFloatingPointType theDisplacement);

  virtual void SetVectorialField(VISU::PUnstructuredGridIDMapper);
  VISU::PUnstructuredGridIDMapper getVectorialField();

  virtual
  void
  SetMapScale(vtkFloatingPointType theMapScale = 1.0);


protected:
  VISU_CutPlanesPL();

  virtual
  ~VISU_CutPlanesPL();

  virtual 
  vtkDataSet* 
  InsertCustomPL();

  virtual
  void
  DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput);

  void
  SetPartPosition(int theNum = 0);

  int myNbParts;
  PlaneOrientation myBasePlane[2];
  vtkFloatingPointType myAng[2][3], myDisplacement[2];
  vtkAppendPolyData *myAppendPolyData;
  std::vector<vtkFloatingPointType> myPartPosition;
  std::vector<int> myPartCondition;

private:
  VISU_CutPlanesPL(const VISU_CutPlanesPL&);  // Not implemented.
  void operator=(const VISU_CutPlanesPL&);  // Not implemented.
};

#endif
