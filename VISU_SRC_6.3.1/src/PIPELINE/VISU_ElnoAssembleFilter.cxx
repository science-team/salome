// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VISU_ElnoAssembleFilter.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_ElnoMeshValue.hxx"

#include <vtkCellData.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro( VISU_ElnoAssembleFilter );


//----------------------------------------------------------------------------
VISU_ElnoAssembleFilter::VISU_ElnoAssembleFilter()
{
  this->SetInputArrayToProcess( 0, // idx
                                0, // port
                                0, // connection
                                vtkDataObject::FIELD_ASSOCIATION_POINTS, // field association
                                "ELNO_POINT_COORDS" ); // name

  this->myIsRestorePoints = false;
}


//----------------------------------------------------------------------------
VISU_ElnoAssembleFilter::~VISU_ElnoAssembleFilter()
{}


//----------------------------------------------------------------------------
void VISU_ElnoAssembleFilter::SetElnoAssembleState( bool theIsRestorePoints )
{
  if ( myIsRestorePoints == theIsRestorePoints )
    return;
    
  myIsRestorePoints = theIsRestorePoints;
  this->Modified();
}

//----------------------------------------------------------------------------
namespace
{
  //----------------------------------------------------------------------------
  template < int points_type, int elno_type >
  int Execute2( vtkPointSet *theInput, 
                vtkPointSet *theOutput,
                vtkDataArray *theElnoPointCoords )
  {
    theOutput->CopyStructure( theInput );
    
    vtkCellData *aCellData = theOutput->GetCellData();
    aCellData->PassData( theInput->GetCellData() );

    vtkPointData *aPointData = theOutput->GetPointData();
    aPointData->PassData( theInput->GetPointData() );

    vtkPoints *anInputPoints = theInput->GetPoints();
    vtkPoints *aPoints = anInputPoints->New( elno_type );
    vtkIdType aNbPoints = theInput->GetNumberOfPoints();
    aPoints->SetNumberOfPoints( aNbPoints );
    
    typedef typename VISU::TL::TEnum2VTKArrayType< elno_type >::TResult TPointsDataArray;
    typedef typename VISU::TL::TEnum2VTKBasicType< elno_type >::TResult TPointsDataType;
    TPointsDataArray* anOutputPointsArray = TPointsDataArray::SafeDownCast( aPoints->GetData() );

    TPointsDataArray* anElnoPointCoords = TPointsDataArray::SafeDownCast( theElnoPointCoords );
    
    for ( vtkIdType aPointId = 0; aPointId < aNbPoints; aPointId++ ) {
      TPointsDataType aCoords[ 3 ];
      anElnoPointCoords->GetTupleValue( aPointId, aCoords );
      anOutputPointsArray->SetTupleValue( aPointId, aCoords );
    }
    
    theOutput->SetPoints( aPoints );

    return 1;
  } 


  //----------------------------------------------------------------------------
  template < int points_type >
  int Execute( vtkPointSet *theInput, 
               vtkPointSet *theOutput,
               vtkDataArray *theElnoPointCoords )
  {
    switch( theElnoPointCoords->GetDataType() ){
    case VTK_DOUBLE:
      return Execute2< points_type, VTK_DOUBLE >( theInput, theOutput, theElnoPointCoords );
    case VTK_FLOAT:
      return Execute2< points_type, VTK_FLOAT >( theInput, theOutput, theElnoPointCoords );
    case VTK_INT:
      return Execute2< points_type, VTK_INT >( theInput, theOutput, theElnoPointCoords );
    case VTK_LONG:
      return Execute2< points_type, VTK_LONG >( theInput, theOutput, theElnoPointCoords );
    default:
      break;
    }
    
    return 0;
  } 


  //----------------------------------------------------------------------------
}


//----------------------------------------------------------------------------
int VISU_ElnoAssembleFilter::RequestData( vtkInformation *vtkNotUsed(request),
                                          vtkInformationVector **inputVector,
                                          vtkInformationVector *outputVector )
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and ouptut
  vtkPointSet *anInput = vtkPointSet::SafeDownCast( inInfo->Get( vtkDataObject::DATA_OBJECT() ) );
  vtkPointSet *anOutput = vtkPointSet::SafeDownCast( outInfo->Get( vtkDataObject::DATA_OBJECT() ) );

  vtkDataArray *anElnoPointCoords = this->GetInputArrayToProcess( 0, inputVector );

  if ( !myIsRestorePoints || !anElnoPointCoords ) {
    anOutput->ShallowCopy( anInput );
    return 1;
  }

  vtkPoints *aPoints = anInput->GetPoints();
  switch( aPoints->GetDataType() ){
  case VTK_DOUBLE:
    return ::Execute< VTK_DOUBLE >( anInput, anOutput, anElnoPointCoords );
  case VTK_FLOAT:
    return ::Execute< VTK_FLOAT >( anInput, anOutput, anElnoPointCoords );
  case VTK_INT:
    return ::Execute< VTK_INT >( anInput, anOutput, anElnoPointCoords );
  case VTK_LONG:
    return ::Execute< VTK_LONG >( anInput, anOutput, anElnoPointCoords );
  default:
    break;
  }  
  
  return 0;
}


//----------------------------------------------------------------------------
