// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_PipeLine.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_Plot3DPL.hxx"
#include "VISU_CutPlanesPL.hxx"
#include "VISU_PipeLineUtils.hxx"

#include <vtkAppendPolyData.h>
#include <vtkCutter.h>
#include <vtkPlane.h>

#include <vtkGeometryFilter.h>
#include <vtkContourFilter.h>
#include <vtkWarpScalar.h>
#include <vtkOutlineFilter.h>


//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_Plot3DPL);


//----------------------------------------------------------------------------
VISU_Plot3DPL
::VISU_Plot3DPL():
  myCellDataToPointData(VISU_CellDataToPointData::New()),
  myAppendPolyData(vtkAppendPolyData::New()),
  myGeometryFilter(vtkGeometryFilter::New()),
  myContourFilter(vtkContourFilter::New()),
  myWarpScalar(vtkWarpScalar::New()),
  myOrientation(VISU_CutPlanesPL::YZ),
  myIsRelative(true),
  myIsContour(false),
  myPosition(0.5),
  myScaleFactor(1.0),
  myMapScaleFactor(1.0)
{
  SetIsShrinkable(false);
  SetIsFeatureEdgesAllowed(false);

  myCellDataToPointData->Delete();
  myAppendPolyData->Delete();
  myGeometryFilter->Delete();
  myContourFilter->Delete();
  myWarpScalar->Delete();

  myAngle[0] = myAngle[1] = myAngle[2] = 0.0;

  SetNumberOfContours(32);
}


//----------------------------------------------------------------------------
VISU_Plot3DPL
::~VISU_Plot3DPL()
{}


//----------------------------------------------------------------------------
unsigned long int 
VISU_Plot3DPL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  aTime = std::max(aTime, myCellDataToPointData->GetMTime());
  aTime = std::max(aTime, myAppendPolyData->GetMTime());
  aTime = std::max(aTime, myGeometryFilter->GetMTime());
  aTime = std::max(aTime, myContourFilter->GetMTime());
  aTime = std::max(aTime, myWarpScalar->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void
VISU_Plot3DPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_Plot3DPL *aPipeLine = dynamic_cast<VISU_Plot3DPL*>(thePipeLine)){
    SetOrientation (aPipeLine->GetPlaneOrientation(),
                    aPipeLine->GetRotateX(), aPipeLine->GetRotateY());
    SetPlanePosition (aPipeLine->GetPlanePosition(),
                      aPipeLine->IsPositionRelative() );
    SetScaleFactor( aPipeLine->GetScaleFactor() );
    SetContourPrs( aPipeLine->GetIsContourPrs() );
    SetNumberOfContours( aPipeLine->GetNumberOfContours() );
  }
}


//----------------------------------------------------------------------------
VISU_CutPlanesPL::PlaneOrientation
VISU_Plot3DPL
::GetOrientation(vtkDataSet* theDataSet)
{
  theDataSet->Update();

  vtkFloatingPointType aBounds[6];
  theDataSet->GetBounds(aBounds);
  vtkFloatingPointType aDelta[3] = {aBounds[1] - aBounds[0], aBounds[3] - aBounds[2], aBounds[5] - aBounds[4]};

  if(aDelta[0] >= aDelta[1] && aDelta[0] >= aDelta[2])
    if(aDelta[1] >= aDelta[2])
      return VISU_CutPlanesPL::XY;
    else
      return VISU_CutPlanesPL::ZX;

  if(aDelta[1] >= aDelta[0] && aDelta[1] >= aDelta[2])
    if(aDelta[0] >= aDelta[2])
      return VISU_CutPlanesPL::XY;
    else
      return VISU_CutPlanesPL::YZ;

  if(aDelta[2] >= aDelta[0] && aDelta[2] >= aDelta[1])
    if(aDelta[0] >= aDelta[1])
      return VISU_CutPlanesPL::ZX;
    else
      return VISU_CutPlanesPL::YZ;

  return VISU_CutPlanesPL::XY;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_Plot3DPL
::GetScaleFactor( VISU_ColoredPL* theColoredPL,
                  vtkDataSet* theDataSet )
{
  theDataSet->Update();
  vtkFloatingPointType aLength = theDataSet->GetLength(); // diagonal length

  vtkFloatingPointType aScalarRange[2];
  theColoredPL->GetSourceRange(aScalarRange);

  static vtkFloatingPointType EPS = 0.3;
  vtkFloatingPointType aRange = aScalarRange[1];
  if(aRange > 0.0)
    return aLength / aRange * EPS;

  return 0.0;
}


//----------------------------------------------------------------------------
void
VISU_Plot3DPL
::Init()
{
  Superclass::Init();

  myOrientation = GetOrientation(GetMergedInput());
  SetScaleFactor( GetScaleFactor( this, GetMergedInput() ) );
}


//----------------------------------------------------------------------------
vtkDataSet*
VISU_Plot3DPL
::InsertCustomPL()
{
  return myAppendPolyData->GetOutput();
}


//----------------------------------------------------------------------------
void
VISU_Plot3DPL
::Update()
{
  vtkDataSet* aMergedInput = GetMergedInput();
  if(VISU::IsQuadraticData(aMergedInput)) // Bug 0020123, note 0005343
    throw std::runtime_error("Impossible to build presentation");

  vtkFloatingPointType aPlaneNormal[3];
  vtkFloatingPointType anOrigin[3];
  GetBasePlane( anOrigin, aPlaneNormal );

  vtkPolyData* aPolyData = 0;
  vtkCutter *aCutPlane = 0;

  if ( !IsPlanarInput() )
  {
    aCutPlane = vtkCutter::New();
    aCutPlane->SetInput(aMergedInput);

    vtkPlane *aPlane = vtkPlane::New();
    aPlane->SetOrigin(anOrigin);
    aPlane->SetNormal(aPlaneNormal);

    aCutPlane->SetCutFunction(aPlane);
    aPlane->Delete();

    aPolyData = aCutPlane->GetOutput();
    aPolyData->Update();
  }

  if ( !aPolyData || aPolyData->GetNumberOfCells() == 0 ) {
    myGeometryFilter->SetInput(aMergedInput);
    aPolyData = myGeometryFilter->GetOutput();
    aPolyData->Update();
  }
  if ( !myIsContour ) // surface prs
  {
    if(VISU::IsDataOnCells(aPolyData)) {
      myCellDataToPointData->SetInput(aPolyData);
      myCellDataToPointData->PassCellDataOn();
      myWarpScalar->SetInput(myCellDataToPointData->GetPolyDataOutput());
    }else
      myWarpScalar->SetInput(aPolyData);
  }
  else // contour prs
  {
    if(VISU::IsDataOnCells(aPolyData)) {
      myCellDataToPointData->SetInput(aPolyData);
      myCellDataToPointData->PassCellDataOn();
      myContourFilter->SetInput(myCellDataToPointData->GetOutput());
    }else
      myContourFilter->SetInput(aPolyData);

    vtkFloatingPointType aScalarRange[2];
    GetSourceRange(aScalarRange);

    myContourFilter->GenerateValues(GetNumberOfContours(),aScalarRange);
    myWarpScalar->SetInput(myContourFilter->GetOutput());
  }

  VISU_CutPlanesPL::ClearAppendPolyData(myAppendPolyData.GetPointer());
  myAppendPolyData->AddInput(myWarpScalar->GetPolyDataOutput());

  if ( aCutPlane )
    aCutPlane->Delete();

  myWarpScalar->SetNormal(aPlaneNormal);

  Superclass::Update();
}


//----------------------------------------------------------------------------
unsigned long int
VISU_Plot3DPL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();

  if(vtkDataObject* aDataObject = myGeometryFilter->GetInput())
    aSize += aDataObject->GetActualMemorySize() * 1024;
  
  if(myCellDataToPointData->GetInput())
    if(vtkDataSet* aDataSet = myCellDataToPointData->GetOutput())
      aSize += aDataSet->GetActualMemorySize() * 1024;

  if(vtkDataObject* aDataObject = myContourFilter->GetInput())
    aSize += aDataObject->GetActualMemorySize() * 1024;

  if(vtkDataObject* aDataObject = myWarpScalar->GetInput())
    aSize += aDataObject->GetActualMemorySize() * 1024;

  int anEnd = myAppendPolyData->GetNumberOfInputConnections(0);
  for(int anId = 0; anId < anEnd; anId++){
    if(vtkDataObject* aDataObject = myAppendPolyData->GetInput(anId))
      aSize += aDataObject->GetActualMemorySize() * 1024;
  }

  return aSize;
}


//----------------------------------------------------------------------------
void
VISU_Plot3DPL
::SetNumberOfContours(int theNumber)
{
  myContourFilter->SetNumberOfContours(theNumber);
}


//----------------------------------------------------------------------------
int
VISU_Plot3DPL
::GetNumberOfContours()
{
  return myContourFilter->GetNumberOfContours();
}


//----------------------------------------------------------------------------
void
VISU_Plot3DPL
::SetScaleFactor(vtkFloatingPointType theScaleFactor)
{
  myScaleFactor = theScaleFactor;
  myWarpScalar->SetScaleFactor(theScaleFactor*myMapScaleFactor);
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_Plot3DPL
::GetScaleFactor()
{
  return myScaleFactor;
}


//----------------------------------------------------------------------------
void
VISU_Plot3DPL::
SetContourPrs(bool theIsContourPrs )
{
  if(myIsContour == theIsContourPrs)
    return;

  myIsContour = theIsContourPrs;
  Modified();
}


//----------------------------------------------------------------------------
bool
VISU_Plot3DPL
::GetIsContourPrs()
{
  return myIsContour;
}


//----------------------------------------------------------------------------
void
VISU_Plot3DPL
::SetPlanePosition(vtkFloatingPointType thePosition,
                   bool theIsRelative)
{
  bool anIsSameValue = VISU::CheckIsSameValue(myIsRelative, theIsRelative);
  anIsSameValue &= (myPosition == thePosition);
  if(anIsSameValue)
    return;

  myIsRelative = theIsRelative;
  myPosition = thePosition;
  Modified();
}


//----------------------------------------------------------------------------
bool
VISU_Plot3DPL
::IsPositionRelative()
{
  return myIsRelative;
}


//----------------------------------------------------------------------------
VISU_CutPlanesPL::PlaneOrientation
VISU_Plot3DPL
::GetPlaneOrientation()
{
  return myOrientation;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_Plot3DPL::
GetRotateX()
{
  switch(myOrientation){
  case VISU_CutPlanesPL::XY: return myAngle[0];
  case VISU_CutPlanesPL::YZ: return myAngle[1];
  case VISU_CutPlanesPL::ZX: return myAngle[2];
  }
  return 0;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_Plot3DPL::
GetRotateY(){
  switch(myOrientation){
  case VISU_CutPlanesPL::XY: return myAngle[1];
  case VISU_CutPlanesPL::YZ: return myAngle[2];
  case VISU_CutPlanesPL::ZX: return myAngle[0];
  }
  return 0;
}


//----------------------------------------------------------------------------
void
VISU_Plot3DPL::
SetOrientation(VISU_CutPlanesPL::PlaneOrientation theOrientation,
               vtkFloatingPointType theXAngle,
               vtkFloatingPointType theYAngle)
{
  bool anIsSameValue = VISU::CheckIsSameValue(GetRotateX(), theXAngle);
  anIsSameValue &= VISU::CheckIsSameValue(GetRotateY(), theYAngle);
  anIsSameValue &= (myOrientation == theOrientation);
  if(anIsSameValue)
    return;

  switch(theOrientation){
  case VISU_CutPlanesPL::XY: myAngle[0] = theXAngle; break;
  case VISU_CutPlanesPL::YZ: myAngle[1] = theXAngle; break;
  case VISU_CutPlanesPL::ZX: myAngle[2] = theXAngle; break;
  }

  switch(theOrientation){
  case VISU_CutPlanesPL::XY: myAngle[1] = theYAngle; break;
  case VISU_CutPlanesPL::YZ: myAngle[2] = theYAngle; break;
  case VISU_CutPlanesPL::ZX: myAngle[0] = theYAngle; break;
  }

  myOrientation = theOrientation;
  Modified();
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_Plot3DPL
::GetPlanePosition()
{
  return myPosition;
}

//=======================================================================
//function : GetBasePlane
//purpose  :
//=======================================================================
void
VISU_Plot3DPL
::GetBasePlane(vtkFloatingPointType theOrigin[3],
               vtkFloatingPointType theNormal[3],
               bool  theCenterOrigine )
{
  VISU_CutPlanesPL::GetDir(theNormal,myAngle,myOrientation);

  vtkFloatingPointType aPosition = myPosition;
  vtkFloatingPointType aBounds[6], aBoundPrj[3];
  if ( myIsRelative )
  {
    GetInput()->GetBounds(aBounds);
    VISU_CutPlanesPL::GetBoundProject(aBoundPrj,aBounds,theNormal);
    aPosition = aBoundPrj[0] + aBoundPrj[2]*myPosition;
  }
  VISU::Mul(theNormal,aPosition,theOrigin);

  if ( theCenterOrigine ) {
    // move theOrigin to the center of aBounds projections to the plane
    GetMergedInput()->GetBounds(aBounds);
    vtkFloatingPointType boundPoints[8][3] = {
      {aBounds[0],aBounds[2],aBounds[4]},
      {aBounds[1],aBounds[2],aBounds[4]},
      {aBounds[0],aBounds[3],aBounds[4]},
      {aBounds[1],aBounds[3],aBounds[4]},
      {aBounds[0],aBounds[2],aBounds[5]},
      {aBounds[1],aBounds[2],aBounds[5]},
      {aBounds[0],aBounds[3],aBounds[5]},
      {aBounds[1],aBounds[3],aBounds[5]}};
    vtkFloatingPointType newOrigin[3] = { 0,0,0 };
    for(int i = 0; i < 8; i++) {
      vtkFloatingPointType proj[3];
      vtkPlane::ProjectPoint( boundPoints[i], theOrigin, theNormal, proj );
      newOrigin[0] += proj[0];
      newOrigin[1] += proj[1];
      newOrigin[2] += proj[2];
    }
    theOrigin[0] = newOrigin[0] / 8.;
    theOrigin[1] = newOrigin[1] / 8.;
    theOrigin[2] = newOrigin[2] / 8.;
  }
}

//=======================================================================
//function : GetMinMaxPosition
//purpose  : return absolute position range
//=======================================================================
void
VISU_Plot3DPL
::GetMinMaxPosition( vtkFloatingPointType& minPos, 
                     vtkFloatingPointType& maxPos )
{
  vtkFloatingPointType aBounds[6], aBoundPrj[3], aNormal[3];
  VISU_CutPlanesPL::GetDir(aNormal,myAngle,myOrientation);
  GetInput()->GetBounds(aBounds);
  VISU_CutPlanesPL::GetBoundProject(aBoundPrj,aBounds,aNormal);
  minPos = aBoundPrj[0];
  maxPos = aBoundPrj[1];
}

//=======================================================================
//function : SetMapScale
//purpose  :
//=======================================================================

void 
VISU_Plot3DPL
::SetMapScale(vtkFloatingPointType theMapScale)
{
  myMapScaleFactor = theMapScale;
  Superclass::SetMapScale(theMapScale);

  if ( myIsContour ) {
    vtkFloatingPointType aRange[2];
    GetSourceRange(aRange);
    vtkFloatingPointType aNewRange[] = { aRange[1] - theMapScale*(aRange[1]-aRange[0]), aRange[1] };
    myContourFilter->GenerateValues(GetNumberOfContours(),aNewRange);
  }
  myWarpScalar->SetScaleFactor(myScaleFactor*theMapScale);

  Modified();
}
