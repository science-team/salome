// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
// File:    VISU_GaussPoints.cxx
// Author:  Alexey PETROV
// Module : VISU
//
#include "VISU_GaussPointsPL.hxx"
#include "VISU_PointSpriteMapperHolder.hxx"
#include "VISU_OpenGLPointSpriteMapper.hxx"
#include "VISU_DeformedShapePL.hxx"
#include "VISU_FieldTransform.hxx"
#include "VISU_LookupTable.hxx"

#include "VISU_PipeLineUtils.hxx"
#include "VISU_AppendFilter.hxx"
#include "VISU_GaussMergeFilter.hxx"

#include <vtkPointSource.h>
#include <vtkElevationFilter.h>
#include <vtkImageGaussianSource.h>
#include <vtkXMLImageDataReader.h>
#include <vtkGeometryFilter.h>
#include <vtkImageData.h>
#include <vtkWarpVector.h>
#include <vtkGlyph3D.h>
#include <vtkSphereSource.h>
#include <vtkPassThroughFilter.h>

//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_GaussPointsPL);

//----------------------------------------------------------------------------
VISU_GaussPointsPL
::VISU_GaussPointsPL():
  myScaleFactor(0.0),
  myMagnificationIncrement(2),
  myAppendFilter(VISU_AppendFilter::New()),
  myMergeFilter(VISU_GaussMergeFilter::New())
{
  SetIsShrinkable(false);
  SetIsFeatureEdgesAllowed(false);

  myWarpVector = vtkWarpVector::New();

  myGlyph = vtkGlyph3D::New();
  myGlyph->SetScaleModeToScaleByScalar();
  myGlyph->SetColorModeToColorByScalar();
  myGlyph->ClampingOn();

  mySphereSource = vtkSphereSource::New();
  mySphereSource->SetThetaResolution( 8 );
  mySphereSource->SetPhiResolution( 8 );
  myGlyph->SetSource( mySphereSource->GetOutput() );

  for(int i = 0; i < 3; i++)
    myPassFilter.push_back(vtkPassThroughFilter::New());

  myPrimitiveType = VISU_OpenGLPointSpriteMapper::PointSprite;    

  myAppendFilter->SetMergingInputs(true);
  myAppendFilter->Delete();

  myMergeFilter->SetMergingInputs(true);
  myMergeFilter->Delete();
}


//----------------------------------------------------------------------------
VISU_GaussPointsPL
::~VISU_GaussPointsPL()
{
  myWarpVector->Delete();

  myGlyph->Delete();

  mySphereSource->Delete();

  for(int i = 0; i < 3; i++)
    myPassFilter[i]->Delete();
}


//----------------------------------------------------------------------------
unsigned long int 
VISU_GaussPointsPL
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();

  aTime = std::max(aTime, myWarpVector->GetMTime());
  aTime = std::max(aTime, myGlyph->GetMTime());
  aTime = std::max(aTime, mySphereSource->GetMTime());
  aTime = std::max(aTime, myAppendFilter->GetMTime());
  aTime = std::max(aTime, myMergeFilter->GetMTime());

  for(int i = 0; i < 3; i++)
    aTime = std::max(aTime, myPassFilter[i]->GetMTime());

  return aTime;
}


//----------------------------------------------------------------------------
void  
VISU_GaussPointsPL
::OnCreateMapperHolder()
{
  myPointSpriteMapperHolder = VISU_PointSpriteMapperHolder::New();
  myPointSpriteMapperHolder->Delete();

  SetMapperHolder(myPointSpriteMapperHolder.GetPointer());
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetGaussPtsIDMapper(const VISU::PGaussPtsIDMapper& theIDMapper)
{
  GetPointSpriteMapperHolder()->SetGaussPtsIDMapper(theIDMapper);
}


//----------------------------------------------------------------------------
const VISU::PGaussPtsIDMapper&  
VISU_GaussPointsPL
::GetGaussPtsIDMapper()
{
  return GetPointSpriteMapperHolder()->GetGaussPtsIDMapper();
}


//----------------------------------------------------------------------------
VISU_PointSpriteMapperHolder*  
VISU_GaussPointsPL
::GetPointSpriteMapperHolder()
{
  GetMapperHolder();

  return myPointSpriteMapperHolder.GetPointer();
}

//----------------------------------------------------------------------------
vtkDataSet*  
VISU_GaussPointsPL
::GetParentMesh()
{
  VISU::TNamedIDMapper* aNamedIDMapper = GetGaussPtsIDMapper()->GetParent();
  return aNamedIDMapper->GetOutput();
}

//----------------------------------------------------------------------------
void
CopyGlyph( vtkGlyph3D* theSource, vtkGlyph3D* theDestination )
{
  vtkFloatingPointType* aSourceRange = theSource->GetRange();
  vtkFloatingPointType* aDestinationRange = theDestination->GetRange();
  if(!VISU::CheckIsSameRange(aDestinationRange, aSourceRange))
    theDestination->SetRange( aSourceRange );

  theDestination->SetScaling( theSource->GetScaling() );
  theDestination->SetClamping( theSource->GetClamping() );
  theDestination->SetScaleMode( theSource->GetScaleMode() );
  theDestination->SetColorMode( theSource->GetColorMode() );
  theDestination->SetScaleFactor( theSource->GetScaleFactor() );
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::DoShallowCopy(VISU_PipeLine *thePipeLine,
                bool theIsCopyInput)
{
  Superclass::DoShallowCopy(thePipeLine, theIsCopyInput);
  VISU_MergedPL::DoShallowCopy(thePipeLine, theIsCopyInput);

  if(VISU_GaussPointsPL *aPipeLine = dynamic_cast<VISU_GaussPointsPL*>(thePipeLine)){
    SetPrimitiveType(aPipeLine->GetPrimitiveType());
    SetBicolor(aPipeLine->GetBicolor());
    SetClamp(aPipeLine->GetClamp());
    SetSize(aPipeLine->GetSize());
    SetMinSize(aPipeLine->GetMinSize());
    SetMaxSize(aPipeLine->GetMaxSize());
    SetMagnification(aPipeLine->GetMagnification());
    SetMagnificationIncrement(aPipeLine->GetMagnificationIncrement());
    SetAlphaThreshold(aPipeLine->GetAlphaThreshold());
    SetResolution(aPipeLine->GetResolution());

    SetIsDeformed( aPipeLine->GetIsDeformed() );
    SetScale( aPipeLine->GetScale() );

    vtkFloatingPointType aRadius = aPipeLine->mySphereSource->GetRadius();
    if(!VISU::CheckIsSameValue(mySphereSource->GetRadius(), aRadius))
      mySphereSource->SetRadius( aRadius );

    CopyGlyph( aPipeLine->myGlyph, this->myGlyph );
  }
}


//----------------------------------------------------------------------------
VISU_OpenGLPointSpriteMapper* 
VISU_GaussPointsPL
::GetPointSpriteMapper()
{
  return GetPointSpriteMapperHolder()->GetPointSpriteMapper();
}


//----------------------------------------------------------------------------
vtkPolyData* 
VISU_GaussPointsPL
::GetPickableDataSet()
{
  return myPassFilter[1]->GetPolyDataOutput();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::Init()
{
  Superclass::Init();

  //SetExtractInside(false);

  vtkDataSet* aDataSet = GetParentMesh();
  vtkFloatingPointType aScaleFactor = VISU_DeformedShapePL::GetScaleFactor( aDataSet );

  vtkFloatingPointType* aScalarRange = GetScalarRange();
  static double EPS = 1.0 / VTK_LARGE_FLOAT;
  if(fabs(aScalarRange[1]) > EPS)
    SetScale( aScaleFactor / aScalarRange[1] );
  else
    SetScale(0.0);
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::Build()
{
  Superclass::Build();

  SetSourceGeometry();

  vtkDataSet* aDataSet = GetFieldTransformFilter()->GetOutput();
  
  myMergeFilter->SetGeometry(myAppendFilter->GetOutput());
  
  myMergeFilter->SetScalars(aDataSet);
  myMergeFilter->SetVectors(aDataSet);
  
  myMergeFilter->RemoveFields();
  myMergeFilter->AddField("VISU_FIELD", aDataSet);
  myMergeFilter->AddField("VISU_POINTS_MAPPER", aDataSet);
  myMergeFilter->AddField("VISU_INPUTS_MAPPER", aDataSet);
  myMergeFilter->AddField("VISU_CELLS_MAPPER", aDataSet);

  myMergeFilter->SetGaussPtsIDMapper(GetGaussPtsIDMapper());
  
  myPassFilter[0]->SetInput(InsertCustomPL());

  myPassFilter[1]->SetInput(myPassFilter[0]->GetOutput());

  // Geometrical Sphere
  myPassFilter[2]->SetInput(myPassFilter[1]->GetOutput());
  
  GetPointSpriteMapper()->SetInput( myPassFilter[2]->GetPolyDataOutput() );

  // Update according the current state
  SetIsDeformed(GetIsDeformed());

  SetPrimitiveType(GetPrimitiveType());
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::Update()
{
  if(IsExternalGeometryUsed() || GetNumberOfGeometry() > 1 ){
    myMergeFilter->Update();
    myPassFilter[0]->SetInput(myMergeFilter->GetOutput());
  }
  else{
    myPassFilter[0]->SetInput(GetFieldTransformFilter()->GetOutput());
  }

  SetAverageCellSize( VISU_DeformedShapePL::GetScaleFactor( GetParentMesh() ) );

  this->UpdateGlyph();

  Superclass::Update();

}


//----------------------------------------------------------------------------
unsigned long int
VISU_GaussPointsPL
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();
  
  if(GetIsDeformed())
    if(vtkDataSet* aDataSet = myWarpVector->GetOutput())
      aSize += aDataSet->GetActualMemorySize() * 1024;

  if(GetPrimitiveType() == VISU_OpenGLPointSpriteMapper::GeomSphere)
    if(vtkDataSet* aDataSet = myGlyph->GetOutput())
      aSize += aDataSet->GetActualMemorySize() * 1024;

  return aSize;
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::UpdateGlyph()
{
  vtkFloatingPointType* aScalarRange = GetScalarRange();

  if( GetPointSpriteMapper()->GetPointSpriteMode() == 0 ) // Results
  {
    myGlyph->ClampingOn();
    myGlyph->SetScaleModeToScaleByScalar();
    myGlyph->SetColorModeToColorByScalar();

    vtkFloatingPointType aScaleFactor = 0.0;
    vtkFloatingPointType aMinSize = GetMinSize();
    vtkFloatingPointType aMaxSize = GetMaxSize();
    if(!VISU::CheckIsSameValue(aMaxSize, aMinSize))
      aScaleFactor = ( aScalarRange[1] - aScalarRange[0] ) / ( aMaxSize - aMinSize );
    
    vtkFloatingPointType aMinRange = aScalarRange[0] - aMinSize * aScaleFactor;
    vtkFloatingPointType aMaxRange = aMinRange + aScaleFactor;
    vtkFloatingPointType aRange[2] = {aMinRange, aMaxRange};

    if(!VISU::CheckIsSameRange(myGlyph->GetRange(), aRange))
      myGlyph->SetRange( aRange );

    if(!VISU::CheckIsSameValue(myGlyph->GetScaleFactor(), 1.0))
      myGlyph->SetScaleFactor( 1.0 );
  }
  else if( GetPointSpriteMapper()->GetPointSpriteMode() == 1 ) // Geometry
  {
    myGlyph->ClampingOff();
    myGlyph->SetScaleModeToDataScalingOff();
    myGlyph->SetColorModeToColorByScale();

    vtkFloatingPointType aScaleFactor = GetSize();
    if(!VISU::CheckIsSameValue(myGlyph->GetScaleFactor(), aScaleFactor))
      myGlyph->SetScaleFactor( aScaleFactor );
  }
  else if( GetPointSpriteMapper()->GetPointSpriteMode() == 2 ) // Outside
  {
    myGlyph->ClampingOff();
    myGlyph->SetScaleModeToDataScalingOff();
    myGlyph->SetColorModeToColorByScalar();

    vtkFloatingPointType aScaleFactor = GetSize();
    if(!VISU::CheckIsSameValue(myGlyph->GetScaleFactor(), aScaleFactor))
      myGlyph->SetScaleFactor( aScaleFactor );
  }

  vtkFloatingPointType aRadius = GetMagnification() * GetAverageCellSize() / 2.0;
  if(!VISU::CheckIsSameValue(mySphereSource->GetRadius(), aRadius))
    mySphereSource->SetRadius( aRadius );
}


//----------------------------------------------------------------------------
VISU::TGaussPointID 
VISU_GaussPointsPL
::GetObjID(vtkIdType theID)
{
  return GetGaussPtsIDMapper()->GetObjID(theID);
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetIsDeformed( bool theIsDeformed )
{
  if(theIsDeformed){
    myWarpVector->SetInput( myPassFilter[0]->GetPolyDataOutput() );
    myPassFilter[1]->SetInput(myWarpVector->GetOutput());
  }else
    myPassFilter[1]->SetInput(myPassFilter[0]->GetOutput());
}

//----------------------------------------------------------------------------
bool
VISU_GaussPointsPL
::GetIsDeformed()
{
  return myPassFilter[1]->GetInput() != myPassFilter[0]->GetOutput();
}

//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetBicolor(bool theBicolor)
{
  if(GetBicolor() == theBicolor)
    return;

  GetMapperTable()->SetBicolor( theBicolor );
  GetBarTable()->SetBicolor( theBicolor );
}

//----------------------------------------------------------------------------
bool
VISU_GaussPointsPL
::GetBicolor()
{
  return GetMapperTable()->GetBicolor();
}

//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetIsColored(bool theIsColored)
{
  GetPointSpriteMapper()->SetPointSpriteMode( theIsColored ? 0 : 1 ); // Results / Geometry
}

//----------------------------------------------------------------------------
bool
VISU_GaussPointsPL
::GetIsColored()
{
  return GetPointSpriteMapper()->GetPointSpriteMode() == 0;
}

//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetPrimitiveType(int thePrimitiveType)
{
  if( thePrimitiveType == VISU_OpenGLPointSpriteMapper::GeomSphere )
  {
    myGlyph->SetInput( myPassFilter[1]->GetOutput() );
    myPassFilter[2]->SetInput(myGlyph->GetOutput());
  }
  else
    myPassFilter[2]->SetInput(myPassFilter[1]->GetOutput());

  GetPointSpriteMapper()->SetPrimitiveType( thePrimitiveType );
  
  myPrimitiveType = thePrimitiveType;
}


//----------------------------------------------------------------------------
int
VISU_GaussPointsPL
::GetPrimitiveType()
{    
  return myPrimitiveType;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetMaximumSupportedSize()
{
  return GetPointSpriteMapper()->GetMaximumSupportedSize();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetClamp(vtkFloatingPointType theClamp)
{
  GetPointSpriteMapper()->SetPointSpriteClamp( theClamp );
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetClamp()
{
  return GetPointSpriteMapper()->GetPointSpriteClamp();
}

//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetSize(vtkFloatingPointType theSize)
{
  GetPointSpriteMapper()->SetPointSpriteSize( theSize );
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetSize()
{
  return GetPointSpriteMapper()->GetPointSpriteSize();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetMinSize(vtkFloatingPointType theMinSize)
{
  GetPointSpriteMapper()->SetPointSpriteMinSize( theMinSize );
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetMinSize()
{
  return GetPointSpriteMapper()->GetPointSpriteMinSize();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetMaxSize(vtkFloatingPointType theMaxSize)
{
  GetPointSpriteMapper()->SetPointSpriteMaxSize( theMaxSize );
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetMaxSize()
{
  return GetPointSpriteMapper()->GetPointSpriteMaxSize();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetMagnification(vtkFloatingPointType theMagnification)
{
  GetPointSpriteMapper()->SetPointSpriteMagnification( theMagnification );
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetMagnification()
{
  return GetPointSpriteMapper()->GetPointSpriteMagnification();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetMagnificationIncrement(vtkFloatingPointType theIncrement)
{
  if(VISU::CheckIsSameValue(myMagnificationIncrement, theIncrement))
    return;

  myMagnificationIncrement = theIncrement;
  Modified();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetAlphaThreshold(vtkFloatingPointType theAlphaThreshold)
{
  GetPointSpriteMapper()->SetPointSpriteAlphaThreshold( theAlphaThreshold );
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetAlphaThreshold()
{
  return GetPointSpriteMapper()->GetPointSpriteAlphaThreshold();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetOpacity(vtkFloatingPointType theOpacity)
{
  GetPointSpriteMapper()->SetPointSpriteOpacity( theOpacity );
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetOpacity()
{
  return GetPointSpriteMapper()->GetPointSpriteOpacity();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetResolution(int theResolution)
{
  mySphereSource->SetThetaResolution( theResolution );
  mySphereSource->SetPhiResolution( theResolution );
}


//----------------------------------------------------------------------------
int
VISU_GaussPointsPL
::GetResolution()
{
  return mySphereSource->GetThetaResolution();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::ChangeMagnification( bool up )
{
  vtkFloatingPointType anIncrement = up ? myMagnificationIncrement : 1.0 / myMagnificationIncrement;
  SetMagnification( GetMagnification() * anIncrement );
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetPointSize(vtkIdType theID, vtkDataArray* theScalarArray)
{
  vtkFloatingPointType aMaxSize = GetAverageCellSize() * GetMaxSize();
  vtkFloatingPointType aMinSize = GetAverageCellSize() * GetMinSize();
  vtkFloatingPointType aDelta = aMaxSize - aMinSize;
  vtkFloatingPointType aVal = theScalarArray->GetTuple1(theID);

  vtkFloatingPointType* aScalarRange = GetScalarRange();
  vtkFloatingPointType aDeltaScalarRange = aScalarRange[1] - aScalarRange[0];

  // to avoid FPE if the minimum is equal to maximum
  if( aDeltaScalarRange < 1.0 / VTK_LARGE_FLOAT )
    return aMinSize;

  return aMinSize + aDelta*(aVal - aScalarRange[0]) / aDeltaScalarRange;
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetMaxPointSize()
{
  return GetAverageCellSize() * GetMaxSize();
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetPointSize(vtkIdType theID)
{
  vtkMapper* aMapper = GetMapper();
  vtkDataSet* aDataSet = aMapper->GetInput();
  vtkPointData* aPointData = aDataSet->GetPointData();
  vtkDataArray* aScalarArray = aPointData->GetScalars();
  return GetPointSize(theID, aScalarArray);
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetAverageCellSize(vtkFloatingPointType theAverageCellSize)
{
  GetPointSpriteMapper()->SetAverageCellSize( theAverageCellSize );
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPointsPL
::GetAverageCellSize()
{
  return GetPointSpriteMapper()->GetAverageCellSize();
}


//----------------------------------------------------------------------------
void
VISU_GaussPointsPL
::SetImageData(vtkImageData* theImageData)
{
  GetPointSpriteMapper()->SetImageData( theImageData );
}


//----------------------------------------------------------------------------
vtkSmartPointer<vtkImageData>
VISU_GaussPointsPL
::MakeTexture( const char* theMainTexture, 
               const char* theAlphaTexture )
{
  if( !theMainTexture || !theAlphaTexture )
    return 0;

  vtkXMLImageDataReader* aMainReader = vtkXMLImageDataReader::New();
  vtkXMLImageDataReader* anAlphaReader = vtkXMLImageDataReader::New();

  aMainReader->SetFileName( theMainTexture );
  anAlphaReader->SetFileName( theAlphaTexture );

  aMainReader->Update();
  anAlphaReader->Update();

  vtkImageData* aMainImageData = aMainReader->GetOutput();
  vtkImageData* anAlphaImageData = anAlphaReader->GetOutput();

  int* aMainImageSize = aMainImageData->GetDimensions();
  int* anAlphaImageSize = anAlphaImageData->GetDimensions();
  if(aMainImageSize[0] != anAlphaImageSize[0] || aMainImageSize[1] != anAlphaImageSize[1])
    return NULL;

  vtkSmartPointer<vtkImageData> aCompositeImageData = vtkImageData::New();
  aCompositeImageData->Delete();

  int aNbCompositeComponents = 4;
  aCompositeImageData->SetDimensions(aMainImageSize);
  aCompositeImageData->SetScalarTypeToUnsignedChar();        
  aCompositeImageData->SetNumberOfScalarComponents(aNbCompositeComponents);
  aCompositeImageData->AllocateScalars();

  unsigned char* aMainDataPtr = (unsigned char*)aMainImageData->GetScalarPointer();
  unsigned char* anAlphaDataPtr = (unsigned char*)anAlphaImageData->GetScalarPointer();
  unsigned char *aCompositeDataPtr = (unsigned char * )aCompositeImageData->GetScalarPointer();

  int aNbMainComponents = aMainImageData->GetNumberOfScalarComponents();
  int aNbAlphaComponents = anAlphaImageData->GetNumberOfScalarComponents();
  int aCompositeSize = aMainImageSize[0] * aMainImageSize[1] * aNbCompositeComponents;
  
  int aMainId = 0, anAlphaId = 0, aCompositeId = 0;
  for(; aCompositeId < aCompositeSize;)
  {
    aCompositeDataPtr[aCompositeId] = aMainDataPtr[aMainId];
    aCompositeDataPtr[aCompositeId + 1] = aMainDataPtr[aMainId + 1];
    aCompositeDataPtr[aCompositeId + 2] = aMainDataPtr[aMainId + 2];
    aCompositeDataPtr[aCompositeId + 3] = anAlphaDataPtr[anAlphaId];

    aMainId += aNbMainComponents;
    anAlphaId += aNbAlphaComponents;
    aCompositeId += aNbCompositeComponents;
  }
  aMainReader->Delete();
  anAlphaReader->Delete();
  aCompositeImageData->Update();

  return aCompositeImageData;
}


void
VISU_GaussPointsPL
::SetScale( vtkFloatingPointType theScale )
{
  if(VISU::CheckIsSameValue(myWarpVector->GetScaleFactor(), theScale))
    return;

  myWarpVector->SetScaleFactor( theScale );
  myScaleFactor = theScale;
}


vtkFloatingPointType 
VISU_GaussPointsPL
::GetScale()
{
  return myWarpVector->GetScaleFactor();
}


void
VISU_GaussPointsPL
::SetMapScale( vtkFloatingPointType theMapScale )
{
  Superclass::SetMapScale( theMapScale );

  vtkFloatingPointType aMapScale = myScaleFactor * theMapScale;
  if(VISU::CheckIsSameValue(myWarpVector->GetScaleFactor(), aMapScale))
    return;

  myWarpVector->SetScaleFactor( aMapScale );
}

void
VISU_GaussPointsPL
::SetSourceGeometry()
{
  if(IsExternalGeometryUsed()){
    ClearGeometry();
    myAppendFilter->AddInput(GetFieldTransformFilter()->GetOutput());
  }
}

vtkDataSet* 
VISU_GaussPointsPL
::InsertCustomPL()
{
  return GetMergedInput();
}

int
VISU_GaussPointsPL
::AddGeometry(vtkDataSet* theGeometry, const VISU::TName& theGeomName)
{
  // Fix for issue 0020167 (like in VISU_ScalarMapPL)
  if(!IsExternalGeometryUsed())
    ClearGeometry();
  AddGeometryName(theGeomName);
  myAppendFilter->AddInput(theGeometry);
  return GetNumberOfGeometry();
}

vtkDataSet*
VISU_GaussPointsPL
::GetGeometry(int theGeomNumber, VISU::TName& theGeomName)
{
  theGeomName = GetGeometryName(theGeomNumber);
  return vtkDataSet::SafeDownCast(myAppendFilter->GetInput(theGeomNumber));
}

int
VISU_GaussPointsPL
::GetNumberOfGeometry()
{
  return myAppendFilter->GetNumberOfInputConnections(0);
}

bool
VISU_GaussPointsPL
::IsExternalGeometryUsed()
{
  return myAppendFilter->GetInput() != GetFieldTransformFilter()->GetOutput();
}

void
VISU_GaussPointsPL
::ClearGeometry()
{
  ClearGeometryNames();
  myAppendFilter->RemoveAllInputs();
}

void
VISU_GaussPointsPL
::GetSourceRange(vtkFloatingPointType theRange[2])
{
  if(!IsExternalGeometryUsed())
    Superclass::GetSourceRange(theRange);
  else
    GetMergedInput()->GetScalarRange( theRange );
}

vtkPointSet* 
VISU_GaussPointsPL
::GetMergedInput()
{
  if(myMergeFilter->GetInput())
    myMergeFilter->Update();
  return myMergeFilter->GetOutput();
}
