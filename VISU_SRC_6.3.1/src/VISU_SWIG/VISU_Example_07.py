#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# Manage view parameters of presentations:
# Representation Mode, Shrink, Shading, Opacity, Line Width
#
import salome

import VISU
import visu_gui

import os
import time

datadir = os.getenv("DATA_DIR") + "/MedFiles/"

myVisu = visu_gui.myVisu
myVisu.SetCurrentStudy(salome.myStudy)
myViewManager = myVisu.GetViewManager()
myView = myViewManager.Create3DView()

sleep_delay = 1

def AfterSet(error_string, method_name, old_value, new_value):
    print method_name, "(): old_value = ", old_value, "new_value  = ", new_value
    if error_string == "":
        time.sleep(sleep_delay)
    else:
        print method_name, "() error = ", error_string
        pass
    pass

def ChangeRepresentation(scalarmap,repres,shrink,shading,opacity,linew):
    if scalarmap is None : print "Error"
    else : print "OK"

    myView.DisplayOnly(scalarmap)
    myView.FitAll()

    time.sleep(sleep_delay)

    # enum PresentationType{ POINT, WIREFRAME, SHADED, INSIDEFRAME, SURFACEFRAME, SHRINK }
    old_prs_type = myView.GetPresentationType(scalarmap)
    if old_prs_type != repres:
        err_str = myView.SetPresentationType(scalarmap, repres)
        AfterSet(err_str, "SetPresentationType", old_prs_type, repres)
        pass

    old_is_shrinked = myView.IsShrinked(scalarmap)
    if old_is_shrinked != shrink:
        err_str = myView.SetShrinked(scalarmap, shrink) # 1 - shrinked, 0 - not shrinked
        AfterSet(err_str, "SetShrinked", old_is_shrinked, shrink)
        pass

    old_is_shaded = myView.IsShaded(scalarmap)
    if old_is_shaded != shading:
        err_str = myView.SetShaded(scalarmap, shading) # 1 - shaded, 0 - not shaded
        AfterSet(err_str, "SetShaded", old_is_shaded, shading)
        pass

    old_opacity = myView.GetOpacity(scalarmap)
    if old_opacity != opacity:
        err_str = myView.SetOpacity(scalarmap, opacity) # double value [0, 1]
        AfterSet(err_str, "SetOpacity", old_opacity, opacity)
        pass

    old_linew = myView.GetLineWidth(scalarmap)
    if old_linew != linew:
        err_str = myView.SetLineWidth(scalarmap, linew) # double value, recommended round [1, 10]
        AfterSet(err_str, "SetLineWidth", old_linew, linew)
        pass

    print ""
    pass

# ResOK_0000.med

print 'Import "ResOK_0000.med"...............',
medFile = datadir + "ResOK_0000.med"
myMeshName = 'dom'
myFieldName = 'vitesse'

myResult = myVisu.ImportFile(medFile)
if myResult is None : print "Error"
else : print "OK"

print "Creating Scalar Map.......",
scmap = myVisu.ScalarMapOnField(myResult,myMeshName,VISU.NODE,myFieldName,1);
ChangeRepresentation(scmap, VISU.INSIDEFRAME, 1, 0, 0.3, 5)

print "Creating Stream Lines.......",
scmap = myVisu.StreamLinesOnField(myResult,myMeshName,VISU.NODE,myFieldName,1);
ChangeRepresentation(scmap, VISU.WIREFRAME, 1, 1, 0.5, 3)

print "Creating Vectors..........",
scmap = myVisu.VectorsOnField(myResult,myMeshName,VISU.NODE,myFieldName,1);
ChangeRepresentation(scmap, VISU.WIREFRAME, 1, 1, 0.7, 2)

print "Creating Iso Surfaces.....",
scmap = myVisu.IsoSurfacesOnField(myResult,myMeshName,VISU.NODE,myFieldName,1);
ChangeRepresentation(scmap, VISU.SHADED, 1, 0, 0.4, 8)

print "Creating Cut Planes.......",
scmap = myVisu.CutPlanesOnField(myResult,myMeshName,VISU.NODE,myFieldName,1);
ChangeRepresentation(scmap, VISU.POINT, 0, 0, 0.6, 4)

# fra.med

print 'Import "fra.med"...............',
medFile = datadir + "fra.med"
myMeshName = 'LE VOLUME'
myFieldName = 'VITESSE'

myResult = myVisu.ImportFile(medFile)
if myResult is None : print "Error"
else : print "OK"

print "Creating Scalar Map.......",
scmap = myVisu.ScalarMapOnField(myResult,myMeshName,VISU.NODE,myFieldName,1);
ChangeRepresentation(scmap, VISU.WIREFRAME, 1, 1, 0.5, 3)

print "Creating Iso Surfaces.....",
scmap = myVisu.IsoSurfacesOnField(myResult,myMeshName,VISU.NODE,myFieldName,1);
ChangeRepresentation(scmap, VISU.WIREFRAME, 1, 1, 0.5, 3)

print "Creating Cut Planes.......",
scmap = myVisu.CutPlanesOnField(myResult,myMeshName,VISU.NODE,myFieldName,1);
ChangeRepresentation(scmap, VISU.SHADED, 1, 1, 0.5, 3)

print "Creating Scalar Map On Deformed Shape.......",
scmap = myVisu.DeformedShapeAndScalarMapOnField(myResult,myMeshName,VISU.NODE,myFieldName,1);
ChangeRepresentation(scmap, VISU.SHADED, 1, 1, 0.5, 3)

print "Creating Deformed Shape.......",
scmap = myVisu.DeformedShapeOnField(myResult,myMeshName,VISU.NODE,myFieldName,1)
scmap.ShowColored(1)
ChangeRepresentation(scmap, VISU.WIREFRAME, 1, 1, 0.5, 3)

print "Creating Cut Lines.......",
scmap = myVisu.CutLinesOnField(myResult,myMeshName,VISU.NODE,myFieldName,1)
ChangeRepresentation(scmap, VISU.WIREFRAME, 1, 1, 0.5, 3)

print "Creating Plot 3D.......",
scmap = myVisu.Plot3DOnField(myResult,myMeshName,VISU.NODE,myFieldName,1)
ChangeRepresentation(scmap, VISU.SHADED, 1, 1, 0.5, 3)
