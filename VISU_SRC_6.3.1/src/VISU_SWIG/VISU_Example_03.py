#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# Animation of "vitesse" field, stored in file TimeStamps.med
# This script is equivalent to non-regression test script 003/A5
#
import salome
import visu_gui
import SALOMEDS
import VISU
import os
import time

medFile = os.getenv("DATA_DIR") + "/MedFiles/TimeStamps.med"

print 'Importing "TimeStamps.med"................',
myVisu = visu_gui.myVisu
myVisu.SetCurrentStudy(salome.myStudy)
myResult = myVisu.ImportFile(medFile)
if myResult is None : print "Error"
else : print "OK"

print "Creating Cut Lines........................",
medMesh = 'dom'
medField = "vitesse"
aCutLines = myVisu.CutLinesOnField(myResult,'dom',VISU.NODE,medField,1)
aCutLines.SetOrientation(VISU.CutPlanes.XY, 0, 0)
aCutLines.SetOrientation2(VISU.CutPlanes.ZX, 0, 0)
aCutLines.SetNbLines(20)
if aCutLines is None : print "Error"
else : print "OK"

print "Creating a Viewer.........................",
myViewManager = myVisu.GetViewManager();
myView = myViewManager.Create3DView();
if myView is None : print "Error"
else : print "OK"
myView.Display(aCutLines);
myView.FitAll();

aCutLinesSObj = salome.myStudy.FindObjectIOR(aCutLines.GetID())
aFather = aCutLinesSObj.GetFather().GetFather();

print "Creating an Animation.....................",
myAnim = myVisu.CreateAnimation(myView); 
if myAnim is None : print "Error"
else : print "OK"

print "Animation.................................",
myAnim.addField(aFather);
myAnim.generatePresentations(0);
myAnim.generateFrames();
myAnim.setSpeed(33)
myAnim.startAnimation();

myView.FitAll()
while 1:
    time.sleep(1)
    if not myAnim.isRunning():
        myAnim.stopAnimation()
        break

print "OK"
