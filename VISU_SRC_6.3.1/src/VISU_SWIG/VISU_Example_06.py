#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# Import a table from file and show it in Plot2d viewer
#
import salome
import math
import SALOMEDS
import VISU
from visu_gui import *

# >>> Getting study builder ==================================================
myStudy = salome.myStudy
myBuilder = myStudy.NewBuilder()

# >>> Getting (loading) VISU component =======================================
myVisu = salome.lcc.FindOrLoadComponent("FactoryServer", "VISU")
myComponent = myStudy.FindComponent("VISU")
myVisu.SetCurrentStudy(myStudy)
if not myComponent:
   myComponent = myBuilder.NewComponent("VISU")
   aName = myBuilder.FindOrCreateAttribute(myComponent, "AttributeName")
   #aName.SetValue("Visu")
   aName.SetValue( salome.sg.getComponentUserName("VISU") )
   
   A2 = myBuilder.FindOrCreateAttribute(myComponent, "AttributePixMap");
   aPixmap = A2._narrow(SALOMEDS.AttributePixMap);
   aPixmap.SetPixMap( "ICON_OBJBROWSER_Visu" );
   
   myBuilder.DefineComponentInstance(myComponent,myVisu)

# >>> Import a tables from a file ============================================
aFileName = os.getenv("DATA_DIR") + "/Tables/tables_test.xls"
sobj = myVisu.ImportTables(aFileName, False)

# >>> Create container and insert curves =====================================
myContainer = myVisu.CreateContainer()

chiter = myStudy.NewChildIterator(sobj)
while chiter.More():
  sobj_table = chiter.Value()

  # >>> Create Visu table ====================================================
  myVisuTableReal = myVisu.CreateTable(sobj_table.GetID())

  nbRows = myVisuTableReal.GetNbRows()

  # >>> Create curves ========================================================
  for i in range(1, nbRows):
    myCurve = myVisu.CreateCurve(myVisuTableReal, 1, i+1)
    myContainer.AddCurve(myCurve)

  chiter.Next()

# >>> Updating Object Browser ================================================
salome.sg.updateObjBrowser(1)

# >>> Display curves in Plot2d viewer ========================================
myViewManager = myVisu.GetViewManager();
myView = myViewManager.CreateXYPlot();
myView.SetTitle("The viewer for Curves from the Table")
myView.Display(myContainer)

# ============================================================================
