// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_IsoSurfaces_i.hh"
#include "VISU_Prs3dUtils.hh"

#include "VISU_IsoSurfacesPL.hxx"
#include "VISU_Result_i.hh"
#include "VISU_Actor.h"
#include "VISU_ScalarMapAct.h"
#include "VISU_IsoSurfActor.h"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"
#include <vtkMapper.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//---------------------------------------------------------------
size_t
VISU::IsoSurfaces_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity,
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber,
	     bool theIsMemoryCheck)
{
  return TSuperClass::IsPossible(theResult,
				 theMeshName,
				 theEntity,
				 theFieldName,
				 theTimeStampNumber,
				 theIsMemoryCheck);
}

//---------------------------------------------------------------
int VISU::IsoSurfaces_i::myNbPresent = 0;

//---------------------------------------------------------------
QString VISU::IsoSurfaces_i::GenerateName()
{ 
  return VISU::GenerateName("IsoSurfaces",myNbPresent++);
}

//---------------------------------------------------------------
const string VISU::IsoSurfaces_i::myComment = "ISOSURFACES";

//---------------------------------------------------------------
const char* 
VISU::IsoSurfaces_i
::GetComment() const
{ 
  return myComment.c_str();
}

//---------------------------------------------------------------
const char*
VISU::IsoSurfaces_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_ISO_SURFACES";
  else
    return "ICON_TREE_ISO_SURFACES_GROUPS";
}

//---------------------------------------------------------------
VISU::IsoSurfaces_i
::IsoSurfaces_i(EPublishInStudyMode thePublishInStudyMode) :
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  MonoColorPrs_i(thePublishInStudyMode),
  myIsoSurfacesPL(NULL),
  myIsLabeled(false),
  myNbLabels(3)
{}


//---------------------------------------------------------------
VISU::Storable* 
VISU::IsoSurfaces_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  VISU::Storable* aRes = TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
  myIsColored = true;
  myColor.R = myColor.G = myColor.B = 0.;
  //myIsLabeled = false;
  //myNbLabels = 3;
  return aRes;
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::IsoSurfaces_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetNbSurfaces(VISU::Storable::FindValue(theMap,"myNbSurface").toInt());
  float aMin = VISU::Storable::FindValue(theMap,"myRange[0]").toDouble();
  float aMax = VISU::Storable::FindValue(theMap,"myRange[1]").toDouble();
  myNbLabels = VISU::Storable::FindValue(theMap,"myNbLabels").toInt();
  myIsLabeled = VISU::Storable::FindValue(theMap,"myIsLabeled").toInt();
  SetSubRange(aMin,aMax);

  SetSubRangeFixed(VISU::Storable::FindValue(theMap,"myIsRangeFixed").toInt());
    
  return this;
}


//---------------------------------------------------------------
void
VISU::IsoSurfaces_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myNbSurface", int(GetNbSurfaces()) );
  Storable::DataToStream( theStr, "myRange[0]", GetSubMin() );
  Storable::DataToStream( theStr, "myRange[1]", GetSubMax() );
  Storable::DataToStream( theStr, "myNbLabels", myNbLabels );
  Storable::DataToStream( theStr, "myIsLabeled", myIsLabeled );
  Storable::DataToStream( theStr, "myIsRangeFixed", IsSubRangeFixed() );
}

//---------------------------------------------------------------
void VISU::IsoSurfaces_i::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);

  if(const IsoSurfaces_i* aPrs3d = dynamic_cast<const IsoSurfaces_i*>(theOrigin)){
    IsoSurfaces_i* anOrigin = const_cast<IsoSurfaces_i*>(aPrs3d);
    ShowLabels(anOrigin->IsLabeled(), anOrigin->GetNbLabels());
  }
}



//---------------------------------------------------------------
VISU::IsoSurfaces_i
::~IsoSurfaces_i()
{
  if(MYDEBUG) MESSAGE("IsoSurfaces_i::~IsoSurfaces_i()");
}


//---------------------------------------------------------------
void 
VISU::IsoSurfaces_i
::SetNbSurfaces(CORBA::Long theNb)
{
  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_IsoSurfacesPL, int>
		   (GetSpecificPL(), &VISU_IsoSurfacesPL::SetNbParts, theNb));
}

//---------------------------------------------------------------
CORBA::Long 
VISU::IsoSurfaces_i
::GetNbSurfaces()
{
  return myIsoSurfacesPL->GetNbParts();
}


//---------------------------------------------------------------
void
VISU::IsoSurfaces_i
::SetSubRange(CORBA::Double theMin, CORBA::Double theMax)
{ 
  VISU::TSetModified aModified(this);

  bool isForced = false;
  vtkFloatingPointType aRange[2] = {theMin, theMax};
  ProcessVoidEvent(new TVoidMemFun2ArgEvent<VISU_IsoSurfacesPL, vtkFloatingPointType*, bool>
		   (GetSpecificPL(), &VISU_IsoSurfacesPL::SetRange, aRange, isForced));
}

//---------------------------------------------------------------
CORBA::Double 
VISU::IsoSurfaces_i
::GetSubMin()
{
  return myIsoSurfacesPL->GetMin();
}

//---------------------------------------------------------------
CORBA::Double 
VISU::IsoSurfaces_i
::GetSubMax()
{
  return myIsoSurfacesPL->GetMax();
}

//---------------------------------------------------------------
void
VISU::IsoSurfaces_i
::SetSubRangeFixed(CORBA::Boolean theIsFixed)
{
  VISU::TSetModified aModified(this);
  
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_IsoSurfacesPL, bool>
                   (GetSpecificPL(), &VISU_IsoSurfacesPL::SetRangeFixed, theIsFixed));
}

//---------------------------------------------------------------
CORBA::Boolean
VISU::IsoSurfaces_i
::IsSubRangeFixed()
{
  return myIsoSurfacesPL->IsRangeFixed();
}


//---------------------------------------------------------------
void 
VISU::IsoSurfaces_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){
    myIsoSurfacesPL = VISU_IsoSurfacesPL::New();
  }else
    myIsoSurfacesPL = dynamic_cast<VISU_IsoSurfacesPL*>(thePipeLine);

  TSuperClass::CreatePipeLine(myIsoSurfacesPL);
}


//----------------------------------------------------------------------------
void 
VISU::IsoSurfaces_i
::DoSetInput(bool theIsInitilizePipe, bool theReInit)
{
  TSuperClass::DoSetInput(theIsInitilizePipe, theReInit);
  if(theIsInitilizePipe || (!IsTimeStampFixed() && !IsRangeFixed()) || theReInit)
    SetSubRange(GetSourceMin(), GetSourceMax());
}

//---------------------------------------------------------------
bool
VISU::IsoSurfaces_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}

//---------------------------------------------------------------
VISU_Actor* VISU::IsoSurfaces_i::CreateActor()
{
  VISU_IsoSurfActor* anActor = VISU_IsoSurfActor::New();
  try{
    VISU::Prs3d_i::CreateActor(anActor);
    anActor->SetBarVisibility(true);
    anActor->SetVTKMapping(true);
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int  aDispMode = aResourceMgr->integerValue("VISU" , "iso_surfaces_represent", 2);
    bool toUseShading = aResourceMgr->booleanValue("VISU", "represent_shading", false);
    anActor->SetRepresentation(aDispMode);
    anActor->SetShading(toUseShading);
    UpdateActor(anActor);
  }catch(...){
    anActor->Delete();
    throw ;
  }

 return anActor;
}

//---------------------------------------------------------------
void VISU::IsoSurfaces_i::UpdateActor(VISU_ActorBase* theActor) 
{
  if(VISU_IsoSurfActor* anActor = dynamic_cast<VISU_IsoSurfActor*>(theActor)){
    anActor->SetLinesLabeled(myIsLabeled, myNbLabels);
  }
  TSuperClass::UpdateActor(theActor);
}

//---------------------------------------------------------------
void
VISU::IsoSurfaces_i
::SetMapScale(double theMapScale)
{
  myIsoSurfacesPL->SetMapScale(theMapScale);
}

//---------------------------------------------------------------
CORBA::Boolean VISU::IsoSurfaces_i::IsLabeled()
{
  return myIsLabeled;
}

//---------------------------------------------------------------
void VISU::IsoSurfaces_i::ShowLabels(CORBA::Boolean theShow, CORBA::Long theNb)
{
  if ((myIsLabeled == theShow) && (myNbLabels == theNb)) return;
  VISU::TSetModified aModified(this);
  myIsLabeled = theShow;
  myNbLabels = theNb;
  myParamsTime.Modified();
}

//---------------------------------------------------------------
CORBA::Long VISU::IsoSurfaces_i::GetNbLabels()
{
  return myNbLabels;
}

