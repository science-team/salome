// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   :
//  Author :
//  Module :
//
#include "VISU_GaussPoints_i.hh"
#include "VISU_ScalarMap_i.hh"
#include "VISU_Prs3dUtils.hh"

#include "VISU_Result_i.hh"
#include "VISU_GaussPtsAct.h"
#include "VISU_GaussPointsPL.hxx"
#include "VISU_GaussPtsDeviceActor.h"

#include "VISU_OpenGLPointSpriteMapper.hxx"
#include "VISU_ScalarBarCtrl.hxx"
#include <VISU_ScalarBarActor.hxx>
#include "VISU_LookupTable.hxx"
#include "VISU_MedConvertor.hxx"

#include "SUIT_ResourceMgr.h"

#include "VISU_PipeLineUtils.hxx"

#include <vtkImageData.h>
#include <vtkXMLImageDataReader.h>
#include <vtkTextProperty.h>
#include <vtkProperty.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

static int INCMEMORY = 10;

using namespace std;

//----------------------------------------------------------------------------
size_t
VISU::GaussPoints_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity,
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber,
	     bool theIsMemoryCheck)
{
  size_t aResult = 0;
  if(theEntity == VISU::NODE)
    return aResult;
  try{
    bool anIsEstimated = true;
    VISU::Result_i::PInput anInput = theResult->GetInput(theMeshName,
							 theEntity,
							 theFieldName,
							 theTimeStampNumber);
    if(!dynamic_cast<const VISU_MedConvertor*>(anInput.get()))
      return aResult;
    size_t aSize = anInput->GetTimeStampOnGaussPtsSize(theMeshName,
						       VISU::TEntity(theEntity),
						       theFieldName,
						       theTimeStampNumber,
						       anIsEstimated);
      aResult = 1;
    if(theIsMemoryCheck){
      if(anIsEstimated)
	aSize *= INCMEMORY;
      aResult = VISU_PipeLine::CheckAvailableMemory(aSize);
      if(MYDEBUG) 
	MESSAGE("GaussPoints_i::IsPossible - CheckAvailableMemory = "<<float(aSize)<<"; aResult = "<<aResult);
    }
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  return aResult;
}

//----------------------------------------------------------------------------
int VISU::GaussPoints_i::myNbPresent = 0;

QString 
VISU::GaussPoints_i
::GenerateName()
{
  return VISU::GenerateName("Gauss Points",myNbPresent++);
}

//----------------------------------------------------------------------------
const string VISU::GaussPoints_i::myComment = "GAUSSPOINTS";

const char* 
VISU::GaussPoints_i
::GetComment() const 
{ 
  return myComment.c_str();
}


//---------------------------------------------------------------
const char*
VISU::GaussPoints_i
::GetIconName()
{
  return "ICON_TREE_GAUSS_POINTS";
}  


//----------------------------------------------------------------------------
VISU::GaussPoints_i
::GaussPoints_i(EPublishInStudyMode thePublishInStudyMode) : 
  ColoredPrs3d_i(thePublishInStudyMode),
  myGaussPointsPL(NULL),
  myColor(Qt::blue),
  myIsActiveLocalScalarBar(true),
  myIsDispGlobalScalarBar(true),
  mySpacing(0.01),
  myFaceLimit(50000),
  myShowBar(true)
{
  if(MYDEBUG) MESSAGE("GaussPoints_i::GaussPoints_i - this = "<<this);
}


//----------------------------------------------------------------------------
VISU::Storable* 
VISU::GaussPoints_i
::Create(const std::string& theMeshName, 
	   VISU::Entity theEntity,
	   const std::string& theFieldName, 
	   CORBA::Long theTimeStampNumber)
{
  TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  int anActiveLocal = aResourceMgr->integerValue( "VISU", "scalar_bar_active_local", GetIsActiveLocalScalarBar() );
  SetIsActiveLocalScalarBar( anActiveLocal == 0 );

  bool aDisplayGlobal = aResourceMgr->booleanValue( "VISU", "scalar_bar_diplay_global", GetIsDispGlobalScalarBar() );
  SetIsDispGlobalScalarBar( aDisplayGlobal );

  int aBicolor = aResourceMgr->integerValue( "VISU", "scalar_bar_bicolor", GetBiColor() );
  SetBiColor( aBicolor == 0 );

  vtkFloatingPointType aSpacing = aResourceMgr->doubleValue( "VISU", "scalar_bar_spacing", GetSpacing() );
  SetSpacing( aSpacing );

  vtkFloatingPointType aScaleFactor = aResourceMgr->doubleValue( "VISU", "deformed_shape_scale_factor", GetScaleFactor() );
  SetScaleFactor( aScaleFactor );

  int aPrimitiveType = aResourceMgr->integerValue( "VISU", "point_sprite_primitive_type", GetPrimitiveType() );
  SetPrimitiveType( VISU::GaussPoints::PrimitiveType(aPrimitiveType) );

  vtkFloatingPointType aClamp = aResourceMgr->doubleValue( "VISU", "point_sprite_clamp", GetClamp() );
  SetClamp( aClamp );

  int aMinSize = aResourceMgr->integerValue( "VISU", "point_sprite_min_size", ( int )( GetMinSize() * 100.0 ) );
  SetMinSize( aMinSize / 100.0 );

  int aMaxSize = aResourceMgr->integerValue( "VISU", "point_sprite_max_size", ( int )( GetMaxSize() * 100.0 ) );
  SetMaxSize( aMaxSize / 100.0 );

  int aGeomSize = aResourceMgr->integerValue( "VISU", "point_sprite_size", ( int )( GetGeomSize() * 100.0 ) );
  SetGeomSize( aGeomSize / 100.0 );

  int aMagnification = aResourceMgr->integerValue( "VISU", "point_sprite_magnification", ( int )( GetMagnification() * 100.0 ) );
  SetMagnification( aMagnification / 100.0 );

  vtkFloatingPointType anIncrement = aResourceMgr->doubleValue( "VISU", "point_sprite_increment", GetMagnificationIncrement() );
  SetMagnificationIncrement( anIncrement );

  bool isColored = aResourceMgr->booleanValue( "VISU", "point_sprite_results", GetIsColored() );
  SetIsColored( isColored );

  QColor aColor = aResourceMgr->colorValue( "VISU", "point_sprite_color", GetQColor() );
  SetQColor( aColor );

  vtkFloatingPointType anAlphaThreshold = aResourceMgr->doubleValue( "VISU", "point_sprite_alpha_threshold", GetAlphaThreshold() );
  SetAlphaThreshold( anAlphaThreshold );

  int aResolution = aResourceMgr->integerValue( "VISU", "geom_sphere_resolution", GetResolution() );
  SetResolution( aResolution );

  int aFaceLimit = aResourceMgr->integerValue( "VISU", "geom_sphere_face_limit", GetFaceLimit() );
  SetFaceLimit( aFaceLimit );

  QString aMainTexture = QString( getenv( "VISU_ROOT_DIR") ) + "/share/salome/resources/visu/sprite_texture.bmp";
  aMainTexture = aResourceMgr->stringValue( "VISU", "point_sprite_main_texture", aMainTexture );

  QString anAlphaTexture = QString( getenv( "VISU_ROOT_DIR") ) + "/share/salome/resources/visu/sprite_alpha.bmp";
  anAlphaTexture = aResourceMgr->stringValue( "VISU", "point_sprite_alpha_texture", anAlphaTexture );

  SetTextures( aMainTexture.toLatin1().data(), anAlphaTexture.toLatin1().data() );
  
  myShowBar = true;

  return this;
}


//---------------------------------------------------------------
void 
VISU::GaussPoints_i
::SameAs(const Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);

  if(const GaussPoints_i* aPrs3d = dynamic_cast<const GaussPoints_i*>(theOrigin)){
    GaussPoints_i* anOrigin = const_cast<GaussPoints_i*>(aPrs3d);

    SetIsActiveLocalScalarBar(anOrigin->GetIsActiveLocalScalarBar());
    SetIsDispGlobalScalarBar(anOrigin->GetIsDispGlobalScalarBar());

    SetSpacing(anOrigin->GetSpacing());

    SetFaceLimit(anOrigin->GetFaceLimit());

    SetColor(anOrigin->GetColor());

    SetBarVisible(anOrigin->IsBarVisible());

    SetTextures(anOrigin->GetMainTexture(), anOrigin->GetAlphaTexture());
  }
}

//----------------------------------------------------------------------------
CORBA::Float
VISU::GaussPoints_i
::GetMemorySize()
{
  return TSuperClass::GetMemorySize();
}

//----------------------------------------------------------------------------
VISU::Storable* 
VISU::GaussPoints_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  // Check if the icon needs to be updated, update if necessary
  SALOMEDS::Study_var aStudy = theSObject->GetStudy();
  SALOMEDS::StudyBuilder_var aStudyBuilder = aStudy->NewBuilder();
  SALOMEDS::GenericAttribute_var anAttr = 
    aStudyBuilder->FindOrCreateAttribute(theSObject, "AttributePixMap");
  SALOMEDS::AttributePixMap_var aPixmap = SALOMEDS::AttributePixMap::_narrow(anAttr);

  CORBA::String_var aPixMapName = aPixmap->GetPixMap();
  if(strcmp(GetIconName(), aPixMapName.in()) != 0)
    aPixmap->SetPixMap(GetIconName());
    
  QString aVal = VISU::Storable::FindValue(theMap,"myShowBar", "1");
  SetBarVisible((aVal.toInt() == 1)? true : false);

  SetIsActiveLocalScalarBar(Storable::FindValue(theMap,"myIsActiveLocalScalarBar").toInt());
  SetIsDispGlobalScalarBar(Storable::FindValue(theMap,"myIsDispGlobalScalarBar").toInt());
  SetBiColor(Storable::FindValue(theMap,"myIsBiColor").toInt());
  SetSpacing(Storable::FindValue(theMap,"mySpacing").toDouble());

  int aPrimitiveType = Storable::FindValue(theMap,"myPrimitiveType").toInt();
  SetPrimitiveType(VISU::GaussPoints::PrimitiveType(aPrimitiveType));
  SetClamp(Storable::FindValue(theMap,"myClamp").toDouble());
  SetMinSize(Storable::FindValue(theMap,"myMinSize").toDouble());
  SetMaxSize(Storable::FindValue(theMap,"myMaxSize").toDouble());
  SetGeomSize(Storable::FindValue(theMap,"myGeomSize").toDouble());

  SetMagnification(Storable::FindValue(theMap,"myMagnification").toDouble());
  SetMagnificationIncrement(Storable::FindValue(theMap,"myMagnificationIncrement").toDouble());

  SetIsDeformed(Storable::FindValue(theMap,"myIsDeformed").toInt());
  SetScaleFactor(Storable::FindValue(theMap,"myScaleFactor").toDouble());

  SetFaceLimit(Storable::FindValue(theMap,"myFaceLimit").toInt());

  SetIsColored(Storable::FindValue(theMap,"myIsColored").toInt());
  int aRed = Storable::FindValue(theMap,"myColor.R").toInt();
  int aGreen = Storable::FindValue(theMap,"myColor.G").toInt();
  int aBlue = Storable::FindValue(theMap,"myColor.B").toInt();
  SetQColor( QColor(aRed, aGreen, aBlue) );

  SetAlphaThreshold(Storable::FindValue(theMap,"myAlphaThreshold").toDouble());

  SetTextures( Storable::FindValue(theMap,"myMainTexture").toLatin1().data(),
	       Storable::FindValue(theMap,"myAlphaTexture").toLatin1().data() );

  SetResolution(Storable::FindValue(theMap,"myResolution").toInt());

  return this;
}


//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myIsActiveLocalScalarBar", myIsActiveLocalScalarBar );
  Storable::DataToStream( theStr, "myIsDispGlobalScalarBar", myIsDispGlobalScalarBar );
  Storable::DataToStream( theStr, "myIsBiColor", GetBiColor() );
  Storable::DataToStream( theStr, "mySpacing", GetSpacing() );

  Storable::DataToStream( theStr, "myPrimitiveType", GetPrimitiveType() );
  Storable::DataToStream( theStr, "myClamp", GetClamp() );
  Storable::DataToStream( theStr, "myMinSize", GetMinSize() );
  Storable::DataToStream( theStr, "myMaxSize", GetMaxSize() );
  Storable::DataToStream( theStr, "myGeomSize", GetGeomSize() );

  Storable::DataToStream( theStr, "myMagnification", GetMagnification() );
  Storable::DataToStream( theStr, "myMagnificationIncrement", GetMagnificationIncrement() );

  Storable::DataToStream( theStr, "myIsDeformed", GetIsDeformed() );
  Storable::DataToStream( theStr, "myScaleFactor", GetScaleFactor() );

  Storable::DataToStream( theStr, "myFaceLimit", GetFaceLimit() );

  Storable::DataToStream( theStr, "myIsColored", GetIsColored() );
  QColor aColor = GetQColor();
  Storable::DataToStream( theStr, "myColor.R", aColor.red() );
  Storable::DataToStream( theStr, "myColor.G", aColor.green() );
  Storable::DataToStream( theStr, "myColor.B", aColor.blue() );

  Storable::DataToStream( theStr, "myAlphaThreshold", GetAlphaThreshold() );
  Storable::DataToStream( theStr, "myMainTexture", GetQMainTexture() );
  Storable::DataToStream( theStr, "myAlphaTexture", GetQAlphaTexture() );

  Storable::DataToStream( theStr, "myResolution", GetResolution() );
  Storable::DataToStream( theStr, "myShowBar",    (IsBarVisible()? 1:0) );
}


VISU::GaussPoints_i
::~GaussPoints_i()
{
  if(MYDEBUG) MESSAGE("GaussPoints_i::~GaussPoints_i() - this = "<<this);
}


//----------------------------------------------------------------------------
CORBA::Long
VISU::GaussPoints_i
::GetFaceLimit()
{
  return myFaceLimit;
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetFaceLimit( CORBA::Long theFaceLimit )
{
  if( myFaceLimit == theFaceLimit )
    return;

  VISU::TSetModified aModified(this);
  
  myFaceLimit = theFaceLimit;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetIsDeformed( CORBA::Boolean theIsDeformed )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetIsDeformed( theIsDeformed );
}

CORBA::Boolean
VISU::GaussPoints_i
::GetIsDeformed()
{
  return myGaussPointsPL->GetIsDeformed();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetScaleFactor( CORBA::Double theScaleFactor )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetScale( theScaleFactor );
}

CORBA::Double
VISU::GaussPoints_i
::GetScaleFactor()
{
  return myGaussPointsPL->GetScale();
}

//----------------------------------------------------------------------------
QColor
VISU::GaussPoints_i
::GetQColor()
{
  return myColor;
}

SALOMEDS::Color
VISU::GaussPoints_i
::GetColor()
{
  SALOMEDS::Color aColor;
  aColor.R = myColor.red();
  aColor.G = myColor.green();
  aColor.B = myColor.blue();
  return aColor;
}


//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetQColor( const QColor& theColor )
{
  if(myColor == theColor)
    return;

  VISU::TSetModified aModified(this);
  
  myColor = theColor;
  myParamsTime.Modified();
}

void
VISU::GaussPoints_i
::SetColor( const SALOMEDS::Color& theColor )
{
  SetQColor(QColor(int(theColor.R), int(theColor.G), int(theColor.B)));
}


//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetIsColored( bool theIsColored )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetIsColored( theIsColored );
}

bool
VISU::GaussPoints_i
::GetIsColored()
{
  return myGaussPointsPL->GetIsColored();
}

//----------------------------------------------------------------------------
bool
VISU::GaussPoints_i
::SetMainTexture( const QString& theMainTexture ) 
{
  if(myMainTexture != theMainTexture){
    VISU::TSetModified aModified(this);
  
    myMainTexture = theMainTexture;
    myParamsTime.Modified();
    return true;
  }

  return false;
}

QString
VISU::GaussPoints_i
::GetQMainTexture() 
{ 
  return myMainTexture; 
}

char*
VISU::GaussPoints_i
::GetMainTexture() 
{ 
  return CORBA::string_dup(myMainTexture.toLatin1().data());
}


//----------------------------------------------------------------------------
bool
VISU::GaussPoints_i
::SetAlphaTexture( const QString& theAlphaTexture ) 
{
  if(myAlphaTexture != theAlphaTexture){
    VISU::TSetModified aModified(this);
  
    myAlphaTexture = theAlphaTexture;
    myParamsTime.Modified();
    return true;
  }

  return false;
}

QString
VISU::GaussPoints_i
::GetQAlphaTexture() 
{ 
  return myAlphaTexture; 
}

char*
VISU::GaussPoints_i
::GetAlphaTexture() 
{ 
  return CORBA::string_dup(myAlphaTexture.toLatin1().data());
}


//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetAlphaThreshold( CORBA::Double theAlphaThreshold )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetAlphaThreshold( theAlphaThreshold );
}

CORBA::Double
VISU::GaussPoints_i
::GetAlphaThreshold()
{
  return myGaussPointsPL->GetAlphaThreshold();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetResolution( CORBA::Long theResolution )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetResolution( theResolution );
}

CORBA::Long
VISU::GaussPoints_i
::GetResolution()
{
  return myGaussPointsPL->GetResolution();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetPrimitiveType(VISU::GaussPoints::PrimitiveType thePrimitiveType)
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetPrimitiveType( thePrimitiveType );
}

VISU::GaussPoints::PrimitiveType
VISU::GaussPoints_i
::GetPrimitiveType()
{
  int aPrimitiveType = myGaussPointsPL->GetPrimitiveType();

  if(aPrimitiveType == VISU_OpenGLPointSpriteMapper::OpenGLPoint)
    return VISU::GaussPoints::POINT;

  if(aPrimitiveType == VISU_OpenGLPointSpriteMapper::GeomSphere)
    return VISU::GaussPoints::SPHERE;

  return VISU::GaussPoints::SPRITE;
}

//----------------------------------------------------------------------------
vtkFloatingPointType
VISU::GaussPoints_i
::GetMaximumSupportedSize()
{
  return myGaussPointsPL->GetMaximumSupportedSize();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetClamp(CORBA::Double theClamp)
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetClamp( theClamp );
}

CORBA::Double
VISU::GaussPoints_i
::GetClamp()
{
  return myGaussPointsPL->GetClamp();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetGeomSize( CORBA::Double theGeomSize )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetSize( theGeomSize );
}

CORBA::Double
VISU::GaussPoints_i
::GetGeomSize()
{
  return myGaussPointsPL->GetSize();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetMinSize( CORBA::Double theMinSize )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetMinSize( theMinSize );
}

CORBA::Double
VISU::GaussPoints_i
::GetMinSize()
{
  return myGaussPointsPL->GetMinSize();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetMaxSize( CORBA::Double theMaxSize )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetMaxSize( theMaxSize );
}

CORBA::Double
VISU::GaussPoints_i
::GetMaxSize()
{
  return myGaussPointsPL->GetMaxSize();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetMagnification( CORBA::Double theMagnification )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetMagnification( theMagnification );
}

CORBA::Double
VISU::GaussPoints_i
::GetMagnification()
{
  return myGaussPointsPL->GetMagnification();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetMagnificationIncrement( CORBA::Double theIncrement )
{
  VISU::TSetModified aModified(this);
  
  myGaussPointsPL->SetMagnificationIncrement( theIncrement );
}

CORBA::Double
VISU::GaussPoints_i
::GetMagnificationIncrement()
{
  return myGaussPointsPL->GetMagnificationIncrement();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetSourceGeometry()
{
  int aNbGroups = myGroupNames.size();
  if(aNbGroups != 0){
    GetSpecificPL()->SetSourceGeometry();
    myGroupNames.clear();
    /*UpdateIcon();*/
  }
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::AddMeshOnGroup(const char* theGroupName)
{
  VISU::Result_i::PInput anInput = GetCResult()->GetInput();
  VISU::PUnstructuredGridIDMapper anIDMapper = anInput->GetMeshOnGroup(GetCMeshName(), theGroupName);
  if(anIDMapper){
    int aNbGroups  = myGroupNames.size();
    if(myGroupNames.find(theGroupName) == myGroupNames.end()){
      GetSpecificPL()->AddGeometry(anIDMapper->GetOutput(), theGroupName);
      myGroupNames.insert(theGroupName);
      /*
      if(aNbGroups == 0)
	UpdateIcon();
      */
      // To update scalar range according to the new input (IPAL21305)
      if(!IsRangeFixed())
        SetSourceRange();
    }
  }
}


//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::RemoveAllGeom()
{
  int aNbGroups  = myGroupNames.size();
  GetSpecificPL()->ClearGeometry();
  myGroupNames.clear();
  /*
  if(aNbGroups != 0)
    UpdateIcon();
  */
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::DoSetInput(bool theIsInitilizePipe, bool theReInit)
{
  VISU::Result_i::PInput anInput = GetCResult()->GetInput(GetCMeshName(),
							  GetEntity(),
							  GetCFieldName(),
							  GetTimeStampNumber());
  if(!anInput)
    throw std::runtime_error("Mesh_i::Build - myResult->GetInput() == NULL !!!");

  SetField(anInput->GetField(GetCMeshName(),
			     GetTEntity(),
			     GetCFieldName()));
  if(!GetField()) 
    throw std::runtime_error("There is no Field with the parameters !!!");

  VISU::PGaussPtsIDMapper aGaussPtsIDMapper =
    anInput->GetTimeStampOnGaussPts(GetCMeshName(),
				    GetTEntity(),
				    GetCFieldName(),
				    GetTimeStampNumber());
  if(!aGaussPtsIDMapper) 
    throw std::runtime_error("There is no TimeStamp with the parameters !!!");

  GetSpecificPL()->SetGaussPtsIDMapper(aGaussPtsIDMapper);
}


//----------------------------------------------------------------------------
bool
VISU::GaussPoints_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}

void VISU::GaussPoints_i::SetBarVisible(CORBA::Boolean theVisible) 
{ 
  if (myShowBar == theVisible)
    return;
  VISU::TSetModified aModified(this);
  myShowBar = theVisible; 
  myParamsTime.Modified();
}
      
//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){
    myGaussPointsPL = VISU_GaussPointsPL::New();
  }else
    myGaussPointsPL = dynamic_cast<VISU_GaussPointsPL*>(thePipeLine);

  TSuperClass::CreatePipeLine(myGaussPointsPL);
}


//----------------------------------------------------------------------------
VISU_PipeLine* 
VISU::GaussPoints_i
::GetActorPipeLine()
{
  // We create a new PipeLine instance in order to provide
  //   different representations for different actors (basic and segmented)
  VISU_GaussPointsPL* aPipeLine = VISU_GaussPointsPL::New();
  aPipeLine->ShallowCopy(GetPipeLine(), true);
  return aPipeLine;
}


//----------------------------------------------------------------------------
bool 
VISU::GaussPoints_i
::OnCreateActor(VISU_GaussPtsAct* theActor)
{
  try{
    TSuperClass::CreateActor(theActor);
    UpdateActor(theActor);
    return true;
  }catch(...){
    theActor->Delete();
    throw ;
  }
  return false;
}

VISU_GaussPtsAct1* 
VISU::GaussPoints_i
::OnCreateActor1()
{
  VISU_GaussPtsAct1* anActor = VISU_GaussPtsAct1::New();
  if(OnCreateActor(anActor))
    return anActor;
  return NULL;
}

VISU_GaussPtsAct2* 
VISU::GaussPoints_i
::OnCreateActor2()
{
  VISU_GaussPtsAct2* anActor = VISU_GaussPtsAct2::New();
  if(OnCreateActor(anActor))
    return anActor;
  return NULL;
}

VISU_Actor* 
VISU::GaussPoints_i
::CreateActor() 
{
  VISU_GaussPtsAct* anActor = VISU_GaussPtsAct::New();
  anActor->SetBarVisibility(myShowBar);
  if(OnCreateActor(anActor))
    return anActor;
  return NULL;
  //  return OnCreateActor1();
}

//----------------------------------------------------------------------------
VISU_GaussPtsAct2* 
VISU::GaussPoints_i
::CloneActor(VISU_GaussPtsAct1* theActor) 
{
  if(MYDEBUG) MESSAGE("GaussPoints_i::CloneActor - this = "<<this);
  if(VISU_GaussPtsAct2* anActor = OnCreateActor2()){
    theActor->Connect(anActor);
    return anActor;
  }
  return NULL;
}


//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::UpdateScalarBar(VISU_ScalarBarActor *theScalarBar,
		  VISU_LookupTable* theLookupTable)
{
  theScalarBar->SetTitle(GetScalarBarTitle().c_str());
  theScalarBar->SetOrientation(GetBarOrientation());
  theScalarBar->SetNumberOfLabels(GetLabels());
  theScalarBar->SetRatios(GetTitleSize(), GetLabelSize(), 
			  GetBarWidth(), GetBarHeight());
  theScalarBar->SetLabelFormat(GetLabelsFormat());

  VISU_LookupTable* aLookupTable = GetSpecificPL()->GetBarTable();
  
  theLookupTable->SetNumberOfColors(aLookupTable->GetNumberOfColors());
  theScalarBar->SetMaximumNumberOfColors(aLookupTable->GetNumberOfColors());

  vtkFloatingPointType anRGB[3];

  vtkTextProperty* aTitleProp = theScalarBar->GetTitleTextProperty();
  aTitleProp->SetFontFamily(GetTitFontType());

  GetTitleColor(anRGB[0],anRGB[1],anRGB[2]);
  aTitleProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);

  IsBoldTitle()? aTitleProp->BoldOn() : aTitleProp->BoldOff();
  IsItalicTitle()? aTitleProp->ItalicOn() : aTitleProp->ItalicOff();
  IsShadowTitle()? aTitleProp->ShadowOn() : aTitleProp->ShadowOff();
  
  vtkTextProperty* aLabelProp = theScalarBar->GetLabelTextProperty();
  aLabelProp->SetFontFamily(GetLblFontType());

  GetLabelColor(anRGB[0],anRGB[1],anRGB[2]);
  aLabelProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);

  IsBoldLabel()? aLabelProp->BoldOn() : aLabelProp->BoldOff();
  IsItalicLabel()? aLabelProp->ItalicOn() : aLabelProp->ItalicOff();
  IsShadowLabel()? aLabelProp->ShadowOn() : aLabelProp->ShadowOff();
  
  theScalarBar->Modified();
}


//----------------------------------------------------------------------------
void 
VISU::GaussPoints_i
::UpdateActor(VISU_ActorBase* theActor) 
{
  if(VISU_GaussPtsAct* anActor = dynamic_cast<VISU_GaussPtsAct*>(theActor)){
    VISU_ScalarBarCtrl *aScalarBarCtrl = anActor->GetScalarBarCtrl();
    if(GetIsColored()){
      GetPipeLine()->GetMapper()->SetScalarVisibility(1);

      if(IsRangeFixed()){
	vtkFloatingPointType* aRange = GetSpecificPL()->GetScalarRange();
	aScalarBarCtrl->SetRangeLocal(aRange);
      }else{
	vtkFloatingPointType aRange[2];
	GetSpecificPL()->GetSourceRange(aRange);
	aScalarBarCtrl->SetRangeLocal(aRange);
      }

      bool anIsMinMaxDone = IsGlobalRangeDefined();
      aScalarBarCtrl->SetGlobalRangeIsDefined(anIsMinMaxDone);

      TMinMax aTMinMax( GetComponentMin( GetScalarMode() ),
			GetComponentMax( GetScalarMode() ));
      aScalarBarCtrl->SetRangeGlobal(aTMinMax.first, aTMinMax.second);
      
      VISU_ScalarBarCtrl::EMode aScalarBarMode = VISU_ScalarBarCtrl::eGlobal;
      if(myIsActiveLocalScalarBar){
	if(myIsDispGlobalScalarBar){
	  aScalarBarMode = VISU_ScalarBarCtrl::eLocal; 
	}else{
	  aScalarBarMode = VISU_ScalarBarCtrl::eSimple; 
	}
      }
      
      if(aScalarBarMode == VISU_ScalarBarCtrl::eGlobal){
	vtkFloatingPointType aRangeGlobal[2];
	//
	aRangeGlobal[0] = aTMinMax.first;
	aRangeGlobal[1] = aTMinMax.second;
	
	GetSpecificPL()->GetMapper()->SetScalarRange(aRangeGlobal);
      }
      
      aScalarBarCtrl->SetMode(aScalarBarMode);
      
      // Position
      aScalarBarCtrl->SetWidth(GetWidth());
      aScalarBarCtrl->SetHeight(GetHeight());

      vtkFloatingPointType aPosition[] = {GetPosX(), GetPosY()};
      aScalarBarCtrl->SetPosition(aPosition);
      
      aScalarBarCtrl->SetSpacing(mySpacing);
      
      // Bicolor
      bool anIsBicolor = GetSpecificPL()->GetBicolor();
      aScalarBarCtrl->SetBicolor(anIsBicolor);
      
      UpdateScalarBar(aScalarBarCtrl->GetLocalBar(),
		      aScalarBarCtrl->GetLocalTable());
      
      UpdateScalarBar(aScalarBarCtrl->GetGlobalBar(),
		      aScalarBarCtrl->GetGlobalTable());

      aScalarBarCtrl->Update();
    }else{
      GetPipeLine()->GetMapper()->SetScalarVisibility(0);

      anActor->GetProperty()->SetColor(myColor.red() / 255.0,
				       myColor.green() / 255.0,
				       myColor.blue() / 255.0);
    }
    
    anActor->SetBarVisibility(myShowBar && GetIsColored()); 

    if( GetSpecificPL()->GetPrimitiveType() != VISU_OpenGLPointSpriteMapper::GeomSphere )
      theActor->SetRepresentation( VTK_POINTS );
    else
      theActor->SetRepresentation( VTK_SURFACE );
      
    // Update values labels

    vtkTextProperty* aProp = anActor->GetsValLabelsProps();
    if ( aProp )
    {
      aProp->SetFontFamily( GetValLblFontType() );
      aProp->SetFontSize( GetValLblFontSize() );
      aProp->SetBold( IsBoldValLbl() );
      aProp->SetItalic( IsItalicValLbl() );
      aProp->SetShadow( IsShadowValLbl() );

      vtkFloatingPointType anRGB[ 3 ];
      GetValLblFontColor( anRGB[ 0 ], anRGB[ 1 ], anRGB[ 2 ] );
      aProp->SetColor( anRGB[ 0 ], anRGB[ 1 ], anRGB[ 2 ] );
    }
  }
  
  TSuperClass::UpdateActor(theActor);
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::UpdateFromActor(VISU_GaussPtsAct* theActor) 
{
  if(MYDEBUG) MESSAGE("GaussPoints_i::UpdateFromActor - this = "<<this);
  myGaussPointsPL->ChangeMagnification(theActor->GetChangeMagnification());
  myParamsTime.Modified();
  UpdateActors();
}


//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetQTextures( const QString& theMainTexture, 
		const QString& theAlphaTexture )
{
  bool updateMainTexture = SetMainTexture( theMainTexture );
  bool updateAlphaTexture = SetAlphaTexture( theAlphaTexture );
  if( !updateMainTexture && !updateAlphaTexture )
    return;

  VISU::TSetModified aModified(this);
  
  using namespace VISU;
  TTextureValue aTextureValue = GetTexture(theMainTexture.toLatin1().data(), 
					   theAlphaTexture.toLatin1().data());
  myGaussPointsPL->SetImageData( aTextureValue.GetPointer() );
}

void
VISU::GaussPoints_i
::SetTextures( const char* theMainTexture, 
	       const char* theAlphaTexture )
{
  SetQTextures(theMainTexture, theAlphaTexture);
}


//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetIsActiveLocalScalarBar(CORBA::Boolean theIsActiveLocalScalarBar)
{
  if(myIsActiveLocalScalarBar == theIsActiveLocalScalarBar)
    return;

  VISU::TSetModified aModified(this);
  
  myIsActiveLocalScalarBar = theIsActiveLocalScalarBar;

  if ( !theIsActiveLocalScalarBar || !IsRangeFixed() )
    SetSourceRange();

  myParamsTime.Modified();
}

CORBA::Boolean
VISU::GaussPoints_i
::GetIsActiveLocalScalarBar() 
{
  return myIsActiveLocalScalarBar;
}

void
VISU::GaussPoints_i
::SetIsDispGlobalScalarBar(CORBA::Boolean theIsDispGlobalScalarBar)
{
  if(myIsDispGlobalScalarBar == theIsDispGlobalScalarBar)
    return;

  VISU::TSetModified aModified(this);
  
  myIsDispGlobalScalarBar = theIsDispGlobalScalarBar;
  myParamsTime.Modified();
}

bool
VISU::GaussPoints_i
::IsGlobalRangeDefined() const 
{
  return GetCResult()->IsMinMaxDone();
}

//----------------------------------------------------------------------------
void
VISU::GaussPoints_i
::SetSourceRange()
{
  VISU::TSetModified aModified(this);

  vtkFloatingPointType aScalarRange[2] = {GetSourceMin(), GetSourceMax()};
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_ColoredPL, vtkFloatingPointType*>
		   (GetSpecificPL(), &VISU_ColoredPL::SetScalarRange, aScalarRange));

  UseFixedRange(false);
}


//----------------------------------------------------------------------------
struct TGetSourceMin: public SALOME_Event
{
  VISU::GaussPoints_i* myColoredPrs3d;

  typedef CORBA::Double TResult;
  TResult myResult;
  
  TGetSourceMin( VISU::GaussPoints_i* theColoredPrs3d ):
    myColoredPrs3d( theColoredPrs3d )
  {}
    
  virtual
  void
  Execute()
  {
    if ( myColoredPrs3d->IsTimeStampFixed() || myColoredPrs3d->GetIsActiveLocalScalarBar() ) {
      vtkFloatingPointType aRange[2];
      myColoredPrs3d->GetSpecificPL()->GetSourceRange(aRange);
      myResult = aRange[0];
    }else{
      VISU::TNames aGroupNames = myColoredPrs3d->GetSpecificPL()->GetGeometryNames();
      VISU::TMinMax aTMinMax = myColoredPrs3d->GetField()->GetMinMax( myColoredPrs3d->GetScalarMode(), aGroupNames );
      myResult = aTMinMax.first;
    }
  }
};


//----------------------------------------------------------------------------
CORBA::Double 
VISU::GaussPoints_i
::GetSourceMin()
{
  return ProcessEvent( new TGetSourceMin( this ) );
}


//----------------------------------------------------------------------------
struct TGetSourceMax: public SALOME_Event
{
  VISU::GaussPoints_i* myColoredPrs3d;

  typedef CORBA::Double TResult;
  TResult myResult;
  
  TGetSourceMax( VISU::GaussPoints_i* theColoredPrs3d ):
    myColoredPrs3d( theColoredPrs3d )
  {}
    
  virtual
  void
  Execute()
  {
    if ( myColoredPrs3d->IsTimeStampFixed() || myColoredPrs3d->GetIsActiveLocalScalarBar() ) {
      vtkFloatingPointType aRange[2];
      myColoredPrs3d->GetSpecificPL()->GetSourceRange(aRange);
      myResult = aRange[1];
    }else{
      VISU::TNames aGroupNames = myColoredPrs3d->GetSpecificPL()->GetGeometryNames();
      VISU::TMinMax aTMinMax = myColoredPrs3d->GetField()->GetMinMax( myColoredPrs3d->GetScalarMode(), aGroupNames );
      myResult = aTMinMax.second;
    }
  }
};


//----------------------------------------------------------------------------
CORBA::Double 
VISU::GaussPoints_i
::GetSourceMax()
{
  return ProcessEvent( new TGetSourceMax( this ) );
}


//----------------------------------------------------------------------------
CORBA::Boolean
VISU::GaussPoints_i
::GetIsDispGlobalScalarBar()
{
  return myIsDispGlobalScalarBar;
}


void
VISU::GaussPoints_i
::SetBiColor(CORBA::Boolean theIsBiColor)
{
  VISU::TSetModified aModified(this);

  GetSpecificPL()->SetBicolor(theIsBiColor);
}

CORBA::Boolean
VISU::GaussPoints_i
::GetBiColor() 
{
  return GetSpecificPL()->GetBicolor();
}

void
VISU::GaussPoints_i
::SetSpacing(CORBA::Double theSpacing)
{
  if(VISU::CheckIsSameValue(mySpacing, theSpacing))
    return;

  VISU::TSetModified aModified(this);

  mySpacing = theSpacing;
  myParamsTime.Modified();
}

CORBA::Double
VISU::GaussPoints_i
::GetSpacing()
{
  return mySpacing;
}

//----------------------------------------------------------------------------
struct TGaussGetComponentMin: public SALOME_Event
{
  VISU::ColoredPrs3d_i* myColoredPrs3d;
  vtkIdType myCompID;
  
  typedef CORBA::Double TResult;
  TResult myResult;
  
  TGaussGetComponentMin( VISU::ColoredPrs3d_i* theColoredPrs3d,
			 vtkIdType theCompID ):
    myColoredPrs3d( theColoredPrs3d ),
    myCompID( theCompID )
  {}
    
  virtual
  void
  Execute()
  {
    VISU::TNames aGroupNames;
    if(VISU::GaussPoints_i* aPrs3d = dynamic_cast<VISU::GaussPoints_i*>(myColoredPrs3d))
      aGroupNames = aPrs3d->GetSpecificPL()->GetGeometryNames();

    VISU::PMinMaxController aMinMaxController = myColoredPrs3d->GetMinMaxController();
    if ( aMinMaxController ) {
      myResult = aMinMaxController->GetComponentMin( myCompID );
    } else {
      VISU::TMinMax aTMinMax = myColoredPrs3d->GetScalarField()->GetMinMax( myCompID, aGroupNames );
      myResult = aTMinMax.first;
    }
  }
};


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU::GaussPoints_i
::GetComponentMin(vtkIdType theCompID)
{
  return ProcessEvent( new TGaussGetComponentMin( this, theCompID ) );
}

//----------------------------------------------------------------------------
struct TGaussGetComponentMax: public SALOME_Event
{
  VISU::ColoredPrs3d_i* myColoredPrs3d;
  vtkIdType myCompID;

  typedef CORBA::Double TResult;
  TResult myResult;
  
  TGaussGetComponentMax( VISU::ColoredPrs3d_i* theColoredPrs3d,
			 vtkIdType theCompID ):
    myColoredPrs3d( theColoredPrs3d ),
    myCompID( theCompID )
  {}
    
  virtual
  void
  Execute()
  {
    VISU::TNames aGroupNames;
    if(VISU::GaussPoints_i* aPrs3d = dynamic_cast<VISU::GaussPoints_i*>(myColoredPrs3d))
      aGroupNames = aPrs3d->GetSpecificPL()->GetGeometryNames();

    VISU::PMinMaxController aMinMaxController = myColoredPrs3d->GetMinMaxController();
    if ( aMinMaxController ) {
      myResult = aMinMaxController->GetComponentMax( myCompID );
    } else {
      VISU::TMinMax aTMinMax = myColoredPrs3d->GetScalarField()->GetMinMax( myCompID, aGroupNames );
      myResult = aTMinMax.second;
    }
  }
};


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU::GaussPoints_i
::GetComponentMax(vtkIdType theCompID)
{
  return ProcessEvent( new TGaussGetComponentMax( this, theCompID ) );
}
