// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PointMap3d_i.cc
//  Author : Dmitry Matveitchev
//  Module : VISU
//
#include "VISU_PointMap3d_i.hh"

#include "VISU_CutLinesBase_i.hh"
#include "VISU_CutSegment_i.hh"
#include "VISU_Result_i.hh"
#include "VISU_ViewManager_i.hh"
#include "VISU_ScalarBarActor.hxx"
#include "SUIT_ResourceMgr.h"

#include "SALOME_Event.h"
#include "VISU_Prs3dUtils.hh"
#include "SPlot2d_Curve.h"
#include "VISU_PipeLineUtils.hxx"

#include "VISU_TableReader.hxx"
#include "VISU_ConvertorUtils.hxx"
#include "VISU_DeformedGridPL.hxx"

#include "SALOME_InteractiveObject.hxx"
#include "VISU_Gen_i.hh"

#include <vtkTextProperty.h>
#include <vtkActorCollection.h>

#include <boost/bind.hpp>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

//----------------------------------------------------------------
//                      PointMap3d Object
//----------------------------------------------------------------
int VISU::PointMap3d_i::myNbPresent = 0;
const string VISU::PointMap3d_i::myComment  = "POINTMAP3D";
/*!
  Generate unique name
*/
QString VISU::PointMap3d_i::GenerateName()
{
  return VISU::GenerateName( "Table3D - ", ++myNbPresent );
}
/*!
  Gets comment string
*/
const char* VISU::PointMap3d_i::GetComment() const
{
  return myComment.c_str();
}
/*!
  Constructor
*/
VISU::PointMap3d_i::PointMap3d_i( SALOMEDS::Study_ptr theStudy, const char* theObjectEntry )
  : Table_i(theStudy, theObjectEntry),
  myActorCollection(vtkActorCollection::New()),
  myIsActiveState(true)
{
  if(MYDEBUG) MESSAGE("PointMap3d_i::PointMap3d_i - this = "<<this);
  SetStudyDocument(theStudy);
  mySObj = theStudy->FindObjectID(theObjectEntry);
  myOffset[0] = myOffset[1] = myOffset[2] = 0;
  myActorCollection->Delete();
}
/*!
  Destructor
*/
VISU::PointMap3d_i::~PointMap3d_i()
{
  if(MYDEBUG) MESSAGE("PointMap3d_i::~PointMap3d_i - this = "<<this);
}

//----------------------------------------------------------------------------
namespace VISU
{
  struct TInvokeSignalEvent: public SALOME_Event
  {
    typedef boost::signal0<void> TSignal;
    const TSignal& mySignal;
    
    TInvokeSignalEvent(const TSignal& theSignal):
      mySignal(theSignal)
    {}
    
    virtual
    void
    Execute()
    {
      mySignal();
    }
  };
}

//----------------------------------------------------------------------------
void
VISU::PointMap3d_i
::SetTitle( const char* theTitle )
{
  SetName( theTitle, true );
}

//----------------------------------------------------------------------------
char*
VISU::PointMap3d_i
::GetTitle()
{
  return CORBA::string_dup( GetName().c_str() );
}

//----------------------------------------------------------------------------
SALOMEDS::SObject_var
VISU::PointMap3d_i
::GetSObject() const
{
  return mySObj;
}

//----------------------------------------------------------------------------
std::string
VISU::PointMap3d_i
::GetObjectEntry() 
{
  CORBA::String_var anEntry = mySObj->GetID();
  return anEntry.in(); 
}

//----------------------------------------------------------------------------
Handle(SALOME_InteractiveObject)
VISU::PointMap3d_i
::GetIO()
{
  if( myIO.IsNull() )
    myIO = new SALOME_InteractiveObject(GetEntry().c_str(), "VISU", GetName().c_str());

  return myIO;
}

//----------------------------------------------------------------------------
/*!
  Creates table3d object
*/
VISU::Storable* VISU::PointMap3d_i::Create()
{
  // generate name ...
  SetName(GetTableTitle().toLatin1().constData(), false);

  if ( GetName() == "" ) {
    if ( !mySObj->_is_nil() ) {
      CutLinesBase_i* pCutLines = NULL;
      CORBA::Object_var anObj = SObjectToObject(mySObj);
      if(!CORBA::is_nil(anObj)){
	VISU::CutLinesBase_var aCutLines = VISU::CutLinesBase::_narrow(anObj);
	if(!aCutLines->_is_nil())
	  pCutLines = dynamic_cast<CutLinesBase_i*>(GetServant(aCutLines).in());
      }
      if (!pCutLines)
	if (mySObj->GetName()) SetName(mySObj->GetName(), false);
    }
  }

  if ( GetName() == "" )
    SetName(GenerateName().toLatin1().constData(), false);

  // Create Pipeline
  myTablePL = VISU_DeformedGridPL::New();
  myTablePL->SetPolyDataIDMapper(GetTableIDMapper());
  myTablePL->Update();

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();

  bool isUnits = aResourceMgr->booleanValue( "VISU", "scalar_bar_display_units", true );
  SetUnitsVisible(isUnits);

  SetSourceRange();

  if( aResourceMgr->booleanValue("VISU", "scalar_bar_logarithmic", false) )
    SetScaling(VISU::LOGARITHMIC);
  else
    SetScaling(VISU::LINEAR);

  int aNumberOfColors = aResourceMgr->integerValue( "VISU", "scalar_bar_num_colors", 64 );
  SetNbColors(aNumberOfColors);

  int aRangeType = aResourceMgr->integerValue("VISU" , "scalar_range_type", 0);
  UseFixedRange(aRangeType == 1);
  if(aRangeType == 1){
    float aMin = aResourceMgr->doubleValue("VISU", "scalar_range_min", 0);
    float aMax = aResourceMgr->doubleValue("VISU", "scalar_range_max", 0);
    SetRange(aMin, aMax);
  }

  int lp = aResourceMgr->integerValue( "VISU", "scalar_bar_label_precision", 3 );
  SetLabelsFormat( VISU::ToFormat( lp ).c_str() );

  // Orientation
  int anOrientation = aResourceMgr->integerValue("VISU", "scalar_bar_orientation", 0);
  if(anOrientation == 1)
    SetBarOrientation(VISU::ColoredPrs3dBase::HORIZONTAL);
  else
    SetBarOrientation(VISU::ColoredPrs3dBase::VERTICAL);

  // Scalar Bar origin
  QString propertyName = QString( "scalar_bar_%1_" ).arg( anOrientation == 0 ? "vertical" : "horizontal" );

  vtkFloatingPointType aXorigin = (myBarOrientation == VISU::ColoredPrs3dBase::VERTICAL) ? 0.01 : 0.2;
  aXorigin = aResourceMgr->doubleValue("VISU", propertyName + "x", aXorigin);
  myPosition[0] = aXorigin;

  vtkFloatingPointType aYorigin = (myBarOrientation == VISU::ColoredPrs3dBase::VERTICAL) ? 0.1 : 0.012;
  aYorigin = aResourceMgr->doubleValue("VISU", propertyName + "y", aYorigin);
  myPosition[1] = aYorigin;

  // Scalar Bar size
  myWidth = (myBarOrientation == VISU::ColoredPrs3dBase::VERTICAL)? 0.1 : 0.6;
  myWidth = aResourceMgr->doubleValue("VISU", propertyName + "width", myWidth);

  myHeight = (myBarOrientation == VISU::ColoredPrs3dBase::VERTICAL)? 0.8:0.12;
  myHeight = aResourceMgr->doubleValue("VISU", propertyName + "height", myHeight);

  myTitleSize = aResourceMgr->doubleValue("VISU", propertyName + "title_size", 0);
  myLabelSize = aResourceMgr->doubleValue("VISU", propertyName + "label_size", 0);
  myBarWidth = aResourceMgr->doubleValue("VISU", propertyName + "bar_width", 0);
  myBarHeight = aResourceMgr->doubleValue("VISU", propertyName + "bar_height", 0);

  // Nb of Labels
  myNumberOfLabels = aResourceMgr->integerValue( "VISU", "scalar_bar_num_labels", 5 );

  // Fonts properties definition
  myIsBoldTitle = myIsItalicTitle = myIsShadowTitle = true;
  myTitFontType = VTK_ARIAL;

  if(aResourceMgr->hasValue( "VISU", "scalar_bar_title_font" )){
    QFont f = aResourceMgr->fontValue( "VISU", "scalar_bar_title_font" );
    if ( f.family() == "Arial" )
      myTitFontType = VTK_ARIAL;
    else if ( f.family() == "Courier" )
      myTitFontType = VTK_COURIER;
    else if ( f.family() == "Times" )
      myTitFontType = VTK_TIMES;
    
    myIsBoldTitle   = f.bold();
    myIsItalicTitle = f.italic();
    myIsShadowTitle =  f.overline();
  }

  QColor aTextColor = aResourceMgr->colorValue( "VISU", "scalar_bar_title_color", QColor( 255, 255, 255 ) );

  myTitleColor[0] = aTextColor.red()   / 255;
  myTitleColor[1] = aTextColor.green() / 255;
  myTitleColor[2] = aTextColor.blue()  / 255;

  myIsBoldLabel = myIsItalicLabel = myIsShadowLabel = true;
  myLblFontType = VTK_ARIAL;

  if( aResourceMgr->hasValue( "VISU", "scalar_bar_label_font" )){
    QFont f = aResourceMgr->fontValue( "VISU", "scalar_bar_label_font" );
    if ( f.family() == "Arial" )
      myLblFontType = VTK_ARIAL;
    else if ( f.family() == "Courier" )
      myLblFontType = VTK_COURIER;
    else if ( f.family() == "Times" )
      myLblFontType = VTK_TIMES;
    
    myIsBoldLabel   = f.bold();
    myIsItalicLabel = f.italic();
    myIsShadowLabel =  f.overline();
  }

  QColor aLabelColor = aResourceMgr->colorValue( "VISU", "scalar_bar_label_color", QColor( 255, 255, 255 ) );

  myLabelColor[0] = aLabelColor.red()   / 255;
  myLabelColor[1] = aLabelColor.green() / 255;
  myLabelColor[2] = aLabelColor.blue()  / 255;

  // scalar bar default position
  bool anIsArrangeBar = aResourceMgr->booleanValue("VISU", "scalar_bars_default_position", 0);
  int aPlace = 1;
  if (anIsArrangeBar){
    aPlace = aResourceMgr->integerValue("VISU", "scalar_bar_position_num",0);
  }
  if(myBarOrientation == VISU::ColoredPrs3dBase::HORIZONTAL){
    myPosition[1] += myHeight*(aPlace-1);
  } else {
    myPosition[0] += myWidth*(aPlace-1);
  }

  return Build( false );
}

/*
  GetIDMapper
*/

VISU::PTableIDMapper
VISU::PointMap3d_i
::GetTableIDMapper()
{
  //Initialisate table mapper
  SALOMEDS::GenericAttribute_var anAttr;
  mySObj->FindAttribute(anAttr, "AttributeTableOfReal");
  SALOMEDS::AttributeTableOfReal_var aTableOfReal = SALOMEDS::AttributeTableOfReal::_narrow(anAttr);

  PTableIDMapper aTableIDMapper( new TTableIDMapper() );
  TTable2D& aTable2D = *aTableIDMapper;

  aTable2D.myTitle = aTableOfReal->GetTitle();

  SALOMEDS::StringSeq_var aColStrList = aTableOfReal->GetColumnTitles();
  SALOMEDS::StringSeq_var aRowStrList = aTableOfReal->GetRowTitles();

  for ( int i = 0; i < aRowStrList->length(); i++ ) {
    aTable2D.myColumnTitles.push_back( aRowStrList[ i ].in() );
  }

  int aCols = aTableOfReal->GetNbColumns();
  int aRows = aTableOfReal->GetNbRows();

  for (int i=1; i<=aCols; i++) {
    TTable2D::TRow aRow;
    aRow.myTitle = aColStrList[ i-1 ].in();
    for (int j=1; j<=aRows; j++) {
      double aVal = aTableOfReal->GetValue(j, i);
      QString aValStr = QString::number(aVal);
      aRow.myValues.push_back( aValStr.toLatin1().constData() );
    }
    if( aRow.myValues.size() > 0 )
      aTable2D.myRows.push_back( aRow );
  }

  return aTableIDMapper;
}

/*
  Create Actor
*/
VISU_PointMap3dActor* VISU::PointMap3d_i::CreateActor()
{
  VISU_PointMap3dActor* anActor = VISU_PointMap3dActor::New();
  anActor->SetPipeLine(myTablePL);
  anActor->SetFactory(this);

  SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
  int  aDispMode = aResourceMgr->integerValue("VISU", "point_map_represent", 2);
  bool toShrink  = aResourceMgr->booleanValue("VISU", "scalar_map_shrink", false);
  anActor->SetRepresentation(aDispMode);
  if (toShrink) anActor->SetShrink();

  Handle (SALOME_InteractiveObject) anIO = new SALOME_InteractiveObject(GetEntry().c_str(), "VISU", GetName().c_str());
  anActor->setIO(anIO);

  myUpdateActorsSignal.connect(boost::bind(&VISU_Actor::UpdateFromFactory,anActor));
  //  myRemoveActorsFromRendererSignal.connect(boost::bind(&VISU_Actor::RemoveFromRender,anActor));

  myActorCollection->AddItem(anActor);

  UpdateActor( anActor );

  return anActor;
}

/*
  Update Actor
*/
void VISU::PointMap3d_i::UpdateActor(VISU_ActorBase* theActor)
{
  if(VISU_PointMap3dActor* anActor = dynamic_cast<VISU_PointMap3dActor*>(theActor)){
    Update();
    VISU_ScalarBarActor *aScalarBar = anActor->GetScalarBar();
    aScalarBar->SetLookupTable(GetSpecificPL()->GetBarTable());
    aScalarBar->SetTitle(GetTitle());
    aScalarBar->SetOrientation(GetBarOrientation());
    aScalarBar->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
    aScalarBar->GetPositionCoordinate()->SetValue(GetPosX(),GetPosY());
    aScalarBar->SetWidth(GetWidth());
    aScalarBar->SetHeight(GetHeight());
    aScalarBar->SetNumberOfLabels(GetLabels());
    aScalarBar->SetRatios(myTitleSize, myLabelSize, 
			  myBarWidth, myBarHeight);
    aScalarBar->SetNumberOfLabels(GetLabels());
    aScalarBar->SetLabelFormat(GetLabelsFormat());

    vtkFloatingPointType anRGB[3];

    vtkTextProperty* aTitleProp = aScalarBar->GetTitleTextProperty();
    aTitleProp->SetFontFamily(GetTitFontType());

    GetTitleColor(anRGB[0],anRGB[1],anRGB[2]);
    aTitleProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);

    IsBoldTitle()? aTitleProp->BoldOn() : aTitleProp->BoldOff();
    IsItalicTitle()? aTitleProp->ItalicOn() : aTitleProp->ItalicOff();
    IsShadowTitle()? aTitleProp->ShadowOn() : aTitleProp->ShadowOff();

    vtkTextProperty* aLabelProp = aScalarBar->GetLabelTextProperty();
    aLabelProp->SetFontFamily(GetLblFontType());

    GetLabelColor(anRGB[0],anRGB[1],anRGB[2]);
    aLabelProp->SetColor(anRGB[0],anRGB[1],anRGB[2]);

    IsBoldLabel()? aLabelProp->BoldOn() : aLabelProp->BoldOff();
    IsItalicLabel()? aLabelProp->ItalicOn() : aLabelProp->ItalicOff();
    IsShadowLabel()? aLabelProp->ShadowOn() : aLabelProp->ShadowOff();

    aScalarBar->Modified();
  }
  theActor->SetPosition(myOffset[0],myOffset[1],myOffset[2]);
}

void
VISU::PointMap3d_i
::UpdateActors()
{
  if(MYDEBUG) MESSAGE("Prs3d_i::UpdateActors - this = "<<this);
  ProcessVoidEvent(new TVoidMemFunEvent<VISU_PipeLine>
		   (GetSpecificPL(), &VISU_PipeLine::Update));

  ProcessVoidEvent(new VISU::TInvokeSignalEvent(myUpdateActorsSignal));
}

//----------------------------------------------------------------------------
void
VISU::PointMap3d_i
::Update()
{
  if(GetMTime() < myUpdateTime.GetMTime())
    return;

  if(MYDEBUG) MESSAGE("PointMap3d_i::Update - this = "<<this);

  try{
    ProcessVoidEvent(new TVoidMemFunEvent<VISU_PipeLine>
		     (GetSpecificPL(), &VISU_PipeLine::Update));
    myUpdateTime.Modified();
  }catch(std::exception&){
    throw;
  }catch(...){
    throw std::runtime_error("PointMap3d_i::Update >> unexpected exception was caught!!!");
  }
}

//----------------------------------------------------------------------------
unsigned long int 
VISU::PointMap3d_i
::GetMTime()
{
  unsigned long int aTime = myParamsTime.GetMTime();
  if( GetSpecificPL() )
    aTime = std::max(aTime, GetSpecificPL()->GetMTime());
  return aTime;
}

/*!
  Builds presentation of table
*/
VISU::Storable* VISU::PointMap3d_i::Build( int theRestoring )
{

  // look for reference SObject with table attribute
  SALOMEDS::SObject_var SO = mySObj;

  if ( !SO->_is_nil() ) {
    CutLinesBase_i* pCutLines = NULL;
    CORBA::Object_var anObj = SObjectToObject(SO);
    if(!CORBA::is_nil(anObj)){
      VISU::CutLinesBase_var aCutLines = VISU::CutLinesBase::_narrow(anObj);
      if(!aCutLines->_is_nil())
        pCutLines = dynamic_cast<CutLinesBase_i*>(GetServant(aCutLines).in());
    }
    SALOMEDS::Study_var aStudy = GetStudyDocument();
    SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
    SALOMEDS::GenericAttribute_var anAttr;
    // look for component
    if ( !theRestoring ) {
      SALOMEDS::SComponent_var SComponent = VISU::FindOrCreateVisuComponent( GetStudyDocument() );
      // create SObject and set attributes
      QString aComment;
      if(pCutLines)
        aComment.sprintf("myComment=%s;mySourceId=CutLines",GetComment());
      else{
        aComment.sprintf("myComment=%s;mySourceId=TableAttr",GetComment());
        SALOMEDS::SObject_var aFatherSObject = SO->GetFather();
        if(aFatherSObject->FindAttribute(anAttr,"AttributeString")){
          SALOMEDS::AttributeString_var aCommentAttr =
            SALOMEDS::AttributeString::_narrow(anAttr);
          CORBA::String_var aValue = aCommentAttr->Value();
          Storable::TRestoringMap aMap;
          Storable::StringToMap(aValue.in(),aMap);
          bool anIsExist;
          QString aMethodName = VISU::Storable::FindValue(aMap,"myComment",&anIsExist);
          if(anIsExist){
            if(aMethodName == "ImportTables"){
              aComment.sprintf("myComment=%s;mySourceId=TableFile",GetComment());
            }
          }
        }
      }

      string anEntry = CreateAttributes( GetStudyDocument(),
                                         SO->GetID(),//SComponent->GetID(),
                                         "ICON_TREE_TABLE",
                                         GetID(),
                                         GetName(),
                                         "",
                                         aComment.toLatin1().constData(),
                                         pCutLines );
      // create SObject referenced to real table object
      mySObj = SALOMEDS::SObject::_duplicate(GetStudyDocument()->FindObjectID( anEntry.c_str() ));
      if(pCutLines) {
        bool isCutSegment = dynamic_cast<CutSegment_i*>(pCutLines);
        pCutLines->BuildTableOfReal(mySObj, isCutSegment);
      }
      // mpv (PAL5357): reference attributes are unnecessary now
      //SALOMEDS::SObject_var refSO = Builder->NewObject( mySObj );
      //Builder->Addreference( refSO, SO );
    }

    return this;
  }
  return NULL;
}
/*!
  Restores table object from stream
*/
VISU::Storable* VISU::PointMap3d_i::Restore( const Storable::TRestoringMap& theMap, SALOMEDS::SObject_ptr SO)
{
  VISU::Table_i::Restore( theMap, SO);
  if(MYDEBUG) MESSAGE(GetComment());
  SetName(VISU::Storable::FindValue(theMap,"myName").toLatin1().constData(), false);
  myTitle = VISU::Storable::FindValue(theMap,"myTitle").toLatin1().constData();
  myOrientation = ( VISU::Table::Orientation )( VISU::Storable::FindValue(theMap,"myOrientation").toInt() );
  mySObj = SALOMEDS::SObject::_duplicate(SO);

  //Create PipeLine
  myTablePL = VISU_DeformedGridPL::New();
  myTablePL->SetPolyDataIDMapper(GetTableIDMapper());
  myTablePL->Update();

  //Restore Other Values
  
  float aMin = VISU::Storable::FindValue(theMap,"myScalarRange[0]").toDouble();
  float aMax = VISU::Storable::FindValue(theMap,"myScalarRange[1]").toDouble();
  SetRange(aMin, aMax);

  UseFixedRange(VISU::Storable::FindValue(theMap,"myIsFixedRange", "0").toInt());

  SetNbColors(VISU::Storable::FindValue(theMap,"myNumberOfColors").toInt());
  SetUnitsVisible(VISU::Storable::FindValue(theMap,"myUnitsVisible", "1").toInt());
  SetLabelsFormat(VISU::Storable::FindValue(theMap,"myLabelsFormat", "%-#6.3g").toLatin1().constData());
  SetBarOrientation((VISU::ColoredPrs3dBase::Orientation)VISU::Storable::FindValue(theMap,"myBarOrientation").toInt());

  SetTitle(VISU::Storable::FindValue(theMap,"myTitle").toLatin1().constData());
  myNumberOfLabels = VISU::Storable::FindValue(theMap,"myNumberOfLabels").toInt();
  myPosition[0] = VISU::Storable::FindValue(theMap,"myPosition[0]").toDouble();
  myPosition[1] = VISU::Storable::FindValue(theMap,"myPosition[1]").toDouble();
  myWidth = VISU::Storable::FindValue(theMap,"myWidth").toDouble();
  myHeight = VISU::Storable::FindValue(theMap,"myHeight").toDouble();
  myTitleSize = VISU::Storable::FindValue(theMap,"myTitleSize").toInt();
  myLabelSize = VISU::Storable::FindValue(theMap,"myLabelSize").toInt();
  myBarWidth = VISU::Storable::FindValue(theMap,"myBarWidth").toInt();
  myBarHeight = VISU::Storable::FindValue(theMap,"myBarHeight").toInt();

  myTitFontType = VISU::Storable::FindValue(theMap,"myTitFontType").toInt();
  myIsBoldTitle = VISU::Storable::FindValue(theMap,"myIsBoldTitle").toInt();
  myIsItalicTitle = VISU::Storable::FindValue(theMap,"myIsItalicTitle").toInt();
  myIsShadowTitle = VISU::Storable::FindValue(theMap,"myIsShadowTitle").toInt();
  myTitleColor[0] = VISU::Storable::FindValue(theMap,"myTitleColor[0]").toFloat();
  myTitleColor[1] = VISU::Storable::FindValue(theMap,"myTitleColor[1]").toFloat();
  myTitleColor[2] = VISU::Storable::FindValue(theMap,"myTitleColor[2]").toFloat();

  myLblFontType = VISU::Storable::FindValue(theMap,"myLblFontType").toInt();
  myIsBoldLabel = VISU::Storable::FindValue(theMap,"myIsBoldLabel").toInt();
  myIsItalicLabel = VISU::Storable::FindValue(theMap,"myIsItalicLabel").toInt();
  myIsShadowLabel = VISU::Storable::FindValue(theMap,"myIsShadowLabel").toInt();
  myLabelColor[0] = VISU::Storable::FindValue(theMap,"myLabelColor[0]").toFloat();
  myLabelColor[1] = VISU::Storable::FindValue(theMap,"myLabelColor[1]").toFloat();
  myLabelColor[2] = VISU::Storable::FindValue(theMap,"myLabelColor[2]").toFloat();

  bool isFound = false;
  QString x,y,z;
  
  x = VISU::Storable::FindValue(theMap,"myOffset[0]",&isFound);
  y = VISU::Storable::FindValue(theMap,"myOffset[1]",&isFound);
  z = VISU::Storable::FindValue(theMap,"myOffset[2]",&isFound);
  if(isFound) {
    myOffset[0] = x.toFloat();
    myOffset[1] = y.toFloat();
    myOffset[2] = z.toFloat();
  }

  myParamsTime.Modified();
  return Build( true );
}
/*!
  Flushes table data into stream
*/
void VISU::PointMap3d_i::ToStream( std::ostringstream& theStr )
{
  Storable::DataToStream( theStr, "myName",        GetName().c_str() );
  Storable::DataToStream( theStr, "myTitle",       myTitle.c_str() );
  Storable::DataToStream( theStr, "myOrientation", myOrientation );

  Storable::DataToStream( theStr, "myScalarRange[0]", GetMin() );
  Storable::DataToStream( theStr, "myScalarRange[1]", GetMax() );
  Storable::DataToStream( theStr, "myIsFixedRange",   IsRangeFixed() );

  Storable::DataToStream( theStr, "myNumberOfColors", int(GetNbColors()) );
  Storable::DataToStream( theStr, "myBarOrientation", myBarOrientation );

  Storable::DataToStream( theStr, "myTitle",          myTitle.c_str() );
  Storable::DataToStream( theStr, "myUnitsVisible",   myIsUnits );
  Storable::DataToStream( theStr, "myNumberOfLabels", myNumberOfLabels );
  Storable::DataToStream( theStr, "myLabelsFormat",   myLabelsFormat.c_str() );
  Storable::DataToStream( theStr, "myPosition[0]",    myPosition[0] );
  Storable::DataToStream( theStr, "myPosition[1]",    myPosition[1] );
  Storable::DataToStream( theStr, "myWidth",          myWidth );
  Storable::DataToStream( theStr, "myHeight",         myHeight );
  Storable::DataToStream( theStr, "myTitleSize",      myTitleSize );
  Storable::DataToStream( theStr, "myLabelSize",     myLabelSize );
  Storable::DataToStream( theStr, "myBarWidth",       myBarWidth );
  Storable::DataToStream( theStr, "myBarHeight",      myBarHeight );

  Storable::DataToStream( theStr, "myTitFontType",    myTitFontType );
  Storable::DataToStream( theStr, "myIsBoldTitle",    myIsBoldTitle );
  Storable::DataToStream( theStr, "myIsItalicTitle",  myIsItalicTitle );
  Storable::DataToStream( theStr, "myIsShadowTitle",  myIsShadowTitle );
  Storable::DataToStream( theStr, "myTitleColor[0]",  myTitleColor[0] );
  Storable::DataToStream( theStr, "myTitleColor[1]",  myTitleColor[1] );
  Storable::DataToStream( theStr, "myTitleColor[2]",  myTitleColor[2] );

  Storable::DataToStream( theStr, "myLblFontType",    myLblFontType );
  Storable::DataToStream( theStr, "myIsBoldLabel",    myIsBoldLabel );
  Storable::DataToStream( theStr, "myIsItalicLabel",  myIsItalicLabel );
  Storable::DataToStream( theStr, "myIsShadowLabel",  myIsShadowLabel );
  Storable::DataToStream( theStr, "myLabelColor[0]",  myLabelColor[0] );
  Storable::DataToStream( theStr, "myLabelColor[1]",  myLabelColor[1] );
  Storable::DataToStream( theStr, "myLabelColor[2]",  myLabelColor[2] );

  Storable::DataToStream( theStr, "myOffset[0]", myOffset[0] );
  Storable::DataToStream( theStr, "myOffset[1]", myOffset[1] );
  Storable::DataToStream( theStr, "myOffset[2]", myOffset[2] );
}
/*!
  Called from engine to restore table from the file
*/
VISU::Storable* VISU::PointMap3d_i::StorableEngine(SALOMEDS::SObject_ptr theSObject,
						   const Storable::TRestoringMap& theMap,
						   const std::string& thePrefix,
						   CORBA::Boolean theIsMultiFile)
{
  SALOMEDS::Study_var aStudy = theSObject->GetStudy();
  VISU::PointMap3d_i* pResent = new VISU::PointMap3d_i( aStudy, "" );
  return pResent->Restore( theMap, theSObject);
}
/*!
  Gets title for the original table object
*/
QString VISU::PointMap3d_i::GetTableTitle()
{
  SALOMEDS::SObject_var SO = mySObj;
  SALOMEDS::StudyBuilder_var Builder = GetStudyDocument()->NewBuilder();
  SALOMEDS::GenericAttribute_var        anAttr;
  SALOMEDS::AttributeTableOfInteger_var anInt;
  SALOMEDS::AttributeTableOfReal_var    aReal;
  if ( !SO->_is_nil() ) {
    if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfInteger" ) ) {
      anInt = SALOMEDS::AttributeTableOfInteger::_narrow( anAttr );
      CORBA::String_var aString = anInt->GetTitle();
      return aString.in();
    }
    else if ( Builder->FindAttribute( SO, anAttr, "AttributeTableOfReal" ) ) {
      aReal = SALOMEDS::AttributeTableOfReal::_narrow( anAttr );
      CORBA::String_var aString = aReal->GetTitle();
      return aString.in();
    }
  }
  return "";
}

//---------------------------------------------------------------
void VISU::PointMap3d_i::RemoveFromStudy()
{
  struct TRemoveFromStudy: public SALOME_Event
  {
    VISU::PointMap3d_i* myRemovable;
    TRemoveFromStudy(VISU::PointMap3d_i* theRemovable):
      myRemovable(theRemovable)
    {}
    
    virtual
    void
    Execute()
    {
      VISU::RemoveFromStudy(myRemovable->GetSObject(),false);
    }
  };

  // Remove the table with all curves
  ProcessVoidEvent(new TRemoveFromStudy(this));
}

//----------------------------------------------------------------
void VISU::PointMap3d_i::SetOffset(CORBA::Float theDx, CORBA::Float theDy, CORBA::Float theDz)
{
  VISU::TSetModified aModified(this);
  myOffset[0] = theDx;
  myOffset[1] = theDy;
  myOffset[2] = theDz;
  myParamsTime.Modified();
}

void VISU::PointMap3d_i::GetOffset(CORBA::Float& theDx, CORBA::Float& theDy, CORBA::Float& theDz)
{
  theDx = myOffset[0];
  theDy = myOffset[1];
  theDz = myOffset[2];
}

void VISU::PointMap3d_i::SetMarkerStd(VISU::MarkerType, VISU::MarkerScale)
{
}

void VISU::PointMap3d_i::SetMarkerTexture(CORBA::Long)
{
}

VISU::MarkerType VISU::PointMap3d_i::GetMarkerType()
{
  return VISU::MT_NONE;
}

VISU::MarkerScale VISU::PointMap3d_i::GetMarkerScale()
{
  return VISU::MS_NONE;
}

CORBA::Long VISU::PointMap3d_i::GetMarkerTexture()
{
  return 0;
}

CORBA::Float VISU::PointMap3d_i::GetMemorySize()
{
  CORBA::Float aSize = GetSpecificPL()->GetMemorySize();

  int anEnd = myActorCollection->GetNumberOfItems();
  for(int anId = 0; anId < anEnd; anId++)
    if(vtkObject* anObject = myActorCollection->GetItemAsObject(anId))
      if(VISU_Actor* anActor = dynamic_cast<VISU_Actor*>(anObject)){
	aSize += anActor->GetMemorySize();
	//cout<<"Prs3d_i::GetMemorySize - "<<this<<"; anActor = "<<aSize / (1024.0 * 1024.0)<<endl;
      }

  // Convert to mega bytes
  return aSize / (1024.0 * 1024.0);
}

//------------------ ColoredPrs3dBase Methods --------------------
CORBA::Double VISU::PointMap3d_i::GetMin()
{
  return myTablePL->GetScalarRange()[0];
}

CORBA::Double VISU::PointMap3d_i::GetMinTableValue()
{
  SALOMEDS::GenericAttribute_var anAttr;
  mySObj->FindAttribute(anAttr, "AttributeTableOfReal");
  SALOMEDS::AttributeTableOfReal_var aTableOfReal = SALOMEDS::AttributeTableOfReal::_narrow(anAttr);
  double aMin = aTableOfReal->GetValue(1, 1);
  
  for (int i=1; i<=aTableOfReal->GetNbColumns(); i++)
    for (int j=1; j<=aTableOfReal->GetNbRows(); j++) {
      double aVal = aTableOfReal->GetValue(j, i);
      if (aVal < aMin)
	aMin = aVal;
    }
  
  return aMin;
}

CORBA::Double VISU::PointMap3d_i::GetMaxTableValue()
{
  SALOMEDS::GenericAttribute_var anAttr;
  mySObj->FindAttribute(anAttr, "AttributeTableOfReal");
  SALOMEDS::AttributeTableOfReal_var aTableOfReal = SALOMEDS::AttributeTableOfReal::_narrow(anAttr);
  double aMax = aTableOfReal->GetValue(1, 1);

  for (int i=1; i<=aTableOfReal->GetNbColumns(); i++)
    for (int j=1; j<=aTableOfReal->GetNbRows(); j++) {
      double aVal = aTableOfReal->GetValue(j, i);
      if (aVal > aMax)
	aMax = aVal;
    }

  return aMax;
}

CORBA::Double VISU::PointMap3d_i::GetMax()
{
  return myTablePL->GetScalarRange()[1];
}

void VISU::PointMap3d_i::SetRange(CORBA::Double theMin, CORBA::Double theMax)
{
  vtkFloatingPointType aScalarRange[2] = {theMin, theMax};
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformedGridPL, vtkFloatingPointType*>
		   (GetSpecificPL(), &VISU_DeformedGridPL::SetScalarRange, aScalarRange));

  UseFixedRange(true);
}

CORBA::Double VISU::PointMap3d_i::GetSourceMin()
{
  vtkFloatingPointType aRange[2];
  GetSpecificPL()->GetSourceRange(aRange);
  return aRange[0];
}

CORBA::Double VISU::PointMap3d_i::GetSourceMax()
{
  vtkFloatingPointType aRange[2];
  GetSpecificPL()->GetSourceRange(aRange);
  return aRange[1];
}

void VISU::PointMap3d_i::SetSourceRange()
{
  GetSpecificPL()->SetSourceRange();
  ProcessVoidEvent(new TVoidMemFunEvent<VISU_DeformedGridPL>
		   (GetSpecificPL(), &VISU_DeformedGridPL::SetSourceRange));

  UseFixedRange(false);
}

CORBA::Boolean VISU::PointMap3d_i::IsRangeFixed()
{
  return myIsFixedRange; 
}

void VISU::PointMap3d_i::UseFixedRange(bool theRange)
{
  if(myIsFixedRange == theRange)
    return;

  myIsFixedRange = theRange;
}

void VISU::PointMap3d_i::SetPosition(CORBA::Double theX, CORBA::Double theY)
{
  bool anIsSameValue = VISU::CheckIsSameValue(myPosition[0], theX);
  anIsSameValue &= VISU::CheckIsSameValue(myPosition[1], theY);
  if(anIsSameValue)
    return;

  myPosition[0] = theX; 
  myPosition[1] = theY;
  myParamsTime.Modified();
}

CORBA::Double VISU::PointMap3d_i::GetPosX()
{
  return myPosition[0];
}

CORBA::Double VISU::PointMap3d_i::GetPosY()
{
  return myPosition[1];
}

void VISU::PointMap3d_i::SetSize(CORBA::Double theWidth, CORBA::Double theHeight)
{
  bool anIsSameValue = VISU::CheckIsSameValue(myWidth, theWidth);
  anIsSameValue &= VISU::CheckIsSameValue(myHeight, theHeight);
  if(anIsSameValue)
    return;

  myWidth = theWidth; 
  myHeight = theHeight;
  myParamsTime.Modified();
}

CORBA::Double VISU::PointMap3d_i::GetHeight()
{
  return myHeight;
}

CORBA::Double VISU::PointMap3d_i::GetWidth()
{
  return myWidth;
}

void VISU::PointMap3d_i::SetNbColors(CORBA::Long theNbColors)
{
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformedGridPL, int>
		   (GetSpecificPL(), &VISU_DeformedGridPL::SetNbColors, theNbColors));
}

CORBA::Long VISU::PointMap3d_i::GetNbColors()
{
  return GetSpecificPL()->GetNbColors();
}

void VISU::PointMap3d_i::SetLabels(CORBA::Long theNbLabels)
{
  if(myNumberOfLabels == theNbLabels)
    return;

  myNumberOfLabels = theNbLabels;
}

CORBA::Long VISU::PointMap3d_i::GetLabels()
{
  return myNumberOfLabels;
}

void VISU::PointMap3d_i::SetBarOrientation(VISU::ColoredPrs3dBase::Orientation theBarOrientation)
{
  if(myBarOrientation == theBarOrientation)
    return;

  if ( ( theBarOrientation == VISU::ColoredPrs3dBase::VERTICAL && myHeight < myWidth ) ||
       ( theBarOrientation == VISU::ColoredPrs3dBase::HORIZONTAL && myHeight > myWidth ) ) {
    vtkFloatingPointType tmp = myHeight;
    myHeight = myWidth;
    myWidth = tmp;
  }

  myBarOrientation = theBarOrientation;
  myParamsTime.Modified();
}

VISU::ColoredPrs3dBase::Orientation VISU::PointMap3d_i::GetBarOrientation()
{
  return myBarOrientation;
}

//------------------- ScaledPrs3d Methods -----------------------

void VISU::PointMap3d_i::SetScaling(VISU::Scaling theScaling)
{
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformedGridPL, int>
		   (GetSpecificPL(), &VISU_DeformedGridPL::SetScaling, theScaling));
}

VISU::Scaling VISU::PointMap3d_i::GetScaling()
{
  return VISU::Scaling(GetSpecificPL()->GetScaling());
}

//------------------- Check Table on Positive Values ------------
bool VISU::PointMap3d_i::IsPositiveTable()
{
  SALOMEDS::GenericAttribute_var anAttr;
  mySObj->FindAttribute(anAttr, "AttributeTableOfReal");
  SALOMEDS::AttributeTableOfReal_var aTableOfReal = SALOMEDS::AttributeTableOfReal::_narrow(anAttr);

  for (int i=1; i<=aTableOfReal->GetNbColumns(); i++)
    for (int j=1; j<=aTableOfReal->GetNbRows(); j++) {
      double aVal = aTableOfReal->GetValue(j, i);
      if (aVal < 0)
	return false;
    }

  return true;
}

//------------------- Plot3dBase Methods ------------------------

void VISU::PointMap3d_i::SetScaleFactor (CORBA::Double theScaleFactor)
{
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformedGridPL, vtkFloatingPointType>
		   (GetSpecificPL(), &VISU_DeformedGridPL::SetScaleFactor, theScaleFactor));
}

CORBA::Double VISU::PointMap3d_i::GetScaleFactor ()
{
  return myTablePL->GetScaleFactor();
}

void VISU::PointMap3d_i::SetContourPrs (CORBA::Boolean theIsContourPrs )
{
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformedGridPL, bool>
		   (GetSpecificPL(), &VISU_DeformedGridPL::SetContourPrs, theIsContourPrs));
}

CORBA::Boolean VISU::PointMap3d_i::GetIsContourPrs()
{
  return myTablePL->GetIsContourPrs();
}

void VISU::PointMap3d_i::SetNbOfContours (CORBA::Long theNb)
{
  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_DeformedGridPL, int>
		   (GetSpecificPL(), &VISU_DeformedGridPL::SetNumberOfContours, theNb));
}

CORBA::Long VISU::PointMap3d_i::GetNbOfContours ()
{
  return myTablePL->GetNumberOfContours();
}

//-------------------- Actor Factory Methods --------------------

bool VISU::PointMap3d_i::GetActiveState ()
{
  return myIsActiveState;
}

void VISU::PointMap3d_i::SetActiveState ( bool theState )
{
  myIsActiveState = theState;
}

void VISU::PointMap3d_i::RemoveActor (VISU_ActorBase* theActor)
{
  myActorCollection->RemoveItem(theActor);
}

void VISU::PointMap3d_i::RemoveActors ()
{
  ProcessVoidEvent(new TInvokeSignalEvent(myRemoveActorsFromRendererSignal));
  myActorCollection->RemoveAllItems();
}


//-----------------------Text Properties & Label Properties------------------
bool VISU::PointMap3d_i::IsBoldTitle() 
{ 
  return myIsBoldTitle;
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetBoldTitle(bool theIsBoldTitle)
{
  if(myIsBoldTitle == theIsBoldTitle)
    return;

  myIsBoldTitle = theIsBoldTitle;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool VISU::PointMap3d_i::IsItalicTitle() 
{ 
  return myIsItalicTitle;
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetItalicTitle(bool theIsItalicTitle)
{ 
  if(myIsItalicTitle == theIsItalicTitle)
    return;

  myIsItalicTitle = theIsItalicTitle;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool VISU::PointMap3d_i::IsShadowTitle() 
{ 
  return myIsShadowTitle;
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetShadowTitle(bool theIsShadowTitle)
{ 
  if(myIsShadowTitle == theIsShadowTitle)
    return;

  myIsShadowTitle = theIsShadowTitle;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
int VISU::PointMap3d_i::GetTitFontType()
{
  return myTitFontType;
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetTitFontType(int theTitFontType)
{
  if(myTitFontType == theTitFontType)
    return;

  myTitFontType = theTitFontType;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::GetTitleColor(vtkFloatingPointType& theR, 
					 vtkFloatingPointType& theG, 
					 vtkFloatingPointType& theB)
{
  theR = myTitleColor[0]; 
  theG = myTitleColor[1]; 
  theB = myTitleColor[2];
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetTitleColor(vtkFloatingPointType theR, 
					 vtkFloatingPointType theG, 
					 vtkFloatingPointType theB)
{
  bool anIsSameValue = VISU::CheckIsSameValue(myTitleColor[0], theR);
  anIsSameValue &= VISU::CheckIsSameValue(myTitleColor[1], theG);
  anIsSameValue &= VISU::CheckIsSameValue(myTitleColor[2], theB);
  if(anIsSameValue)
    return;

  myTitleColor[0] = theR; 
  myTitleColor[1] = theG; 
  myTitleColor[2] = theB; 
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool VISU::PointMap3d_i::IsBoldLabel()
{
  return myIsBoldLabel;
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetBoldLabel(bool theIsBoldLabel) 
{
  if(myIsBoldLabel == theIsBoldLabel)
    return;

  myIsBoldLabel = theIsBoldLabel;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool VISU::PointMap3d_i::IsItalicLabel() 
{
  return myIsItalicLabel;
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetItalicLabel(bool theIsItalicLabel)
{
  if(myIsItalicLabel == theIsItalicLabel)
    return;

  myIsItalicLabel = theIsItalicLabel;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
bool VISU::PointMap3d_i::IsShadowLabel() 
{
  return myIsShadowLabel;
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetShadowLabel(bool theIsShadowLabel)
{
  if(myIsShadowLabel == theIsShadowLabel)
    return;

  myIsShadowLabel = theIsShadowLabel;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
int VISU::PointMap3d_i::GetLblFontType()
{
  return myLblFontType;
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetLblFontType(int theLblFontType)
{
  if(myLblFontType == theLblFontType)
    return;

  myLblFontType = theLblFontType;
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::GetLabelColor(vtkFloatingPointType& theR, 
				  vtkFloatingPointType& theG, 
				  vtkFloatingPointType& theB)
{
  theR = myLabelColor[0]; 
  theG = myLabelColor[1]; 
  theB = myLabelColor[2];
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetLabelColor(vtkFloatingPointType theR, 
				  vtkFloatingPointType theG, 
				  vtkFloatingPointType theB)
{
  bool anIsSameValue = VISU::CheckIsSameValue(myLabelColor[0], theR);
  anIsSameValue &= VISU::CheckIsSameValue(myLabelColor[1], theG);
  anIsSameValue &= VISU::CheckIsSameValue(myLabelColor[2], theB);
  if(anIsSameValue)
    return;

  myLabelColor[0] = theR; 
  myLabelColor[1] = theG; 
  myLabelColor[2] = theB; 
  myParamsTime.Modified();
}

//----------------------------------------------------------------------------
CORBA::Long VISU::PointMap3d_i::GetTitleSize() 
{
  return myTitleSize;
}

//----------------------------------------------------------------------------
CORBA::Long VISU::PointMap3d_i::GetLabelSize() 
{
  return myLabelSize;
}

//----------------------------------------------------------------------------
CORBA::Long VISU::PointMap3d_i::GetBarWidth() 
{
  return myBarWidth;
}

//----------------------------------------------------------------------------
CORBA::Long VISU::PointMap3d_i::GetBarHeight() 
{ 
  return myBarHeight;
}

//----------------------------------------------------------------------------
void
VISU::PointMap3d_i::SetLabelsFormat(const char* theFormat)
{
  if( myLabelsFormat != theFormat ){
    myLabelsFormat = theFormat;
    myParamsTime.Modified();
  }
}

//----------------------------------------------------------------------------
char* VISU::PointMap3d_i::GetLabelsFormat() 
{ 
  return CORBA::string_dup(myLabelsFormat.c_str());
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetUnitsVisible(CORBA::Boolean isVisible)
{
  if( myIsUnits != isVisible ){
    myIsUnits = isVisible;
    myParamsTime.Modified();
  }
}

//----------------------------------------------------------------------------
CORBA::Boolean VISU::PointMap3d_i::IsUnitsVisible()
{
  return myIsUnits;
}

//----------------------------------------------------------------------------
void VISU::PointMap3d_i::SetRatios(CORBA::Long theTitleSize, 
			      CORBA::Long theLabelSize, 
			      CORBA::Long theBarWidth, CORBA::Long theBarHeight) 
{
  bool anIsSameValue = VISU::CheckIsSameValue(myTitleSize, theTitleSize);
  anIsSameValue &= VISU::CheckIsSameValue(myLabelSize, theLabelSize);
  anIsSameValue &= VISU::CheckIsSameValue(myBarWidth, theBarWidth);
  anIsSameValue &= VISU::CheckIsSameValue(myBarHeight, theBarHeight);
  if(anIsSameValue)
    return;

  myTitleSize = theTitleSize; 
  myLabelSize = theLabelSize; 
  myBarWidth = theBarWidth; 
  myBarHeight = theBarHeight;
  myParamsTime.Modified();
}
