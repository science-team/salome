// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISUConfig.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISUConfig.hh"

#include "SUIT_ResourceMgr.h"
#include "SUIT_Session.h"

#include "SALOMEDSClient_GenericAttribute.hxx"
#include "SALOMEDSClient_AttributeIOR.hxx"

#include "SalomeApp_Application.h"
#include "SalomeApp_Study.h"

#include "SALOME_Event.h"

#include <QRegExp>

#include <fstream>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

using namespace std;

namespace VISU
{

  SUIT_Session*
  GetSession()
  {
    return SUIT_Session::session();
  }

  SUIT_ResourceMgr*
  GetResourceMgr()
  {
    return GetSession()->resourceMgr();
  }

  //---------------------------------------------------------------------------

  QMutex* Base_i::myMutex = NULL; //apo - &VISUMutex;
  CORBA::ORB_var Base_i::myOrb;
  PortableServer::POA_var Base_i::myPoa;
  SALOME_NamingService* Base_i::myNamingService;
  SALOME_LifeCycleCORBA* Base_i::myEnginesLifeCycle;
  VISU_Gen_i* Base_i::myVisuGenImpl;

  Base_i::Base_i() {}

  Base_i::~Base_i() {}

  char* Base_i::GetID(){
    if(myID == ""){
      CORBA::Object_var anObject = _this();
      CORBA::String_var anIOR = myOrb->object_to_string(anObject);
      myID = anIOR.in();
    }
    return CORBA::string_dup(myID.c_str());
  }

  //---------------------------------------------------------------------------
  static int mySCnt = 0;
  static QMutex aMutex(QMutex::Recursive);

  Mutex::Mutex(QMutex* theMutex): myMutex(&aMutex)
  {
    if(MYDEBUG) MESSAGE("Mutex::Mutex : "<<!mySCnt);
    if(!mySCnt++)
      myMutex->lock();
  }

  Mutex::~Mutex()
  {
    if(!(--mySCnt))
      myMutex->unlock();
    if(MYDEBUG) MESSAGE("Mutex::~Mutex : "<<!mySCnt);
  }


  //---------------------------------------------------------------------------
  Storable::TStorableEngineMap Storable::myStorableEngineMap;

  std::string 
  Storable
  ::ToString()
  {
    ostringstream anOutputStream;
    Storable::DataToStream( anOutputStream, "myComment", GetComment() );
    ToStream(anOutputStream);
    anOutputStream<<ends;
    if(MYDEBUG) MESSAGE("Storable::ToString - "<<anOutputStream.str());
    return anOutputStream.str();
  }


  //---------------------------------------------------------------
  CORBA::Boolean 
  Storable
  ::CanCopy(SALOMEDS::SObject_ptr theObject) 
  {
    return false;
  }


  //---------------------------------------------------------------
  bool 
  Storable
  ::CopyFrom(SALOMEDS::SObject_ptr theObject, 
	     CORBA::Long& theObjectID,
	     const std::string& theTmpDir,
	     TFileNames& theFileNames)
  {
    TFileName aFileName("copy_persistent");
    std::string aCopyPersist =  theTmpDir + aFileName;
    std::ofstream anOutputFileStream(aCopyPersist.c_str(), ios::out);
    anOutputFileStream<<ToString()<<endl;

    theFileNames.push_back(aFileName);
    theObjectID = 1;

    return true;
  }


  //---------------------------------------------------------------
  void 
  Storable
  ::RegistryStorableEngine(const std::string& theComment, 
			   TStorableEngine theEngine)
  {
    if(!myStorableEngineMap.insert(TStorableEngineMap::value_type(theComment, theEngine)).second)
      throw std::logic_error("Storable::Registry >> dupliacte registring !!!");
  }


  //---------------------------------------------------------------
  Storable* 
  Storable
  ::Create(SALOMEDS::SObject_ptr theSObject,
	   const std::string& theLocalPersistentID, 
	   const std::string& thePrefix,
	   CORBA::Boolean theIsMultiFile)
  {
    try{
      QString aString( CorrectPersistentString( theLocalPersistentID ).c_str() );
      TRestoringMap aRestoringMap;
      StringToMap(aString, aRestoringMap);
      bool anIsExists;
      QString aComment = VISU::Storable::FindValue(aRestoringMap, "myComment", &anIsExists);
      if ( anIsExists ) {
	TStorableEngineMap::const_iterator anIter = myStorableEngineMap.find(aComment.toLatin1().data());
	if(MYDEBUG) MESSAGE("Storable::Create - "<<aComment.toLatin1().data()<<" "<<(anIter != myStorableEngineMap.end()));
	if(anIter == myStorableEngineMap.end()) 
	  return NULL;
	TStorableEngine aStorableEngine = anIter->second;
	return aStorableEngine(theSObject, aRestoringMap, thePrefix, theIsMultiFile);
      }
    }catch(std::exception& exc){
      INFOS("Follow exception was occured :\n"<<exc.what());
    }catch(...){
      INFOS("Unknown exception was occured!");
    }
    return NULL;
  }


  //---------------------------------------------------------------
  VISU::VISUType
  Storable
  ::Comment2Type(const std::string& theComment)
  {
    if ( theComment == "CURVE" ) 
      return VISU::TCURVE;
    if ( theComment == "TABLE" ) 
      return VISU::TTABLE;
    if ( theComment == "POINTMAP3D" ) 
      return VISU::TPOINTMAP3D;
    if ( theComment == "CONTAINER" ) 
      return VISU::TCONTAINER;
    if ( theComment == "MESH" ) 
      return VISU::TMESH;
    if ( theComment == "SCALARMAP" ) 
      return VISU::TSCALARMAP;
    if ( theComment == "PRSMERGER" ) 
      return VISU::TSCALARMAP;
    if ( theComment == "ISOSURFACE" ) 
      return VISU::TISOSURFACES;
    if ( theComment == "DEFORMEDSHAPE" ) 
      return VISU::TDEFORMEDSHAPE;
    if ( theComment == "DEFORMEDSHAPEANDSCALARMAP" ) 
      return VISU::TDEFORMEDSHAPEANDSCALARMAP;
    if ( theComment == "GAUSSPOINTS" ) 
      return VISU::TGAUSSPOINTS;
    if ( theComment == "PLOT3D" ) 
      return VISU::TPLOT3D;
    if ( theComment == "CUTPLANES" ) 
      return VISU::TCUTPLANES;
    if ( theComment == "CUTSEGMENT" ) 
      return VISU::TCUTSEGMENT;
    if ( theComment == "VECTORS" ) 
      return VISU::TVECTORS;
    if ( theComment == "STREAMLINES" ) 
      return VISU::TSTREAMLINES;
    if ( theComment == "VISUGEN" ) 
      return VISU::TVISUGEN;
    if ( theComment == "VIEWMANAGER" ) 
      return VISU::TVIEWMANAGER;
    if ( theComment == "RESULT" ) 
      return VISU::TRESULT;
    if ( theComment == "XYPLOT" ) 
      return VISU::TXYPLOT;
    if ( theComment == "TABLEVIEW" ) 
      return VISU::TTABLEVIEW;
    if ( theComment == "VIEW3D" ) 
      return VISU::TVIEW3D;
    if ( theComment == "GAUSSVIEW" ) 
      return VISU::TGAUSSVIEW;
    if ( theComment == "ENTITY" ) 
      return VISU::TENTITY;
    if ( theComment == "FAMILY" ) 
      return VISU::TFAMILY;
    if ( theComment == "PART" ) 
      return VISU::TPART; // MULTIPR
    if ( theComment == "GROUP" ) 
      return VISU::TGROUP;
    if ( theComment == "FIELD" ) 
      return VISU::TFIELD;
    if ( theComment == "TIMESTAMP" ) 
      return VISU::TTIMESTAMP;
    if ( theComment == "ANIMATION" ) 
      return VISU::TANIMATION;
    if ( theComment == "EVOLUTION" ) 
      return VISU::TEVOLUTION;
    if ( theComment == "ALL" ) 
      return VISU::TALL;
    
    return VISU::TNONE;
  }
  

  //---------------------------------------------------------------
  VISU::VISUType
  Storable
  ::RestoringMap2Type(const TRestoringMap& theRestoringMap)
  {
    QString aComment = Storable::FindValue( theRestoringMap, "myComment", "" );
    return Comment2Type(aComment.toLatin1().data());
  }


  //---------------------------------------------------------------
  VISU::VISUType
  Storable
  ::Stream2Type(const std::string& thePersistentString)
  {
    QString aString(thePersistentString.c_str());
    if(aString.isEmpty())
      return VISU::TNONE;

    VISU::Storable::TRestoringMap aRestoringMap;
    Storable::StringToMap( aString, aRestoringMap );
    return RestoringMap2Type( aRestoringMap );	  
  }


  //---------------------------------------------------------------
  VISU::VISUType
  Storable
  ::SObject2Type(const _PTR(SObject)& theSObject)
  {
    _PTR(GenericAttribute) anAttr;
    if (theSObject->FindAttribute(anAttr, "AttributeString")) {
      _PTR(AttributeString) aDataAttr(anAttr);
      std::string aData = aDataAttr->Value();
      return Stream2Type(aData);
    }

    return VISU::TNONE;
  }


  //---------------------------------------------------------------
  std::string 
  Storable
  ::CorrectPersistentString(const std::string& thePersistentString)
  {
    QString aString(thePersistentString.c_str());
    if(aString.isEmpty())
      return aString.toLatin1().data();

    VISU::Storable::TRestoringMap aRestoringMap;
    Storable::StringToMap( aString, aRestoringMap );
	  
    bool anIsFound = false;
    QString aComment = Storable::FindValue( aRestoringMap, "myComment", &anIsFound );
    if ( anIsFound ) {
      if ( aComment == "PRSMERGER" )
	aString.replace( "myComment=PRSMERGER", "myComment=SCALARMAP" );
    }

    return aString.toLatin1().data();
  }


  //---------------------------------------------------------------
  std::string
  Storable
  ::FindEntry(SALOMEDS::Study_ptr theStudyDocument, 
	      const std::string& theStartEntry,
	      const TRestoringMap& theRestoringMap, 
	      int IsAllLevels)
  {
    SALOMEDS::SObject_var aSObject = theStudyDocument->FindObjectID(theStartEntry.c_str());
    SALOMEDS::ChildIterator_var anIter = theStudyDocument->NewChildIterator(aSObject);
    anIter->InitEx(IsAllLevels);
    for(; anIter->More(); anIter->Next()) {
      aSObject = anIter->Value();
      SALOMEDS::GenericAttribute_var anAttr;
      if (aSObject->FindAttribute(anAttr,"AttributeString")) {
	SALOMEDS::AttributeString_var aDataAttr = SALOMEDS::AttributeString::_narrow(anAttr);
	CORBA::String_var aString = aDataAttr->Value();
	VISU::Storable::TRestoringMap aRestoringMap;
	Storable::StringToMap( aString.in(), aRestoringMap );
	bool anIsSame = true;
	TRestoringMap::const_iterator anIter = theRestoringMap.begin();
	for(; anIter != theRestoringMap.end(); anIter++){
	  const TRestoringMap::key_type& aKey = anIter->first;
	  TRestoringMap::const_iterator anIter2 = aRestoringMap.find(aKey);
	  if(anIter2 != aRestoringMap.end()){
	    if (anIter->second == anIter2->second)
	      continue;
	  }
	  anIsSame = false;
	  break;
	}
	if(anIsSame){
	  CORBA::String_var anEntry = aSObject->GetID();
	  return anEntry.in();
	}
      }
    }
    return "";
  }


  //----------------------------------------------------------------------------
  void 
  Storable
  ::StringToMap(const QString& theString, 
		TRestoringMap& theMap)
  {
    if(0 && MYDEBUG) MESSAGE("Storable::StringToMap : string="<<theString.toLatin1().data());
    QStringList strList = theString.split( ";", QString::SkipEmptyParts );
    for ( int i = 0; i < strList.count(); i++ ) {
      QString next = strList[ i ];
      int pos = next.indexOf("=");
      QString aName, aValue;
      if ( pos < 0 ) {
	aName = next.trimmed();
	aValue = "";
      }
      else {
	aName = next.left( pos ).trimmed();
	aValue = next.mid( pos+1 );
	if ( aValue.isNull() )
	  aValue = "";
      }
      if ( !aName.isEmpty() )
	theMap.insert( TRestoringMap::value_type( (const char*)aName.toLatin1(), aValue ) );
    }
  }


  //----------------------------------------------------------------------------
  Storable::TRestoringMap
  Storable
  ::GetStorableMap(_PTR(SObject) theSObject)
  {
    Storable::TRestoringMap aRestoringMap;
    if(theSObject){
      _PTR(GenericAttribute) anAttr;
      if(theSObject->FindAttribute(anAttr,"AttributeString")){
	_PTR(AttributeString) aComment (anAttr);
	std::string aValue = aComment->Value();
	VISU::Storable::StringToMap(aValue.c_str(), aRestoringMap);
      }
    }
    return aRestoringMap;
  }


  //----------------------------------------------------------------------------
  void
  Storable
  ::DataToStream(std::ostringstream& theStr, 
		 const QString& theName, 
		 const QString& theVal) 
  {
    QString output = ( !theName.isNull() ? theName : QString("") )
                   + QString( "=" )
		   + ( !theVal.isNull()  ? theVal  : QString("") );
    theStr<<(const char*)output.toLatin1()<<";";
  }


  //---------------------------------------------------------------
  void 
  Storable
  ::DataToStream(std::ostringstream& theStr, 
		 const QString& theName, 
		 int theVal) 
  {
    QString output = ( !theName.isNull() ? theName : QString("") )
                   + QString( "=" )
		   + QString::number( theVal );
    theStr<<(const char*)output.toLatin1()<<";";
  }


  //---------------------------------------------------------------
  void 
  Storable
  ::DataToStream(std::ostringstream& theStr, 
		 const QString& theName, 
		 long theVal) 
  {
    QString output = ( !theName.isNull() ? theName : QString("") )
                   + QString( "=" )
		   + QString::number( theVal );
    theStr<<(const char*)output.toLatin1()<<";";
  }


  //---------------------------------------------------------------
  void
  Storable
  ::DataToStream(std::ostringstream& theStr, 
		 const QString& theName, 
		 double theVal) 
  {
    QString output = ( !theName.isNull() ? theName : QString("") )
                   + QString( "=" )
		   + QString::number( theVal );
    theStr<<output.toLatin1().data()<<";";
  }


  //---------------------------------------------------------------
  QString
  Storable
  ::FindValue(const TRestoringMap& theMap, 
	      const std::string& theArg, 
	      bool* theIsFind)
  {
    TRestoringMap::const_iterator anIter = theMap.find(theArg);
    if(anIter == theMap.end()) {
      if(MYDEBUG) MESSAGE("Storable::Init >> there is no value for "<<theArg);
      if(theIsFind != NULL) 
	*theIsFind = false;
      //throw std::logic_error(string("Storable::Init >> there is no value for ") + theArg);
      static QString BAD_VALUE("NULL");
      return BAD_VALUE;
    }
    if(theIsFind != NULL) 
      *theIsFind = true;
    return anIter->second;
  }


  //---------------------------------------------------------------
  QString
  Storable
  ::FindValue(const TRestoringMap& theMap, 
	      const std::string& theArg, 
	      const QString& theDefaultValue)
  {
    bool anIsFound = false;
    QString aValue = FindValue(theMap,theArg,&anIsFound);
    if(anIsFound)
      return aValue;
    return theDefaultValue;
  }


  //---------------------------------------------------------------------------
  PortableServer::ServantBase_var
  GetServant(CORBA::Object_ptr theObject)
  {
    if(CORBA::is_nil(theObject))  
      return NULL;
    try{
      PortableServer::POA_ptr aPOA = Base_i::GetPOA();
      PortableServer::Servant aServant = aPOA->reference_to_servant(theObject);
      return aServant;
    } catch (...) {
      INFOS("GetServant - Unknown exception was occured!!!");
      return NULL;
    }
  }


  //---------------------------------------------------------------------------
  CORBA::Object_var 
  SObjectToObject(SALOMEDS::SObject_ptr theSObject)
  {
    SALOMEDS::GenericAttribute_var anAttr;
    CORBA::Object_var anObject;
    if(CORBA::is_nil(theSObject))
      return anObject;
    try{
      if(theSObject->FindAttribute(anAttr, "AttributeIOR")){
	SALOMEDS::AttributeIOR_var anIOR  = SALOMEDS::AttributeIOR::_narrow(anAttr);
	CORBA::String_var aValue = anIOR->Value();
	CORBA::ORB_ptr anORB = Base_i::GetORB();
	if(strcmp(aValue,"") != 0)
	  anObject = anORB->string_to_object(aValue);
      }
    }catch(...){
      INFOS("SObjectToObject - Unknown exception was occured!!!");
    }
    return anObject;
  }

  //---------------------------------------------------------------------------
  CORBA::Object_var
  ClientSObjectToObject(_PTR(SObject) theSObject)
  {
    _PTR(GenericAttribute) anAttr;
    CORBA::Object_var anObject;
    if(!theSObject)
      return anObject;
    try{
      if(theSObject->FindAttribute(anAttr, "AttributeIOR")){
	_PTR(AttributeIOR) anIOR  = anAttr;
	CORBA::String_var aValue = anIOR->Value().c_str();
	CORBA::ORB_ptr anORB = Base_i::GetORB();
	if(strcmp(aValue,"") != 0)
	  anObject = anORB->string_to_object(aValue);
      }
    }catch(...){
      INFOS("ClientSObjectToObject - Unknown exception was occured!!!");
    }
    return anObject;
  }


  //---------------------------------------------------------------------------
  std::string
  CreateAttributes(SALOMEDS::Study_ptr theStudyDocument,
		   const std::string& theFatherEntry, 
		   const std::string& theIconName,
		   const std::string& theIOR, 
		   const std::string& theName,
		   const std::string& thePersistentRef, 
		   const std::string& theComment,
		   CORBA::Boolean theCreateNew)
  {
    SALOMEDS::StudyBuilder_var aStudyBuilder = theStudyDocument->NewBuilder();
    SALOMEDS::SObject_var aFather = theStudyDocument->FindObjectID(theFatherEntry.c_str());
    SALOMEDS::SObject_var aNewSObject;
    if (theCreateNew) 
      aNewSObject = aStudyBuilder->NewObject(aFather);
    else
      aNewSObject = aFather;
    SALOMEDS::GenericAttribute_var anAttr;
    if(theIOR != NO_IOR){
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributeIOR");
      SALOMEDS::AttributeIOR_var aCustomAttr = SALOMEDS::AttributeIOR::_narrow(anAttr);
      aCustomAttr->SetValue(theIOR.c_str());
    }
    if(theName != NO_NAME){
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributeName");
      SALOMEDS::AttributeName_var aCustomAttr = SALOMEDS::AttributeName::_narrow(anAttr);
      aCustomAttr->SetValue(theName.c_str());
    }
    if(thePersistentRef != NO_PERFSITENT_REF){
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributePersistentRef");
      SALOMEDS::AttributePersistentRef_var aCustomAttr = SALOMEDS::AttributePersistentRef::_narrow(anAttr);
      aCustomAttr->SetValue(thePersistentRef.c_str());
    }
    if(theComment != NO_COMMENT){
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributeString");
      SALOMEDS::AttributeString_var aCustomAttr = SALOMEDS::AttributeString::_narrow(anAttr);
      aCustomAttr->SetValue(theComment.c_str());
    }
    if(theIconName != NO_ICON){
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributePixMap");
      SALOMEDS::AttributePixMap_var aCustomAttr = SALOMEDS::AttributePixMap::_narrow(anAttr);
      aCustomAttr->SetPixMap(theIconName.c_str());
    }
    CORBA::String_var anEntry = aNewSObject->GetID();
    std::string aRet(anEntry);
    if(MYDEBUG)
      INFOS("CreateAttributes - StudyId = "<<theStudyDocument->StudyId()<<"; anEntry = "<<aRet<<"; IOR = '"<<theIOR<<"'");
    return aRet;
  }
  
  std::string
  CreateAttributes(_PTR(Study) theStudyDocument,
		   const std::string& theFatherEntry, 
		   const std::string& theIconName,
		   const std::string& theIOR, 
		   const std::string& theName,
		   const std::string& thePersistentRef, 
		   const std::string& theComment,
		   CORBA::Boolean theCreateNew)
  {
    _PTR(StudyBuilder) aStudyBuilder = theStudyDocument->NewBuilder();
    _PTR(SObject) aFather = theStudyDocument->FindObjectID(theFatherEntry);
    _PTR(SObject) aNewSObject;
    if (theCreateNew)
    {
      aNewSObject = aStudyBuilder->NewObject(aFather);
    }
    else
      aNewSObject = aFather;

    _PTR(GenericAttribute) anAttr;
    if (theIOR != NO_IOR) {
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributeIOR");
      _PTR(AttributeIOR) aCustomAttr (anAttr);
      aCustomAttr->SetValue(theIOR);
    }
    if (theName != NO_NAME) {
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributeName");
      _PTR(AttributeName) aCustomAttr (anAttr);
      aCustomAttr->SetValue(theName);
    }
    if (thePersistentRef != NO_PERFSITENT_REF) {
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributePersistentRef");
      _PTR(AttributePersistentRef) aCustomAttr (anAttr);
      aCustomAttr->SetValue(thePersistentRef);
    }
    if (theComment != NO_COMMENT) {
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributeString");
      _PTR(AttributeString) aCustomAttr (anAttr);
      aCustomAttr->SetValue(theComment);
    }
    if (theIconName != NO_ICON) {
      anAttr = aStudyBuilder->FindOrCreateAttribute(aNewSObject, "AttributePixMap");
      _PTR(AttributePixMap) aCustomAttr (anAttr);
      aCustomAttr->SetPixMap(theIconName);
    }
    std::string aRet = aNewSObject->GetID();
    if (MYDEBUG)
      INFOS("CreateAttributes - StudyId = " << theStudyDocument->StudyId()
	    << "; anEntry = " << aRet << "; IOR = '" << theIOR << "'");
    return aRet;
  }


  //---------------------------------------------------------------
  QString 
  GenerateName(const std::string& theFmt, int /*theId*/) // theId was deprecated because of incorrect work IPAL21151
  {
    QString aName;
    aName.sprintf("%s", theFmt.c_str());
    int count = 0;

    SalomeApp_Application* anApp =
      dynamic_cast< SalomeApp_Application* >( SUIT_Session::session()->activeApplication() );
    if ( anApp) {
      SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>( anApp->activeStudy() );

      if(aStudy){
	while (1) {
	  std::vector<_PTR(SObject)> aList = aStudy->studyDS()->FindObjectByName(aName.toLatin1().data(), "VISU");
	  if ( aList.size() > 0 ) {
	    count++;
	    aName.sprintf("%s:%d", theFmt.c_str(), count);
	  } else
	    break;
	}
      }
    }
    
    if(count > 0)
      aName.sprintf("%s:%d", theFmt.c_str(), count);

    return aName;
  }


  //---------------------------------------------------------------
  SALOMEDS::StudyManager_var 
  GetStudyManager()
  {
    static SALOMEDS::StudyManager_var aStudyManager;
    if(CORBA::is_nil(aStudyManager)){
      SALOME_NamingService *aNamingService = SalomeApp_Application::namingService();
      CORBA::Object_var anObject = aNamingService->Resolve("/myStudyManager");
      aStudyManager = SALOMEDS::StudyManager::_narrow(anObject);
    }
    return aStudyManager;
  }


  //---------------------------------------------------------------
  SALOMEDS::Study_var 
  GetDSStudy(_PTR(Study) theStudy)
  {
    int aStudyID = theStudy->StudyId();
    return GetStudyManager()->GetStudyByID(aStudyID);
  }


  //---------------------------------------------------------------
  struct TGetStudyEvent: public SALOME_Event
  {
    SALOMEDS::Study_var myStudy;
    
    typedef SalomeApp_Study* TResult;
    TResult myResult;
    
    TGetStudyEvent(SALOMEDS::Study_ptr theStudy):
      myStudy(SALOMEDS::Study::_duplicate(theStudy)),
      myResult(NULL)
    {}
    
    virtual
    void
    Execute()
    {
      if(CORBA::is_nil(myStudy))
	return;
      
      int aStudyId = myStudy->StudyId();
      SUIT_Session* aSession = SUIT_Session::session();
      QList<SUIT_Application*> anApplications = aSession->applications();
      QListIterator<SUIT_Application*> anIter (anApplications);
      while ( anIter.hasNext() ) {
	SUIT_Application* anApp = anIter.next();
	if (SUIT_Study* aSStudy = anApp->activeStudy()) {
	  if (SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(aSStudy)) {
	    if (_PTR(Study) aCStudy = aStudy->studyDS()) {
	      if (aStudyId == aCStudy->StudyId()) {
		myResult = aStudy;
		break;
	      }
	    }
	  }
	}
      }
    }
  };
  

  //---------------------------------------------------------------
  SalomeApp_Study* 
  GetGUIStudy(SALOMEDS::Study_ptr theStudy)
  {
    return ProcessEvent(new TGetStudyEvent(theStudy));
  }


  //---------------------------------------------------------------
  _PTR(Study) 
  GetStudy(SALOMEDS::Study_ptr theStudy)
  {
    if(SalomeApp_Study* aStudy = ProcessEvent(new TGetStudyEvent(theStudy)))
      return aStudy->studyDS();
    
    return _PTR(Study)();
  }


  //---------------------------------------------------------------
  SALOMEDS::SObject_var 
  GetSObject( _PTR(SObject) obj )
  {
    _PTR(Study) aStudy = obj->GetStudy();
    SALOMEDS::Study_var aSalomeDSStudy = GetDSStudy( aStudy );
    std::string id = obj->GetID();
    return aSalomeDSStudy->FindObjectID( id.c_str() );
  }

  //---------------------------------------------------------------
  _PTR(SObject) 
  GetClientSObject(SALOMEDS::SObject_ptr theSObject,
		   _PTR(Study) theStudy)
  {
    CORBA::String_var anEntry = theSObject->GetID();
    return theStudy->FindObjectID( anEntry.in() );
  }


  //---------------------------------------------------------------
  void
  RemoveFromStudy (SALOMEDS::SObject_ptr theSObject,
		   bool theIsAttrOnly,
		   bool theDestroySubObjects)
  {
    if (theSObject->_is_nil()) return;

    SALOMEDS::Study_var aStudyDocument = theSObject->GetStudy();
    SALOMEDS::StudyBuilder_var aStudyBuilder = aStudyDocument->NewBuilder();
    if (theIsAttrOnly) {
      aStudyBuilder->RemoveAttribute(theSObject,"AttributeIOR");
      return;
    }

    // Remove possible sub-objects
    SALOMEDS::ChildIterator_var aChildIter = aStudyDocument->NewChildIterator(theSObject);
    for (aChildIter->InitEx(true); aChildIter->More(); aChildIter->Next()) {
      SALOMEDS::SObject_var aChildSObject = aChildIter->Value();
      CORBA::Object_var aChildObj = VISU::SObjectToObject(aChildSObject);
      if (CORBA::is_nil(aChildObj)) continue;

      VISU::RemovableObject_var aRemovableObject = VISU::RemovableObject::_narrow(aChildObj);
      if (CORBA::is_nil(aRemovableObject)) continue;

      aRemovableObject->RemoveFromStudy();
    }

    // asl, fix for PAL10455: Remove references to SObject
    SALOMEDS::Study::ListOfSObject_var aRefs = aStudyDocument->FindDependances( theSObject );
    for( int i=0, n=aRefs->length(); i<n; i++ )
    {
      SALOMEDS::SObject_var o = aRefs[i];
      if( o->GetFatherComponent()->ComponentDataType()==theSObject->GetFatherComponent()->ComponentDataType() )
      {
	aStudyBuilder->RemoveReference( o );
	aStudyBuilder->RemoveObjectWithChildren( o );
      }
    }

    // Remove the SObject itself
    aStudyBuilder->RemoveObjectWithChildren(theSObject);
  }


  //---------------------------------------------------------------
  void
  RemoveFromStudy (_PTR(SObject) theSObject,
		   bool theIsAttrOnly,
		   bool theDestroySubObjects)
  {
    if (!theSObject) return;

    _PTR(Study) aStudyDocument = theSObject->GetStudy();
    _PTR(StudyBuilder) aStudyBuilder = aStudyDocument->NewBuilder();
    if (theIsAttrOnly) {
      aStudyBuilder->RemoveAttribute(theSObject,"AttributeIOR");
      return;
    }

    // Remove possible sub-objects
    _PTR(ChildIterator) aChildIter = aStudyDocument->NewChildIterator(theSObject);
    for (aChildIter->InitEx(true); aChildIter->More(); aChildIter->Next()) {
      _PTR(SObject) aChildSObject = aChildIter->Value();
      CORBA::Object_var aChildObj = VISU::ClientSObjectToObject(aChildSObject);
      if (CORBA::is_nil(aChildObj)) 
	continue;

      VISU::RemovableObject_var aRemovableObject = VISU::RemovableObject::_narrow(aChildObj);
      if (CORBA::is_nil(aRemovableObject))
	continue;

      aRemovableObject->RemoveFromStudy();
    }

    // Remove the SObject itself
    aStudyBuilder->RemoveObjectWithChildren(theSObject);
  }
}
