// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_PrsObject_i.cxx
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_PrsObject_i.hh"

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

//---------------------------------------------------------------
VISU::RemovableObject_i
::RemovableObject_i()
{
  if(MYDEBUG) MESSAGE("RemovableObject_i::RemovableObject_i - this = "<<this);
}


//---------------------------------------------------------------
VISU::RemovableObject_i
::~RemovableObject_i() 
{
  if(MYDEBUG) MESSAGE("RemovableObject_i::~RemovableObject_i - this = "<<this);
}


//---------------------------------------------------------------
void
VISU::RemovableObject_i
::SetName(const std::string& theName,
	  bool theIsUpdateStudyAttr)
{
  myName = theName;
  if(theIsUpdateStudyAttr){
    SALOMEDS::SObject_var aSObject = GetStudyDocument()->FindObjectID(GetEntry().c_str());
    if(!aSObject->_is_nil()){
      SALOMEDS::StudyBuilder_var aBuilder = GetStudyDocument()->NewBuilder();
      SALOMEDS::GenericAttribute_var anAttr = aBuilder->FindOrCreateAttribute( aSObject, "AttributeName" );
      SALOMEDS::AttributeName_var aNameAttr = SALOMEDS::AttributeName::_narrow( anAttr );
      aNameAttr->SetValue( theName.c_str() );
    }
  }
}


//---------------------------------------------------------------
std::string
VISU::RemovableObject_i
::GetName() const 
{ 
  return myName;
}


//---------------------------------------------------------------
SALOMEDS::Study_var
VISU::RemovableObject_i
::GetStudyDocument() const 
{ 
  return myStudyDocument;
}


//---------------------------------------------------------------
SalomeApp_Study*
VISU::RemovableObject_i
::GetGUIStudy() const 
{ 
  return myGUIStudy;
}


//---------------------------------------------------------------
void
VISU::RemovableObject_i
::SetStudyDocument(SALOMEDS::Study_ptr theStudy) 
{ 
  myStudyDocument = SALOMEDS::Study::_duplicate(theStudy);
  myGUIStudy = VISU::GetGUIStudy(theStudy);
}


//---------------------------------------------------------------
std::string 
VISU::RemovableObject_i
::GetEntry() 
{ 
  CORBA::String_var anIOR = GetID();
  SALOMEDS::SObject_var aSObject = GetStudyDocument()->FindObjectIOR(anIOR.in());
  CORBA::String_var anEntry("");
  if(!CORBA::is_nil(aSObject.in()))
    anEntry = aSObject->GetID();
  return anEntry.in();
}


//---------------------------------------------------------------
VISU::PrsObject_i
::PrsObject_i(SALOMEDS::Study_ptr theStudy)
{
  SetStudyDocument(theStudy);
}


