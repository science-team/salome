// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_Vectors_i.cc
//  Author : Alexey PETROV
//  Module : VISU
//
#include "VISU_Vectors_i.hh"
#include "VISU_Prs3dUtils.hh"
#include "VISU_Result_i.hh"

#include "VISU_VectorsAct.h"
#include "VISU_VectorsPL.hxx"
#include "VISU_PipeLineUtils.hxx"
#include "VISU_Convertor.hxx"

#include "SUIT_ResourceMgr.h"
#include "SALOME_Event.h"

#include <vtkProperty.h>

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

static int INCMEMORY = 4+12;

using namespace std;

//---------------------------------------------------------------
size_t 
VISU::Vectors_i
::IsPossible(Result_i* theResult, 
	     const std::string& theMeshName, 
	     VISU::Entity theEntity,
	     const std::string& theFieldName, 
	     CORBA::Long theTimeStampNumber,
	     bool theIsMemoryCheck)
{
  try{
    size_t aResult = TSuperClass::IsPossible(theResult,
					     theMeshName,
					     theEntity,
					     theFieldName,
					     theTimeStampNumber,
					     false);
    if(theIsMemoryCheck && aResult){
      bool anIsEstimated = true;
      VISU::Result_i::PInput anInput = theResult->GetInput(theMeshName,
							   theEntity,
							   theFieldName,
							   theTimeStampNumber);
      size_t aSize = anInput->GetTimeStampOnMeshSize(theMeshName,
						     (VISU::TEntity)theEntity,
						     theFieldName,
						     theTimeStampNumber,
						     anIsEstimated);
      if(anIsEstimated)
	aSize *= INCMEMORY;
      aResult = VISU_PipeLine::CheckAvailableMemory(aSize);
      if(MYDEBUG) 
	MESSAGE("Vectors_i::IsPossible - CheckAvailableMemory = "<<float(aSize)<<"; aResult = "<<aResult);
    }
    return aResult;
  }catch(std::exception& exc){
    INFOS("Follow exception was occured :\n"<<exc.what());
  }catch(...){
    INFOS("Unknown exception was occured!");
  }
  return 0;
}

//---------------------------------------------------------------
int VISU::Vectors_i::myNbPresent = 0;

//---------------------------------------------------------------
QString 
VISU::Vectors_i
::GenerateName() 
{
  return VISU::GenerateName("Vectors",myNbPresent++);
}

//---------------------------------------------------------------
const string VISU::Vectors_i::myComment = "VECTORS";

//---------------------------------------------------------------
const char* 
VISU::Vectors_i
::GetComment() const
{
  return myComment.c_str();
}


//---------------------------------------------------------------
const char*
VISU::Vectors_i
::GetIconName()
{
  if (!IsGroupsUsed())
    return "ICON_TREE_VECTORS";
  else
    return "ICON_TREE_VECTORS_GROUPS";
}

//---------------------------------------------------------------
VISU::Vectors_i
::Vectors_i(EPublishInStudyMode thePublishInStudyMode) :
  ColoredPrs3d_i(thePublishInStudyMode),
  ScalarMap_i(thePublishInStudyMode),
  MonoColorPrs_i(thePublishInStudyMode),
  DeformedShape_i(thePublishInStudyMode),
  myLineWidth(1.0),
  myVectorsPL(NULL)
{}


//---------------------------------------------------------------
void
VISU::Vectors_i
::SameAs(const VISU::Prs3d_i* theOrigin)
{
  TSuperClass::SameAs(theOrigin);

  if(const Vectors_i* aPrs3d = dynamic_cast<const Vectors_i*>(theOrigin)){
    Vectors_i* anOrigin = const_cast<Vectors_i*>(aPrs3d);
    SetLineWidth(anOrigin->GetLineWidth());
  }
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::Vectors_i
::Create(const std::string& theMeshName, 
	 VISU::Entity theEntity,
	 const std::string& theFieldName, 
	 CORBA::Long theTimeStampNumber)
{
  return TSuperClass::Create(theMeshName,theEntity,theFieldName,theTimeStampNumber);
}


//---------------------------------------------------------------
VISU::Storable* 
VISU::Vectors_i
::Restore(SALOMEDS::SObject_ptr theSObject,
	  const Storable::TRestoringMap& theMap)
{
  if(!TSuperClass::Restore(theSObject, theMap))
    return NULL;

  SetGlyphType(VISU::Vectors::GlyphType(VISU::Storable::FindValue(theMap,"myTypeGlyph").toInt()));
  SetGlyphPos(VISU::Vectors::GlyphPos(VISU::Storable::FindValue(theMap,"myPosGlyph").toInt()));
  SetLineWidth(VISU::Storable::FindValue(theMap,"myLineWidth").toDouble());

  return this;
}


//---------------------------------------------------------------
void 
VISU::Vectors_i
::ToStream(std::ostringstream& theStr)
{
  TSuperClass::ToStream(theStr);

  Storable::DataToStream( theStr, "myTypeGlyph", int(GetGlyphType()));
  Storable::DataToStream( theStr, "myPosGlyph",  int(GetGlyphPos()));
  Storable::DataToStream( theStr, "myLineWidth", GetLineWidth());
}


//---------------------------------------------------------------
VISU::Vectors_i
::~Vectors_i()
{
  if(MYDEBUG) MESSAGE("Vectors_i::~Vectors_i()");
}


//---------------------------------------------------------------
void
VISU::Vectors_i
::SetLineWidth(CORBA::Double theWidth) 
{ 
  if(VISU::CheckIsSameValue(myLineWidth, theWidth))
    return;

  VISU::TSetModified aModified(this);

  myLineWidth = theWidth;
  myParamsTime.Modified();
}

//---------------------------------------------------------------
CORBA::Double
VISU::Vectors_i
::GetLineWidth() 
{ 
  return myLineWidth;
}


//---------------------------------------------------------------
void 
VISU::Vectors_i
::SetGlyphType(VISU::Vectors::GlyphType theType) 
{ 
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_VectorsPL, VISU_VectorsPL::GlyphType>
		   (GetSpecificPL(), &VISU_VectorsPL::SetGlyphType, VISU_VectorsPL::GlyphType(theType)));
}

//---------------------------------------------------------------
VISU::Vectors::GlyphType 
VISU::Vectors_i
::GetGlyphType() 
{ 
  return VISU::Vectors::GlyphType(myVectorsPL->GetGlyphType());
}


//---------------------------------------------------------------
void
VISU::Vectors_i
::SetGlyphPos(VISU::Vectors::GlyphPos thePos) 
{ 
  VISU::TSetModified aModified(this);

  ProcessVoidEvent(new TVoidMemFun1ArgEvent<VISU_VectorsPL, VISU_VectorsPL::GlyphPos>
		   (GetSpecificPL(), &VISU_VectorsPL::SetGlyphPos, VISU_VectorsPL::GlyphPos(thePos)));
}

//---------------------------------------------------------------
VISU::Vectors::GlyphPos 
VISU::Vectors_i::GetGlyphPos() 
{ 
  return VISU::Vectors::GlyphPos(myVectorsPL->GetGlyphPos());
}


//---------------------------------------------------------------
void
VISU::Vectors_i
::CreatePipeLine(VISU_PipeLine* thePipeLine)
{
  if(!thePipeLine){
    myVectorsPL = VISU_VectorsPL::New();
  }else
    myVectorsPL = dynamic_cast<VISU_VectorsPL*>(thePipeLine);

  TSuperClass::CreatePipeLine(myVectorsPL);
}


//---------------------------------------------------------------
VISU_PipeLine* 
VISU::Vectors_i
::GetActorPipeLine()
{
  VISU_PipeLine* aPipeLine = VISU_VectorsPL::New();
  aPipeLine->ShallowCopy(GetPipeLine(), true);
  return aPipeLine;
}


//---------------------------------------------------------------
bool
VISU::Vectors_i
::CheckIsPossible() 
{
  return IsPossible(GetCResult(),GetCMeshName(),GetEntity(),GetCFieldName(),GetTimeStampNumber(),true);
}

//---------------------------------------------------------------
VISU_Actor* 
VISU::Vectors_i
::CreateActor()
{
  VISU_VectorsAct* anActor = VISU_VectorsAct::New();
  try{
    VISU::Prs3d_i::CreateActor(anActor);
    anActor->SetVTKMapping(true);
    anActor->SetBarVisibility(true);
    anActor->GetProperty()->SetColor(myColor.R,myColor.G,myColor.B);
    anActor->GetProperty()->SetLineWidth(GetLineWidth());
    SUIT_ResourceMgr* aResourceMgr = VISU::GetResourceMgr();
    int  aDispMode = aResourceMgr->integerValue("VISU", "vectors_represent", 1);
    bool toUseShading = aResourceMgr->booleanValue("VISU", "represent_shading", false);
    anActor->SetRepresentation(aDispMode);
    anActor->SetShading(toUseShading);
    UpdateActor(anActor);
  }catch(...){
    anActor->Delete();
    throw ;
  }
  return anActor;
}


//---------------------------------------------------------------
void 
VISU::Vectors_i
::UpdateActor(VISU_ActorBase* theActor) 
{
  if(VISU_VectorsAct* anActor = dynamic_cast<VISU_VectorsAct*>(theActor)){
    TSuperClass::UpdateActor(anActor);
    anActor->GetProperty()->SetLineWidth(GetLineWidth());
    if(VISU_VectorsPL* aVectorsPL = dynamic_cast<VISU_VectorsPL*>(anActor->GetPipeLine())){
      aVectorsPL->ShallowCopy(GetPipeLine(), true);
      aVectorsPL->Update();
      aVectorsPL->SetMapScale(myVectorsPL->GetMapScale());
    }
  }
}
