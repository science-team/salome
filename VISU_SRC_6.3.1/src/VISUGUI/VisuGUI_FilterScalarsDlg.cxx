// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

#include "VisuGUI_FilterScalarsDlg.h"
#include "VISU_ColoredPrs3d_i.hh"
#include "VISU_ColoredPrs3dHolder_i.hh"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"

#include <SalomeApp_Application.h>
#include <LightApp_SelectionMgr.h>

#include <SUIT_Desktop.h>
#include <SUIT_MessageBox.h>
#include <SUIT_Session.h>
#include <SUIT_ResourceMgr.h>

#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QDoubleValidator>
  

VisuGUI_FilterScalarsDlg::VisuGUI_FilterScalarsDlg( VisuGUI* theModule )
  : QDialog(VISU::GetDesktop(theModule), Qt::WindowTitleHint | Qt::WindowSystemMenuHint ),
    myVisuGUI( theModule )
{
  setModal( false );
  setWindowTitle(tr("TITLE"));
  setSizeGripEnabled(true);
  setAttribute( Qt::WA_DeleteOnClose, true );
  
  QVBoxLayout* aMainLayout = new QVBoxLayout(this);
  
  myRangeBox = new QGroupBox(this);
  myRangeBox->setTitle(tr("BOXTITLE"));
  myRangeBox->setCheckable(true);
  aMainLayout->addWidget(myRangeBox);

  QHBoxLayout* aBoxLayout = new QHBoxLayout(myRangeBox);
  aBoxLayout->addWidget(new QLabel(tr("MINLBL"), myRangeBox));
  myMinEdit = new QLineEdit( myRangeBox );
  myMinEdit->setValidator( new QDoubleValidator(myMinEdit) );
  aBoxLayout->addWidget(myMinEdit);

  aBoxLayout->addWidget(new QLabel(tr("MAXLBL"), myRangeBox));
  myMaxEdit = new QLineEdit( myRangeBox );
  myMaxEdit->setValidator( new QDoubleValidator(myMaxEdit) );
  aBoxLayout->addWidget(myMaxEdit);

  QGroupBox* aGroupButtons = new QGroupBox(this);
  QHBoxLayout* aButtonsLayout = new QHBoxLayout(aGroupButtons);

  QPushButton* aBtnOk = new QPushButton(tr("BUT_OK"), aGroupButtons);
  aBtnOk->setAutoDefault(true);
  aBtnOk->setDefault(true);
  aButtonsLayout->addWidget(aBtnOk);

  QPushButton* aBtnCancel = new QPushButton(tr("BUT_CANCEL"), aGroupButtons);
  aBtnCancel->setAutoDefault(true);
  aButtonsLayout->addWidget(aBtnCancel);

  aButtonsLayout->addStretch();

  QPushButton* aBtnHelp = new QPushButton(tr("BUT_HELP"), aGroupButtons);
  aBtnHelp->setAutoDefault(true);
  aButtonsLayout->addWidget(aBtnHelp);

  aMainLayout->addWidget(aGroupButtons);

  connect(aBtnOk,     SIGNAL(clicked()), this, SLOT(accept()));
  connect(aBtnCancel, SIGNAL(clicked()), this, SLOT(reject()));
  connect(aBtnHelp,   SIGNAL(clicked()), this, SLOT(onHelp()));

  SalomeApp_Application* anApp = theModule->getApp();
  LightApp_SelectionMgr* aSelectionMgr = anApp->selectionMgr();
  connect( aSelectionMgr, SIGNAL( currentSelectionChanged() ), this, SLOT( onSelectionEvent() ) );

  onSelectionEvent();
}


VisuGUI_FilterScalarsDlg::~VisuGUI_FilterScalarsDlg()
{}

void VisuGUI_FilterScalarsDlg::onSelectionEvent()
{
  typedef SALOME::GenericObjPtr< VISU::ColoredPrs3d_i  > TColoredPrs3dPtr;
  VISU::TSelectionInfo aSelectionInfo = VISU::GetSelectedObjects( myVisuGUI );
  myColoredPrs3d = TColoredPrs3dPtr();
  if ( !aSelectionInfo.empty() ) {
    VISU::TSelectionItem aSelectionItem = aSelectionInfo.front();
    if ( VISU::Base_i* aBase = aSelectionItem.myObjectInfo.myBase ) {
      if ( VISU::ColoredPrs3d_i* aColoredPrs3d = dynamic_cast< VISU::ColoredPrs3d_i* >( aBase ) ) {
        myColoredPrs3d = aColoredPrs3d;
      } else if (VISU::ColoredPrs3dHolder_i* aHolder = 
                 dynamic_cast< VISU::ColoredPrs3dHolder_i* >( aBase )) {
        myColoredPrs3d = aHolder->GetPrs3dDevice();
      }
      if (myColoredPrs3d.get() != NULL) {
        myRangeBox->setChecked( myColoredPrs3d->IsScalarFilterUsed() );
        myMinEdit->setText( QString::number( myColoredPrs3d->GetScalarFilterMin() ) );
        myMaxEdit->setText( QString::number( myColoredPrs3d->GetScalarFilterMax() ) );
        setEnabled( true );
        return;
      }
    }
  }
  setEnabled( false );
  return;
}

void VisuGUI_FilterScalarsDlg::accept()
{
  myColoredPrs3d->SetScalarFilterRange( myMinEdit->text().toDouble(), myMaxEdit->text().toDouble() );
  myColoredPrs3d->UseScalarFiltering( myRangeBox->isChecked() );

  bool isToUpdate = true;
  if( myColoredPrs3d->IsForcedHidden() )
    if( SVTK_ViewWindow* aViewWindow = VISU::GetActiveViewWindow<SVTK_ViewWindow>( myVisuGUI ) )
      if( isToUpdate = VISU::PublishInView( myVisuGUI, myColoredPrs3d, aViewWindow, true ) )
        myColoredPrs3d->SetForcedHidden( false );

  if( isToUpdate )
    myColoredPrs3d->UpdateActors();

  QDialog::accept();
}

void VisuGUI_FilterScalarsDlg::reject()
{
  QDialog::reject();
}
  
void VisuGUI_FilterScalarsDlg::onHelp()
{
  QString aHelpFileName = "viewing_3d_presentations_page.html#filter_by_scalars_anchor";
  LightApp_Application* app = (LightApp_Application*)(SUIT_Session::session()->activeApplication());
  if (app)
    app->onHelpContextModule(myVisuGUI ? app->moduleName(myVisuGUI->moduleName()) : QString(""), aHelpFileName);
  else {
    QString platform;
#ifdef WIN32
    platform = "winapplication";
#else
    platform = "application";
#endif
    SUIT_MessageBox::warning(0, tr("WRN_WARNING"),
                             tr("EXTERNAL_BROWSER_CANNOT_SHOW_PAGE").
                             arg(app->resourceMgr()->stringValue("ExternalBrowser", platform)).arg(aHelpFileName),
                             tr("BUT_OK"));
  }
}
