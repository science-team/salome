// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Plot3DDlg.cxx
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISUGUI/VisuGUI_Plot3DDlg.cxx,v 1.13.2.1.6.1.8.1 2011-06-02 06:00:21 vsr Exp $
//
#include "VisuGUI_Plot3DDlg.h"

#include "VisuGUI.h"
#include "VisuGUI_Tools.h"
#include "VisuGUI_ViewTools.h"
#include "VisuGUI_InputPane.h"

#include "VISU_ColoredPrs3dFactory.hh"
#include "VISU_ViewManager_i.hh"
#include "VISU_Plot3DPL.hxx"

#include "SVTK_ViewWindow.h"

#include "SALOME_Actor.h"
#include "SUIT_Desktop.h"
#include "SUIT_Session.h"
#include "SUIT_MessageBox.h"
#include "SUIT_ResourceMgr.h"
#include "LightApp_Application.h"
#include <SalomeApp_IntSpinBox.h>
#include <SalomeApp_DoubleSpinBox.h>

#include <QLayout>
#include <QValidator>
#include <QTabWidget>
#include <QRadioButton>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QButtonGroup>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>

#include <vtkUnstructuredGrid.h>
#include <vtkDataSetMapper.h>
#include <vtkRenderer.h>
#include <vtkPlaneSource.h>
#include <vtkPolyData.h>
#include <vtkMath.h>

using namespace std;

#define SURFACE_PRS_ID 0
#define CONTOUR_PRS_ID 1

//=======================================================================
//function : renderViewFrame
//purpose  :
//=======================================================================
static void renderViewFrame (SVTK_ViewWindow* vw)
{
  if (vw) {
//    vw->getRenderer()->ResetCameraClippingRange();
    vw->Repaint();
  }
}

//=======================================================================
//class    : TPlane
//purpose  : actor of plane preview
//=======================================================================
class TPlane : public SALOME_Actor
{
  vtkFloatingPointType mySize;
  vtkDataSetMapper*    myMapper;
  vtkPlaneSource*      myPlaneSource;

 public:
  // constructor
  TPlane(vtkFloatingPointType planeSize): mySize(planeSize)
  {
    Init();
  }
  // set plane parameters
  void Set(vtkFloatingPointType origin[3], vtkFloatingPointType normal[3])
  {
    vtkFloatingPointType point2[3], point1[3];
    vtkMath::Perpendiculars(normal, point1, point2, 0.);
    for (int i = 0; i < 3; ++i) {
      point1[ i ] = point1[ i ] * mySize + origin[ i ];
      point2[ i ] = point2[ i ] * mySize + origin[ i ];
    }
    myPlaneSource->SetOrigin(origin);
    myPlaneSource->SetPoint1(point1);
    myPlaneSource->SetPoint2(point2);
    myPlaneSource->SetCenter(origin);
  }
  vtkTypeMacro(TPlane,SALOME_Actor);

 protected:
  void Init() {
    myPlaneSource = vtkPlaneSource::New();
    myMapper = vtkDataSetMapper::New();
    myMapper->SetInput(myPlaneSource->GetOutput());
    // actor methods
    VisibilityOff();
    PickableOff();
    SetInfinitive(true);
    SetOpacity(0.85);
    SetMapper(myMapper);
  }
  ~TPlane() {
    myMapper->RemoveAllInputs();
    myMapper->Delete();
    // commented: porting to vtk 5.0
    //myPlaneSource->UnRegisterAllOutputs();
    myPlaneSource->Delete();
  };
  // Not implemented.
  TPlane(const TPlane&);
  void operator=(const TPlane&);
};

//=======================================================================
//function : VisuGUI_Plot3DPane
//purpose  :
//=======================================================================
VisuGUI_Plot3DPane::VisuGUI_Plot3DPane (QWidget* parent)
     : QWidget(parent), myInitFromPrs(false), myPreviewActor(NULL),
       myViewWindow(VISU::GetActiveViewWindow<SVTK_ViewWindow>()), myPrs(NULL), myPipeCopy(NULL)
{
  QVBoxLayout* aMainLay = new QVBoxLayout( this );
  aMainLay->setAlignment(Qt::AlignTop);
  aMainLay->setSpacing(6);

  // Orientation

  GBOrientation = new QButtonGroup ( this );
  GBoxOrient = new QGroupBox(tr("ORIENTATION"),this);
  aMainLay->addWidget(GBoxOrient);
  
  //GBOrientation->setColumnLayout(0, Qt::Vertical);
  //GBOrientation->layout()->setSpacing(0);
  //GBOrientation->layout()->setMargin(0);
  QGridLayout* BGOrientationLayout = new QGridLayout (GBoxOrient);
  BGOrientationLayout->setAlignment(Qt::AlignTop);
  BGOrientationLayout->setSpacing(6);
  BGOrientationLayout->setMargin(11);

  QRadioButton *RBxy, *RByz, *RBzx;
  RBxy = new QRadioButton (tr("// X-Y"), GBoxOrient );
  RByz = new QRadioButton (tr("// Y-Z"), GBoxOrient );
  RBzx = new QRadioButton (tr("// Z-X"), GBoxOrient );
  BGOrientationLayout->addWidget(RBxy, 0, 0);
  BGOrientationLayout->addWidget(RByz, 0, 1);
  BGOrientationLayout->addWidget(RBzx, 0, 2);

  GBOrientation->addButton( RBxy, 0 );
  GBOrientation->addButton( RByz, 1 );
  GBOrientation->addButton( RBzx, 2 );

  // Rotation

  QGroupBox* GBrot = new QGroupBox (tr("ROTATIONS"), this);
  aMainLay->addWidget( GBrot );
  //GBrot->setColumnLayout(0, Qt::Vertical);
  //GBrot->layout()->setSpacing(0);
  //GBrot->layout()->setMargin(0);
  QGridLayout* GBrotLayout = new QGridLayout (GBrot);
  GBrotLayout->setAlignment(Qt::AlignTop);
  GBrotLayout->setSpacing(6);
  GBrotLayout->setMargin(11);
  // label 1
  LabelRot1 = new QLabel (tr("ROTATION_X"), GBrot );
  GBrotLayout->addWidget(LabelRot1, 0, 0);
  // spin 1
  Rot1 = new SalomeApp_DoubleSpinBox (GBrot);
  VISU::initSpinBox( Rot1, -180, 180, 5, "angle_precision" );
  Rot1->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
  GBrotLayout->addWidget(Rot1, 0, 1);
  // label 2
  LabelRot2 = new QLabel (tr("ROTATION_Y"), GBrot);
  GBrotLayout->addWidget(LabelRot2, 1, 0);
  // spin 2
  Rot2 = new SalomeApp_DoubleSpinBox (GBrot);
  VISU::initSpinBox( Rot2, -180, 180, 5, "angle_precision" );  
  Rot2->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
  GBrotLayout->addWidget(Rot2, 1, 1);

  // Position

  QGroupBox* GBpos = new QGroupBox (tr("POSITION"), this);
  aMainLay->addWidget( GBpos );
  //GBpos->setColumnLayout(0, Qt::Horizontal);
  //GBpos->layout()->setSpacing(0);
  //GBpos->layout()->setMargin(0);
  QGridLayout* GBposLayout = new QGridLayout (GBpos);
  GBposLayout->setAlignment(Qt::AlignTop);
  GBposLayout->setSpacing(6);
  GBposLayout->setMargin(11);
  // value label
  QLabel * valueLabel = new QLabel (tr("POSITION_VALUE"), GBpos);
  GBposLayout->addWidget(valueLabel, 0, 0);
  // value spin
  PositionSpn = new SalomeApp_DoubleSpinBox (GBpos);
  VISU::initSpinBox( PositionSpn, 0, 1, 0.1, "parametric_precision" );
  GBposLayout->addWidget(PositionSpn, 0, 1);
  // Relative CheckBox
  RelativeChkB = new QCheckBox (tr("RELATIVE"), GBpos);
  RelativeChkB->setChecked(true);
  GBposLayout->addWidget(RelativeChkB, 0, 2);

  // Scale, Presentation type, Nb Contours, Preview

  QFrame* bottomFrame = new QFrame (this);
  aMainLay->addWidget( bottomFrame );
  QGridLayout* bottomLayout = new QGridLayout (bottomFrame);
  bottomLayout->setAlignment(Qt::AlignTop);
  bottomLayout->setSpacing(11);
  bottomLayout->setMargin(0);
  // scale
  QLabel* scaleLabel = new QLabel (tr("SCALE"), bottomFrame);
  ScaleSpn = new SalomeApp_DoubleSpinBox (bottomFrame);
  VISU::initSpinBox( ScaleSpn, -1.e38, 1.e38, 0.1, "visual_data_precision" );  
  // Presentation type
  GBPrsType = new QButtonGroup ( bottomFrame);
  QGroupBox* aGB = new QGroupBox (tr("PRESENTATION_TYPE"), bottomFrame);
  QHBoxLayout* aHBoxLay = new QHBoxLayout( aGB );
  
  QRadioButton* aRB = new QRadioButton (tr("SURFACE"), aGB);
  aHBoxLay->addWidget( aRB );
  GBPrsType->addButton( aRB, 0 );
  aRB = new QRadioButton (tr("CONTOUR"), aGB);
  aHBoxLay->addWidget( aRB );
  GBPrsType->addButton( aRB, 1 );
  // nb Contours
  QLabel* nbContLabel = new QLabel (tr("NUMBER_CONTOURS"), bottomFrame);
  NbContoursSpn = new SalomeApp_IntSpinBox ( bottomFrame );
  NbContoursSpn->setAcceptNames( false );
  NbContoursSpn->setMinimum( 1 );
  NbContoursSpn->setMaximum( 999 );
  NbContoursSpn->setSingleStep( 1 );
  
  // Preview
  PreviewChkB = new QCheckBox (tr("PREVIEW"), bottomFrame);
  PreviewChkB->setChecked(false);

  bottomLayout->addWidget(scaleLabel,    0, 0);
  bottomLayout->addWidget(ScaleSpn,      0, 1);
  bottomLayout->addWidget(aGB, 1, 0, 1, 2);
  bottomLayout->addWidget(nbContLabel,   2, 0);
  bottomLayout->addWidget(NbContoursSpn, 2, 1);
  bottomLayout->addWidget(PreviewChkB,   3, 0);

  // signals and slots connections
  connect(GBOrientation, SIGNAL(buttonClicked(int)),   this, SLOT(orientationChanged(int)));
  connect(Rot1,          SIGNAL(valueChanged(double)), this, SLOT(updatePreview()));
  connect(Rot2,          SIGNAL(valueChanged(double)), this, SLOT(updatePreview()));
  connect(PositionSpn,   SIGNAL(valueChanged(double)), this, SLOT(onPositionSpn()));
  connect(RelativeChkB,  SIGNAL(toggled(bool)),        this, SLOT(onRelativePos(bool)));
  connect(GBPrsType,     SIGNAL(buttonClicked(int)),   this, SLOT(onPrsType(int)));
  connect(PreviewChkB,   SIGNAL(toggled(bool)),        this, SLOT(updatePreview()));
}

//=======================================================================
//function : destructor
//purpose  :
//=======================================================================
VisuGUI_Plot3DPane::~VisuGUI_Plot3DPane()
{}

//=======================================================================
//function : storePrsParams
//purpose  : create a copy of Prs parameters and then store current
//           control values into the Prs
//=======================================================================
void VisuGUI_Plot3DPane::storePrsParams()
{
  if (!myPipeCopy)
    myPipeCopy = VISU_Plot3DPL::New();
  if (myPrs) {
    myPipeCopy->ShallowCopy(myPrs->GetPipeLine(), true);
    storeToPrsObject(myPrs);
  }
}

//=======================================================================
//function : restorePrsParams
//purpose  : restore Prs parameters from the copy
//=======================================================================
void VisuGUI_Plot3DPane::restorePrsParams()
{
  if (!myPipeCopy)
    myPipeCopy = VISU_Plot3DPL::New();
  if (myPrs)
    myPrs->GetPipeLine()->ShallowCopy(myPipeCopy, false);
}

//=======================================================================
//function : onPositionSpn
//purpose  : update absolute position range
//=======================================================================
void VisuGUI_Plot3DPane::onPositionSpn()
{
  if (myPrs && !RelativeChkB->isChecked()) {
    vtkFloatingPointType minPos, maxPos;
    storePrsParams();
    myPrs->GetSpecificPL()->GetMinMaxPosition(minPos, maxPos);
    restorePrsParams();
    if (minPos > PositionSpn->value())
      minPos = PositionSpn->value();
    if (maxPos < PositionSpn->value())
      maxPos = PositionSpn->value();
    PositionSpn->setRange(minPos, maxPos);
  }
  updatePreview();
}

//=======================================================================
//function : orientationChanged
//purpose  : update rotation labels and preview
//=======================================================================
void VisuGUI_Plot3DPane::orientationChanged(int Id)
{
  if (Id == 0) { // RBxy->isChecked()
    LabelRot1->setText(tr("ROTATION_X"));
    LabelRot2->setText(tr("ROTATION_Y"));
  } else if (Id == 1) { // RByz->isChecked()
    LabelRot1->setText(tr("ROTATION_Y"));
    LabelRot2->setText(tr("ROTATION_Z"));
  } else {
    LabelRot1->setText(tr("ROTATION_Z"));
    LabelRot2->setText(tr("ROTATION_X"));
  }
  updatePreview();
}

//=======================================================================
//function : onRelativePos
//purpose  : update position value and range
//=======================================================================
void VisuGUI_Plot3DPane::onRelativePos(bool isRelativePos)
{
  vtkFloatingPointType minPos = 0., maxPos = 1., pos = PositionSpn->value();
  if (myPrs) {
    storePrsParams();
    myPrs->GetSpecificPL()->GetMinMaxPosition(minPos, maxPos);
    restorePrsParams();
    if (-1e-7 < (maxPos - minPos) && (maxPos - minPos) < 1e-7) {
      pos = 0;
    } else {
      if (isRelativePos) // absolute -> relative
        pos = (pos - minPos) / (maxPos - minPos);
      else  // relative -> absolute
        pos = minPos * (1. - pos) + maxPos * pos;
    }
  }
  if (isRelativePos) {
    minPos = 0.;
    maxPos = 1.;
  }
  PositionSpn->setMinimum(minPos);
  PositionSpn->setMaximum(maxPos);
  PositionSpn->setSingleStep((maxPos - minPos) / 10.);
  PositionSpn->setValue(pos);
}

//=======================================================================
//function : onPrsType
//purpose  :
//=======================================================================
void VisuGUI_Plot3DPane::onPrsType(int id)
{
  NbContoursSpn->setEnabled(id == CONTOUR_PRS_ID);
}

//=======================================================================
//function : updatePreview
//purpose  :
//=======================================================================
void VisuGUI_Plot3DPane::updatePreview()
{
  if(myPreviewActor){
    vtkRenderer* aRend       = myPreviewActor->GetRenderer();
    vtkRenderWindow* aWnd = aRend->GetRenderWindow();
    if (!aWnd) return;
  }
 if (myInitFromPrs || !myPrs || !myViewWindow)
    return;
  bool fitall = false;
  if (PreviewChkB->isChecked()) // place preview plane
  {
    // get plane preview actor
    TPlane* planePreview = (TPlane*) myPreviewActor;
    if (!planePreview) {
      myPreviewActor = planePreview = new TPlane(myPrs->GetInput()->GetLength());
      myViewWindow->AddActor(planePreview);
      fitall = !VISU::FindActor(myViewWindow, myPrs);
    }
    // set plane parameters corresponding to control values
    storePrsParams();
    vtkFloatingPointType normal[3], origin[3];
    myPrs->GetSpecificPL()->GetBasePlane(origin, normal, true);
    planePreview->Set(origin, normal);
    restorePrsParams();
  }
  if (myPreviewActor)
    myPreviewActor->SetVisibility(PreviewChkB->isChecked());

  renderViewFrame(myViewWindow);

  if (fitall && VISU::GetResourceMgr()->booleanValue("VISU","automatic_fit_all",false)) {
    myPreviewActor->SetInfinitive(false);
    myViewWindow->onFitAll();
    myPreviewActor->SetInfinitive(true);
  }
}

//=======================================================================
//function : initFromPrsObject
//purpose  :
//=======================================================================
void VisuGUI_Plot3DPane::initFromPrsObject(VISU::Plot3D_i* thePrs)
{
  myInitFromPrs = true;
  myPrs = thePrs;

  // orientation
  int id;
  switch (thePrs->GetOrientationType()) {
  case VISU::Plot3D::XY: id = 0; break;
  case VISU::Plot3D::YZ: id = 1; break;
  default: id = 2;
  }
  GBOrientation->button(id)->click();

  // rotation
  Rot1->setValue(thePrs->GetRotateX() * 180./PI);
  Rot2->setValue(thePrs->GetRotateY() * 180./PI);

  // position
  RelativeChkB->setChecked(thePrs->IsPositionRelative());
  onRelativePos(thePrs->IsPositionRelative()); // update range
  PositionSpn->setValue(thePrs->GetPlanePosition());

  // scale
  ScaleSpn->setValue(thePrs->GetScaleFactor());

  // prs type
  id = thePrs->GetIsContourPrs() ? CONTOUR_PRS_ID : SURFACE_PRS_ID;
  GBPrsType->button(id)->click();

  // nb contours
  NbContoursSpn->setValue(thePrs->GetNbOfContours());

  // disable cutting plane controls if the mesh is planar

  if (thePrs->GetPipeLine()->IsPlanarInput())
  {
    //GBOrientation->setEnabled(false);
    GBoxOrient->setEnabled(false);
    Rot1         ->setEnabled(false);
    Rot2         ->setEnabled(false);
    PositionSpn  ->setEnabled(false);
    RelativeChkB ->setEnabled(false);
    PreviewChkB  ->setEnabled(false);
  }

  myInitFromPrs = false;
  updatePreview();
}

//=======================================================================
//function : storeToPrsObject
//purpose  :
//=======================================================================
int VisuGUI_Plot3DPane::storeToPrsObject(VISU::Plot3D_i* thePrs)
{
  if (myInitFromPrs)
    return 0;
  // orientation
  int id = GBOrientation->id (GBOrientation->checkedButton());
  VISU::Plot3D::Orientation ori;
  switch (id) {
  case 0 : ori = VISU::Plot3D::XY; break;
  case 1 : ori = VISU::Plot3D::YZ; break;
  default: ori = VISU::Plot3D::ZX;
  }
  // rotation
  thePrs->SetOrientation(ori, Rot1->value()*PI/180., Rot2->value()*PI/180.);

  // position
  thePrs->SetPlanePosition(PositionSpn->value(), RelativeChkB->isChecked());

  // scale
  thePrs->SetScaleFactor(ScaleSpn->value());

  // prs type
  id = GBPrsType->id (GBPrsType->checkedButton());
  thePrs->SetContourPrs(id == CONTOUR_PRS_ID);

  // nb contours
  thePrs->SetNbOfContours(NbContoursSpn->value());

  return 1;
}

//=======================================================================
//function : check
//purpose  :
//=======================================================================
bool VisuGUI_Plot3DPane::check()
{
  if(!myPreviewActor) return true;
  
  vtkRenderer* aRend    = myPreviewActor->GetRenderer();
  vtkRenderWindow* aWnd = aRend->GetRenderWindow();
  if(aRend && aWnd){
    myPreviewActor->SetVisibility(false);
    myViewWindow->RemoveActor(myPreviewActor);
    myPreviewActor->Delete();
    myPreviewActor = 0;
  }
    
  return true;
}

void VisuGUI_Plot3DPane::setPlane(int theOrientation, double theXRotation, double theYRotation, double thePlanePos)
{
  // Set plane
  int id;
  switch (theOrientation) {
  case VISU::Plot3D::XY: id = 0; break;
  case VISU::Plot3D::YZ: id = 1; break;
  default: id = 2;
  }
  GBOrientation->button(id)->click();
  GBoxOrient->setEnabled(false);


  // Set rotation
  Rot1->setValue(theXRotation * 180./PI);
  Rot1->setEnabled(false);
  Rot2->setValue(theYRotation * 180./PI);
  Rot2->setEnabled(false);

  // Set position
  RelativeChkB->setChecked(false);
  onRelativePos(false); // update range
  PositionSpn->setValue(thePlanePos);
  RelativeChkB->setEnabled(false);
  PositionSpn->setEnabled(false);
}


//=======================================================================
//function : Constructor
//purpose  :
//=======================================================================
VisuGUI_Plot3DDlg::VisuGUI_Plot3DDlg (SalomeApp_Module* theModule)
  : VisuGUI_ScalarBarBaseDlg(theModule)
{
  setWindowTitle(tr("TITLE"));
  setSizeGripEnabled(TRUE);

  QVBoxLayout* TopLayout = new QVBoxLayout(this);
  TopLayout->setSpacing(6);
  TopLayout->setMargin(11);

  myTabBox = new QTabWidget (this);
  myIsoPane = new VisuGUI_Plot3DPane (this);
  if ( myIsoPane->layout()  )
    myIsoPane->layout()->setMargin(5);
  myTabBox->addTab(myIsoPane, tr("PLOT3D_TAB_TITLE"));
  myInputPane = new VisuGUI_InputPane(VISU::TPLOT3D, theModule, this);
  myTabBox->addTab(GetScalarPane(), tr("SCALAR_BAR_TAB_TITLE"));
  myTabBox->addTab(myInputPane, tr("INPUT_TAB_TITLE"));

  TopLayout->addWidget(myTabBox);

  QGroupBox* GroupButtons = new QGroupBox (this );
  GroupButtons->setGeometry(QRect(10, 10, 281, 48));
  //GroupButtons->setColumnLayout(0, Qt::Vertical);
  //GroupButtons->layout()->setSpacing(0);
  //GroupButtons->layout()->setMargin(0);
  QGridLayout* GroupButtonsLayout = new QGridLayout (GroupButtons);
  GroupButtonsLayout->setAlignment(Qt::AlignTop);
  GroupButtonsLayout->setSpacing(6);
  GroupButtonsLayout->setMargin(11);

  QPushButton* buttonOk = new QPushButton (tr("BUT_OK"), GroupButtons);
  buttonOk->setAutoDefault(TRUE);
  buttonOk->setDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonOk, 0, 0);
  GroupButtonsLayout->addItem(new QSpacerItem (5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum), 0, 1);
  QPushButton* buttonCancel = new QPushButton (tr("BUT_CANCEL") , GroupButtons);
  buttonCancel->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonCancel, 0, 2);
  QPushButton* buttonHelp = new QPushButton (tr("BUT_HELP") , GroupButtons);
  buttonHelp->setAutoDefault(TRUE);
  GroupButtonsLayout->addWidget(buttonHelp, 0, 3);

  TopLayout->addWidget(GroupButtons);

  // signals and slots connections
  connect(buttonOk,     SIGNAL(clicked()), this, SLOT(accept()));
  connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
  connect(buttonHelp,   SIGNAL(clicked()), this, SLOT(onHelp()));
}

VisuGUI_Plot3DDlg::~VisuGUI_Plot3DDlg()
{}

//=======================================================================
//function : accept
//purpose  :
//=======================================================================
void VisuGUI_Plot3DDlg::accept()
{
  if (myIsoPane->check() && GetScalarPane()->check())
    VisuGUI_ScalarBarBaseDlg::accept();
}

//=======================================================================
//function : reject
//purpose  :
//=======================================================================
void VisuGUI_Plot3DDlg::reject()
{
  VisuGUI_ScalarBarBaseDlg::reject();
}

//=======================================================================
//function : initFromPrsObject
//purpose  :
//=======================================================================
void VisuGUI_Plot3DDlg::initFromPrsObject( VISU::ColoredPrs3d_i* thePrs, 
                                           bool theInit )
{
  if( theInit )
    myPrsCopy = VISU::TSameAsFactory<VISU::TPLOT3D>().Create(thePrs, VISU::ColoredPrs3d_i::EDoNotPublish);

  VisuGUI_ScalarBarBaseDlg::initFromPrsObject(myPrsCopy, theInit);

  myIsoPane->initFromPrsObject(myPrsCopy);

  if( !theInit )
    return;

  myInputPane->initFromPrsObject( myPrsCopy );
  myTabBox->setCurrentIndex( 0 );
}

//=======================================================================
//function : storeToPrsObject
//purpose  :
//=======================================================================
int VisuGUI_Plot3DDlg::storeToPrsObject (VISU::ColoredPrs3d_i* thePrs)
{
  if(!myInputPane->check() || !GetScalarPane()->check())
    return 0;
  
  int anIsOk = myInputPane->storeToPrsObject( myPrsCopy );
  anIsOk &= GetScalarPane()->storeToPrsObject( myPrsCopy );
  anIsOk &= myIsoPane->storeToPrsObject( myPrsCopy );

  VISU::TSameAsFactory<VISU::TPLOT3D>().Copy(myPrsCopy, thePrs);

  return anIsOk;
}

//=======================================================================
//function : onHelp
//purpose  :
//=======================================================================
QString VisuGUI_Plot3DDlg::GetContextHelpFilePath()
{
  return "plot_3d_page.html";
}

//=======================================================================
//function : setPlane
//purpose  :
//=======================================================================
void VisuGUI_Plot3DDlg::setPlane(int theOrientation, double theXRotation, double theYRotation, double thePlanePos)
{
  myIsoPane->setPlane(theOrientation, theXRotation, theYRotation, thePlanePos);
}
