// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_VectorsDlg.h
//  Author : Laurent CORNABE & Hubert ROLLAND
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/VISUGUI/VisuGUI_VectorsDlg.h,v 1.12.2.1.6.1.8.1 2011-06-02 06:00:23 vsr Exp $
//
#ifndef VISUGUI_VECTORSDLG_H
#define VISUGUI_VECTORSDLG_H

#include "VisuGUI_Prs3dDlg.h"

class QGroupBox;
class QTabWidget;
class QRadioButton;
class QButtonGroup;
class QCheckBox;
class QPushButton;
class QtxColorButton;

#include "SALOMEconfig.h"
#include CORBA_CLIENT_HEADER(VISU_Gen)

class SalomeApp_Module;
class SalomeApp_IntSpinBox;
class VisuGUI_InputPane;
class SalomeApp_DoubleSpinBox;

namespace VISU
{
  class Vectors_i;
}

class VisuGUI_VectorsDlg : public VisuGUI_ScalarBarBaseDlg
{
    Q_OBJECT

public:
    VisuGUI_VectorsDlg (SalomeApp_Module* theModule);
    ~VisuGUI_VectorsDlg();

    void   setScaleFactor( double sf );
    double getScaleFactor();
    void   setLineWidth( int lw );
    int    getLineWidth();
    void   setUseMagnColor( bool on );
    bool   getUseMagnColor();
    void   setUseGlyphs( bool on );
    bool   getUseGlyphs();
    void   setColor( QColor color);
    QColor getColor();

    void   setGlyphType(VISU::Vectors::GlyphType type );
    VISU::Vectors::GlyphType  getGlyphType();

    void   setGlyphPos(VISU::Vectors::GlyphPos pos);
    VISU::Vectors::GlyphPos getGlyphPos();
    void   enableMagnColor( bool enable );

    virtual void initFromPrsObject( VISU::ColoredPrs3d_i* thePrs,
                                    bool theInit );

    virtual int  storeToPrsObject(VISU::ColoredPrs3d_i* thePrs);

protected slots:
  virtual QString GetContextHelpFilePath();

private:
    QGroupBox*      TopGroup;
    QCheckBox*      UseMagn;
    QtxColorButton* SelColor;
    QLabel*         LineWidLabel;
    SalomeApp_IntSpinBox* LinWid;
    QLabel*         ScaleLabel;
    SalomeApp_DoubleSpinBox* ScalFact;
    QCheckBox*      UseGlyph;
    QButtonGroup*   TypeGlyph;
    QGroupBox*      TypeGB;
    QRadioButton*   RBCones6;
    QRadioButton*   RBCones2;
    QRadioButton*   RBArrows;
    QButtonGroup*   PosGlyph;
    QGroupBox*      PosGB;
    QRadioButton*   RBTail;
    QRadioButton*   RBCent;
    QRadioButton*   RBHead;
    QGroupBox*      GroupButtons;
    QPushButton*    buttonOk;
    QPushButton*    buttonCancel;
    QPushButton*    buttonHelp;

    QColor          myColor;
    QTabWidget*            myTabBox;

    VisuGUI_ScalarBarPane* myScalarPane;
    VisuGUI_InputPane*     myInputPane;

    SALOME::GenericObjPtr<VISU::Vectors_i> myPrsCopy;

private slots:
    void enableGlyphType();
//void setVColor();
    void enableSetColor();
};

#endif // VISUGUI_VECTORSDLG_H
