// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_Tools.cxx
//  Author : Sergey Anikin
//  Module : VISU
//
#include "VisuGUI_ViewTools.h"

#include "VISU_Actor.h"

#include "SVTK_ViewModel.h"
#include "SVTK_ViewWindow.h"

#include <VISU_Tools.h>

#include <SUIT_Desktop.h>

#include <SalomeApp_Study.h>

namespace VISU 
{
  //----------------------------------------------------------------------------
  VISU_Actor*
  PublishInView(VisuGUI* theModule,
                Prs3d_i* thePrs,
                SVTK_ViewWindow* theViewWindow,
                bool theIsHighlight)
  {
    if (theViewWindow) {
      QApplication::setOverrideCursor( Qt::WaitCursor );
      try {
        if (VISU_Actor* anActor = thePrs->CreateActor()) {
          theViewWindow->AddActor(anActor);
          if (theIsHighlight)
            theViewWindow->highlight(anActor->getIO(),true);
          theViewWindow->getRenderer()->ResetCameraClippingRange();
          theViewWindow->Repaint();
          QApplication::restoreOverrideCursor();
	  
	  if(!thePrs->GetEntry().empty())
	    VISU::SetVisibilityState(thePrs->GetEntry(), Qtx::ShownState);
	  else {
	    VISU::ColoredPrs3d_i* prs = dynamic_cast<VISU::ColoredPrs3d_i*>(thePrs);
	    if(prs && !prs->GetHolderEntry().empty()) {
	      VISU::SetVisibilityState(prs->GetHolderEntry(), Qtx::ShownState);
	    }
	  }
          return anActor;
        }
      } catch(std::exception& exc) {
        thePrs->RemoveActors();

        QApplication::restoreOverrideCursor();
        INFOS(exc.what());
        SUIT_MessageBox::warning
          (GetDesktop(theModule), QObject::tr("WRN_VISU"),
           QObject::tr("ERR_CANT_CREATE_ACTOR") + ": " + QObject::tr(exc.what()));
      }
    }
    return NULL;
  }


  //---------------------------------------------------------------
  VISU_Actor*
  UpdateViewer(VisuGUI* theModule,
               VISU::Prs3d_i* thePrs,
               bool theDispOnly,
               const bool theIsHighlight)
  {
    if (SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule)) {
      vtkRenderer *aRen = aViewWindow->getRenderer();
      VTK::ActorCollectionCopy aCopy(aRen->GetActors());
      vtkActorCollection *anActColl = aCopy.GetActors();
      anActColl->InitTraversal();
      VISU_Actor* aResActor = NULL;
      bool isOk = true;
      while (vtkActor *anAct = anActColl->GetNextActor()) {
        if (VISU_Actor* anActor = dynamic_cast<VISU_Actor*>(anAct)) {
          if (VISU::Prs3d_i* aPrs3d = anActor->GetPrs3d()) {
            if (thePrs == aPrs3d) {
              try {
                aResActor = anActor;
                thePrs->UpdateActors();
                aResActor->VisibilityOn();
              } catch (std::runtime_error& exc) {
                thePrs->RemoveActors();
                isOk = false;

                INFOS(exc.what());
                SUIT_MessageBox::warning
                  (GetDesktop(theModule), QObject::tr("WRN_VISU"),
                   QObject::tr("ERR_CANT_BUILD_PRESENTATION") + ": " + QObject::tr(exc.what()) );
              }
            } else if (theDispOnly) {
              anActor->VisibilityOff();
            }
          } else if (theDispOnly && anActor->GetVisibility()) {
            anActor->VisibilityOff();
          }
        }
      }
      if (aResActor) {
        if (theIsHighlight && isOk)
          aViewWindow->highlight(aResActor->getIO(), true);
        aViewWindow->getRenderer()->ResetCameraClippingRange();
        aViewWindow->Repaint();
        return aResActor;
      }
      return PublishInView(theModule, thePrs, aViewWindow, theIsHighlight);
    }
    return NULL;
  }


  //---------------------------------------------------------------
  void
  ErasePrs3d(VisuGUI* theModule,
             VISU::Prs3d_i* thePrs,
             bool theDoRepaint)
  {
    if(SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule)){
      if(VISU_Actor* anActor = FindActor(aViewWindow, thePrs)){
        anActor->VisibilityOff();
        if(theDoRepaint)
          aViewWindow->Repaint();
      }
    }
  }

  //----------------------------------------------------------------------------
  void
  ErasePrs(VisuGUI* theModule,
           Base_i* theBase, 
           bool theDoRepaint)
  {    
    if(!theBase)
      return;

    bool updateState = true;

    switch (theBase->GetType()) {
    case VISU::TCURVE: {
      if (VISU::Curve_i* aCurve = dynamic_cast<VISU::Curve_i*>(theBase)) {
        PlotCurve(theModule, aCurve, VISU::eErase );
	updateState = false;
      }
      break;
    }
    case VISU::TCONTAINER: {
      if (VISU::Container_i* aContainer = dynamic_cast<VISU::Container_i*>(theBase)) {
        PlotContainer(theModule, aContainer, VISU::eErase );
	updateState = false;
      }
      break;
    }
    case VISU::TTABLE: {
      if (VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>(theBase)) {
        PlotTable(theModule, aTable, VISU::eErase );
	updateState = false;
      }
      break;
    }
    case VISU::TPOINTMAP3D: {
      if (VISU::PointMap3d_i* aTable3d = dynamic_cast<VISU::PointMap3d_i*>(theBase)) {
        if (SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule)) {
          if(VISU_ActorBase* anActor = FindActorBase(aViewWindow, aTable3d)){
            anActor->VisibilityOff();
            if(theDoRepaint)
            aViewWindow->Repaint();
          }
        } else {
          if (VISU::Table_i* aTable = dynamic_cast<VISU::Table_i*>(theBase)) {
            PlotTable(theModule, aTable, VISU::eErase );
	    updateState = false;
	  }
        }
      }
      break;
    }
    default: {
      if(VISU::Prs3d_i* aPrs3d = VISU::GetPrs3dFromBase(theBase)){
        if(SVTK_ViewWindow* aViewWindow = GetActiveViewWindow<SVTK_ViewWindow>(theModule)){
          RemoveScalarBarPosition(theModule, aViewWindow, aPrs3d);
          ErasePrs3d(theModule, aPrs3d, theDoRepaint);
        }
      }
    }} // switch (aType)
    
    /* Update visibility state */
    if(updateState) {
      if( RemovableObject_i* obj = dynamic_cast<RemovableObject_i*>(theBase)) {
	SalomeApp_Application* anApp = dynamic_cast<SalomeApp_Application*>(theModule->application());
	if(anApp) {
	  SalomeApp_Study* aStudy = dynamic_cast<SalomeApp_Study*>(anApp->activeStudy());
	  std::string entry = obj->GetEntry();
	  if(aStudy && !entry.empty()) 
	    aStudy->setVisibilityState(QString(entry.c_str()), Qtx::HiddenState);
	}
      } 
    }
  }
}
