// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU VISUGUI : GUI of VISU component
//  File   : VisuGUI_InputPanel.cxx
//  Author : Oleg Uvarov
//  Module : VISU
//
#ifndef VISUGUI_INPUTPANEL_H
#define VISUGUI_INPUTPANEL_H

#include <QtxDockWidget.h>

#include <QMap>

class VisuGUI_BasePanel;

/*!
 * class VisuGUI_InputPanel
 * Dockable window. Container for GUI controls
 */
class VisuGUI_InputPanel : public QtxDockWidget
{
  Q_OBJECT
  
public:
  VisuGUI_InputPanel( QWidget* theParent = 0 );
  virtual ~VisuGUI_InputPanel();

  void showPanel( VisuGUI_BasePanel* thePanel );
  void hidePanel( VisuGUI_BasePanel* thePanel );
  void clear();

  bool isEmpty() const;
  bool isShown( VisuGUI_BasePanel* thePanel ) const;

protected slots:
  void onClosePanel();

private:
  QWidget* myGrp;
  QMap<VisuGUI_BasePanel*, bool> myPanels;
  VisuGUI_BasePanel* myCurrentPanel;
};

#endif


