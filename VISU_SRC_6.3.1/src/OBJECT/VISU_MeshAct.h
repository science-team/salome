// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_MeshAct.h
//  Author : Laurent CORNABE with the help of Nicolas REJNERI
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/OBJECT/VISU_MeshAct.h,v 1.9.2.2.6.1.8.2 2011-06-02 06:00:17 vsr Exp $
//
#ifndef VISU_MeshAct_HeaderFile
#define VISU_MeshAct_HeaderFile

#include "VISU_OBJECT.h"
#include "VISU_DataSetActor.h"
#include "SVTK_DeviceActor.h"


class VISU_OBJECT_EXPORT VISU_MeshAct : public VISU_DataSetActor 
{
 public:
  vtkTypeMacro(VISU_MeshAct,VISU_DataSetActor);
  static 
  VISU_MeshAct* 
  New();

  //! Copies all properties from the given actor
  virtual
  void
  DeepCopy(VISU_Actor *theActor);

  //! Apply view transformation
  virtual
  void
  SetTransform(VTKViewer_Transform* theTransform); 

  virtual
  vtkProperty* 
  GetSurfaceProperty(); 

  virtual
  vtkProperty* 
  GetEdgeProperty(); 

  virtual
  vtkProperty* 
  GetNodeProperty(); 

  virtual
  void
  SetOpacity(vtkFloatingPointType theValue);

  virtual
  vtkFloatingPointType
  GetOpacity();

  virtual
  void
  SetLineWidth(vtkFloatingPointType theLineWidth);

  virtual
  vtkFloatingPointType
  GetLineWidth();

  virtual
  void
  SetRepresentation(int theMode);

  virtual
  void
  SetShrinkable(bool theIsShrinkable);

  virtual
  void
  SetShrinkFactor(vtkFloatingPointType theFactor = 0.8); 

  virtual
  void
  SetShrink(); 

  virtual
  void
  UnShrink(); 

  virtual
  void
  SetFeatureEdgesAllowed(bool theIsFeatureEdgesAllowed);

  virtual
  void
  SetFeatureEdgesEnabled(bool theIsFeatureEdgesEnabled);

  virtual
  void
  SetFeatureEdgesAngle(vtkFloatingPointType theAngle = 30.0); 

  virtual
  void
  SetFeatureEdgesFlags(bool theIsFeatureEdges,
                       bool theIsBoundaryEdges,
                       bool theIsManifoldEdges,
                       bool theIsNonManifoldEdges);

  virtual
  void
  SetFeatureEdgesColoring(bool theIsColoring);

  //! Gets memory size used by the instance (bytes).
  virtual
  unsigned long int
  GetMemorySize();
 
  virtual
  int
  RenderOpaqueGeometry(vtkViewport *ren);

  virtual
  int
#if (VTK_XVERSION < 0x050100)
  RenderTranslucentGeometry(vtkViewport *ren);
#else
  RenderTranslucentPolygonalGeometry(vtkViewport *ren);

  virtual
  int
  HasTranslucentPolygonalGeometry();
#endif

  virtual
  EQuadratic2DRepresentation GetQuadratic2DRepresentation() const;
  
  virtual void 
  SetQuadratic2DRepresentation( EQuadratic2DRepresentation theMode );

  virtual
  void
  SetMarkerStd( VTK::MarkerType, VTK::MarkerScale );

  virtual
  void
  SetMarkerTexture( int, VTK::MarkerTexture );

 protected:
  VISU_MeshAct();
  ~VISU_MeshAct();

  virtual 
  void
  SetMapperInput(vtkDataSet* theDataSet);

  SVTK_DeviceActor *mySurfaceActor;
  SVTK_DeviceActor *myEdgeActor;
  SVTK_DeviceActor *myNodeActor;
};

#endif
