# Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  VISU OBJECT : interactive object for VISU entities implementation
#  File   : Makefile.in
#  Module : VISU
#
include $(top_srcdir)/adm_local/unix/make_common_starter.am

lib_LTLIBRARIES= libVisuObject.la

salomeinclude_HEADERS = \
	VISU_BoostSignals.h \
	VISU_ActorFactory.h \
	VISU_PickingSettings.h \
	VISU_GaussPtsSettings.h \
	VISU_GaussPtsActorFactory.h \
	VISU_GaussPtsDeviceActor.h \
	VISU_Event.h \
	VISU_Actor.h \
	VISU_DataSetActor.h \
	VISU_MeshAct.h \
	VISU_ScalarMapAct.h \
	VISU_GaussPtsAct.h \
	VISU_VectorsAct.h \
	VISU_PointMap3dActor.h \
	VISU_ActorBase.h \
	VISU_IsoSurfActor.h \
	VISU_SelectVisiblePoints.h \
	VISU_OBJECT.h

dist_libVisuObject_la_SOURCES = \
	VISU_Actor.cxx \
	VISU_DataSetActor.cxx \
	VISU_MeshAct.cxx \
	VISU_ScalarMapAct.cxx \
	VISU_PickingSettings.cxx \
	VISU_GaussPtsDeviceActor.cxx \
	VISU_GaussPtsSettings.cxx \
	VISU_GaussPtsAct.cxx \
	VISU_VectorsAct.cxx \
	VISU_PointMap3dActor.cxx \
	VISU_ActorBase.cxx \
	VISU_IsoSurfActor.cxx \
	VISU_SelectVisiblePoints.cxx

libVisuObject_la_CPPFLAGS= \
	$(QT_INCLUDES) \
	@CAS_CPPFLAGS@ @CAS_CXXFLAGS@ \
	$(VTK_INCLUDES) \
	$(BOOST_CPPFLAGS) \
	$(MED_CXXFLAGS) \
	$(GUI_CXXFLAGS) \
	$(KERNEL_CXXFLAGS) \
	-I$(srcdir)/../PIPELINE \
	-I$(srcdir)/../CONVERTOR

libVisuObject_la_LDFLAGS= \
	$(GUI_LDFLAGS) \
	$(KERNEL_LDFLAGS) \
	$(BOOST_LIB_SIGNALS) \
	$(VTK_LIBS)

libVisuObject_la_LIBADD=  \
	-lSalomeObject -lqtx -lsuit \
	../PIPELINE/libVisuPipeLine.la \
	-lSVTK
