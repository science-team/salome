// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : 
//  Author : 
//  Module : VISU
//
#include "VISU_DataSetActor.h"
#include "VISU_UnstructuredGridPL.hxx"
#include "VISU_PipeLineUtils.hxx"

#include <VTKViewer_DataSetMapper.h>

#include <vtkObjectFactory.h>
#include <vtkImplicitBoolean.h>
#include <SALOME_ExtractGeometry.h>
#include <SALOME_ExtractPolyDataGeometry.h>
#include <vtkImplicitFunctionCollection.h>
#include <vtkPlane.h>

#include <boost/bind.hpp>

using namespace std;

#ifdef _DEBUG_
static int MYDEBUG = 0;
#else
static int MYDEBUG = 0;
#endif

//----------------------------------------------------------------------------
vtkStandardNewMacro(VISU_DataSetActor);

//----------------------------------------------------------------------------
VISU_DataSetActor
::VISU_DataSetActor():
  myMapper(VTKViewer_DataSetMapper::New()),
  myExtractor(SALOME_ExtractGeometry::New()),
  myPolyDataExtractor(SALOME_ExtractPolyDataGeometry::New()),
  myFunction(vtkImplicitBoolean::New())
{
  if(MYDEBUG) MESSAGE("VISU_DataSetActor::VISU_DataSetActor - this = "<<this);

  myExtractor->SetImplicitFunction(myFunction);
  myPolyDataExtractor->SetImplicitFunction(myFunction);
  //myExtractor->ExtractBoundaryCellsOn();
  //myPolyDataExtractor->ExtractBoundaryCellsOn();

  myFunction->SetOperationTypeToIntersection();

  myMapper->Delete();
  myExtractor->Delete();
  myPolyDataExtractor->Delete();
  myFunction->Delete();
}

//----------------------------------------------------------------------------
VISU_DataSetActor
::~VISU_DataSetActor()
{
  if(MYDEBUG) MESSAGE("~VISU_DataSetActor() - this = "<<this);
}

//----------------------------------------------------------------------------
void
VISU_DataSetActor
::ShallowCopyPL(VISU_PipeLine* thePipeLine)
{
  Superclass::ShallowCopyPL(thePipeLine);

  if(VISU_UnstructuredGridPL* aPipeLine = dynamic_cast<VISU_UnstructuredGridPL*>(thePipeLine)){
    vtkDataSetMapper* aTarget = GetDataSetMapper();
    vtkDataSetMapper* aSource = aPipeLine->GetDataSetMapper();    
    VISU::CopyDataSetMapper(aTarget, aSource, true);
  }
}

//----------------------------------------------------------------------------
void
VISU_DataSetActor
::SetMapperInput(vtkDataSet* theDataSet) 
{
  if (theDataSet->IsA("vtkPolyData")) {
    myPolyDataExtractor->SetInput(theDataSet);
    myMapper->SetInput(myPolyDataExtractor->GetOutput());
  } else {
    myExtractor->SetInput(theDataSet);
    myMapper->SetInput(myExtractor->GetOutput());
  }
  //  myMapper->SetInput(theDataSet);
  SetMapper(myMapper.GetPointer());
}

//----------------------------------------------------------------------------
vtkDataSetMapper* VISU_DataSetActor::GetDataSetMapper()
{
  return myMapper.GetPointer();
}

//----------------------------------------------------------------------------
void VISU_DataSetActor::RemoveAllClippingPlanes()
{
  myFunction->GetFunction()->RemoveAllItems();
  myFunction->Modified();
}

//----------------------------------------------------------------------------
vtkIdType VISU_DataSetActor::GetNumberOfClippingPlanes()
{
  return myFunction->GetFunction()->GetNumberOfItems();
}

//----------------------------------------------------------------------------
bool VISU_DataSetActor::AddClippingPlane(vtkPlane* thePlane)
{
  vtkImplicitFunctionCollection* aFunctions = GetClippingPlanes();
  aFunctions->InitTraversal();
  vtkImplicitFunction* aItem;
  while ((aItem = aFunctions->GetNextItem())) {
    if (thePlane == aItem)
      return false;
  }
  myFunction->AddFunction(thePlane);
  return true;
}

//----------------------------------------------------------------------------
vtkPlane* VISU_DataSetActor::GetClippingPlane(vtkIdType theID)
{
  vtkPlane* aPlane = NULL;
  if ((theID >= 0) && (theID < GetNumberOfClippingPlanes())) {
    vtkImplicitFunctionCollection* aFunction = myFunction->GetFunction();
    vtkImplicitFunction* aFun = NULL;
    aFunction->InitTraversal();
    for (vtkIdType i = 0; i <= theID; i++)
      aFun = aFunction->GetNextItem();
    aPlane = dynamic_cast<vtkPlane*>(aFun);
  }
  return aPlane;
}
  
//----------------------------------------------------------------------------
vtkImplicitFunctionCollection* VISU_DataSetActor::GetClippingPlanes()
{
  return myFunction->GetFunction();
}
