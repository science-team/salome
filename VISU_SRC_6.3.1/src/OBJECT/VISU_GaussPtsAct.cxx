// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  VISU OBJECT : interactive object for VISU entities implementation
//  File   : VISU_MeshAct.hxx
//  Author : Laurent CORNABE with the help of Nicolas REJNERI
//  Module : VISU
//  $Header: /home/server/cvs/VISU/VISU_SRC/src/OBJECT/VISU_GaussPtsAct.cxx,v 1.8.2.6.6.1.8.2 2011-06-02 06:00:17 vsr Exp $
//
#include "VISU_GaussPtsAct.h"
#include "VISU_GaussPointsPL.hxx"
#include "VISU_GaussPtsSettings.h"
#include "VISU_GaussPtsDeviceActor.h"
#include "VISU_PickingSettings.h"
#include "VISU_WidgetCtrl.hxx"
#include "VISU_SphereWidget.hxx"
#include "VISU_OpenGLPointSpriteMapper.hxx"
#include "VISU_ScalarBarCtrl.hxx"
#include "VISU_ScalarBarActor.hxx"

#include "VISU_Event.h" 
#include "SVTK_Actor.h"
#include "VTKViewer_FramedTextActor.h"
#include <SALOME_ExtractPolyDataGeometry.h>

#include <vtkCamera.h>
#include <vtkRenderer.h>
#include <vtkPointPicker.h>
#include <vtkMatrix4x4.h>
#include <vtkScalarBarWidget.h>
#include <vtkTextMapper.h>
#include <vtkTextProperty.h>
#include <vtkImplicitBoolean.h>
#include <vtkImplicitFunctionCollection.h>
#include <vtkPlane.h>

#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkDataArray.h>

#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyData.h>
#include <vtkActor.h>
#include <vtkTransform.h>

#include <vtkObjectFactory.h>
#include <vtkCallbackCommand.h>
#include <vtkInteractorStyle.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkOutlineSource.h>

#include <sstream>
#include <boost/bind.hpp>

#include <vtkUnstructuredGrid.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindow.h>
#include <vtkCellArray.h>
#include <vtkWarpVector.h>
#include <vtkCellDataToPointData.h>
#include <vtkCell.h>
#include <vtkMath.h>

#include <vtkViewport.h>

#include "utilities.h"

#ifdef _DEBUG_
static int MYDEBUG = 0;
static int MYDEBUG1 = 0;
static int MYDEBUG2 = 0;
#else
static int MYDEBUG = 0;
static int MYDEBUG1 = 0;
static int MYDEBUG2 = 0;
#endif


namespace
{
  bool
  CheckIsSameValue(vtkFloatingPointType theTarget,
                   vtkFloatingPointType theSource)
  {
    static vtkFloatingPointType TOL = 10.0 / VTK_LARGE_FLOAT;
    return fabs(theTarget - theSource)  < TOL;
  }

  bool
  CheckIsSameVector(vtkFloatingPointType* theTarget,
                    vtkFloatingPointType* theSource,
                    size_t theLength)
  {
    for ( size_t anId = 0; anId < theLength; anId++ ) {
      if ( !CheckIsSameValue( theTarget[ anId ], theSource[ anId ] ) )
        return false;
    }

    return true;
  }
  
}

//============================================================================
vtkStandardNewMacro(VISU_GaussPtsAct);

//----------------------------------------------------------------------------
VISU_GaussPtsAct
::VISU_GaussPtsAct():
  myEventCallbackCommand(vtkCallbackCommand::New()),
  myPriority(0.0),
  myDeviceActor(VISU_GaussPtsDeviceActor::New()),
  myWidgetCtrl(NULL),
  myGaussPointsPL(NULL),
  myLastPreHighlightObjID(-1),
  myCursorPyramid(VISU_CursorPyramid::New()),
  myCursorPyramidSelected(VISU_CursorPyramid::New()),
  myCellSource(vtkUnstructuredGrid::New()),
  myCellActor(SVTK_Actor::New()),
  myScalarBarCtrl(VISU_ScalarBarCtrl::New()),
  myBarVisibility(true),
  myInsideCursorSettings(NULL),
  myCurrentPL(NULL),
  myMapper(vtkPolyDataMapper::New()),
  myPolyDataExtractor(SALOME_ExtractPolyDataGeometry::New()),
  myFunction(vtkImplicitBoolean::New()),
  myWarpVector(vtkWarpVector::New()),
  myCellDataToPointData(vtkCellDataToPointData::New())
{
  if(MYDEBUG) MESSAGE("VISU_GaussPtsAct::VISU_GaussPtsAct - this = "<<this);

  myEventCallbackCommand->Delete();
  myDeviceActor->Delete();

  myCursorPyramid->Delete();
  myCursorPyramid->SetPickable(0);
  myCursorPyramid->SetVisibility(0);

  myCursorPyramidSelected->Delete();
  myCursorPyramidSelected->SetPickable(0);
  myCursorPyramidSelected->SetVisibility(0);

  myCellSource->Delete();
  myCellActor->Delete();

  myEventCallbackCommand->SetClientData(this); 
  myEventCallbackCommand->SetCallback(VISU_GaussPtsAct::ProcessEvents);

  vtkMatrix4x4 *aMatrix = vtkMatrix4x4::New();

  myDeviceActor->SetProperty(GetProperty());
  myDeviceActor->SetUserMatrix(aMatrix);
  myDeviceActor->SetVisibility(true);
  myDeviceActor->SetPickable(false);

  myCellSource->Allocate();
  myCellActor->Initialize();
  myCellActor->SetRepresentation(VTK_WIREFRAME);
  myCellActor->SetSource(myCellSource.GetPointer());

  myCellActor->SetVisibility(0);
  myCellActor->SetPickable(0);
  myCellActor->GetProperty()->SetAmbient(1.0);
  myCellActor->GetProperty()->SetDiffuse(0.0);

  myScalarBarCtrl->Delete();

  myMapper->Delete();
  aMatrix->Delete();

  myWarpVector->Delete();
  myCellDataToPointData->Delete();

  myPolyDataExtractor->SetImplicitFunction(myFunction);
  //myPolyDataExtractor->ExtractBoundaryCellsOn();

  myFunction->SetOperationTypeToIntersection();
  myPolyDataExtractor->Delete();
  myFunction->Delete();

  if( VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get() )
    aPickingSettings->AddObserver(VISU::UpdatePickingSettingsEvent, 
                                  myEventCallbackCommand.GetPointer(), 
                                  myPriority);
}

VISU_GaussPtsAct
::~VISU_GaussPtsAct()
{
  if(MYDEBUG) MESSAGE("VISU_GaussPtsAct::~VISU_GaussPtsAct - this = "<<this);
  SetWidgetCtrl(NULL);
  SetInteractor(NULL);
  SetInsideCursorSettings(NULL);
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::SetPipeLine(VISU_PipeLine* thePipeLine) 
{
  if(MYDEBUG) MESSAGE("VISU_GaussPtsAct::SetPipeLine - this = "<<this);

  myGaussPointsPL = dynamic_cast<VISU_GaussPointsPL*>(thePipeLine);

  Superclass::SetPipeLine(thePipeLine);
}

VISU_GaussPointsPL*
VISU_GaussPtsAct
::GetGaussPointsPL()
{
  return myGaussPointsPL.GetPointer();
}

//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::DeepCopy(VISU_Actor *theActor)
{
  if(VISU_GaussPtsAct* anActor = dynamic_cast<VISU_GaussPtsAct*>(theActor)){
    Superclass::DeepCopy(theActor);
    SetBarVisibility(anActor->GetBarVisibility());
  }
}

//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::ShallowCopyPL(VISU_PipeLine* thePipeLine)
{
  if(MYDEBUG) MESSAGE("VISU_GaussPtsAct::ShallowCopyPL - this = "<<this);
  
  Superclass::ShallowCopyPL(thePipeLine);
  
  UpdateInsideCursorSettings();
  
  Highlight(isHighlighted());
}


//----------------------------------------------------------------------------

vtkMapper* 
VISU_GaussPtsAct
::GetMapper()
{
  //vtkMapper* aMapper = myCurrentPL->GetPointSpriteMapper();
  //aMapper->Update();
  //return aMapper;
  myMapper->Update();
  return myMapper.GetPointer();
}

vtkFloatingPointType* 
VISU_GaussPtsAct
::GetBounds()
{
  return GetMapper()->GetBounds();
}

vtkDataSet* 
VISU_GaussPtsAct
::GetInput()
{
  return GetMapper()->GetInput();
}

VISU_PipeLine* 
VISU_GaussPtsAct
::GetCurrentPL() 
{ 
  return myCurrentPL;
}


//----------------------------------------------------------------------------
unsigned long int
VISU_GaussPtsAct
::GetMemorySize()
{
  vtkDataSet* aDataSet = myDeviceActor->GetMapper()->GetInput();
  unsigned long int aSize = aDataSet->GetActualMemorySize() * 1024;

  aSize += Superclass::GetMemorySize();

  return aSize;
}

//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::AddToRender(vtkRenderer* theRenderer)
{
  if(MYDEBUG) MESSAGE("VISU_GaussPtsAct::AddToRender - this = "<<this);

  Superclass::AddToRender(theRenderer);

  myDeviceActor->AddToRender(theRenderer);

  theRenderer->AddActor(myCellActor.GetPointer());

  myCursorPyramid->AddToRender(theRenderer);
  myCursorPyramidSelected->AddToRender(theRenderer);
  myScalarBarCtrl->AddToRender(theRenderer);
}

void 
VISU_GaussPtsAct
::RemoveFromRender(vtkRenderer* theRenderer)
{
  if(MYDEBUG) MESSAGE("VISU_GaussPtsAct::RemoveFromRender - this = "<<this);

  myDeviceActor->RemoveFromRender(theRenderer);
  
  theRenderer->RemoveActor(myCellActor.GetPointer());
  
  myCursorPyramid->RemoveFromRender(theRenderer);
  myCursorPyramidSelected->RemoveFromRender(theRenderer);
  myScalarBarCtrl->RemoveFromRender(theRenderer);

  Superclass::RemoveFromRender(theRenderer);
}

void 
VISU_GaussPtsAct
::SetTransform(VTKViewer_Transform* theTransform)
{
  Superclass::SetTransform(theTransform);

  myDeviceActor->SetTransform(theTransform);
}

int
VISU_GaussPtsAct
::RenderOpaqueGeometry(vtkViewport *viewport)
{
  GetMatrix(myDeviceActor->GetUserMatrix());
  return 1;
}

int
VISU_GaussPtsAct
::RenderTranslucentGeometry(vtkViewport *viewport)
{
  GetMatrix(myDeviceActor->GetUserMatrix());
  return 1;
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::SetWidgetCtrl(VISU_WidgetCtrl* theWidgetCtrl)
{
  if(myWidgetCtrl == theWidgetCtrl)
    return;

  if(MYDEBUG) 
    MESSAGE("VISU_GaussPtsAct::SetWidgetCtrl - this = "<<this<<"; theWidget = "<<theWidgetCtrl);

  if(myWidgetCtrl)
    myWidgetCtrl->RemoveObserver(myEventCallbackCommand.GetPointer());

  if(theWidgetCtrl){
    theWidgetCtrl->AddObserver(vtkCommand::EnableEvent, 
                               myEventCallbackCommand.GetPointer(), 
                               myPriority);
    theWidgetCtrl->AddObserver(vtkCommand::DisableEvent, 
                               myEventCallbackCommand.GetPointer(), 
                               myPriority);
    theWidgetCtrl->AddObserver(vtkCommand::StartInteractionEvent, 
                               myEventCallbackCommand.GetPointer(), 
                               myPriority);
    theWidgetCtrl->AddObserver(vtkCommand::EndInteractionEvent, 
                               myEventCallbackCommand.GetPointer(), 
                               myPriority);
  }

  myWidgetCtrl = theWidgetCtrl;
}

bool
VISU_GaussPtsAct
::GetChangeMagnification()
{
  return myChangeMagnification;
}

void
VISU_GaussPtsAct
::ChangeMagnification( bool up )
{
  myEventCallbackCommand->SetAbortFlag(1);

  vtkFloatingPointType aMagnification = myInsideCursorSettings->GetMagnification();
  vtkFloatingPointType anIncrement = myInsideCursorSettings->GetIncrement();
  vtkFloatingPointType coefficient = up ? anIncrement : 1 / anIncrement;

  myInsideCursorSettings->SetMagnification( aMagnification * coefficient );
  myInsideCursorSettings->InvokeEvent(VISU::UpdateFromSettingsEvent,NULL);

  Update();
}

//----------------------------------------------------------------------------
namespace
{
  VISU_GaussPointsPL*
  CreatePipeLine(VISU_GaussPointsPL* thePipeLine)
  {
    VISU_GaussPointsPL* aPipeLine = VISU_GaussPointsPL::New();
    aPipeLine->ShallowCopy(thePipeLine, true);
    aPipeLine->Update();
    return aPipeLine;
  }
}

void
VISU_GaussPtsAct
::SetMapperInput(vtkDataSet* theDataSet) 
{
  if(MYDEBUG) MESSAGE("VISU_GaussPtsAct::SetMapperInput - this = "<<this);

  myDeviceActor->SetExtractor(myPolyDataExtractor);
  myDeviceActor->SetPipeLine(GetGaussPointsPL());
  myCurrentPL = myDeviceActor->GetPipeLine();

  //SetMapper(myCurrentPL->GetPointSpriteMapper());

  //  myPolyDataExtractor->SetInput(myCurrentPL->GetPickableDataSet());
  //myMapper->SetInput(myPolyDataExtractor->GetOutput());
  myMapper->SetInput(myCurrentPL->GetPickableDataSet());
  SetMapper(myMapper.GetPointer());
}


//----------------------------------------------------------------------------
namespace
{
  inline  
  vtkFloatingPointType 
  GetRadius(vtkIdType theVTKID,
            vtkDataArray *theScalarArray,
            VISU_GaussPointsPL* theGaussPointsPL)
  {
    vtkFloatingPointType aRadius = 0.5;
    if(theGaussPointsPL->GetPointSpriteMapper()->GetPointSpriteMode() == 1) // Geometry mode
      aRadius *= theGaussPointsPL->GetSize() * theGaussPointsPL->GetAverageCellSize();
    else if(theGaussPointsPL->GetBicolor()){
      vtkFloatingPointType aVal = theScalarArray->GetTuple1(theVTKID);
      if(aVal > 0.0)
        aRadius *= theGaussPointsPL->GetMaxSize();
      else
        aRadius *= theGaussPointsPL->GetMinSize();
      aRadius *= theGaussPointsPL->GetAverageCellSize();
    }else
      aRadius *= theGaussPointsPL->GetPointSize(theVTKID,theScalarArray);

    return aRadius;
  }
}

vtkFloatingPointType
VISU_GaussPtsAct
::GetRadius(vtkIdType theObjID,
            vtkIdType theVTKID,
            vtkDataArray *theScalarArray)
{
  return ::GetRadius(theVTKID,theScalarArray,myDeviceActor->GetPipeLine());
}

//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_GaussPtsAct
::GetMagnification(vtkIdType theObjID)
{
  return myDeviceActor->GetPipeLine()->GetMagnification();
}


//----------------------------------------------------------------------------
namespace
{
  inline  
  vtkFloatingPointType 
  GetClamp(VISU_GaussPointsPL* theGaussPointsPL)
  {
    vtkFloatingPointType aClamp = theGaussPointsPL->GetClamp();
    if(theGaussPointsPL->GetPrimitiveType() == VISU_OpenGLPointSpriteMapper::GeomSphere)
      return -aClamp;
    return aClamp;
  }
}

vtkFloatingPointType 
VISU_GaussPtsAct
::GetClamp(vtkIdType theObjID)
{
  return ::GetClamp(myDeviceActor->GetPipeLine());
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::SetOpacity(vtkFloatingPointType theValue)
{
  GetGaussPointsPL()->SetOpacity(theValue);
  Superclass::SetOpacity(theValue);
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::SetFactory(VISU::TActorFactory* theActorFactory)
{ 
  using namespace VISU;
  myGaussPtsActorFactory = dynamic_cast<TGaussPtsActorFactory*>(theActorFactory);
  myUpdatePrs3dSignal.connect(boost::bind(&TGaussPtsActorFactory::UpdateFromActor,
                                          myGaussPtsActorFactory,
                                          _1));
  Superclass::SetFactory(theActorFactory);
}

VISU::TGaussPtsActorFactory*
VISU_GaussPtsAct
::GetGaussPtsFactory()
{
  return myGaussPtsActorFactory;
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::SetVisibility(int theMode)
{
  Superclass::SetVisibility(theMode);
  myDeviceActor->SetVisibility(GetVisibility()); // VSV
  myScalarBarCtrl->SetVisibility(theMode);
}

int
VISU_GaussPtsAct
::IsSegmentationEnabled()
{
  return myWidgetCtrl  && myWidgetCtrl->GetEnabled();
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::SetBarVisibility(bool theMode)
{
  myBarVisibility = theMode;
  myScalarBarCtrl->SetCtrlVisibility(theMode); 
  myScalarBarCtrl->SetVisibility(GetVisibility()); 
}

bool
VISU_GaussPtsAct
::GetBarVisibility()
{ 
  return myBarVisibility;
}

VISU_ScalarBarCtrl* 
VISU_GaussPtsAct
::GetScalarBarCtrl()
{
  return myScalarBarCtrl.GetPointer();
}

//----------------------------------------------------------------------------
void
VISU_GaussPtsAct
::SetInteractor(vtkRenderWindowInteractor* theInteractor)
{
  if(theInteractor == myInteractor)
    return;

  if(myInteractor)
    myInteractor->RemoveObserver(myEventCallbackCommand.GetPointer());
  
  if(theInteractor){
    theInteractor->AddObserver(vtkCommand::CharEvent, 
                               myEventCallbackCommand.GetPointer(), 
                               myPriority);
    theInteractor->AddObserver(VISU::SetSMDecreaseMagnificationEvent, 
                               myEventCallbackCommand.GetPointer(), 
                               myPriority);
    theInteractor->AddObserver(VISU::SetSMIncreaseMagnificationEvent, 
                               myEventCallbackCommand.GetPointer(), 
                               myPriority);
  }
  Superclass::SetInteractor(theInteractor);

  Modified();
}
//-------------------------------------------------------------------------
void 
VISU_GaussPtsAct
::ProcessEvents(vtkObject* vtkNotUsed(theObject), 
                unsigned long theEvent,
                void* theClientData, 
                void* vtkNotUsed(theCallData))
{
  if(vtkObject* anObject = reinterpret_cast<vtkObject*>(theClientData))
    if(VISU_GaussPtsAct* self = dynamic_cast<VISU_GaussPtsAct*>(anObject)) {
      if(theEvent == VISU::UpdateInsideSettingsEvent)
        self->UpdateInsideCursorSettings();
      else
        self->OnInteractorEvent(theEvent);
    }
}

//-------------------------------------------------------------------------
void
VISU_GaussPtsAct
::OnInteractorEvent(unsigned long theEvent)
{
  switch(theEvent){
  case vtkCommand::CharEvent: {
    switch(myInteractor->GetKeyCode()) {
    case 'M' :
    case 'm' :
    {
      if( IsSegmentationEnabled() )
      {
        this->ChangeMagnification( myInteractor->GetShiftKey() );
        return;
      }

      myChangeMagnification = myInteractor->GetShiftKey();
      break;
    }
    case 'D' :
    case 'd' :
    {
      if( IsSegmentationEnabled() && myWidgetCtrl->IsSphereActive() )
      {
        VISU_SphereWidget* aSphereWidget = myWidgetCtrl->GetSphereWidget();
        aSphereWidget->ChangeRadius( myInteractor->GetShiftKey() );
        myWidgetCtrl->InvokeEvent(vtkCommand::EndInteractionEvent,NULL);
        myWidgetCtrl->GetInteractor()->Render();
      }
      return;
    }
    default:
      return;
    }
    myUpdatePrs3dSignal(this);
    break;
  }
  case VISU::SetSMDecreaseMagnificationEvent: 
  case VISU::SetSMIncreaseMagnificationEvent: 
    myChangeMagnification = theEvent == VISU::SetSMIncreaseMagnificationEvent;
    myUpdatePrs3dSignal(this);
    break;
  case VISU::UpdatePickingSettingsEvent:
    UpdatePickingSettings();
    break;
  case vtkCommand::EndInteractionEvent:
  case vtkCommand::EnableEvent:
  case vtkCommand::DisableEvent: 
    myCurrentPL->GetMapper()->Update();
    Highlight(isHighlighted());
    break;
  default:
    break;
  }
}

//----------------------------------------------------------------------------
bool
VISU_GaussPtsAct
::IsInfinitive()
{
  if(vtkMapper* aMapper = myDeviceActor->GetMapper()){
    if(vtkDataSet* aDataSet= aMapper->GetInput()){
      aDataSet->Update();
      myIsInfinite = aDataSet->GetNumberOfCells() < 2;
    }
  }
  return myIsInfinite;
}

//----------------------------------------------------------------------------
int
VISU_GaussPtsAct
::GetPickable()
{
  if(Superclass::GetPickable()){
    if(vtkMapper* aMapper = GetMapper()){
      if(vtkDataSet* aDataSet= aMapper->GetInput()){
        aDataSet->Update();
        return aDataSet->GetNumberOfCells() > 0;
      }
    }
  }

  return false;
}

//----------------------------------------------------------------------------
bool
VISU_GaussPtsAct
::PreHighlight(vtkInteractorStyle* theInteractorStyle, 
               SVTK_SelectionEvent* theSelectionEvent,
               bool theIsHighlight)
{
  if ( !GetPickable() )
    return false;

  myPreHighlightActor->SetVisibility(false);
  myCursorPyramid->SetVisibility(false);  

/*  if (!myBarVisibility)
    return false;*/


  if(!theIsHighlight)
    myLastPreHighlightObjID = -1;

  Selection_Mode aSelectionMode = theSelectionEvent->mySelectionMode;

  if(aSelectionMode == ActorSelection || !theIsHighlight)
    return Superclass::PreHighlight(theInteractorStyle,
                                    theSelectionEvent,
                                    theIsHighlight);  
  
  bool anIsChanged = (mySelectionMode != aSelectionMode);
  bool anIsPreselected = myIsPreselected;
  myIsPreselected = false;

  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();
  
  if(aSelectionMode == GaussPointSelection && theIsHighlight){
    SVTK::TPickLimiter aPickLimiter( myPointPicker, this );
    myPointPicker->Pick(theSelectionEvent->myX, 
                        theSelectionEvent->myY, 
                        0.0, 
                        theInteractorStyle->GetCurrentRenderer());
    
    if(myPointPicker->GetActor() != this)
      return (anIsPreselected != myIsPreselected);
    
    vtkIdType aVtkId = myPointPicker->GetPointId();
    
    if(aVtkId >= 0 && mySelector->IsValid(this,aVtkId,true) && hasIO()){
      vtkIdType anObjId = GetNodeObjId( aVtkId );
      myIsPreselected = (anObjId >= 0);
      if(myIsPreselected){
        anIsChanged = (myLastPreHighlightObjID != anObjId);
        if(anIsChanged){
          vtkFloatingPointType* aNodeCoord = GetNodeCoord(anObjId);          
          //Take into account translation
          vtkFloatingPointType aLocalNodeCoord[3];
          this->Transform->Push();
          this->Transform->PostMultiply();
          this->Transform->Identity();
          
          this->Transform->Translate(this->Position[0],
                                     this->Position[1],
                                     this->Position[2]);
          double aPosition[3];
          this->Transform->GetPosition(aPosition);

          aLocalNodeCoord[0] = aNodeCoord[0] + aPosition[0]; 
          aLocalNodeCoord[1] = aNodeCoord[1] + aPosition[1];
          aLocalNodeCoord[2] = aNodeCoord[2] + aPosition[2];

          vtkDataSet* aDataSet = GetInput();
          vtkPointData* aPointData = aDataSet->GetPointData();
          if(vtkDataArray *aScalarArray = aPointData->GetScalars()){
            vtkFloatingPointType aPyramidHeight = aPickingSettings->GetPyramidHeight();
            aPyramidHeight = aPyramidHeight*myGaussPointsPL->GetMaxPointSize();
            //vtkFloatingPointType aColor[3] = myPreHighlightActor->GetProperty()->GetColor();
            vtkFloatingPointType aColor[3];
            theInteractorStyle->GetCurrentRenderer()->GetBackground( aColor );
            aColor[0] = 1. - aColor[0];
            aColor[1] = 1. - aColor[1];
            aColor[2] = 1. - aColor[2];

            myCursorPyramid->Init(aPyramidHeight,
                                  aPickingSettings->GetCursorSize(),
                                  GetRadius(anObjId,aVtkId,aScalarArray),
                                  GetMagnification(anObjId),
                                  GetClamp(anObjId),
                                  aNodeCoord,
                                  aColor);
          }
          this->Transform->Pop();
          myLastPreHighlightObjID = anObjId;
        }
        myCursorPyramid->SetVisibility(true);
      }
    }
  }
  
  mySelectionMode = aSelectionMode;
  anIsChanged |= (anIsPreselected != myIsPreselected);

  return anIsChanged;
}


//----------------------------------------------------------------------------
inline
void
ChangeZoom(VISU_PickingSettings *thePickingSettings,
           vtkRenderer* theRenderer,
           vtkIdType theInitialHasIndex,
           vtkIdType theCurrentHasIndex)
{
  if( (theInitialHasIndex + theCurrentHasIndex == 1) && thePickingSettings){
    vtkCamera *aCamera = theRenderer->GetActiveCamera();

    vtkFloatingPointType aZoomFactor = thePickingSettings->GetZoomFactor();
    double aScale = aCamera->GetParallelScale();
    if ( !theInitialHasIndex && theCurrentHasIndex ) {
      aCamera->SetParallelScale( aScale/aZoomFactor );
    }
    else {
      aCamera->SetParallelScale( aScale*aZoomFactor );
    }
  }
  
}

bool
VISU_GaussPtsAct
::Highlight(vtkInteractorStyle* theInteractorStyle, 
            SVTK_SelectionEvent* theSelectionEvent,
            bool theIsHighlight)
{ 
  if ( !GetPickable() )
    return false;

  //  int anInitialHasIndex = myIsSubElementsHighlighted && mySelectionMode == GaussPointSelection;
  Handle(SALOME_InteractiveObject) anIO = getIO();
  //  myIsSubElementsHighlighted = false;

  Selection_Mode aSelectionMode = theSelectionEvent->mySelectionMode;

  if(!theIsHighlight && aSelectionMode == GaussPointSelection){
    mySelector->RemoveIObject(anIO);

    /*    ChangeZoom(myPickingSettings,
               theInteractorStyle,
               anInitialHasIndex,
               false,
               anIO);*/

    return true;
  }
  
/*  if (!myBarVisibility)
    return false;*/


  if(aSelectionMode == ActorSelection)
    return Superclass::Highlight(theInteractorStyle,
                                 theSelectionEvent,
                                 theIsHighlight);

  if(aSelectionMode == GaussPointSelection && !theSelectionEvent->myIsRectangle){
    vtkRenderer *aRenderer = theInteractorStyle->GetCurrentRenderer();

    SVTK::TPickLimiter aPickLimiter( myPointPicker, this );
    myPointPicker->Pick(theSelectionEvent->myX, 
                        theSelectionEvent->myY,
                        0.0, 
                        aRenderer);

    if(myPointPicker->GetActor() != this) {
      mySelector->ClearIObjects();

      /*      ChangeZoom(myPickingSettings,
                 theInteractorStyle,
                 anInitialHasIndex,
                 false,
                 anIO);*/

      return true;
    }

    vtkIdType aVtkId = myPointPicker->GetPointId();
    if( aVtkId >= 0  && mySelector->IsValid( this, aVtkId, true ) && hasIO()) {
      vtkIdType anObjId = GetNodeObjId( aVtkId );
      if(anObjId >= 0){
        //      myIsHighlighted = true;
        // Update the Selector
        if(mySelector->IsSelected(myIO))
          mySelector->AddOrRemoveIndex(myIO,anObjId,theSelectionEvent->myIsShift);
        else{
          if(!theSelectionEvent->myIsShift){
            mySelector->ClearIObjects();
          }
          mySelector->AddOrRemoveIndex(myIO,anObjId,theSelectionEvent->myIsShift);
          mySelector->AddIObject(this);
        }
        /*
        vtkFloatingPointType* aNodeCoord = GetNodeCoord(anObjId);
        //
        // FlyTo
        vtkRenderWindowInteractor* anInteractor = theInteractorStyle->GetInteractor();
        vtkFloatingPointType aDollyWas = anInteractor->GetDolly();
        int aNumberOfFlyFramesWas = anInteractor->GetNumberOfFlyFrames();
        
        anInteractor->SetDolly(0.);
        anInteractor->SetNumberOfFlyFrames(myPickingSettings->GetStepNumber());
        anInteractor->FlyTo(aRenderer,aNodeCoord);
        aRenderer->ResetCameraClippingRange();
        anInteractor->SetDolly(aDollyWas);
        anInteractor->SetNumberOfFlyFrames(aNumberOfFlyFramesWas);
        
        anInteractor->InvokeEvent(SVTK::ChangeRotationPoint, aNodeCoord);*/

        mySelectionMode = aSelectionMode;

        /*      ChangeZoom(myPickingSettings,
                   theInteractorStyle,
                   anInitialHasIndex,
                   true,
                   anIO);*/

        return true;
      }// if( anObjId >= 0 ) {
    }//if( aVtkId >= 0  && mySelector->IsValid( this, aVtkId, true ) && hasIO()) 
  }//if(!theSelectionEvent->myIsRectangle){

  /* ChangeZoom(myPickingSettings,
             theInteractorStyle,
             anInitialHasIndex,
             false,
             anIO);*/
  
  return false;
}



void VISU_GaussPtsAct::SetPosition(double _arg[3]){
  Superclass::SetPosition(_arg);
  Highlight(isHighlighted());
}

//==================================================================
// function : Highlight
// purpose  :
//==================================================================
void
VISU_GaussPtsAct
::Highlight(bool theIsHighlight)
{
  if(!mySelector.GetPointer())
    return;

  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();

  Selection_Mode aSelectionMode = mySelector->SelectionMode();
  
  bool anInitialHasIndex = isSubElementsHighlighted() && mySelectionMode == GaussPointSelection;
  
  TColStd_IndexedMapOfInteger aMapIndex;
  mySelector->GetIndex( getIO(), aMapIndex );
  bool aCurrentHasIndex = aMapIndex.Extent() == 1;
  bool anIsVisible = GetVisibility() && aCurrentHasIndex && theIsHighlight;
  bool aShowTextActor = aPickingSettings->GetInfoWindowEnabled();
  bool aShowCellActor = aPickingSettings->GetDisplayParentMesh();

  myOutlineActor->SetVisibility(false);
  myCursorPyramid->SetVisibility(false);
  myTextActor->SetVisibility(anIsVisible && aShowTextActor);
  myCellActor->SetVisibility(anIsVisible && aShowCellActor);
  GetScalarBarCtrl()->SetIsMarked(anIsVisible);
  myCursorPyramidSelected->SetVisibility(anIsVisible);
  //GetScalarBarCtrl()->Update();

  myIsSubElementsHighlighted = aCurrentHasIndex;

  // Zoom if necessary
  if( GetVisibility() && aPickingSettings->GetCameraMovementEnabled() )
  {
    ChangeZoom(aPickingSettings,
               GetRenderer(),
               anInitialHasIndex,
               aCurrentHasIndex);
  }

  if( aSelectionMode == ActorSelection ) {
    Superclass::Highlight(theIsHighlight);
    return;
  }

  if(!theIsHighlight)
    return;

  if( aSelectionMode != GaussPointSelection )
    return;

  if ( !aCurrentHasIndex || !myBarVisibility )
    return;

  //  myTextActor->SetVisibility(anIsVisible);
  //  myCellActor->SetVisibility(anIsVisible && myPickingSettings->GetDisplayParentMesh());
  //  GetScalarBarCtrl()->SetIsMarked(anIsVisible);
  //  myCursorPyramidSelected->SetVisibility(anIsVisible);
//GetScalarBarCtrl()->Update();

  // to fix a bug with incorrect representation after the first highlight action
  myIsHighlighted = aCurrentHasIndex;

  // Zoom if necessary
//   ChangeZoom(myPickingSettings,
//           GetRenderer(),
//           anInitialHasIndex,
//           aCurrentHasIndex);

  //  TColStd_IndexedMapOfInteger aMapIndex;
  mySelector->GetIndex( getIO(), aMapIndex );
  if(aMapIndex.Extent() != 1 || !myBarVisibility) {
    return;
  }

  vtkRenderer *aRenderer = GetRenderer();
  vtkCamera* aCamera = aRenderer->GetActiveCamera();

  int anObjId = aMapIndex(1);
  vtkFloatingPointType* aNodeCoord = GetNodeCoord(anObjId);
  if ( !aNodeCoord )
    return;
  //Take into account translation
  vtkFloatingPointType aLocalNodeCoord[3];
  this->Transform->Push();
  this->Transform->PostMultiply();
  this->Transform->Identity();
  this->Transform->Translate(this->Position[0],
                             this->Position[1],
                             this->Position[2]);
  double aPosition[3];
  this->Transform->GetPosition(aPosition);
  aLocalNodeCoord[0] = aNodeCoord[0] + aPosition[0]; 
  aLocalNodeCoord[1] = aNodeCoord[1] + aPosition[1];
  aLocalNodeCoord[2] = aNodeCoord[2] + aPosition[2];
  this->Transform->Pop();

  vtkFloatingPointType aFocalPnt[3];
  aCamera->GetFocalPoint(aFocalPnt);

  if ( CheckIsSameVector(aLocalNodeCoord, aFocalPnt, 3 ) ) 
    return;

  // FlyTo
  if( anIsVisible && aPickingSettings->GetCameraMovementEnabled() )
  {
    vtkRenderWindowInteractor* anInteractor = myInteractor;
    vtkFloatingPointType aDollyWas = anInteractor->GetDolly();
    int aNumberOfFlyFramesWas = anInteractor->GetNumberOfFlyFrames();
  
    anInteractor->SetDolly(0.0);
    anInteractor->SetNumberOfFlyFrames(aPickingSettings->GetStepNumber());
    anInteractor->FlyTo(aRenderer, aNodeCoord);
    aRenderer->ResetCameraClippingRange();
    anInteractor->SetDolly(aDollyWas);
    anInteractor->SetNumberOfFlyFrames(aNumberOfFlyFramesWas);
    anInteractor->InvokeEvent(SVTK::ChangeRotationPoint, aNodeCoord);
  }
  //
  std::ostringstream aStr;
  //  int anObjId = aMapIndex(1);
  aStr<<"Global ID: "<<anObjId;
  //
  vtkIdType aVtkId = GetNodeVTKID(anObjId);
  if(aVtkId >= 0){
    vtkFloatingPointType aWorldCoord[4] = {aLocalNodeCoord[0], aLocalNodeCoord[1], aLocalNodeCoord[2], 1.};
    //
    vtkDataSet* aDataSet = GetInput();
    vtkPointData* aDataSetAttributes = aDataSet->GetPointData();
    //
    if(vtkDataArray* aScalarArray = aDataSetAttributes->GetScalars()){
      vtkFloatingPointType aVal = aScalarArray->GetTuple1(aVtkId);
      //
      //      GetScalarBarCtrl()->SetIsMarked(true);
      GetScalarBarCtrl()->SetMarkValue(aVal);
      GetScalarBarCtrl()->Update();
      //
      vtkFloatingPointType aPyramidHeight = aPickingSettings->GetPyramidHeight();
      aPyramidHeight = aPyramidHeight*myGaussPointsPL->GetMaxPointSize();
      myCursorPyramidSelected->Init(aPyramidHeight,
                                    aPickingSettings->GetCursorSize(),
                                    GetRadius(anObjId,aVtkId,aScalarArray),
                                    GetMagnification(anObjId),
                                    GetClamp(anObjId),
                                    aLocalNodeCoord,
                                    myHighlightActor->GetProperty()->GetColor());
      myCursorPyramidSelected->SetVisibility(anIsVisible && theIsHighlight);
      //
      const VISU::PGaussPtsIDMapper& aGaussPtsIDMapper = 
        myGaussPointsPL->GetGaussPtsIDMapper();
      VISU::TNamedIDMapper* aParent = aGaussPtsIDMapper->GetParent();
      
      VISU::TGaussPointID aGaussPointID = myGaussPointsPL->GetObjID(anObjId);
      VISU::TCellID aCellID = aGaussPointID.first;
      VISU::TLocalPntID aLocalPntID = aGaussPointID.second;
      aStr<<"\nParentCellID: "<<aCellID;
      std::string aParentName = aParent->GetElemName(aCellID);
      if(aParentName != "") {
        aStr<<"\nParentCellName: '"<<aParentName<<"'";
      }
      aStr<<"\nLocalPntID: "<<aLocalPntID;
      aStr<<"\nScalar: "<<aVal;
    }

    if(vtkDataArray* aFieldArray = aDataSetAttributes->GetArray("VISU_FIELD")){
      int aNbComp = aFieldArray->GetNumberOfComponents();
      std::vector<vtkFloatingPointType> aTuple(aNbComp);
      aFieldArray->GetTuple(aVtkId, &aTuple[0]);
      
      aStr<<"\nData: {";
      int anId = 0;
      while(anId < aNbComp){
        vtkFloatingPointType aComp = aTuple[anId++];
        aStr<<aComp;
        if(anId < aNbComp)
          aStr<<"; ";
      }
      aStr<<"}";
    }
    //
    // myTextActor
    std::string aString = aStr.str();
    myTextActor->SetModePosition(aPickingSettings->GetInfoWindowPosition());
    myTextActor->SetTransparency(aPickingSettings->GetInfoWindowTransparency());
    myTextActor->SetWorldPoint(aWorldCoord);
    myTextActor->SetText(aString.c_str());
    myTextActor->SetMoveFrameFlag(1);
    myTextActor->SetVisibility(anIsVisible && theIsHighlight && aShowTextActor);
    //
    // myCellActor
    const VISU::PGaussPtsIDMapper& aGaussPtsIDMapper = myGaussPointsPL->GetGaussPtsIDMapper();
    VISU::TNamedIDMapper* aParent = aGaussPtsIDMapper->GetParent();
    //
    myCellSource->Reset();
    myCellSource->Modified(); // a VTK bug
    vtkUnstructuredGrid* aCellDataSet = aParent->GetUnstructuredGridOutput();
    
    // get parent cell and insert it to myCellSource
    VISU::TGaussPointID aGaussPointID = aGaussPtsIDMapper->GetObjID(anObjId);
    vtkIdType aCellID = aGaussPointID.first;
    vtkCell* aCell = aParent->GetElemCell(aCellID);
    myCellSource->InsertNextCell(aCell->GetCellType(),aCell->GetPointIds());
    
    if (myGaussPointsPL->GetIsDeformed()) {
      // find neighbour cells ids
      vtkIdList* aNeighbourCells = vtkIdList::New();
      aNeighbourCells->Allocate(VTK_CELL_SIZE);
      
      vtkIdList* aCellPoints = aCell->GetPointIds();
      
      vtkIdList *aPointCells = vtkIdList::New();
      aPointCells->Allocate(VTK_CELL_SIZE);
      
      vtkIdType aNbPoints = aCellPoints->GetNumberOfIds();
      for (vtkIdType i = 0; i < aNbPoints; i++) {
        aCellDataSet->GetPointCells(aCellPoints->GetId(i), aPointCells);

        // add cell ids
        vtkIdType aNbCells = aPointCells->GetNumberOfIds();
        for (vtkIdType j = 0; j < aNbCells; j++)
          aNeighbourCells->InsertUniqueId(aPointCells->GetId(j));
      }

      aPointCells->Delete();

      // get vector data
      vtkDataArray* anInputVectors = aDataSetAttributes->GetVectors();
      if (!anInputVectors)
        return;
      
      // insert neighbour cells to the special dataset
      vtkUnstructuredGrid *aCellsToWarp = vtkUnstructuredGrid::New();
      aCellsToWarp->SetPoints(aCellDataSet->GetPoints());

      vtkIdType aNbNeighbourCells = aNeighbourCells->GetNumberOfIds();

      vtkDataArray *aVectorsToSet = vtkDataArray::CreateDataArray(anInputVectors->GetDataType());
      aVectorsToSet->SetNumberOfComponents(3);
      aVectorsToSet->SetNumberOfTuples(aNbNeighbourCells);

      vtkDataArray *aCellVectors = vtkDataArray::CreateDataArray(anInputVectors->GetDataType());
      aCellVectors->SetNumberOfComponents(3);
      
      int aNbComp = anInputVectors->GetNumberOfComponents();
      std::vector<vtkFloatingPointType> aTuple(aNbComp);
      
      for (vtkIdType i = 0; i < aNbNeighbourCells; i++) {
        vtkIdType aVTKCellId = aNeighbourCells->GetId(i);
        vtkIdType anObjCellId = aParent->GetElemObjID(aVTKCellId);
        
        vtkCell* aCurCell = aParent->GetElemCell(anObjCellId);
        
        vtkIdType aNewCellId = aCellsToWarp->InsertNextCell(aCurCell->GetCellType(), aCurCell->GetPointIds());
        
        // get gauss points corresponding to the current cell
        vtkIdType aPointVtkId = -1;
        vtkIdType aLocalPntId = 0;
        aPointVtkId = aGaussPtsIDMapper->GetVTKID(VISU::TGaussPointID(anObjCellId, aLocalPntId));
        
        if (aPointVtkId >= 0) {
          // Compute average vector
          aCellVectors->Reset();
          while (aPointVtkId >= 0) {
            anInputVectors->GetTuple(aPointVtkId, &aTuple[0]);

            if (aNbComp >= 3)
              aCellVectors->InsertNextTuple3(aTuple[0], aTuple[1], aTuple[2]);
            else if (aNbComp == 2)
              aCellVectors->InsertNextTuple3(aTuple[0], aTuple[1], 0);
            else if (aNbComp == 1)
              aCellVectors->InsertNextTuple3(aTuple[0], 0, 0);

            aPointVtkId = aGaussPtsIDMapper->GetVTKID(VISU::TGaussPointID(anObjCellId, ++aLocalPntId));
          }

          double aXCoord = 0, anYCoord = 0, aZCoord = 0;

          vtkIdType aNbVectors = aCellVectors->GetNumberOfTuples();
          
          for (vtkIdType aVecId = 0; aVecId < aNbVectors; aVecId++) {
            aXCoord  += aCellVectors->GetComponent(aVecId, 0);
            anYCoord += aCellVectors->GetComponent(aVecId, 1);
            aZCoord  += aCellVectors->GetComponent(aVecId, 2);
          }

          aXCoord = aXCoord / aNbVectors;
          anYCoord = anYCoord / aNbVectors;
          aZCoord = aZCoord / aNbVectors;
          
          // set vector data for the cell
          aVectorsToSet->SetTuple3(aNewCellId, aXCoord, anYCoord, aZCoord);
        }
        else
          aVectorsToSet->SetTuple3(aNewCellId, 0, 0, 0);
      }
      
      aCellsToWarp->GetCellData()->SetVectors(aVectorsToSet);
      
      aVectorsToSet->Delete();
      aCellVectors->Delete();
      aNeighbourCells->Delete();
      
      // warp
      myWarpVector->SetScaleFactor(myGaussPointsPL->GetScale());
      
      myCellDataToPointData->SetInput(aCellsToWarp);
      myCellDataToPointData->PassCellDataOn();
      aCellsToWarp->Delete();
             
      myWarpVector->SetInput(myCellDataToPointData->GetUnstructuredGridOutput());
      vtkUnstructuredGrid* aWarpedDataSet = myWarpVector->GetUnstructuredGridOutput();
      aWarpedDataSet->Update();
      myCellSource->SetPoints(aWarpedDataSet->GetPoints());
    }
    else
      myCellSource->SetPoints(aCellDataSet->GetPoints());
    
    myCellActor->SetVisibility(anIsVisible && theIsHighlight && aShowCellActor);
  }
}

void
VISU_GaussPtsAct
::UpdatePickingSettings()
{
  //printf( "VISU_GaussPtsAct::UpdatePickingSettings()\n" );
  VISU_PickingSettings* aPickingSettings = VISU_PickingSettings::Get();
  if(!aPickingSettings)
    return;

  myTextActor->SetModePosition(aPickingSettings->GetInfoWindowPosition());
  myTextActor->SetTransparency(aPickingSettings->GetInfoWindowTransparency());

  vtkFloatingPointType aHeight = myGaussPointsPL->GetMaxPointSize()*aPickingSettings->GetPyramidHeight();
  vtkFloatingPointType aCursorSize = aPickingSettings->GetCursorSize();
  myCursorPyramid->SetPreferences(aHeight,aCursorSize);
  myCursorPyramidSelected->SetPreferences(aHeight,aCursorSize);

  myHighlightActor->GetProperty()->SetColor( aPickingSettings->GetColor() );
  myPointPicker->SetTolerance( aPickingSettings->GetPointTolerance() );

  Highlight(isHighlighted());

  Update();
}

//----------------------------------------------------------------
void
VISU_GaussPtsAct
::SetInsideCursorSettings(VISU_InsideCursorSettings* theInsideCursorSettings)
{
  if(myInsideCursorSettings == theInsideCursorSettings)
    return;

  if(myInsideCursorSettings)
    myInsideCursorSettings->RemoveObserver(myEventCallbackCommand.GetPointer());

  myInsideCursorSettings = theInsideCursorSettings;

  if(theInsideCursorSettings){
    theInsideCursorSettings->AddObserver(VISU::UpdateInsideSettingsEvent, 
                                         myEventCallbackCommand.GetPointer(), 
                                         myPriority);
    UpdateInsideCursorSettings();
  }
}

void
VISU_GaussPtsAct
::UpdateInsideCursorSettings()
{
  UpdateInsideCursorSettings( myDeviceActor );
}

void
VISU_GaussPtsAct
::UpdateInsideCursorSettings( PDeviceActor theActor )
{
  if(!myInsideCursorSettings || myInsideCursorSettings->GetInitial())
    return;

  VISU_GaussPointsPL* aPipeline = theActor->GetPipeLine();

  vtkImplicitFunction* anImplicitFunction = aPipeline->GetImplicitFunction();

  aPipeline->ShallowCopy(GetGaussPointsPL(), true);

  aPipeline->SetImplicitFunction(anImplicitFunction); // To restore

  aPipeline->SetPrimitiveType( myInsideCursorSettings->GetPrimitiveType() );
  aPipeline->SetClamp( myInsideCursorSettings->GetClamp() );
  aPipeline->SetImageData( myInsideCursorSettings->GetTexture() );
  aPipeline->SetAlphaThreshold( myInsideCursorSettings->GetAlphaThreshold() );
  aPipeline->SetResolution( myInsideCursorSettings->GetResolution() );
  aPipeline->SetMinSize( myInsideCursorSettings->GetMinSize() );
  aPipeline->SetMaxSize( myInsideCursorSettings->GetMaxSize() );
  aPipeline->SetMagnification( myInsideCursorSettings->GetMagnification() );
  aPipeline->SetMagnificationIncrement( myInsideCursorSettings->GetIncrement() );

  if( myInsideCursorSettings->GetPrimitiveType() != VISU_OpenGLPointSpriteMapper::GeomSphere )
    theActor->GetProperty()->SetRepresentation( VTK_POINTS );
  else
    theActor->GetProperty()->SetRepresentation( VTK_SURFACE );

  aPipeline->Update();

  Highlight(isHighlighted());

  Update();
}

//----------------------------------------------------------------------------
unsigned long int 
VISU_GaussPtsAct
::GetMTime()
{
  unsigned long int aTime = Superclass::GetMTime();
  aTime = std::max(aTime, myGaussPointsPL->GetPointSpriteMapper()->GetMTime() );
  return aTime;
}

void VISU_GaussPtsAct::RemoveAllClippingPlanes()
{
  myFunction->GetFunction()->RemoveAllItems();
  myFunction->Modified();
}

vtkIdType VISU_GaussPtsAct::GetNumberOfClippingPlanes()
{
  return myFunction->GetFunction()->GetNumberOfItems();
}

bool VISU_GaussPtsAct::AddClippingPlane(vtkPlane* thePlane)
{
  vtkImplicitFunctionCollection* aFunctions = GetClippingPlanes();
  aFunctions->InitTraversal();
  vtkImplicitFunction* aItem;
  while ((aItem = aFunctions->GetNextItem())) {
    if (thePlane == aItem)
      return false;
  }
  myFunction->AddFunction(thePlane);
  return true;
}

vtkPlane* VISU_GaussPtsAct::GetClippingPlane(vtkIdType theID)
{
  vtkPlane* aPlane = NULL;
  if ((theID >= 0) && (theID < GetNumberOfClippingPlanes())) {
    vtkImplicitFunctionCollection* aFunction = myFunction->GetFunction();
    vtkImplicitFunction* aFun = NULL;
    aFunction->InitTraversal();
    for (vtkIdType i = 0; i <= theID; i++)
      aFun = aFunction->GetNextItem();
    aPlane = dynamic_cast<vtkPlane*>(aFun);
  }
  return aPlane;
}

vtkImplicitFunctionCollection* VISU_GaussPtsAct::GetClippingPlanes()
{
  return myFunction->GetFunction();
}

//----------------------------------------------------------------------------
vtkDataSet* VISU_GaussPtsAct::GetValLabelsInput()
{
  return GetInput();
}



//==============================================================================
vtkStandardNewMacro(VISU_GaussPtsAct1);


//----------------------------------------------------------------------------
VISU_GaussPtsAct1
::VISU_GaussPtsAct1():
  myInsideDeviceActor(VISU_GaussPtsDeviceActor::New()),
  myOutsideDeviceActor(VISU_GaussPtsDeviceActor::New()),
  myOutsideCursorSettings(NULL)
{
  if(MYDEBUG1) MESSAGE("VISU_GaussPtsAct1::VISU_GaussPtsAct1 - this = "<<this);

  myEventCallbackCommand->SetCallback(VISU_GaussPtsAct1::ProcessEvents);

  vtkMatrix4x4 *aMatrix = vtkMatrix4x4::New();

  myInsideDeviceActor->SetVisibility(false);
  myInsideDeviceActor->SetPickable(false);
  myInsideDeviceActor->SetUserMatrix(aMatrix);
  myInsideDeviceActor->Delete();

  myOutsideDeviceActor->SetVisibility(false);
  myOutsideDeviceActor->SetPickable(false);
  myOutsideDeviceActor->SetUserMatrix(aMatrix);
  myOutsideDeviceActor->Delete();
  aMatrix->Delete();
}

VISU_GaussPtsAct1
::~VISU_GaussPtsAct1()
{
  if(MYDEBUG1) MESSAGE("VISU_GaussPtsAct1::~VISU_GaussPtsAct1 - this = "<<this);

  SetOutsideCursorSettings(NULL);
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct1
::ShallowCopyPL(VISU_PipeLine* thePipeLine)
{
  if(MYDEBUG1) MESSAGE("VISU_GaussPtsAct1::ShallowCopyPL - this = "<<this);

  Superclass::ShallowCopyPL(thePipeLine);

  UpdateOutsideCursorSettings();
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct1
::AddToRender(vtkRenderer* theRenderer)
{
  Superclass::AddToRender(theRenderer);

  //myInsideDeviceActor->AddToRender(theRenderer);
  //myOutsideDeviceActor->AddToRender(theRenderer);
}

void 
VISU_GaussPtsAct1
::RemoveFromRender(vtkRenderer* theRenderer)
{
  //myInsideDeviceActor->RemoveFromRender(theRenderer);
  //myOutsideDeviceActor->RemoveFromRender(theRenderer);

  Superclass::RemoveFromRender(theRenderer);
}

void 
VISU_GaussPtsAct1
::SetTransform(VTKViewer_Transform* theTransform)
{
  Superclass::SetTransform(theTransform);

  myInsideDeviceActor->SetTransform(theTransform);
  myOutsideDeviceActor->SetTransform(theTransform);
}


int
VISU_GaussPtsAct1
::RenderOpaqueGeometry(vtkViewport *viewport)
{

  Superclass::RenderOpaqueGeometry(viewport);
  GetMatrix(myInsideDeviceActor->GetUserMatrix());
  GetMatrix(myOutsideDeviceActor->GetUserMatrix());
  return 1;
}

int
VISU_GaussPtsAct1
::RenderTranslucentGeometry(vtkViewport *viewport)
{ 
  Superclass::RenderTranslucentGeometry(viewport);
  GetMatrix(myInsideDeviceActor->GetUserMatrix());
  GetMatrix(myOutsideDeviceActor->GetUserMatrix());

  return 1;
}

//----------------------------------------------------------------------------
void
VISU_GaussPtsAct1
::SetWidgetCtrl(VISU_WidgetCtrl* theWidgetCtrl)
{
  if(MYDEBUG1) MESSAGE("VISU_GaussPtsAct1::SetWidgetCtrl - this = "<<this);

  if(myWidgetCtrl == theWidgetCtrl){
    return;
  }

  if(myWidgetCtrl){
    myInsideDeviceActor->GetPipeLine()->SetImplicitFunction(NULL);
    myInsideDeviceActor->SetVisibility(false);

    myOutsideDeviceActor->GetPipeLine()->SetImplicitFunction(NULL);  
    myOutsideDeviceActor->SetVisibility(false);
  }

  Superclass::SetWidgetCtrl(theWidgetCtrl);

  if(theWidgetCtrl){
    myInsideDeviceActor->GetPipeLine()->SetImplicitFunction(theWidgetCtrl->ImplicitFunction());
    myInsideDeviceActor->SetVisibility(GetVisibility() && IsSegmentationEnabled());

    myOutsideDeviceActor->GetPipeLine()->SetImplicitFunction(theWidgetCtrl->ImplicitFunction());  
    myOutsideDeviceActor->SetVisibility(GetVisibility() && IsSegmentationEnabled());
  }
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct1
::SetMapperInput(vtkDataSet* theDataSet) 
{
  if(MYDEBUG1) MESSAGE("VISU_GaussPtsAct1::SetMapperInput - this = "<<this);

  Superclass::SetMapperInput(theDataSet);

//   if(VISU_GaussPointsPL* aPipeLine = CreatePipeLine(GetGaussPointsPL())){
//     myInsideDeviceActor->SetPipeLine(aPipeLine);
//     aPipeLine->Delete();
//   }

//   if(VISU_GaussPointsPL* aPipeLine = CreatePipeLine(GetGaussPointsPL())){
//     myOutsideDeviceActor->SetPipeLine(aPipeLine);
//     aPipeLine->Delete();

//     aPipeLine->SetExtractBoundaryCells(true);
//     aPipeLine->SetExtractInside(true);
//   }
}


//----------------------------------------------------------------------------
vtkFloatingPointType
VISU_GaussPtsAct1
::GetRadius(vtkIdType theObjID,
            vtkIdType theVTKID,
            vtkDataArray *theScalarArray)
{
  VISU_GaussPointsPL* aPipeLine = myDeviceActor->GetPipeLine();
  if(IsSegmentationEnabled()){
    if(myInsideDeviceActor->GetPipeLine()->GetNodeVTKID(theObjID) < 0)
      aPipeLine = myOutsideDeviceActor->GetPipeLine();
    else
      aPipeLine = myInsideDeviceActor->GetPipeLine();
  }
  return ::GetRadius(theVTKID,theScalarArray,aPipeLine);
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_GaussPtsAct1
::GetMagnification(vtkIdType theObjID)
{
  VISU_GaussPointsPL* aPipeLine = myDeviceActor->GetPipeLine();
  if(IsSegmentationEnabled()){
    if(myInsideDeviceActor->GetPipeLine()->GetNodeVTKID(theObjID) < 0)
      aPipeLine = myOutsideDeviceActor->GetPipeLine();
    else
      aPipeLine = myInsideDeviceActor->GetPipeLine();
  }
  return aPipeLine->GetMagnification();
}


//----------------------------------------------------------------------------
vtkFloatingPointType 
VISU_GaussPtsAct1
::GetClamp(vtkIdType theObjID)
{
  if(IsSegmentationEnabled()){
    if(myInsideDeviceActor->GetPipeLine()->GetNodeVTKID(theObjID) < 0)
      return ::GetClamp(myOutsideDeviceActor->GetPipeLine());
    else
      return ::GetClamp(myInsideDeviceActor->GetPipeLine());
  }
  return Superclass::GetClamp(theObjID);
}


//----------------------------------------------------------------

void 
VISU_GaussPtsAct1::SetPosition(double _arg[3]){
  if(MYDEBUG1) MESSAGE("VISU_GaussPtsAct1::SetPosition - this = "<<this);               
  Superclass::SetPosition(_arg);
  myUpdatePositionSignal(_arg);
}

void 
VISU_GaussPtsAct1
::Connect(VISU_GaussPtsAct2* theActor)
{
  mySetVisibilitySignal.connect(boost::bind(&VISU_GaussPtsAct2::SetVisibility,theActor,_1));
  myUpdatePositionSignal.connect(boost::bind(&VISU_GaussPtsAct2::SetPosition, theActor,_1));
}

void
VISU_GaussPtsAct1
::SetVisibility(int theMode)
{
  if(MYDEBUG1) MESSAGE("VISU_GaussPtsAct1::SetVisibility - this = "<<this);

  Superclass::SetVisibility(theMode);

  bool aVisisbility = GetVisibility();
  bool anIsSegementation = IsSegmentationEnabled();
  myDeviceActor->SetVisibility(aVisisbility/* && !anIsSegementation*/);
  myInsideDeviceActor->SetVisibility(aVisisbility && anIsSegementation);
  myOutsideDeviceActor->SetVisibility(aVisisbility && anIsSegementation);

  mySetVisibilitySignal(GetVisibility());
}

//----------------------------------------------------------------
void
VISU_GaussPtsAct1
::UpdateInsideCursorSettings()
{
  Superclass::UpdateInsideCursorSettings( myInsideDeviceActor );
}

//----------------------------------------------------------------
void
VISU_GaussPtsAct1
::SetOutsideCursorSettings(VISU_OutsideCursorSettings* theOutsideCursorSettings)
{
  if(myOutsideCursorSettings == theOutsideCursorSettings)
    return;

  if(myOutsideCursorSettings)
    myOutsideCursorSettings->RemoveObserver(myEventCallbackCommand.GetPointer());

  myOutsideCursorSettings = theOutsideCursorSettings;

  if(theOutsideCursorSettings)
  {
    theOutsideCursorSettings->AddObserver(VISU::UpdateOutsideSettingsEvent, 
                                          myEventCallbackCommand.GetPointer(), 
                                          myPriority);
    UpdateOutsideCursorSettings();
  }
}

void
VISU_GaussPtsAct1
::UpdateOutsideCursorSettings()
{
  if(!myOutsideCursorSettings || myOutsideCursorSettings->GetInitial())
    return;

  VISU_GaussPointsPL* aPipeline = myOutsideDeviceActor->GetPipeLine();

  vtkImplicitFunction* anImplicitFunction = aPipeline->GetImplicitFunction();
  vtkFloatingPointType aMagnification = aPipeline->GetMagnification();

  aPipeline->ShallowCopy(GetGaussPointsPL(), true);

  aPipeline->SetImplicitFunction(anImplicitFunction); // To restore
  aPipeline->SetExtractBoundaryCells(true);
  //aPipeline->SetExtractInside(true);
  aPipeline->SetMagnification( aMagnification );

  aPipeline->SetPrimitiveType( myOutsideCursorSettings->GetPrimitiveType() );

  if( myOutsideCursorSettings->GetPrimitiveType() != VISU_OpenGLPointSpriteMapper::GeomSphere )
    myOutsideDeviceActor->GetProperty()->SetRepresentation( VTK_POINTS );
  else
    myOutsideDeviceActor->GetProperty()->SetRepresentation( VTK_SURFACE );

  aPipeline->SetClamp( myOutsideCursorSettings->GetClamp() );
  aPipeline->SetImageData( myOutsideCursorSettings->GetTexture() );
  aPipeline->SetAlphaThreshold( myOutsideCursorSettings->GetAlphaThreshold() );
  aPipeline->SetResolution( myOutsideCursorSettings->GetResolution() );
  aPipeline->SetSize( myOutsideCursorSettings->GetSize() );
  aPipeline->SetMagnification( myOutsideCursorSettings->GetMagnification() );
  aPipeline->SetMagnificationIncrement( myOutsideCursorSettings->GetIncrement() );

  if( myOutsideCursorSettings->GetUniform() )
  {
    myOutsideDeviceActor->GetPointSpriteMapper()->ScalarVisibilityOff();
    myOutsideDeviceActor->GetPointSpriteMapper()->SetPointSpriteMode( 1 ); // Geometry mode
    myOutsideDeviceActor->GetProperty()->SetColor( myOutsideCursorSettings->GetColor() );
  }
  else
  {
    myOutsideDeviceActor->GetPointSpriteMapper()->SetPointSpriteMode( 2 ); // Outside cursor mode
    myOutsideDeviceActor->GetPointSpriteMapper()->SetColorModeToMapScalars();
    myOutsideDeviceActor->GetPointSpriteMapper()->ScalarVisibilityOn();
  }

  aPipeline->Update();

  Highlight(isHighlighted());

  Update();
}


//----------------------------------------------------------------
void 
VISU_GaussPtsAct1
::ProcessEvents(vtkObject* theObject, 
                unsigned long theEvent,
                void* theClientData, 
                void* theCallData)
{
  if(vtkObject* anObject = reinterpret_cast<vtkObject*>(theClientData))
    if(VISU_GaussPtsAct1* self = dynamic_cast<VISU_GaussPtsAct1*>(anObject))
      if(theEvent == VISU::UpdateOutsideSettingsEvent)
        self->UpdateOutsideCursorSettings();

  Superclass::ProcessEvents(theObject,theEvent,theClientData,theCallData);
}

void
VISU_GaussPtsAct1
::OnInteractorEvent(unsigned long theEvent)
{
  switch(theEvent){
  case vtkCommand::StartInteractionEvent: 
    myDeviceActor->SetVisibility(GetVisibility());
    myInsideDeviceActor->SetVisibility(false);
    myOutsideDeviceActor->SetVisibility(false);
    break;
  case vtkCommand::EnableEvent: 
  case vtkCommand::DisableEvent:
  case vtkCommand::EndInteractionEvent: {
    bool aVisisbility = GetVisibility();
    bool anIsSegementation = IsSegmentationEnabled();
    myDeviceActor->SetVisibility(aVisisbility/* && !anIsSegementation*/);
    myInsideDeviceActor->SetVisibility(aVisisbility && anIsSegementation);
    myOutsideDeviceActor->SetVisibility(aVisisbility && anIsSegementation);
    break;
  }
  default:
    break;
  }
  
  Superclass::OnInteractorEvent(theEvent);
}

//----------------------------------------------------------------------------
unsigned long int
VISU_GaussPtsAct1
::GetMemorySize()
{
  unsigned long int aSize = Superclass::GetMemorySize();
  if(IsSegmentationEnabled()){
    aSize += myInsideDeviceActor->GetMemorySize();
    aSize += myOutsideDeviceActor->GetMemorySize();
  }
  return aSize;
}



//==============================================================================
vtkStandardNewMacro(VISU_GaussPtsAct2);


//----------------------------------------------------------------------------
VISU_GaussPtsAct2
::VISU_GaussPtsAct2()
{
  if(MYDEBUG2) MESSAGE("VISU_GaussPtsAct2::VISU_GaussPtsAct2 - this = "<<this);
  myDeviceActor->SetVisibility(false);
}

VISU_GaussPtsAct2
::~VISU_GaussPtsAct2()
{
  if(MYDEBUG2) MESSAGE("VISU_GaussPtsAct2::~VISU_GaussPtsAct2 - this = "<<this);
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct2
::ShallowCopyPL(VISU_PipeLine* thePipeLine)
{
  if(MYDEBUG2) MESSAGE("VISU_GaussPtsAct2::ShallowCopyPL - this = "<<this);

  Superclass::ShallowCopyPL(thePipeLine);
}

//----------------------------------------------------------------------------
void
VISU_GaussPtsAct2::SetPosition(double _arg[3]){
  if(MYDEBUG1) MESSAGE("VISU_GaussPtsAct2::SetPosition - this = "<<this);       
  Superclass::SetPosition(_arg);
}

//----------------------------------------------------------------------------
void
VISU_GaussPtsAct2
::SetMapperInput(vtkDataSet* theDataSet) 
{
  if(MYDEBUG2) MESSAGE("VISU_GaussPtsAct2::SetMapperInput - this = "<<this);
  Superclass::SetMapperInput(theDataSet);
//   if(VISU_GaussPointsPL* aPipeLine = CreatePipeLine(GetGaussPointsPL())){
//     myDeviceActor->SetPipeLine(aPipeLine);
//     myCurrentPL = aPipeLine;
//     aPipeLine->Delete();

//     myMapper->SetInput(myCurrentPL->GetPickableDataSet());
//     SetMapper(myMapper.GetPointer());
//   }
}

//----------------------------------------------------------------------------
void
VISU_GaussPtsAct2
::SetWidgetCtrl(VISU_WidgetCtrl* theWidgetCtrl)
{
  if(MYDEBUG2) MESSAGE("VISU_GaussPtsAct2::SetWidgetCtrl - this = "<<this);

  if(myWidgetCtrl == theWidgetCtrl)
    return;

  if(myWidgetCtrl){
    theWidgetCtrl->RemoveObserver(myEventCallbackCommand.GetPointer());
    myDeviceActor->GetPipeLine()->SetImplicitFunction(NULL);
    myDeviceActor->SetVisibility(false);
  }

  Superclass::SetWidgetCtrl(theWidgetCtrl);

  if(theWidgetCtrl){
    myDeviceActor->GetPipeLine()->SetImplicitFunction(theWidgetCtrl->ImplicitFunction());
    myDeviceActor->SetVisibility(GetVisibility() && IsSegmentationEnabled());
  }
}


//----------------------------------------------------------------------------
unsigned long int
VISU_GaussPtsAct2
::GetMemorySize()
{
  if(IsSegmentationEnabled())
    return Superclass::GetMemorySize();
  return 0;
}


//----------------------------------------------------------------------------
void
VISU_GaussPtsAct2
::SetVisibility(int theMode)
{
  if(MYDEBUG2) MESSAGE("VISU_GaussPtsAct2::SetVisibility - this = "<<this);

  Superclass::SetVisibility(theMode);

  myDeviceActor->SetVisibility(GetVisibility() && IsSegmentationEnabled());
}

void
VISU_GaussPtsAct2
::OnInteractorEvent(unsigned long theEvent)
{
  switch(theEvent){
  case vtkCommand::EnableEvent: 
  case vtkCommand::DisableEvent:
    myDeviceActor->SetVisibility(GetVisibility() && IsSegmentationEnabled());
    if(theEvent == vtkCommand::EnableEvent)
      Update();
    break;
  case vtkCommand::EndInteractionEvent:
    Update();
    break;
  default:
    break;
  }
  Superclass::OnInteractorEvent(theEvent);
}
