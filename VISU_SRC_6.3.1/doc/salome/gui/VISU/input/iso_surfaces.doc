/*!

\page iso_surfaces_page Iso Surfaces presentation

\image html iso.png "Example of Iso Surfaces presentation"

\n <b>Iso Surfaces</b> presentation combines all equal scalar values
applied to the cells and on the basis of them constructs  isobaric
surfaces, which form this presentation.

<em>To create an Iso Surfaces presentation:</em>
\par
&ndash; Right-click on one of the time stamps of the field in the
Object browser and from the pop-up menu choose <b>Iso Surfaces</b>, or
\n &ndash; Click on one of the time stamps of the field in the Object
browser and select from the main menu <b>Visualization > Iso
Surfaces</b>, or click <em>"Iso surfaces"</em> icon in the
<b>Visualization Toolbar</b>

\image html isosurf.jpg
<center><em>"Iso surfaces" icon</em></center>

\image html isosurfaces.png

\par
<ul>
<li><b>Iso Surface</b> tab allows to set additional parameters of the
<b>Iso Surfaces</b> presentation:</li>
<ul>
<li><b>Minimum / Maximum value</b> fields allow to enter the range of
scalar or vector values applied to the cells, on the basis of which
this presentation will be created (note that these fields are editable
only if <b>Use custom range</b> button is checked, otherwise range of
the scalar bar values is used).</li>
<li><b>Update scalar bar range with these values</b> button allows you
to update the range of the values, displayed with by the scalar bar,
with the previously defined range of values.</li>
<li><b>Number of surfaces</b>, which will be generated in the framework of this presentation.</li>
</ul>
<li><b>Scalar Bar</b> tab allows to define the parameters of the
scalar bar displayed with this presentation (\ref scalar_map_page "see also").</li>
</ul>

After you have finished with setting these parameters, click \b OK. Your
presentation with scalar bar will be immediately displayed in the
viewer.

<br><b>See Also</b> a sample TUI Script of  
\ref tui_iso_surf_page "Iso Surfaces presentation" creation.

*/
