/*!

\page cut_segment_page Cut Segment presentation
\image html cutsegmentprsnt.png "Example of Cut Segment presentation"

\n <b>Cut Segment</b> is a simplified variant of
\ref cut_lines_page "Cut Lines" presentation, displaying a single line
instead of a set of lines. The axis of this line is
defined by two points in a 3D space.

<em>To create a Cut Segment presentation:</em>
\par
&ndash; Right-click on one of the time stamps of the field in the
Object browser and from the pop-up menu choose <b>Cut Segment</b>, or
\n &ndash; Click on one of the time stamps of the field in the Object
browser and select from the main menu <b>Visualization > Cut
Segment</b>, or click <em>"Cut Segment"</em> icon in the <b>Visualization
Toolbar</b>.

\image html cutsegmenticon.jpg
<center><em>"Cut Segment" icon</em></center>

\image html cutsegment.png

\par
<b>Cut Segment:</b> this tab of the dialog box contains the
parameters of the cut segment.
<ul>
<li><b>Point 1</b> and <b>Point 2</b> spin boxes allow to set the coordinates
of two points, defining an axis of the segment.</li>
<li><b>Show preview</b> check box allows to edit the parameters of the
presentation and simultaneously observe the preview of this
presentation in the viewer.</li>
<li><b>Invert curve</b> check box allows to invert the resulting
curve.</li>
<li><b>Use absolute length</b> check box allows to see the real length
of the line, instead of [0,1] interval.</li>
<li><b>Generate Data Table:</b> If this check box is marked, Post-Pro
will automatically generate a data table on the basis of your Cut
Segment presentation. This table will be created in the structure of the
study.</li>
<li><b>Generate Curve:</b> If this check box is marked, Post Pro
will automatically generate curve line on the basis of values taken
from the generated data table. This curve will be created in the
structure of the study and can be visualized in a XY plot.</li>
</ul>
See more about table presentations and curve lines 
\ref table_presentations_page "here".

\par
<b>Scalar Bar</b> tab allows to define the parameters of the scalar bar
displayed with this presentation (\ref scalar_map_page "see also").

After you have finished with setting these parameters, click \b
OK. Your presentation with scalar bar will be immediately displayed in
the viewer:

<b>Tip:</b> From <b>Cut Segment</b> presentation you can create a
<b>data table</b>. This table will consist of the field scalar values
located on the cut line of the constructed presentation. After that
your data table can be used for construction of a 2d plot
based on the scalar values from the table (see also:
\ref creating_curves_page "Creating curves" and 
\ref creating_plot2d_page "Creating Plot 2D presentation").

<br><b>See Also</b> a sample TUI Script of  
\ref tui_cut_segment_page "Cut Segment" presentation creation.

*/
