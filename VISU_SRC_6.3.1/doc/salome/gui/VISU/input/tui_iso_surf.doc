/*!

\page tui_iso_surf_page Iso Surfaces

Create <b>Iso Surfaces</b> presentation on a field of the imported MED file:

\code
import os

import salome

import VISU
import visu_gui

# The directory containing MED files
datadir = os.getenv("DATA_DIR")

# Get VISU engine
myVisu = visu_gui.myVisu

# Import a MED file
medFile = os.path.join(datadir,"MedFiles","fra.med")
myResult = myVisu.ImportFile(medFile)

# Create iso surfaces for the first timestamp of 'VITESSE' field
meshName = 'LE VOLUME'
fieldEntity = VISU.NODE
fieldName = 'VITESSE'
myIsoSurfaces = myVisu.IsoSurfacesOnField(myResult, meshName, fieldEntity, fieldName, 1)

# Set the number of surfaces
myIsoSurfaces.SetNbSurfaces(12)

# Show two value labels per surface
myIsoSurfaces.ShowLabels(True, 2)

# Update the object browser
salome.sg.updateObjBrowser(1)

# Display the newly created iso surfaces
myViewManager = myVisu.GetViewManager()
myView = myViewManager.Create3DView()

myView.DisplayOnly(myIsoSurfaces)
myView.FitAll()
\endcode

<br>Please, see \ref VISU.IsoSurfaces "IsoSurfaces interface reference documentation" 
for more details.

*/
