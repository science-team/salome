/*!

\page vectors_page Vectors Presentation

\image html vectors.png "Example of Vectors presentation"

\n <b>Vector Field</b> presentation visualizes vectors of the
corresponding cells of the mesh in graphical mode.

<em>To create a Vector  presentation:</em>
\par
&ndash; Right-click on one of the time stamps of the field in the
Object browser and from the pop-up menu choose \b Vectors, or
\n &ndash; Click on one of the time stamps of the field in the Object
browser and select from the main menu <b>Visualization > Vectors</b>,
or click <em>"Vectors"</em> icon in the <b>Visualization Toolbar</b>.

\image html vecticon.jpg
<center><em>"Vectors" icon</em></center>

\image html vectorfield.png

\par
<ul>
<li>\b Vectors tab allows you to set additional parameters of your \b
Vectors presentation:</li>
<ul>
<li><b>Scale Factor:</b> visualization scaling of all vector values applied to the cells of the mesh.</li>
<li><b>Line width:</b> width of the lines representing vectors in your presentation.</li>
<li><b>Magnitude coloring</b> check box: this option allows to color your
presentation according the \ref scalar_map_page "scalar range" defined
in Scalar Bar properties or to display it using only one color (this
color can be selected if you click <b>Select Color</b> button).</li>
<li><b>Use glyphs</b> check box: this option allows you to visualize
vectors with pointers and select their \b type and \b position on the
line of the vectors. You can choose <b>Glyph Type</b> (\b Arrows, big
or small \b Cones) and <b>Glyph Position</b> (at the \b Tail, at the
\b Head or in the \b Center).</li>
</ul>
<li><b>Scalar Bar</b> tab allows to define the parameters of the
scalar bar displayed with this presentation (\ref scalar_map_page "see also").</li>
</ul>

After you have finished with setting these parameters, click \b
OK. Your presentation with scalar bar will be immediately displayed in
the viewer.

<br><b>See Also</b> a sample TUI Script of  
\ref tui_vectors_page "Vectors creation" operation.

*/
