/*!

\page tui_scalar_map_page Scalar Map

Create <b>Scalar Map</b> presentation on a field of the imported MED file:

\code
import os

import salome

import VISU
import visu_gui

# The directory containing MED files
datadir = os.getenv("DATA_DIR")

# Get VISU engine
myVisu = visu_gui.myVisu

# Import a MED file
medFile = os.path.join(datadir,"MedFiles","pointe.med")
myResult = myVisu.ImportFile(medFile)

# Create a scalar map for the first timestamp of 'fieldcelldoublevector' field
meshName = 'maa1'
fieldEntity = VISU.CELL
fieldName = 'fieldcelldoublevector'
myScalarMap = myVisu.ScalarMapOnField(myResult, meshName, fieldEntity, fieldName, 1)

# Set the logarithmic scaling
myScalarMap.SetScaling(VISU.LOGARITHMIC)

# Set scalar range to [2, 5]
myScalarMap.SetRange(2, 5)

# Update the object browser
salome.sg.updateObjBrowser(1)

# Display the newly created scalar map
myViewManager = myVisu.GetViewManager()
myView = myViewManager.Create3DView()

myView.DisplayOnly(myScalarMap)
myView.FitAll()
\endcode

<br>Please, see \ref VISU.ScalarMap "ScalarMap interface reference documentation" 
for more details.

*/
