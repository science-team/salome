/*!

\page taper_page Taper

\n \b Taper mesh quality criterion represents the ratio of the areas
of two triangles separated by a diagonal. So it can be calculated only
for elements consisting of 4 nodes.

\image html image25.png

<br><em>To apply the Taper quality criterion to your mesh:</em>

<ol>
<li>Display your mesh in the viewer.</li>

<li>Choose <b>Controls > Face Controls > Taper</b> or click
<em>"Taper"</em> button in the toolbar.

\image html image36.png
<center><em>"Taper" button</em></center>

Your mesh will be displayed in the viewer with its elements colored
according to the applied mesh quality control criterion:

\image html image90.jpg
</li>
</ol>

<br><b>See Also</b> a sample TUI Script of a 
\ref tui_taper "Taper quality control" operation.  

*/