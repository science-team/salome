/*!

\page additional_hypo_page Additional Hypotheses

\n <b>Additional Hypotheses</b> can be applied as a supplement to the
main hypotheses, introducing additional concepts to mesh creation.

To define an <b>Additional Hypothesis</b> simply select it in
<b>Create Mesh</b> menu. These hypotheses are actually changes in the
rules of mesh creation and as such don't possess adjustable values.

<h2>Non Conform mesh allowed hypothesis</h2>

<b>Non Conform mesh allowed</b> hypothesis allows to generate non-conform
meshes (that is, meshes having some edges ending on an edge or face of
adjacent elements).

<h2>Quadratic Mesh</h2>

Quadratic Mesh hypothesis allows to build a quadratic mesh (whose
edges are not straight but broken lines and can be defined by three
points: first, middle and last) instead of an ordinary one.

<h2>Propagation of 1D Hypothesis on opposite edges</h2>

<b>Propagation of 1D Hypothesis on opposite edges</b> allows to propagate a
hypothesis onto an opposite edge. If a local hypothesis and
propagation are defined on an edge of a quadrangular face, the
opposite edge will have the same hypothesis, unless another hypothesis
has been locally defined on the opposite edge.
 
<br><b>See Also</b> a sample TUI Script of a 
\ref tui_propagation "Propagation hypothesis" operation

<h2>Quadrangle Preference</h2>

This additional hypothesis can be used together with Quadrangle (Mapping) and Netgen 2D
algorithms. 

It allows Netgen 2D to build quadrangular meshes at any conditions.

It allows Quadrangle (Mapping) to build quadrangular meshes even if the number
of nodes at the opposite edges of a meshed face is not equal,
otherwise this mesh will contain some triangular elements. <i>This use
case is obsolete now. Use <b>Quadrangle Parameters</b> hypothesis with
type <b>Quadrangle Preference</b> set instead.</i>
<br>
This hypothesis has one restriction on its work: the total quantity of
segments on all four sides of the face must be even (divisible by 2).

<h2>Triangle Preference <i>(obsolete)</i></h2>

This additional hypothesis can be used only together with Quadrangle (Mapping)
algorithm. It allows to build triangular mesh faces in the refinement
area if the number of nodes at the opposite edges of a meshed face is not equal,
otherwise refinement area will contain some quadrangular elements.
<i>This hypothesis is obsolete now. Use <b>Quadrangle Parameters</b>
hypothesis with type <b>Triangle Preference</b> set instead.</i>

\anchor viscous_layers_anchor
<h2>Viscous Layers</h2>

<b>Viscous Layers</b> additional hypothesis can be used together with
several 3D algorithms: NETGEN 3D, GHS3D and Hexahedron(i,j,k). This
hypothesis allows creation of layers of highly stretched prisms near
mesh boundary, which is beneficial for high quality viscous
computations. The prisms constructed on the quadrangular mesh faces are
actually the hexahedrons.


\image html viscous_layers_hyp.png

<ul>
<li><b>Name</b> - allows to define the name of the hypothesis.</li>
<li><b>Total thicknes</b> - gives the total thickness of prism layers.</li>
<li><b>Number of layers</b> - defines the number of prism layers.</li>
<li><b>Stretch factor</b> - defines the growth factor of prism height
from the mesh boundary inwards.</li>
<li><b>Faces without layers</b> - defines geometrical faces on which
prism layers should not be constructed. By default the prism layers
are not constructed on geometrical faces shared by solids.</li>
</ul>

\image html viscous_layers_mesh.png A group containing viscous layer prisms.

<br><b>See also</b> a sample TUI script of a \ref tui_viscous_layers
"Viscous layers construction".


*/
