// Copyright (C) 2007-2011  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SMESH SMESH_I : idl implementation based on 'SMESH' unit's calsses
// File      : SMESH_2smeshpy.cxx
// Created   : Fri Nov 18 13:20:10 2005
// Author    : Edward AGAPOV (eap)
//
#include "SMESH_2smeshpy.hxx"

#include "utilities.h"
#include "SMESH_PythonDump.hxx"
#include "SMESH_NoteBook.hxx"
#include "Resource_DataMapOfAsciiStringAsciiString.hxx"

#include "SMESH_Gen_i.hxx"
/* SALOME headers that include CORBA headers that include windows.h 
 * that defines GetObject symbol as GetObjectA should stand before SALOME headers
 * that declare methods named GetObject - to apply the same rules of GetObject renaming
 * and thus to avoid mess with GetObject symbol on Windows */

IMPLEMENT_STANDARD_HANDLE (_pyObject          ,Standard_Transient);
IMPLEMENT_STANDARD_HANDLE (_pyCommand         ,Standard_Transient);
IMPLEMENT_STANDARD_HANDLE (_pyGen             ,_pyObject);
IMPLEMENT_STANDARD_HANDLE (_pyMesh            ,_pyObject);
IMPLEMENT_STANDARD_HANDLE (_pySubMesh         ,_pyObject);
IMPLEMENT_STANDARD_HANDLE (_pyMeshEditor      ,_pyObject);
IMPLEMENT_STANDARD_HANDLE (_pyHypothesis      ,_pyObject);
IMPLEMENT_STANDARD_HANDLE (_pySelfEraser      ,_pyObject);
IMPLEMENT_STANDARD_HANDLE (_pyAlgorithm       ,_pyHypothesis);
IMPLEMENT_STANDARD_HANDLE (_pyComplexParamHypo,_pyHypothesis);
IMPLEMENT_STANDARD_HANDLE (_pyNumberOfSegmentsHyp,_pyHypothesis);

IMPLEMENT_STANDARD_RTTIEXT(_pyObject          ,Standard_Transient);
IMPLEMENT_STANDARD_RTTIEXT(_pyCommand         ,Standard_Transient);
IMPLEMENT_STANDARD_RTTIEXT(_pyGen             ,_pyObject);
IMPLEMENT_STANDARD_RTTIEXT(_pyMesh            ,_pyObject);
IMPLEMENT_STANDARD_RTTIEXT(_pySubMesh         ,_pyObject);
IMPLEMENT_STANDARD_RTTIEXT(_pyMeshEditor      ,_pyObject);
IMPLEMENT_STANDARD_RTTIEXT(_pyHypothesis      ,_pyObject);
IMPLEMENT_STANDARD_RTTIEXT(_pySelfEraser      ,_pyObject);
IMPLEMENT_STANDARD_RTTIEXT(_pyAlgorithm       ,_pyHypothesis);
IMPLEMENT_STANDARD_RTTIEXT(_pyComplexParamHypo,_pyHypothesis);
IMPLEMENT_STANDARD_RTTIEXT(_pyNumberOfSegmentsHyp,_pyHypothesis);
IMPLEMENT_STANDARD_RTTIEXT(_pyLayerDistributionHypo,_pyHypothesis);
IMPLEMENT_STANDARD_RTTIEXT(_pySegmentLengthAroundVertexHyp,_pyHypothesis);

using namespace std;
using SMESH::TPythonDump;

/*!
 * \brief Container of commands into which the initial script is split.
 *        It also contains data coresponding to SMESH_Gen contents
 */
static Handle(_pyGen) theGen;

static TCollection_AsciiString theEmptyString;

//#define DUMP_CONVERSION

#if !defined(_DEBUG_) && defined(DUMP_CONVERSION)
#undef DUMP_CONVERSION
#endif


namespace {

  //================================================================================
  /*!
   * \brief Set of TCollection_AsciiString initialized by C array of C strings
   */
  //================================================================================

  struct TStringSet: public set<TCollection_AsciiString>
  {
    /*!
     * \brief Filling. The last string must be ""
     */
    void Insert(const char* names[]) {
      for ( int i = 0; names[i][0] ; ++i )
        insert( (char*) names[i] );
    }
    /*!
     * \brief Check if a string is in
     */
    bool Contains(const TCollection_AsciiString& name ) {
      return find( name ) != end();
    }
  };
}

//================================================================================
/*!
 * \brief Convert python script using commands of smesh.py
  * \param theScript - Input script
  * \retval TCollection_AsciiString - Convertion result
  *
  * Class SMESH_2smeshpy declared in SMESH_PythonDump.hxx
 */
//================================================================================

TCollection_AsciiString
SMESH_2smeshpy::ConvertScript(const TCollection_AsciiString& theScript,
                              Resource_DataMapOfAsciiStringAsciiString& theEntry2AccessorMethod,
                              Resource_DataMapOfAsciiStringAsciiString& theObjectNames)
{
  theGen = new _pyGen( theEntry2AccessorMethod, theObjectNames );

  // split theScript into separate commands

  SMESH_NoteBook * aNoteBook = new SMESH_NoteBook();
  
  int from = 1, end = theScript.Length(), to;
  while ( from < end && ( to = theScript.Location( "\n", from, end )))
  {
    if ( to != from )
        // cut out and store a command
        aNoteBook->AddCommand( theScript.SubString( from, to - 1 ));
      from = to + 1;
  }
  
  aNoteBook->ReplaceVariables();

  TCollection_AsciiString aNoteScript = aNoteBook->GetResultScript();
  delete aNoteBook;
  aNoteBook = 0;
  
  // split theScript into separate commands
  from = 1, end = aNoteScript.Length();
  while ( from < end && ( to = aNoteScript.Location( "\n", from, end )))
  {
    if ( to != from )
      // cut out and store a command
      theGen->AddCommand( aNoteScript.SubString( from, to - 1 ));
    from = to + 1;
  }

  // finish conversion
  theGen->Flush();
#ifdef DUMP_CONVERSION
  MESSAGE_BEGIN ( std::endl << " ######## RESULT ######## " << std::endl<< std::endl );
#endif

  // reorder commands after conversion
  list< Handle(_pyCommand) >::iterator cmd;
  bool orderChanges;
  do {
    orderChanges = false;
    for ( cmd = theGen->GetCommands().begin(); cmd != theGen->GetCommands().end(); ++cmd )
      if ( (*cmd)->SetDependentCmdsAfter() )
        orderChanges = true;
  } while ( orderChanges );
  
  // concat commands back into a script
  TCollection_AsciiString aScript;
  for ( cmd = theGen->GetCommands().begin(); cmd != theGen->GetCommands().end(); ++cmd )
  {
#ifdef DUMP_CONVERSION
    MESSAGE_ADD ( "## COM " << (*cmd)->GetOrderNb() << ": "<< (*cmd)->GetString() << std::endl );
#endif
    if ( !(*cmd)->IsEmpty() ) {
      aScript += "\n";
      aScript += (*cmd)->GetString();
    }
  }
  aScript += "\n";

  theGen.Nullify();

  return aScript;
}

//================================================================================
/*!
 * \brief _pyGen constructor
 */
//================================================================================

_pyGen::_pyGen(Resource_DataMapOfAsciiStringAsciiString& theEntry2AccessorMethod,
               Resource_DataMapOfAsciiStringAsciiString& theObjectNames)
  : _pyObject( new _pyCommand( TPythonDump::SMESHGenName(), 0 )),
    myID2AccessorMethod( theEntry2AccessorMethod ),
    myObjectNames( theObjectNames )
{
  myNbCommands = 0;
  // make that GetID() to return TPythonDump::SMESHGenName()
  GetCreationCmd()->GetString() += "=";
}

//================================================================================
/*!
 * \brief name of SMESH_Gen in smesh.py
 */
//================================================================================

const char* _pyGen::AccessorMethod() const
{
  return SMESH_2smeshpy::GenName();
}

//================================================================================
/*!
 * \brief Convert a command using a specific converter
  * \param theCommand - the command to convert
 */
//================================================================================

Handle(_pyCommand) _pyGen::AddCommand( const TCollection_AsciiString& theCommand)
{
  // store theCommand in the sequence
  myCommands.push_back( new _pyCommand( theCommand, ++myNbCommands ));

  Handle(_pyCommand) aCommand = myCommands.back();
#ifdef DUMP_CONVERSION
  MESSAGE ( "## COM " << myNbCommands << ": "<< aCommand->GetString() );
#endif

  _pyID objID = aCommand->GetObject();

  if ( objID.IsEmpty() )
    return aCommand;

  // SMESH_Gen method?
  if ( objID == this->GetID() || objID == SMESH_2smeshpy::GenName()) {
    this->Process( aCommand );
    return aCommand;
  }

  // SMESH_Mesh method?
  map< _pyID, Handle(_pyMesh) >::iterator id_mesh = myMeshes.find( objID );
  if ( id_mesh != myMeshes.end() )
  {
    // check for mesh editor object
    if ( aCommand->GetMethod() == "GetMeshEditor" ) { // MeshEditor creation
      _pyID editorID = aCommand->GetResultValue();
      Handle(_pyMeshEditor) editor = new _pyMeshEditor( aCommand );
      myMeshEditors.insert( make_pair( editorID, editor ));
      return aCommand;
    } 
    // check for SubMesh objects
    else if ( aCommand->GetMethod() == "GetSubMesh" ) { // SubMesh creation
      _pyID subMeshID = aCommand->GetResultValue();
      Handle(_pySubMesh) subMesh = new _pySubMesh( aCommand );
      myObjects.insert( make_pair( subMeshID, subMesh ));
    }
    
    id_mesh->second->Process( aCommand );
    return aCommand;
  }

  // SMESH_MeshEditor method?
  map< _pyID, Handle(_pyMeshEditor) >::iterator id_editor = myMeshEditors.find( objID );
  if ( id_editor != myMeshEditors.end() )
  {
    id_editor->second->Process( aCommand );
    TCollection_AsciiString processedCommand = aCommand->GetString();
    // some commands of SMESH_MeshEditor create meshes
    if ( aCommand->GetMethod().Search("MakeMesh") != -1 ) {
      Handle(_pyMesh) mesh = new _pyMesh( aCommand, aCommand->GetResultValue() );
      aCommand->GetString() = processedCommand; // discard changes made by _pyMesh
      myMeshes.insert( make_pair( mesh->GetID(), mesh ));
    }
    if ( aCommand->GetMethod() == "MakeBoundaryMesh") {
      _pyID meshID = aCommand->GetResultValue(0);
      if ( !myMeshes.count( meshID ) )
      {
        Handle(_pyMesh) mesh = new _pyMesh( aCommand, meshID );
        aCommand->GetString() = processedCommand; // discard changes made by _pyMesh
        myMeshes.insert( make_pair( meshID, mesh ));
      }
    }
    return aCommand;
  }
  // SMESH_Hypothesis method?
  list< Handle(_pyHypothesis) >::iterator hyp = myHypos.begin();
  for ( ; hyp != myHypos.end(); ++hyp )
    if ( !(*hyp)->IsAlgo() && objID == (*hyp)->GetID() ) {
      (*hyp)->Process( aCommand );
      return aCommand;
    }

  // other object method?
  map< _pyID, Handle(_pyObject) >::iterator id_obj = myObjects.find( objID );
  if ( id_obj != myObjects.end() ) {
    id_obj->second->Process( aCommand );
    return aCommand;
  }
//   if ( theCommand.Search( "aFilterManager" ) != -1 ) {
//     if ( theCommand.Search( "CreateFilterManager" ) != -1 )
//       myFilterManager = new _pySelfEraser( aCommand );
//     else if ( !myFilterManager.IsNull() )
//       myFilterManager->Process( aCommand );
//     return aCommand;
//   }

  // Add access to a wrapped mesh
  AddMeshAccessorMethod( aCommand );

  // Add access to a wrapped algorithm
  //  AddAlgoAccessorMethod( aCommand ); // ??? what if algo won't be wrapped at all ???

  // PAL12227. PythonDump was not updated at proper time; result is
  //     aCriteria.append(SMESH.Filter.Criterion(17,26,0,'L1',26,25,1e-07,SMESH.EDGE,-1))
  // TypeError: __init__() takes exactly 11 arguments (10 given)
  char wrongCommand[] = "SMESH.Filter.Criterion(";
  if ( int beg = theCommand.Location( wrongCommand, 1, theCommand.Length() ))
  {
    _pyCommand tmpCmd( theCommand.SubString( beg, theCommand.Length() ), -1);
    // there must be 10 arguments, 5-th arg ThresholdID is missing,
    const int wrongNbArgs = 9, missingArg = 5;
    if ( tmpCmd.GetNbArgs() == wrongNbArgs )
    {
      for ( int i = wrongNbArgs; i > missingArg; --i )
        tmpCmd.SetArg( i + 1, tmpCmd.GetArg( i ));
      tmpCmd.SetArg(  missingArg, "''");
      aCommand->GetString().Trunc( beg - 1 );
      aCommand->GetString() += tmpCmd.GetString();
    }
  }
  return aCommand;
}

//================================================================================
/*!
 * \brief Convert the command or remember it for later conversion 
  * \param theCommand - The python command calling a method of SMESH_Gen
 */
//================================================================================

void _pyGen::Process( const Handle(_pyCommand)& theCommand )
{
  // there are methods to convert:
  // CreateMesh( shape )
  // Concatenate( [mesh1, ...], ... )
  // CreateHypothesis( theHypType, theLibName )
  // Compute( mesh, geom )
  // Evaluate( mesh, geom )
  // mesh creation
  TCollection_AsciiString method = theCommand->GetMethod();

  if ( method == "CreateMesh" || method == "CreateEmptyMesh")
  {
    Handle(_pyMesh) mesh = new _pyMesh( theCommand );
    myMeshes.insert( make_pair( mesh->GetID(), mesh ));
    return;
  }
  if ( method == "CreateMeshesFromUNV" || method == "CreateMeshesFromSTL" || method == "CopyMesh" )
  {
    Handle(_pyMesh) mesh = new _pyMesh( theCommand, theCommand->GetResultValue() );
    myMeshes.insert( make_pair( mesh->GetID(), mesh ));
    return;
  }
  if( method == "CreateMeshesFromMED")
  {
    for(int ind = 0;ind<theCommand->GetNbResultValues();ind++)
    {
      Handle(_pyMesh) mesh = new _pyMesh( theCommand, theCommand->GetResultValue(ind));
      myMeshes.insert( make_pair( theCommand->GetResultValue(ind), mesh ));     
    }
  }

  // CreateHypothesis()
  if ( method == "CreateHypothesis" )
  {
    // issue 199929, remove standard library name (default parameter)
    const TCollection_AsciiString & aLibName = theCommand->GetArg( 2 );
    if ( aLibName.Search( "StdMeshersEngine" ) != -1 ) {
      // keep first argument
      TCollection_AsciiString arg = theCommand->GetArg( 1 );
      theCommand->RemoveArgs();
      theCommand->SetArg( 1, arg );
    }

    myHypos.push_back( _pyHypothesis::NewHypothesis( theCommand ));
    return;
  }

  // smeshgen.Compute( mesh, geom ) --> mesh.Compute()
  if ( method == "Compute" )
  {
    const _pyID& meshID = theCommand->GetArg( 1 );
    map< _pyID, Handle(_pyMesh) >::iterator id_mesh = myMeshes.find( meshID );
    if ( id_mesh != myMeshes.end() ) {
      theCommand->SetObject( meshID );
      theCommand->RemoveArgs();
      id_mesh->second->Flush();
      return;
    }
  }

  // smeshgen.Evaluate( mesh, geom ) --> mesh.Evaluate(geom)
  if ( method == "Evaluate" )
  {
    const _pyID& meshID = theCommand->GetArg( 1 );
    map< _pyID, Handle(_pyMesh) >::iterator id_mesh = myMeshes.find( meshID );
    if ( id_mesh != myMeshes.end() ) {
      theCommand->SetObject( meshID );
      _pyID geom = theCommand->GetArg( 2 );
      theCommand->RemoveArgs();
      theCommand->SetArg( 1, geom );
      return;
    }
  }

  // objects erasing creation command if no more it's commands invoked:
  // SMESH_Pattern, FilterManager
  if ( method == "GetPattern" || method == "CreateFilterManager" ) {
    Handle(_pyObject) obj = new _pySelfEraser( theCommand );
    if ( !myObjects.insert( make_pair( obj->GetID(), obj )).second )
      theCommand->Clear(); // already created
  }

  // Concatenate( [mesh1, ...], ... )
  if ( method == "Concatenate" || method == "ConcatenateWithGroups")
  {
    if ( method == "ConcatenateWithGroups" ) {
      theCommand->SetMethod( "Concatenate" );
      theCommand->SetArg( theCommand->GetNbArgs() + 1, "True" );
    }
    Handle(_pyMesh) mesh = new _pyMesh( theCommand, theCommand->GetResultValue() );
    myMeshes.insert( make_pair( mesh->GetID(), mesh ));
    AddMeshAccessorMethod( theCommand );
  }

  // Replace name of SMESH_Gen

  // names of SMESH_Gen methods fully equal to methods defined in smesh.py
  static TStringSet smeshpyMethods;
  if ( smeshpyMethods.empty() ) {
    const char * names[] =
      { "SetEmbeddedMode","IsEmbeddedMode","SetCurrentStudy","GetCurrentStudy",
        "GetPattern","GetSubShapesId",
        "" }; // <- mark of array end
    smeshpyMethods.Insert( names );
  }
  if ( smeshpyMethods.Contains( theCommand->GetMethod() ))
    // smeshgen.Method() --> smesh.Method()
    theCommand->SetObject( SMESH_2smeshpy::SmeshpyName() );
  else
    // smeshgen.Method() --> smesh.smesh.Method()
    theCommand->SetObject( SMESH_2smeshpy::GenName() );
}

//================================================================================
/*!
 * \brief Convert the remembered commands
 */
//================================================================================

void _pyGen::Flush()
{
  // create empty command
  myLastCommand = new _pyCommand();

  map< _pyID, Handle(_pyMesh) >::iterator id_mesh = myMeshes.begin();
  for ( ; id_mesh != myMeshes.end(); ++id_mesh )
    if ( ! id_mesh->second.IsNull() )
      id_mesh->second->Flush();

  list< Handle(_pyHypothesis) >::iterator hyp = myHypos.begin();
  for ( ; hyp != myHypos.end(); ++hyp )
    if ( !hyp->IsNull() ) {
      (*hyp)->Flush();
      // smeshgen.CreateHypothesis() --> smesh.smesh.CreateHypothesis()
      if ( !(*hyp)->IsWrapped() )
        (*hyp)->GetCreationCmd()->SetObject( SMESH_2smeshpy::GenName() );
    }

  map< _pyID, Handle(_pyObject) >::iterator id_obj = myObjects.begin();
  for ( ; id_obj != myObjects.end(); ++id_obj )
    if ( ! id_obj->second.IsNull() )
      id_obj->second->Flush();

  myLastCommand->SetOrderNb( ++myNbCommands );
  myCommands.push_back( myLastCommand );
}

//================================================================================
/*!
 * \brief Add access method to mesh that is an argument
  * \param theCmd - command to add access method
  * \retval bool - true if added
 */
//================================================================================

bool _pyGen::AddMeshAccessorMethod( Handle(_pyCommand) theCmd ) const
{
  bool added = false;
  map< _pyID, Handle(_pyMesh) >::const_iterator id_mesh = myMeshes.begin();
  for ( ; id_mesh != myMeshes.end(); ++id_mesh ) {
    if ( theCmd->AddAccessorMethod( id_mesh->first, id_mesh->second->AccessorMethod() ))
      added = true;
  }
  return added;
}

//================================================================================
/*!
 * \brief Add access method to algo that is an object or an argument
  * \param theCmd - command to add access method
  * \retval bool - true if added
 */
//================================================================================

bool _pyGen::AddAlgoAccessorMethod( Handle(_pyCommand) theCmd ) const
{
  bool added = false;
  list< Handle(_pyHypothesis) >::const_iterator hyp = myHypos.begin();
  for ( ; hyp != myHypos.end(); ++hyp ) {
    if ( (*hyp)->IsAlgo() && /*(*hyp)->IsWrapped() &&*/
         theCmd->AddAccessorMethod( (*hyp)->GetID(), (*hyp)->AccessorMethod() ))
      added = true;
  }
  return added;
}

//================================================================================
/*!
 * \brief Find hypothesis by ID (entry)
  * \param theHypID - The hypothesis ID
  * \retval Handle(_pyHypothesis) - The found hypothesis
 */
//================================================================================

Handle(_pyHypothesis) _pyGen::FindHyp( const _pyID& theHypID )
{
  list< Handle(_pyHypothesis) >::iterator hyp = myHypos.begin();
  for ( ; hyp != myHypos.end(); ++hyp )
    if ( !hyp->IsNull() && theHypID == (*hyp)->GetID() )
      return *hyp;
  return Handle(_pyHypothesis)();
}

//================================================================================
/*!
 * \brief Find algorithm the created algorithm
  * \param theGeom - The shape ID the algorithm was created on
  * \param theMesh - The mesh ID that created the algorithm
  * \param dim - The algo dimension
  * \retval Handle(_pyHypothesis) - The found algo
 */
//================================================================================

Handle(_pyHypothesis) _pyGen::FindAlgo( const _pyID& theGeom, const _pyID& theMesh,
                                        const Handle(_pyHypothesis)& theHypothesis )
{
  list< Handle(_pyHypothesis) >::iterator hyp = myHypos.begin();
  for ( ; hyp != myHypos.end(); ++hyp )
    if ( !hyp->IsNull() &&
         (*hyp)->IsAlgo() &&
         theHypothesis->CanBeCreatedBy( (*hyp)->GetAlgoType() ) &&
         (*hyp)->GetGeom() == theGeom &&
         (*hyp)->GetMesh() == theMesh )
      return *hyp;
  return 0;
}

//================================================================================
/*!
 * \brief Find subMesh by ID (entry)
  * \param theSubMeshID - The subMesh ID
  * \retval Handle(_pySubMesh) - The found subMesh
 */
//================================================================================

Handle(_pySubMesh) _pyGen::FindSubMesh( const _pyID& theSubMeshID )
{
  map< _pyID, Handle(_pyObject) >::iterator id_subMesh = myObjects.find(theSubMeshID);
  if ( id_subMesh != myObjects.end() )
    return Handle(_pySubMesh)::DownCast( id_subMesh->second );
  return Handle(_pySubMesh)();
}


//================================================================================
/*!
 * \brief Change order of commands in the script
  * \param theCmd1 - One command
  * \param theCmd2 - Another command
 */
//================================================================================

void _pyGen::ExchangeCommands( Handle(_pyCommand) theCmd1, Handle(_pyCommand) theCmd2 )
{
  list< Handle(_pyCommand) >::iterator pos1, pos2;
  pos1 = find( myCommands.begin(), myCommands.end(), theCmd1 );
  pos2 = find( myCommands.begin(), myCommands.end(), theCmd2 );
  myCommands.insert( pos1, theCmd2 );
  myCommands.insert( pos2, theCmd1 );
  myCommands.erase( pos1 );
  myCommands.erase( pos2 );

  int nb1 = theCmd1->GetOrderNb();
  theCmd1->SetOrderNb( theCmd2->GetOrderNb() );
  theCmd2->SetOrderNb( nb1 );
//   cout << "BECOME " << theCmd1->GetOrderNb() << "\t" << theCmd1->GetString() << endl
//        << "BECOME " << theCmd2->GetOrderNb() << "\t" << theCmd2->GetString() << endl << endl;
}

//================================================================================
/*!
 * \brief Set one command after the other
  * \param theCmd - Command to move
  * \param theAfterCmd - Command ater which to insert the first one
 */
//================================================================================

void _pyGen::SetCommandAfter( Handle(_pyCommand) theCmd, Handle(_pyCommand) theAfterCmd )
{
  setNeighbourCommand( theCmd, theAfterCmd, true );
}

//================================================================================
/*!
 * \brief Set one command before the other
  * \param theCmd - Command to move
  * \param theBeforeCmd - Command before which to insert the first one
 */
//================================================================================

void _pyGen::SetCommandBefore( Handle(_pyCommand) theCmd, Handle(_pyCommand) theBeforeCmd )
{
  setNeighbourCommand( theCmd, theBeforeCmd, false );
}

//================================================================================
/*!
 * \brief Set one command before or after the other
  * \param theCmd - Command to move
  * \param theOtherCmd - Command ater or before which to insert the first one
 */
//================================================================================

void _pyGen::setNeighbourCommand( Handle(_pyCommand)& theCmd,
                                  Handle(_pyCommand)& theOtherCmd,
                                  const bool theIsAfter )
{
  list< Handle(_pyCommand) >::iterator pos;
  pos = find( myCommands.begin(), myCommands.end(), theCmd );
  myCommands.erase( pos );
  pos = find( myCommands.begin(), myCommands.end(), theOtherCmd );
  myCommands.insert( (theIsAfter ? ++pos : pos), theCmd );

  int i = 1;
  for ( pos = myCommands.begin(); pos != myCommands.end(); ++pos)
    (*pos)->SetOrderNb( i++ );
}

//================================================================================
/*!
 * \brief Set command be last in list of commands
  * \param theCmd - Command to be last
 */
//================================================================================

Handle(_pyCommand)& _pyGen::GetLastCommand()
{
  return myLastCommand;
}

//================================================================================
/*!
 * \brief Set method to access to object wrapped with python class
  * \param theID - The wrapped object entry
  * \param theMethod - The accessor method
 */
//================================================================================

void _pyGen::SetAccessorMethod(const _pyID& theID, const char* theMethod )
{
  myID2AccessorMethod.Bind( theID, (char*) theMethod );
}

//================================================================================
/*!
 * \brief Generated new ID for object and assign with existing name
  * \param theID - ID of existing object
 */
//================================================================================

_pyID _pyGen::GenerateNewID( const _pyID& theID )
{
  int index = 1;
  _pyID aNewID;
  do {
    aNewID = theID + _pyID( ":" ) + _pyID( index++ );
  }
  while ( myObjectNames.IsBound( aNewID ) );
    
  myObjectNames.Bind( aNewID, myObjectNames.IsBound( theID ) 
                      ? (myObjectNames.Find( theID ) + _pyID( "_" ) + _pyID( index-1 ))
                      : _pyID( "A" ) + aNewID );
  return aNewID;
}

//================================================================================
/*!
 * \brief Find out type of geom group
  * \param grpID - The geom group entry
  * \retval int - The type
 */
//================================================================================

// static bool sameGroupType( const _pyID&                   grpID,
//                            const TCollection_AsciiString& theType)
// {
//   // define group type as smesh.Mesh.Group() does
//   int type = -1;
//   SALOMEDS::Study_var study = SMESH_Gen_i::GetSMESHGen()->GetCurrentStudy();
//   SALOMEDS::SObject_var aSObj = study->FindObjectID( grpID.ToCString() );
//   if ( !aSObj->_is_nil() ) {
//     GEOM::GEOM_Object_var aGeomObj = GEOM::GEOM_Object::_narrow( aSObj->GetObject() );
//     if ( !aGeomObj->_is_nil() ) {
//       switch ( aGeomObj->GetShapeType() ) {
//       case GEOM::VERTEX: type = SMESH::NODE; break;
//       case GEOM::EDGE:   type = SMESH::EDGE; break;
//       case GEOM::FACE:   type = SMESH::FACE; break;
//       case GEOM::SOLID:
//       case GEOM::SHELL:  type = SMESH::VOLUME; break;
//       case GEOM::COMPOUND: {
//         GEOM::GEOM_Gen_ptr aGeomGen = SMESH_Gen_i::GetSMESHGen()->GetGeomEngine();
//         if ( !aGeomGen->_is_nil() ) {
//           GEOM::GEOM_IGroupOperations_var aGrpOp =
//             aGeomGen->GetIGroupOperations( study->StudyId() );
//           if ( !aGrpOp->_is_nil() ) {
//             switch ( aGrpOp->GetType( aGeomObj )) {
//             case TopAbs_VERTEX: type = SMESH::NODE; break;
//             case TopAbs_EDGE:   type = SMESH::EDGE; break;
//             case TopAbs_FACE:   type = SMESH::FACE; break;
//             case TopAbs_SOLID:  type = SMESH::VOLUME; break;
//             default:;
//             }
//           }
//         }
//       }
//       default:;
//       }
//     }
//   }
//   if ( type < 0 ) {
//     MESSAGE("Type of the group " << grpID << " not found");
//     return false;
//   }
//   if ( theType.IsIntegerValue() )
//     return type == theType.IntegerValue();

//   switch ( type ) {
//   case SMESH::NODE:   return theType.Location( "NODE", 1, theType.Length() );
//   case SMESH::EDGE:   return theType.Location( "EDGE", 1, theType.Length() );
//   case SMESH::FACE:   return theType.Location( "FACE", 1, theType.Length() );
//   case SMESH::VOLUME: return theType.Location( "VOLUME", 1, theType.Length() );
//   default:;
//   }
//   return false;
// }

//================================================================================
/*!
 * \brief 
  * \param theCreationCmd - 
 */
//================================================================================

_pyMesh::_pyMesh(const Handle(_pyCommand) theCreationCmd)
  : _pyObject(theCreationCmd), myHasEditor(false)
{
  // convert my creation command
  Handle(_pyCommand) creationCmd = GetCreationCmd();
  //TCollection_AsciiString str = creationCmd->GetMethod();
//   if(str != "CreateMeshesFromUNV" &&
//      str != "CreateMeshesFromMED" &&
//      str != "CreateMeshesFromSTL")
  creationCmd->SetObject( SMESH_2smeshpy::SmeshpyName() ); 
  creationCmd->SetMethod( "Mesh" );

  theGen->SetAccessorMethod( GetID(), "GetMesh()" );
}

//================================================================================
/*!
 * \brief 
  * \param theCreationCmd - 
 */
//================================================================================
_pyMesh::_pyMesh(const Handle(_pyCommand) theCreationCmd, const TCollection_AsciiString& id):
  _pyObject(theCreationCmd), myHasEditor(false)
{
  // convert my creation command
  Handle(_pyCommand) creationCmd = GetCreationCmd();
  creationCmd->SetObject( SMESH_2smeshpy::SmeshpyName() ); 
  theGen->SetAccessorMethod( id, "GetMesh()" );
}

//================================================================================
/*!
 * \brief Convert a IDL API command of SMESH::Mesh to a method call of python Mesh
  * \param theCommand - Engine method called for this mesh
 */
//================================================================================

void _pyMesh::Process( const Handle(_pyCommand)& theCommand )
{
  // some methods of SMESH_Mesh interface needs special conversion
  // to methods of Mesh python class
  //
  // 1. GetSubMesh(geom, name) + AddHypothesis(geom, algo)
  //     --> in Mesh_Algorithm.Create(mesh, geom, hypo, so)
  // 2. AddHypothesis(geom, hyp)
  //     --> in Mesh_Algorithm.Hypothesis(hyp, args, so)
  // 3. CreateGroupFromGEOM(type, name, grp)
  //     --> in Mesh.Group(grp, name="")
  // 4. ExportToMED(f, auto_groups, version)
  //     --> in Mesh.ExportMED( f, auto_groups, version )
  // 5. etc

  const TCollection_AsciiString method = theCommand->GetMethod();
  // ----------------------------------------------------------------------
  if ( method == "GetSubMesh" ) {
    Handle(_pySubMesh) subMesh = theGen->FindSubMesh( theCommand->GetResultValue() );
    if ( !subMesh.IsNull() ) {
      subMesh->SetCreator( this );
      mySubmeshes.push_back( subMesh );
    }
  }
  // ----------------------------------------------------------------------
  else if ( method == "AddHypothesis" ) { // mesh.AddHypothesis(geom, HYPO )
    myAddHypCmds.push_back( theCommand );
    // set mesh to hypo
    const _pyID& hypID = theCommand->GetArg( 2 );
    Handle(_pyHypothesis) hyp = theGen->FindHyp( hypID );
    if ( !hyp.IsNull() ) {
      myHypos.push_back( hyp );
      if ( hyp->GetMesh().IsEmpty() )
        hyp->SetMesh( this->GetID() );
    }
  }
  // ----------------------------------------------------------------------
  else if ( method == "CreateGroupFromGEOM" ) {// (type, name, grp)
    _pyID grp = theCommand->GetArg( 3 );
    // VSR 24/12/2010. PAL21106: always use GroupOnGeom() function on dump
    // next if(){...} section is commented
    //if ( sameGroupType( grp, theCommand->GetArg( 1 )) ) { // --> Group(grp)
    //  theCommand->SetMethod( "Group" );
    //  theCommand->RemoveArgs();
    //  theCommand->SetArg( 1, grp );
    //}
    //else {
      _pyID type = theCommand->GetArg( 1 );
      _pyID name = theCommand->GetArg( 2 );
      theCommand->SetMethod( "GroupOnGeom" );
      theCommand->RemoveArgs();
      theCommand->SetArg( 1, grp );
      theCommand->SetArg( 2, name );
      theCommand->SetArg( 3, type );
    //}
  }
  // ----------------------------------------------------------------------
  else if ( method == "ExportToMED" ||   // ExportToMED() --> ExportMED()
            method == "ExportToMEDX" ) { // ExportToMEDX() --> ExportMED()
    theCommand->SetMethod( "ExportMED" );
  }
  // ----------------------------------------------------------------------
  else if ( method == "CreateGroup" ) { // CreateGroup() --> CreateEmptyGroup()
    theCommand->SetMethod( "CreateEmptyGroup" );
  }
  // ----------------------------------------------------------------------
  else if ( method == "RemoveHypothesis" ) // (geom, hyp)
  {
    _pyID hypID = theCommand->GetArg( 2 );

    // check if this mesh still has corresponding addition command
    bool hasAddCmd = false;
    list< Handle(_pyCommand) >::iterator cmd = myAddHypCmds.begin();
    while ( cmd != myAddHypCmds.end() )
    {
      // AddHypothesis(geom, hyp)
      if ( hypID == (*cmd)->GetArg( 2 )) { // erase both (add and remove) commands
        theCommand->Clear();
        (*cmd)->Clear();
        cmd = myAddHypCmds.erase( cmd );
        hasAddCmd = true;
      }
      else {
        ++cmd;
      }
    }
    Handle(_pyHypothesis) hyp = theGen->FindHyp( hypID );
    if ( ! hasAddCmd && hypID.Length() != 0 ) { // hypo addition already wrapped
      // RemoveHypothesis(geom, hyp) --> RemoveHypothesis( hyp, geom=0 )
      _pyID geom = theCommand->GetArg( 1 );
      theCommand->RemoveArgs();
      theCommand->SetArg( 1, hypID );
      if ( geom != GetGeom() )
        theCommand->SetArg( 2, geom );
    }
    // remove hyp from myHypos
    myHypos.remove( hyp );
  }
  // check for SubMesh order commands
  else if ( theCommand->GetMethod() == "GetMeshOrder" ||
            theCommand->GetMethod() == "SetMeshOrder" ) {
    // In fact arguments and result values does not support complex containers
    // such as list of list
    // So, here we parse it manually
    // GetMeshOrder
    //for(int ind = 0, n = theCommand->GetNbResultValues();ind<n;ind++) {
    //  Handle(_pySubMesh) subMesh = theGen->FindSubMesh( theCommand->GetResultValue(ind) );
    // SetMeshOrder
    //for(int ind = 0, n = theCommand->GetNbArgs();ind<n;ind++) {
    //  Handle(_pySubMesh) subMesh = theGen->FindSubMesh( theCommand->GetArg(ind) );
    const bool isArg = theCommand->GetMethod() == "SetMeshOrder";
    const TCollection_AsciiString& cmdStr = theCommand->GetString();
    int begPos = (/*isArg ? cmdStr.Search( "(" ) :*/ cmdStr.Search( "[" )) + 1;
    int endPos = (isArg ? cmdStr.Search( ")" ) : cmdStr.Search( "=" )) - 1;
    if ( begPos != -1 && begPos < endPos && endPos <= cmdStr.Length() ) {
      TCollection_AsciiString aSubStr = cmdStr.SubString( begPos, endPos );
      Standard_Integer index = 1;
      TCollection_AsciiString anIDStr = aSubStr.Token("\t ,[]", index++);
      while ( !anIDStr.IsEmpty() ) {
        Handle(_pySubMesh) subMesh = theGen->FindSubMesh( anIDStr );
        if ( !subMesh.IsNull() )
          subMesh->Process( theCommand );
        anIDStr = aSubStr.Token("\t ,[]", index++);
      }
    }
  }
  // add accessor method if necessary
  else
  {
    if ( NeedMeshAccess( theCommand ))
      // apply theCommand to the mesh wrapped by smeshpy mesh
      AddMeshAccess( theCommand );
  }
}

//================================================================================
/*!
 * \brief Return True if addition of accesor method is needed
 */
//================================================================================

bool _pyMesh::NeedMeshAccess( const Handle(_pyCommand)& theCommand )
{
  // names of SMESH_Mesh methods fully equal to methods of class Mesh, so
  // no conversion is needed for them at all:
  static TStringSet sameMethods;
  if ( sameMethods.empty() ) {
    const char * names[] =
      { "ExportDAT","ExportUNV","ExportSTL", "RemoveGroup","RemoveGroupWithContents",
        "GetGroups","UnionGroups","IntersectGroups","CutGroups","GetLog","GetId","ClearLog",
        "GetStudyId","HasDuplicatedGroupNamesMED","GetMEDMesh","NbNodes","NbElements",
        "NbEdges","NbEdgesOfOrder","NbFaces","NbFacesOfOrder","NbTriangles",
        "NbTrianglesOfOrder","NbQuadrangles","NbQuadranglesOfOrder","NbPolygons","NbVolumes",
        "NbVolumesOfOrder","NbTetras","NbTetrasOfOrder","NbHexas","NbHexasOfOrder",
        "NbPyramids","NbPyramidsOfOrder","NbPrisms","NbPrismsOfOrder","NbPolyhedrons",
        "NbSubMesh","GetElementsId","GetElementsByType","GetNodesId","GetElementType",
        "GetSubMeshElementsId","GetSubMeshNodesId","GetSubMeshElementType","Dump","GetNodeXYZ",
        "GetNodeInverseElements","GetShapeID","GetShapeIDForElem","GetElemNbNodes",
        "GetElemNode","IsMediumNode","IsMediumNodeOfAnyElem","ElemNbEdges","ElemNbFaces",
        "IsPoly","IsQuadratic","BaryCenter","GetHypothesisList", "SetAutoColor", "GetAutoColor",
        "Clear", "ConvertToStandalone", "GetMeshOrder", "SetMeshOrder"
        ,"" }; // <- mark of end
    sameMethods.Insert( names );
  }

  return !sameMethods.Contains( theCommand->GetMethod() );
}

//================================================================================
/*!
 * \brief Convert creation and addition of all algos and hypos
 */
//================================================================================

void _pyMesh::Flush()
{
  list < Handle(_pyCommand) >::iterator cmd;

  // try to convert algo addition like this:
  // mesh.AddHypothesis(geom, ALGO ) --> ALGO = mesh.Algo()
  for ( cmd = myAddHypCmds.begin(); cmd != myAddHypCmds.end(); ++cmd )
  {
    Handle(_pyCommand) addCmd = *cmd;

    _pyID algoID = addCmd->GetArg( 2 );
    Handle(_pyHypothesis) algo = theGen->FindHyp( algoID );
    if ( algo.IsNull() || !algo->IsAlgo() )
      continue;

    // check and create new algorithm instance if it is already wrapped
    if ( algo->IsWrapped() ) {
      _pyID localAlgoID = theGen->GenerateNewID( algoID );
      TCollection_AsciiString aNewCmdStr = localAlgoID +
        TCollection_AsciiString( " = " ) + theGen->GetID() +
        TCollection_AsciiString( ".CreateHypothesis( \"" ) + algo->GetAlgoType() +
        TCollection_AsciiString( "\" )" );
      
      Handle(_pyCommand) newCmd = theGen->AddCommand( aNewCmdStr );
      Handle(_pyAlgorithm) newAlgo = Handle(_pyAlgorithm)::DownCast(theGen->FindHyp( localAlgoID ));
      if ( !newAlgo.IsNull() ) {
        newAlgo->Assign( algo, this->GetID() );
        newAlgo->SetCreationCmd( newCmd );
        algo = newAlgo;
        // set algorithm creation
        theGen->SetCommandBefore( newCmd, addCmd );
      }
      else
        newCmd->Clear();
    }
    _pyID geom = addCmd->GetArg( 1 );
    bool isLocalAlgo = ( geom != GetGeom() );
    
    // try to convert
    if ( algo->Addition2Creation( addCmd, this->GetID() )) // OK
    {
      // wrapped algo is created atfer mesh creation
      GetCreationCmd()->AddDependantCmd( addCmd );

      if ( isLocalAlgo ) {
        // mesh.AddHypothesis(geom, ALGO ) --> mesh.AlgoMethod(geom)
        addCmd->SetArg( addCmd->GetNbArgs() + 1,
                        TCollection_AsciiString( "geom=" ) + geom );
        // sm = mesh.GetSubMesh(geom, name) --> sm = ALGO.GetSubMesh()
        list < Handle(_pySubMesh) >::iterator smIt;
        for ( smIt = mySubmeshes.begin(); smIt != mySubmeshes.end(); ++smIt ) {
          Handle(_pySubMesh) subMesh = *smIt;
          Handle(_pyCommand) subCmd = subMesh->GetCreationCmd();
          if ( geom == subCmd->GetArg( 1 )) {
            subCmd->SetObject( algo->GetID() );
            subCmd->RemoveArgs();
            subMesh->SetCreator( algo );
          }
        }
      }
    }
    else // KO - ALGO was already created
    {
      // mesh.AddHypothesis(geom, ALGO) --> mesh.AddHypothesis(ALGO, geom=0)
      addCmd->RemoveArgs();
      addCmd->SetArg( 1, algoID );
      if ( isLocalAlgo )
        addCmd->SetArg( 2, geom );
    }
  }

  // try to convert hypo addition like this:
  // mesh.AddHypothesis(geom, HYPO ) --> HYPO = algo.Hypo()
  for ( cmd = myAddHypCmds.begin(); cmd != myAddHypCmds.end(); ++cmd )
  {
    Handle(_pyCommand) addCmd = *cmd;
    _pyID hypID = addCmd->GetArg( 2 );
    Handle(_pyHypothesis) hyp = theGen->FindHyp( hypID );
    if ( hyp.IsNull() || hyp->IsAlgo() )
      continue;
    bool converted = hyp->Addition2Creation( addCmd, this->GetID() );
    if ( !converted ) {
      // mesh.AddHypothesis(geom, HYP) --> mesh.AddHypothesis(HYP, geom=0)
      _pyID geom = addCmd->GetArg( 1 );
      addCmd->RemoveArgs();
      addCmd->SetArg( 1, hypID );
      if ( geom != GetGeom() )
        addCmd->SetArg( 2, geom );
    }
  }

  // sm = mesh.GetSubMesh(geom, name) --> sm = mesh.GetMesh().GetSubMesh(geom, name)
//   for ( cmd = mySubmeshes.begin(); cmd != mySubmeshes.end(); ++cmd ) {
//     Handle(_pyCommand) subCmd = *cmd;
//     if ( subCmd->GetNbArgs() > 0 )
//       AddMeshAccess( subCmd );
//   }
  myAddHypCmds.clear();
  mySubmeshes.clear();

  // flush hypotheses
  list< Handle(_pyHypothesis) >::iterator hyp = myHypos.begin();
  for ( ; hyp != myHypos.end(); ++hyp )
    (*hyp)->Flush();
}

//================================================================================
/*!
 * \brief MeshEditor convert its commands to ones of mesh
 */
//================================================================================

_pyMeshEditor::_pyMeshEditor(const Handle(_pyCommand)& theCreationCmd):
  _pyObject( theCreationCmd )
{
  myMesh = theCreationCmd->GetObject();
  myCreationCmdStr = theCreationCmd->GetString();
  theCreationCmd->Clear();
}

//================================================================================
/*!
 * \brief convert its commands to ones of mesh
 */
//================================================================================

void _pyMeshEditor::Process( const Handle(_pyCommand)& theCommand)
{
  // names of SMESH_MeshEditor methods fully equal to methods of python class Mesh, so
  // commands calling this methods are converted to calls of methods of Mesh
  static TStringSet sameMethods;
  if ( sameMethods.empty() ) {
    const char * names[] = {
      "RemoveElements","RemoveNodes","RemoveOrphanNodes","AddNode","Add0DElement","AddEdge","AddFace","AddPolygonalFace",
      "AddVolume","AddPolyhedralVolume","AddPolyhedralVolumeByFaces","MoveNode", "MoveClosestNodeToPoint",
      "InverseDiag","DeleteDiag","Reorient","ReorientObject","TriToQuad","SplitQuad","SplitQuadObject",
      "BestSplit","Smooth","SmoothObject","SmoothParametric","SmoothParametricObject",
      "ConvertToQuadratic","ConvertFromQuadratic","RenumberNodes","RenumberElements",
      "RotationSweep","RotationSweepObject","RotationSweepObject1D","RotationSweepObject2D",
      "ExtrusionSweep","AdvancedExtrusion","ExtrusionSweepObject","ExtrusionSweepObject1D","ExtrusionSweepObject2D",
      "ExtrusionAlongPath","ExtrusionAlongPathObject","ExtrusionAlongPathX",
      "ExtrusionAlongPathObject1D","ExtrusionAlongPathObject2D",
      "Mirror","MirrorObject","Translate","TranslateObject","Rotate","RotateObject",
      "FindCoincidentNodes",/*"FindCoincidentNodesOnPart",*/"MergeNodes","FindEqualElements",
      "MergeElements","MergeEqualElements","SewFreeBorders","SewConformFreeBorders",
      "SewBorderToSide","SewSideElements","ChangeElemNodes","GetLastCreatedNodes",
      "GetLastCreatedElems",
      "MirrorMakeMesh","MirrorObjectMakeMesh","TranslateMakeMesh",
      "TranslateObjectMakeMesh","RotateMakeMesh","RotateObjectMakeMesh","MakeBoundaryMesh"
      ,"" }; // <- mark of the end
    sameMethods.Insert( names );
  }

  // names of SMESH_MeshEditor methods which differ from methods of class Mesh
  // only by last two arguments
  static TStringSet diffLastTwoArgsMethods;
  if (diffLastTwoArgsMethods.empty() ) {
    const char * names[] = {
      "MirrorMakeGroups","MirrorObjectMakeGroups",
      "TranslateMakeGroups","TranslateObjectMakeGroups",
      "RotateMakeGroups","RotateObjectMakeGroups",
      ""};// <- mark of the end
    diffLastTwoArgsMethods.Insert( names );
  }

  const TCollection_AsciiString & method = theCommand->GetMethod();
  bool isPyMeshMethod = sameMethods.Contains( method );
  if ( !isPyMeshMethod )
  {
    //Replace SMESH_MeshEditor "MakeGroups" functions by the Mesh 
    //functions with the flag "theMakeGroups = True" like:
    //SMESH_MeshEditor.CmdMakeGroups => Mesh.Cmd(...,True)
    int pos = method.Search("MakeGroups");
    if( pos != -1)
    {
      isPyMeshMethod = true;

      // 1. Remove "MakeGroups" from the Command
      TCollection_AsciiString aMethod = theCommand->GetMethod();
      int nbArgsToAdd = diffLastTwoArgsMethods.Contains(aMethod) ? 2 : 1;
      aMethod.Trunc(pos-1);
      theCommand->SetMethod(aMethod);

      // 2. And add last "True" argument(s)
      while(nbArgsToAdd--)
        theCommand->SetArg(theCommand->GetNbArgs()+1,"True");
    }
  }

  // set "ExtrusionAlongPathX()" instead of "ExtrusionAlongPathObjX()"
  if ( !isPyMeshMethod && method == "ExtrusionAlongPathObjX")
  {
    isPyMeshMethod=true;
    theCommand->SetMethod("ExtrusionAlongPathX");
  }

  // set "FindCoincidentNodesOnPart()" instead of "FindCoincidentNodesOnPartBut()"
  if ( !isPyMeshMethod && method == "FindCoincidentNodesOnPartBut")
  {
    isPyMeshMethod=true;
    theCommand->SetMethod("FindCoincidentNodesOnPart");
  }
  // DoubleNodeElemGroupNew() -> DoubleNodeElemGroup()
  // DoubleNodeGroupNew() -> DoubleNodeGroup()
  // DoubleNodeGroupsNew() -> DoubleNodeGroups()
  // DoubleNodeElemGroupsNew() -> DoubleNodeElemGroups()
  if ( !isPyMeshMethod && ( method == "DoubleNodeElemGroupNew"  ||
                            method == "DoubleNodeElemGroupsNew" ||
                            method == "DoubleNodeGroupNew"      ||
                            method == "DoubleNodeGroupsNew"))
  {
    isPyMeshMethod=true;
    theCommand->SetMethod( method.SubString( 1, method.Length()-3));
    theCommand->SetArg(theCommand->GetNbArgs()+1,"True");
  }
  // ConvertToQuadraticObject(bool,obj) -> ConvertToQuadratic(bool,obj)
  // ConvertFromQuadraticObject(obj) -> ConvertFromQuadratic(obj)
  if ( !isPyMeshMethod && ( method == "ConvertToQuadraticObject" ||
                            method == "ConvertFromQuadraticObject" ))
  {
    isPyMeshMethod=true;
    theCommand->SetMethod( method.SubString( 1, method.Length()-6));
    // prevent moving creation of the converted sub-mesh to the end of the script
    bool isFromQua = ( method.Value( 8 ) == 'F' );
    Handle(_pySubMesh) sm = theGen->FindSubMesh( theCommand->GetArg( isFromQua ? 1 : 2 ));
    if ( !sm.IsNull() )
      sm->Process( theCommand );
  }

  // meshes made by *MakeMesh() methods are not wrapped by _pyMesh,
  // so let _pyMesh care of it (TMP?)
  //     if ( theCommand->GetMethod().Search("MakeMesh") != -1 )
  //       _pyMesh( new _pyCommand( theCommand->GetString(), 0 )); // for theGen->SetAccessorMethod()
  if ( isPyMeshMethod )
  {
    theCommand->SetObject( myMesh );
  }
  else
  {
    // editor creation command is needed only if any editor function is called
    theGen->AddMeshAccessorMethod( theCommand ); // for *Object()
    if ( !myCreationCmdStr.IsEmpty() ) {
      GetCreationCmd()->GetString() = myCreationCmdStr;
      myCreationCmdStr.Clear();
    }
  }
}

//================================================================================
/*!
 * \brief _pyHypothesis constructor
  * \param theCreationCmd - 
 */
//================================================================================

_pyHypothesis::_pyHypothesis(const Handle(_pyCommand)& theCreationCmd):
  _pyObject( theCreationCmd )
{
  myIsAlgo = myIsWrapped = /*myIsConverted = myIsLocal = myDim = */false;
}

//================================================================================
/*!
 * \brief Creates algorithm or hypothesis
  * \param theCreationCmd - The engine command creating a hypothesis
  * \retval Handle(_pyHypothesis) - Result _pyHypothesis
 */
//================================================================================

Handle(_pyHypothesis) _pyHypothesis::NewHypothesis( const Handle(_pyCommand)& theCreationCmd)
{
  // theCreationCmd: CreateHypothesis( "theHypType", "theLibName" )
  ASSERT (( theCreationCmd->GetMethod() == "CreateHypothesis"));

  Handle(_pyHypothesis) hyp, algo;

  // "theHypType"
  const TCollection_AsciiString & hypTypeQuoted = theCreationCmd->GetArg( 1 );
  if ( hypTypeQuoted.IsEmpty() )
    return hyp;
  // theHypType
  TCollection_AsciiString  hypType =
    hypTypeQuoted.SubString( 2, hypTypeQuoted.Length() - 1 );

  algo = new _pyAlgorithm( theCreationCmd );
  hyp  = new _pyHypothesis( theCreationCmd );

  // 1D Regular_1D ----------
  if ( hypType == "Regular_1D" ) {
    // set mesh's method creating algo,
    // i.e. convertion result will be "regular1d = Mesh.Segment()",
    // and set hypType by which algo creating a hypothesis is searched for
    algo->SetConvMethodAndType("Segment", hypType.ToCString());
  }
  else if ( hypType == "CompositeSegment_1D" ) {
    algo->SetConvMethodAndType("Segment", "Regular_1D");
    algo->myArgs.Append( "algo=smesh.COMPOSITE");
  }
  else if ( hypType == "LocalLength" ) {
    // set algo's method creating hyp, and algo type
    hyp->SetConvMethodAndType( "LocalLength", "Regular_1D");
    // set method whose 1 arg will become the 1-st arg of hyp creation command
    // i.e. convertion result will be "locallength = regular1d.LocalLength(<arg of SetLength()>)"
    hyp->AddArgMethod( "SetLength" );
  }
  else if ( hypType == "MaxLength" ) {
    // set algo's method creating hyp, and algo type
    hyp->SetConvMethodAndType( "MaxSize", "Regular_1D");
    // set method whose 1 arg will become the 1-st arg of hyp creation command
    // i.e. convertion result will be "maxsize = regular1d.MaxSize(<arg of SetLength()>)"
    hyp->AddArgMethod( "SetLength" );
  }
  else if ( hypType == "NumberOfSegments" ) {
    hyp = new _pyNumberOfSegmentsHyp( theCreationCmd );
    hyp->SetConvMethodAndType( "NumberOfSegments", "Regular_1D");
    // arg of SetNumberOfSegments() will become the 1-st arg of hyp creation command
    hyp->AddArgMethod( "SetNumberOfSegments" );
    // arg of SetScaleFactor() will become the 2-nd arg of hyp creation command
    hyp->AddArgMethod( "SetScaleFactor" );
    hyp->AddArgMethod( "SetReversedEdges" );
  }
  else if ( hypType == "Arithmetic1D" ) {
    hyp = new _pyComplexParamHypo( theCreationCmd );
    hyp->SetConvMethodAndType( "Arithmetic1D", "Regular_1D");
    hyp->AddArgMethod( "SetStartLength" );
    hyp->AddArgMethod( "SetEndLength" );
    hyp->AddArgMethod( "SetReversedEdges" );
  }
  else if ( hypType == "StartEndLength" ) {
    hyp = new _pyComplexParamHypo( theCreationCmd );
    hyp->SetConvMethodAndType( "StartEndLength", "Regular_1D");
    hyp->AddArgMethod( "SetStartLength" );
    hyp->AddArgMethod( "SetEndLength" );
    hyp->AddArgMethod( "SetReversedEdges" );
  }
  else if ( hypType == "Deflection1D" ) {
    hyp->SetConvMethodAndType( "Deflection1D", "Regular_1D");
    hyp->AddArgMethod( "SetDeflection" );
  }
  else if ( hypType == "Propagation" ) {
    hyp->SetConvMethodAndType( "Propagation", "Regular_1D");
  }
  else if ( hypType == "QuadraticMesh" ) {
    hyp->SetConvMethodAndType( "QuadraticMesh", "Regular_1D");
  }
  else if ( hypType == "AutomaticLength" ) {
    hyp->SetConvMethodAndType( "AutomaticLength", "Regular_1D");
    hyp->AddArgMethod( "SetFineness");
  }
  else if ( hypType == "SegmentLengthAroundVertex" ) {
    hyp = new _pySegmentLengthAroundVertexHyp( theCreationCmd );
    hyp->SetConvMethodAndType( "LengthNearVertex", "Regular_1D" );
    hyp->AddArgMethod( "SetLength" );
  }
  // 1D Python_1D ----------
  else if ( hypType == "Python_1D" ) {
    algo->SetConvMethodAndType( "Segment", hypType.ToCString());
    algo->myArgs.Append( "algo=smesh.PYTHON");
  }
  else if ( hypType == "PythonSplit1D" ) {
    hyp->SetConvMethodAndType( "PythonSplit1D", "Python_1D");
    hyp->AddArgMethod( "SetNumberOfSegments");
    hyp->AddArgMethod( "SetPythonLog10RatioFunction");
  }
  // MEFISTO_2D ----------
  else if ( hypType == "MEFISTO_2D" ) { // MEFISTO_2D
    algo->SetConvMethodAndType( "Triangle", hypType.ToCString());
  }
  else if ( hypType == "MaxElementArea" ) {
    hyp->SetConvMethodAndType( "MaxElementArea", "MEFISTO_2D");
    hyp->SetConvMethodAndType( "MaxElementArea", "NETGEN_2D_ONLY");
    hyp->AddArgMethod( "SetMaxElementArea");
  }
  else if ( hypType == "LengthFromEdges" ) {
    hyp->SetConvMethodAndType( "LengthFromEdges", "MEFISTO_2D");
    hyp->SetConvMethodAndType( "LengthFromEdges", "NETGEN_2D_ONLY");
  }
  // Quadrangle_2D ----------
  else if ( hypType == "Quadrangle_2D" ) {
    algo->SetConvMethodAndType( "Quadrangle" , hypType.ToCString());
  }
  else if ( hypType == "QuadranglePreference" ) {
    hyp->SetConvMethodAndType( "QuadranglePreference", "Quadrangle_2D");
    hyp->SetConvMethodAndType( "SetQuadAllowed", "NETGEN_2D_ONLY");
  }
  else if ( hypType == "TrianglePreference" ) {
    hyp->SetConvMethodAndType( "TrianglePreference", "Quadrangle_2D");
  }     
  // RadialQuadrangle_1D2D ----------
  else if ( hypType == "RadialQuadrangle_1D2D" ) {
    algo->SetConvMethodAndType( "Quadrangle" , hypType.ToCString());
    algo->myArgs.Append( "algo=smesh.RADIAL_QUAD" );
  }
  else if ( hypType == "NumberOfLayers2D" ) {
    hyp->SetConvMethodAndType( "NumberOfLayers", "RadialQuadrangle_1D2D");
    hyp->AddArgMethod( "SetNumberOfLayers" );
  }
  else if ( hypType == "LayerDistribution2D" ) {
    hyp = new _pyLayerDistributionHypo( theCreationCmd, "Get2DHypothesis" );
    hyp->SetConvMethodAndType( "LayerDistribution", "RadialQuadrangle_1D2D");
  }
  // BLSURF ----------
  else if ( hypType == "BLSURF" ) {
    algo->SetConvMethodAndType( "Triangle", hypType.ToCString());
    algo->myArgs.Append( "algo=smesh.BLSURF" );
  }
  else if ( hypType == "BLSURF_Parameters") {
    hyp->SetConvMethodAndType( "Parameters", "BLSURF");
  }
  // NETGEN ----------
  else if ( hypType == "NETGEN_2D") { // 1D-2D
    algo->SetConvMethodAndType( "Triangle" , hypType.ToCString());
    algo->myArgs.Append( "algo=smesh.NETGEN" );
  }
  else if ( hypType == "NETGEN_Parameters_2D") {
    hyp->SetConvMethodAndType( "Parameters", "NETGEN_2D");
  }
  else if ( hypType == "NETGEN_SimpleParameters_2D") {
    hyp->SetConvMethodAndType( "Parameters", "NETGEN_2D");
    hyp->myArgs.Append( "which=smesh.SIMPLE" );
  }
  else if ( hypType == "NETGEN_2D3D") { // 1D-2D-3D
    algo->SetConvMethodAndType( "Tetrahedron" , hypType.ToCString());
    algo->myArgs.Append( "algo=smesh.FULL_NETGEN" );
  }
  else if ( hypType == "NETGEN_Parameters") {
    hyp->SetConvMethodAndType( "Parameters", "NETGEN_2D3D");
  }
  else if ( hypType == "NETGEN_SimpleParameters_3D") {
    hyp->SetConvMethodAndType( "Parameters", "NETGEN_2D3D");
    hyp->myArgs.Append( "which=smesh.SIMPLE" );
  }
  else if ( hypType == "NETGEN_2D_ONLY") { // 2D
    algo->SetConvMethodAndType( "Triangle" , hypType.ToCString());
    algo->myArgs.Append( "algo=smesh.NETGEN_2D" );
  }
  else if ( hypType == "NETGEN_3D") { // 3D
    algo->SetConvMethodAndType( "Tetrahedron" , hypType.ToCString());
    algo->myArgs.Append( "algo=smesh.NETGEN" );
  }
  else if ( hypType == "MaxElementVolume") {
    hyp->SetConvMethodAndType( "MaxElementVolume", "NETGEN_3D");
    hyp->AddArgMethod( "SetMaxElementVolume" );
  }
  // GHS3D_3D ----------
  else if ( hypType == "GHS3D_3D" ) {
    algo->SetConvMethodAndType( "Tetrahedron", hypType.ToCString());
    algo->myArgs.Append( "algo=smesh.GHS3D" );
  }
  else if ( hypType == "GHS3D_Parameters") {
    hyp->SetConvMethodAndType( "Parameters", "GHS3D_3D");
  }
  // Hexa_3D ---------
  else if ( hypType == "BLSURF" ) {
    algo->SetConvMethodAndType( "Hexahedron", hypType.ToCString());
  }
  // Repetitive Projection_1D ---------
  else if ( hypType == "Projection_1D" ) {
    algo->SetConvMethodAndType( "Projection1D", hypType.ToCString());
  }
  else if ( hypType == "ProjectionSource1D" ) {
    hyp->SetConvMethodAndType( "SourceEdge", "Projection_1D");
    hyp->AddArgMethod( "SetSourceEdge");
    hyp->AddArgMethod( "SetSourceMesh");
    // 2 args of SetVertexAssociation() will become the 3-th and 4-th args of hyp creation command
    hyp->AddArgMethod( "SetVertexAssociation", 2 );
  }
  // Projection_2D ---------
  else if ( hypType == "Projection_2D" ) {
    algo->SetConvMethodAndType( "Projection2D", hypType.ToCString());
  }
  else if ( hypType == "ProjectionSource2D" ) {
    hyp->SetConvMethodAndType( "SourceFace", "Projection_2D");
    hyp->AddArgMethod( "SetSourceFace");
    hyp->AddArgMethod( "SetSourceMesh");
    hyp->AddArgMethod( "SetVertexAssociation", 4 );
  }
  // Projection_3D ---------
  else if ( hypType == "Projection_3D" ) {
    algo->SetConvMethodAndType( "Projection3D", hypType.ToCString());
  }
  else if ( hypType == "ProjectionSource3D" ) {
    hyp->SetConvMethodAndType( "SourceShape3D", "Projection_3D");
    hyp->AddArgMethod( "SetSource3DShape");
    hyp->AddArgMethod( "SetSourceMesh");
    hyp->AddArgMethod( "SetVertexAssociation", 4 );
  }
  // Prism_3D ---------
  else if ( hypType == "Prism_3D" ) {
    algo->SetConvMethodAndType( "Prism", hypType.ToCString());
  }
  // RadialPrism_3D ---------
  else if ( hypType == "RadialPrism_3D" ) {
    algo->SetConvMethodAndType( "Prism", hypType.ToCString());
  }
  else if ( hypType == "NumberOfLayers" ) {
    hyp->SetConvMethodAndType( "NumberOfLayers", "RadialPrism_3D");
    hyp->AddArgMethod( "SetNumberOfLayers" );
  }
  else if ( hypType == "LayerDistribution" ) {
    hyp = new _pyLayerDistributionHypo( theCreationCmd, "Get3DHypothesis" );
    hyp->SetConvMethodAndType( "LayerDistribution", "RadialPrism_3D");
  }

  return algo->IsValid() ? algo : hyp;
}

//================================================================================
/*!
 * \brief Convert the command adding a hypothesis to mesh into a smesh command
  * \param theCmd - The command like mesh.AddHypothesis( geom, hypo )
  * \param theAlgo - The algo that can create this hypo
  * \retval bool - false if the command cant be converted
 */
//================================================================================

bool _pyHypothesis::Addition2Creation( const Handle(_pyCommand)& theCmd,
                                       const _pyID&              theMesh)
{
  ASSERT(( theCmd->GetMethod() == "AddHypothesis" ));

  if ( !IsWrappable( theMesh ))
    return false;

  myGeom = theCmd->GetArg( 1 );

  Handle(_pyHypothesis) algo;
  if ( !IsAlgo() ) {
    // find algo created on myGeom in theMesh
    algo = theGen->FindAlgo( myGeom, theMesh, this );
    if ( algo.IsNull() )
      return false;
    // attach hypothesis creation command to be after algo creation command
    // because it can be new created instance of algorithm
    algo->GetCreationCmd()->AddDependantCmd( theCmd );
  }
  myIsWrapped = true;

  // mesh.AddHypothesis(geom,hyp) --> hyp = <theMesh or algo>.myCreationMethod(args)
  theCmd->SetResultValue( GetID() );
  theCmd->SetObject( IsAlgo() ? theMesh : algo->GetID());
  theCmd->SetMethod( IsAlgo() ? GetAlgoCreationMethod() : GetCreationMethod( algo->GetAlgoType() ));
  // set args
  theCmd->RemoveArgs();
  for ( int i = 1; i <= myArgs.Length(); ++i ) {
    if ( !myArgs( i ).IsEmpty() )
      theCmd->SetArg( i, myArgs( i ));
    else
      theCmd->SetArg( i, "[]");
  }
  // set a new creation command
  GetCreationCmd()->Clear();
  // replace creation command by wrapped instance
  // please note, that hypothesis attaches to algo creation command (see upper)
  SetCreationCmd( theCmd );
  

  // clear commands setting arg values
  list < Handle(_pyCommand) >::iterator argCmd = myArgCommands.begin();
  for ( ; argCmd != myArgCommands.end(); ++argCmd )
    (*argCmd)->Clear();

  // set unknown arg commands after hypo creation
  Handle(_pyCommand) afterCmd = myIsWrapped ? theCmd : GetCreationCmd();
  list<Handle(_pyCommand)>::iterator cmd = myUnknownCommands.begin();
  for ( ; cmd != myUnknownCommands.end(); ++cmd ) {
    afterCmd->AddDependantCmd( *cmd );
  }

  return myIsWrapped;
}

//================================================================================
/*!
 * \brief Remember hypothesis parameter values
 * \param theCommand - The called hypothesis method
 */
//================================================================================

void _pyHypothesis::Process( const Handle(_pyCommand)& theCommand)
{
  ASSERT( !myIsAlgo );
  // set args
  int nbArgs = 0;
  for ( int i = 1; i <= myArgMethods.Length(); ++i ) {
    if ( myArgMethods( i ) == theCommand->GetMethod() ) {
      while ( myArgs.Length() < nbArgs + myNbArgsByMethod( i ))
        myArgs.Append( "[]" );
      for ( int iArg = 1; iArg <= myNbArgsByMethod( i ); ++iArg )
        myArgs( nbArgs + iArg ) = theCommand->GetArg( iArg ); // arg value
      myArgCommands.push_back( theCommand );
      return;
    }
    nbArgs += myNbArgsByMethod( i );
  }
  myUnknownCommands.push_back( theCommand );
}

//================================================================================
/*!
 * \brief Finish conversion
 */
//================================================================================

void _pyHypothesis::Flush()
{
  if ( IsWrapped() ) {
  }
  else {
    list < Handle(_pyCommand) >::iterator cmd = myArgCommands.begin();
    for ( ; cmd != myArgCommands.end(); ++cmd ) {
      // Add access to a wrapped mesh
      theGen->AddMeshAccessorMethod( *cmd );
      // Add access to a wrapped algorithm
      theGen->AddAlgoAccessorMethod( *cmd );
    }
    cmd = myUnknownCommands.begin();
    for ( ; cmd != myUnknownCommands.end(); ++cmd ) {
      // Add access to a wrapped mesh
      theGen->AddMeshAccessorMethod( *cmd );
      // Add access to a wrapped algorithm
      theGen->AddAlgoAccessorMethod( *cmd );
    }
  }
  // forget previous hypothesis modifications
  myArgCommands.clear();
  myUnknownCommands.clear();
}

//================================================================================
/*!
 * \brief clear creation, arg and unkown commands
 */
//================================================================================

void _pyHypothesis::ClearAllCommands()
{
  GetCreationCmd()->Clear();
  list<Handle(_pyCommand)>::iterator cmd = myArgCommands.begin();
  for ( ; cmd != myArgCommands.end(); ++cmd )
    ( *cmd )->Clear();
  cmd = myUnknownCommands.begin();
  for ( ; cmd != myUnknownCommands.end(); ++cmd )
    ( *cmd )->Clear();
}


//================================================================================
/*!
 * \brief Assign fields of theOther to me except myIsWrapped
 */
//================================================================================

void _pyHypothesis::Assign( const Handle(_pyHypothesis)& theOther,
                            const _pyID&                 theMesh )
{
  myIsWrapped = false;
  myMesh = theMesh;

  // myCreationCmd = theOther->myCreationCmd;
  myIsAlgo = theOther->myIsAlgo;
  myGeom = theOther->myGeom;
  myType2CreationMethod = theOther->myType2CreationMethod;
  myArgs = theOther->myArgs;
  myArgMethods = theOther->myArgMethods;
  myNbArgsByMethod = theOther->myNbArgsByMethod;
  myArgCommands = theOther->myArgCommands;
  myUnknownCommands = theOther->myUnknownCommands;
}

//================================================================================
/*!
 * \brief Remember hypothesis parameter values
  * \param theCommand - The called hypothesis method
 */
//================================================================================

void _pyComplexParamHypo::Process( const Handle(_pyCommand)& theCommand)
{
  if( theCommand->GetMethod() == "SetLength" )
  {
    // NOW it becomes OBSOLETE
    // ex: hyp.SetLength(start, 1)
    //     hyp.SetLength(end,   0)
    ASSERT(( theCommand->GetArg( 2 ).IsIntegerValue() ));
    int i = 2 - theCommand->GetArg( 2 ).IntegerValue();
    while ( myArgs.Length() < i )
      myArgs.Append( "[]" );
    myArgs( i ) = theCommand->GetArg( 1 ); // arg value
    myArgCommands.push_back( theCommand );
  }
  else
  {
    _pyHypothesis::Process( theCommand );
  }
}
//================================================================================
/*!
 * \brief Clear SetObjectEntry() as it is called by methods of Mesh_Segment
 */
//================================================================================

void _pyComplexParamHypo::Flush()
{
  if ( IsWrapped() )
  {
    list < Handle(_pyCommand) >::iterator cmd = myUnknownCommands.begin();
    for ( ; cmd != myUnknownCommands.end(); ++cmd )
      if ((*cmd)->GetMethod() == "SetObjectEntry" )
        (*cmd)->Clear();
  }
}

//================================================================================
/*!
 * \brief Convert methods of 1D hypotheses to my own methods
  * \param theCommand - The called hypothesis method
 */
//================================================================================

void _pyLayerDistributionHypo::Process( const Handle(_pyCommand)& theCommand)
{
  if ( theCommand->GetMethod() != "SetLayerDistribution" )
    return;

  _pyID newName; // name for 1D hyp = "HypType" + "_Distribution"

  const _pyID& hyp1dID = theCommand->GetArg( 1 );
  Handle(_pyHypothesis) hyp1d = theGen->FindHyp( hyp1dID );
  if ( hyp1d.IsNull() ) // apparently hypId changed at study restoration
    hyp1d = my1dHyp;
  else if ( !my1dHyp.IsNull() && hyp1dID != my1dHyp->GetID() ) {
    // 1D hypo is already set, so distribution changes and the old
    // 1D hypo is thrown away
    my1dHyp->ClearAllCommands();
  }
  my1dHyp = hyp1d;

  if ( !myArgCommands.empty() )
    myArgCommands.front()->Clear();
  myArgCommands.push_back( theCommand );
}

//================================================================================
/*!
 * \brief 
  * \param theAdditionCmd - command to be converted
  * \param theMesh - mesh instance
  * \retval bool - status
 */
//================================================================================

bool _pyLayerDistributionHypo::Addition2Creation( const Handle(_pyCommand)& theAdditionCmd,
                                                  const _pyID&              theMesh)
{
  myIsWrapped = false;

  if ( my1dHyp.IsNull() )
    return false;

  // set "SetLayerDistribution()" after addition cmd
  theAdditionCmd->AddDependantCmd( myArgCommands.front() );

  _pyID geom = theAdditionCmd->GetArg( 1 );

  Handle(_pyHypothesis) algo = theGen->FindAlgo( geom, theMesh, this );
  if ( !algo.IsNull() )
  {
    my1dHyp->SetMesh( theMesh );
    my1dHyp->SetConvMethodAndType(my1dHyp->GetAlgoCreationMethod().ToCString(),
                                  algo->GetAlgoType().ToCString());
    if ( !my1dHyp->Addition2Creation( theAdditionCmd, theMesh ))
      return false;

    // clear "SetLayerDistribution()" cmd
    myArgCommands.back()->Clear();

    // Convert my creation => me = RadialPrismAlgo.Get3DHypothesis()

    // find RadialPrism algo created on <geom> for theMesh
    GetCreationCmd()->SetObject( algo->GetID() );
    GetCreationCmd()->SetMethod( myAlgoMethod );
    GetCreationCmd()->RemoveArgs();
    theAdditionCmd->AddDependantCmd( GetCreationCmd() );
    myIsWrapped = true;
  }
  return myIsWrapped;
}

//================================================================================
/*!
 * \brief 
 */
//================================================================================

void _pyLayerDistributionHypo::Flush()
{
  // as creation of 1D hyp was written later then it's edition,
  // we need to find all it's edition calls and process them
  if ( !my1dHyp.IsNull() )
  {
    _pyID hyp1dID = my1dHyp->GetCreationCmd()->GetResultValue();

    // make a new name for 1D hyp = "HypType" + "_Distribution"
    _pyID newName;
    if ( my1dHyp->IsWrapped() ) {
      newName = my1dHyp->GetCreationCmd()->GetMethod();
    }
    else {
      TCollection_AsciiString hypTypeQuoted = my1dHyp->GetCreationCmd()->GetArg(1);
      newName = hypTypeQuoted.SubString( 2, hypTypeQuoted.Length() - 1 );
    }
    newName += "_Distribution";
    my1dHyp->GetCreationCmd()->SetResultValue( newName );

    list< Handle(_pyCommand) >& cmds = theGen->GetCommands();
    list< Handle(_pyCommand) >::iterator cmdIt = cmds.begin();
    for ( ; cmdIt != cmds.end(); ++cmdIt ) {
      const _pyID& objID = (*cmdIt)->GetObject();
      if ( objID == hyp1dID ) {
        my1dHyp->Process( *cmdIt );
        my1dHyp->GetCreationCmd()->AddDependantCmd( *cmdIt );
        ( *cmdIt )->SetObject( newName );
      }
    }
    // Set new hyp name to SetLayerDistribution() cmd
    if ( !myArgCommands.empty() && !myArgCommands.back()->IsEmpty() )
      myArgCommands.back()->SetArg( 1, newName );
  }
}

//================================================================================
/*!
 * \brief additionally to Addition2Creation, clears SetDistrType() command
  * \param theCmd - AddHypothesis() command
  * \param theMesh - mesh to which a hypothesis is added
  * \retval bool - convertion result
 */
//================================================================================

bool _pyNumberOfSegmentsHyp::Addition2Creation( const Handle(_pyCommand)& theCmd,
                                                const _pyID&              theMesh)
{
  if ( IsWrappable( theMesh ) && myArgs.Length() > 1 ) {
    // scale factor (2-nd arg) is provided: clear SetDistrType(1) command
    bool scaleDistrType = false;
    list<Handle(_pyCommand)>::reverse_iterator cmd = myUnknownCommands.rbegin();
    for ( ; cmd != myUnknownCommands.rend(); ++cmd ) {
      if ( (*cmd)->GetMethod() == "SetDistrType" ) {
        if ( (*cmd)->GetArg( 1 ) == "1" ) {
          scaleDistrType = true;
          (*cmd)->Clear();
        }
        else if ( !scaleDistrType ) {
          // distribution type changed: remove scale factor from args
          myArgs.Remove( 2, myArgs.Length() );
          break;
        }
      }
    }
  }
  return _pyHypothesis::Addition2Creation( theCmd, theMesh );
}

//================================================================================
/*!
 * \brief remove repeated commands defining distribution
 */
//================================================================================

void _pyNumberOfSegmentsHyp::Flush()
{
  // find number of the last SetDistrType() command
  list<Handle(_pyCommand)>::reverse_iterator cmd = myUnknownCommands.rbegin();
  int distrTypeNb = 0;
  for ( ; !distrTypeNb && cmd != myUnknownCommands.rend(); ++cmd )
    if ( (*cmd)->GetMethod() == "SetDistrType" )
      distrTypeNb = (*cmd)->GetOrderNb();
    else if (IsWrapped() && (*cmd)->GetMethod() == "SetObjectEntry" )
      (*cmd)->Clear();

  // clear commands before the last SetDistrType()
  list<Handle(_pyCommand)> * cmds[2] = { &myArgCommands, &myUnknownCommands };
  for ( int i = 0; i < 2; ++i ) {
    set<TCollection_AsciiString> uniqueMethods;
    list<Handle(_pyCommand)> & cmdList = *cmds[i];
    for ( cmd = cmdList.rbegin(); cmd != cmdList.rend(); ++cmd )
    {
      bool clear = ( (*cmd)->GetOrderNb() < distrTypeNb );
      const TCollection_AsciiString& method = (*cmd)->GetMethod();
      if ( !clear || method == "SetNumberOfSegments" ) {
        bool isNewInSet = uniqueMethods.insert( method ).second;
        clear = !isNewInSet;
      }
      if ( clear )
        (*cmd)->Clear();
    }
    cmdList.clear();
  }
}

//================================================================================
/*!
 * \brief Convert the command adding "SegmentLengthAroundVertex" to mesh
 * into regular1D.LengthNearVertex( length, vertex )
  * \param theCmd - The command like mesh.AddHypothesis( vertex, SegmentLengthAroundVertex )
  * \param theMesh - The mesh needing this hypo
  * \retval bool - false if the command cant be converted
 */
//================================================================================
  
bool _pySegmentLengthAroundVertexHyp::Addition2Creation( const Handle(_pyCommand)& theCmd,
                                                         const _pyID&              theMeshID)
{
  if ( IsWrappable( theMeshID )) {

    _pyID vertex = theCmd->GetArg( 1 );

    // the problem here is that segment algo will not be found
    // by pyHypothesis::Addition2Creation() for <vertex>, so we try to find
    // geometry where segment algorithm is assigned
    Handle(_pyHypothesis) algo;
    _pyID geom = vertex;
    while ( algo.IsNull() && !geom.IsEmpty()) {
      // try to find geom as a father of <vertex>
      geom = FatherID( geom );
      algo = theGen->FindAlgo( geom, theMeshID, this );
    }
    if ( algo.IsNull() )
      return false; // also possible to find geom as brother of veretex...
    // set geom instead of vertex
    theCmd->SetArg( 1, geom );

    // set vertex as a second arg
    if ( myArgs.Length() < 1) myArgs.Append( "1" ); // :(
    myArgs.Append( vertex );

    // mesh.AddHypothesis(vertex, SegmentLengthAroundVertex) -->
    // theMeshID.LengthNearVertex( length, vertex )
    return _pyHypothesis::Addition2Creation( theCmd, theMeshID );
  }
  return false;
}

//================================================================================
/*!
 * \brief _pyAlgorithm constructor
 * \param theCreationCmd - The command like "algo = smeshgen.CreateHypothesis(type,lib)"
 */
//================================================================================

_pyAlgorithm::_pyAlgorithm(const Handle(_pyCommand)& theCreationCmd)
  : _pyHypothesis( theCreationCmd )
{
  myIsAlgo = true;
}

//================================================================================
/*!
 * \brief Convert the command adding an algorithm to mesh
  * \param theCmd - The command like mesh.AddHypothesis( geom, algo )
  * \param theMesh - The mesh needing this algo 
  * \retval bool - false if the command cant be converted
 */
//================================================================================
  
bool _pyAlgorithm::Addition2Creation( const Handle(_pyCommand)& theCmd,
                                      const _pyID&              theMeshID)
{
  // mesh.AddHypothesis(geom,algo) --> theMeshID.myCreationMethod()
  if ( _pyHypothesis::Addition2Creation( theCmd, theMeshID )) {
    theGen->SetAccessorMethod( GetID(), "GetAlgorithm()" );
    return true;
  }
  return false;
}

//================================================================================
/*!
 * \brief Return starting position of a part of python command
  * \param thePartIndex - The index of command part
  * \retval int - Part position
 */
//================================================================================

int _pyCommand::GetBegPos( int thePartIndex )
{
  if ( IsEmpty() )
    return EMPTY;
  if ( myBegPos.Length() < thePartIndex )
    return UNKNOWN;
  return myBegPos( thePartIndex );
}

//================================================================================
/*!
 * \brief Store starting position of a part of python command
  * \param thePartIndex - The index of command part
  * \param thePosition - Part position
 */
//================================================================================

void _pyCommand::SetBegPos( int thePartIndex, int thePosition )
{
  while ( myBegPos.Length() < thePartIndex )
    myBegPos.Append( UNKNOWN );
  myBegPos( thePartIndex ) = thePosition;
}

//================================================================================
/*!
 * \brief Returns whitespace symbols at the line beginning
  * \retval TCollection_AsciiString - result
 */
//================================================================================

TCollection_AsciiString _pyCommand::GetIndentation()
{
  int end = 1;
  if ( GetBegPos( RESULT_IND ) == UNKNOWN )
    GetWord( myString, end, true );
  else
    end = GetBegPos( RESULT_IND );
  return myString.SubString( 1, end - 1 );
}

//================================================================================
/*!
 * \brief Return substring of python command looking like ResultValue = Obj.Meth()
  * \retval const TCollection_AsciiString & - ResultValue substring
 */
//================================================================================

const TCollection_AsciiString & _pyCommand::GetResultValue()
{
  if ( GetBegPos( RESULT_IND ) == UNKNOWN )
  {
    int begPos = myString.Location( "=", 1, Length() );
    if ( begPos )
      myRes = GetWord( myString, begPos, false );
    else
      begPos = EMPTY;
    SetBegPos( RESULT_IND, begPos );
  }
  return myRes;
}

//================================================================================
/*!
 * \brief Return number of python command result value ResultValue = Obj.Meth()
  * \retval const int
 */
//================================================================================

const int _pyCommand::GetNbResultValues()
{
  int begPos = 1;
  int Nb=0;
  int endPos = myString.Location( "=", 1, Length() );
  TCollection_AsciiString str = "";
  while ( begPos < endPos) {
    str = GetWord( myString, begPos, true );
    begPos = begPos+ str.Length();
    Nb++;
  }
  return (Nb-1);
}


//================================================================================
/*!
 * \brief Return substring of python command looking like
 *  ResultValue1 , ResultValue1,... = Obj.Meth() with res index
 * \retval const TCollection_AsciiString & - ResultValue with res index substring
 */
//================================================================================
const TCollection_AsciiString & _pyCommand::GetResultValue(int res)
{
  int begPos = 1;
  int Nb=0;
  int endPos = myString.Location( "=", 1, Length() );
  while ( begPos < endPos) {
    myRes = GetWord( myString, begPos, true );
    begPos = begPos + myRes.Length();
    Nb++;
    if(res == Nb){
      myRes.RemoveAll('[');myRes.RemoveAll(']');
      return myRes;
    }
    if(Nb>res)
      break;
  }
  return theEmptyString;
}

//================================================================================
/*!
 * \brief Return substring of python command looking like ResVal = Object.Meth()
  * \retval const TCollection_AsciiString & - Object substring
 */
//================================================================================

const TCollection_AsciiString & _pyCommand::GetObject()
{
  if ( GetBegPos( OBJECT_IND ) == UNKNOWN )
  {
    // beginning
    int begPos = GetBegPos( RESULT_IND ) + myRes.Length();
    if ( begPos < 1 ) {
      begPos = myString.Location( "=", 1, Length() ) + 1;
      // is '=' in the string argument (for example, name) or not
      int nb1 = 0; // number of ' character at the left of =
      int nb2 = 0; // number of " character at the left of =
      for ( int i = 1; i < begPos-1; i++ ) {
        if ( myString.Value( i )=='\'' )
          nb1 += 1;
        else if ( myString.Value( i )=='"' )
          nb2 += 1;
      }
      // if number of ' or " is not divisible by 2,
      // then get an object at the start of the command
      if ( nb1 % 2 != 0 || nb2 % 2 != 0 )
        begPos = 1;
    }
    myObj = GetWord( myString, begPos, true );
    // check if object is complex,
    // so far consider case like "smesh.smesh.Method()"
    if ( int bracketPos = myString.Location( "(", begPos, Length() )) {
      //if ( bracketPos==0 ) bracketPos = Length();
      int dotPos = begPos+myObj.Length();
      while ( dotPos+1 < bracketPos ) {
        if ( int pos = myString.Location( ".", dotPos+1, bracketPos ))
          dotPos = pos;
        else
          break;
      }
      if ( dotPos > begPos+myObj.Length() )
        myObj = myString.SubString( begPos, dotPos-1 );
    }
    // store
    SetBegPos( OBJECT_IND, begPos );
  }
  //SCRUTE(myObj);
  return myObj;
}

//================================================================================
/*!
 * \brief Return substring of python command looking like ResVal = Obj.Method()
  * \retval const TCollection_AsciiString & - Method substring
 */
//================================================================================

const TCollection_AsciiString & _pyCommand::GetMethod()
{
  if ( GetBegPos( METHOD_IND ) == UNKNOWN )
  {
    // beginning
    int begPos = GetBegPos( OBJECT_IND ) + myObj.Length();
    bool forward = true;
    if ( begPos < 1 ) {
      begPos = myString.Location( "(", 1, Length() ) - 1;
      forward = false;
    }
    // store
    myMeth = GetWord( myString, begPos, forward );
    SetBegPos( METHOD_IND, begPos );
  }
  //SCRUTE(myMeth);
  return myMeth;
}

//================================================================================
/*!
 * \brief Return substring of python command looking like ResVal = Obj.Meth(Arg1,...)
  * \retval const TCollection_AsciiString & - Arg<index> substring
 */
//================================================================================

const TCollection_AsciiString & _pyCommand::GetArg( int index )
{
  if ( GetBegPos( ARG1_IND ) == UNKNOWN )
  {
    // find all args
    int begPos = GetBegPos( METHOD_IND ) + myMeth.Length();
    if ( begPos < 1 )
      begPos = myString.Location( "(", 1, Length() ) + 1;

    int i = 0, prevLen = 0, nbNestings = 0;
    while ( begPos != EMPTY ) {
      begPos += prevLen;
      if( myString.Value( begPos ) == '(' )
        nbNestings++;
      // check if we are looking at the closing parenthesis
      while ( begPos <= Length() && isspace( myString.Value( begPos )))
        ++begPos;
      if ( begPos > Length() )
        break;
      if ( myString.Value( begPos ) == ')' ) {
        nbNestings--;
        if( nbNestings == 0 )
          break;
      }
      myArgs.Append( GetWord( myString, begPos, true, true ));
      SetBegPos( ARG1_IND + i, begPos );
      prevLen = myArgs.Last().Length();
      if ( prevLen == 0 )
        myArgs.Remove( myArgs.Length() ); // no more args
      i++;
    }
  }
  if ( myArgs.Length() < index )
    return theEmptyString;
  return myArgs( index );
}

//================================================================================
/*!
 * \brief Check if char is a word part
  * \param c - The character to check
  * \retval bool - The check result
 */
//================================================================================

static inline bool isWord(const char c, const bool dotIsWord)
{
  return
    !isspace(c) && c != ',' && c != '=' && c != ')' && c != '(' && ( dotIsWord || c != '.');
}

//================================================================================
/*!
 * \brief Looks for a word in the string and returns word's beginning
  * \param theString - The input string
  * \param theStartPos - The position to start the search, returning word's beginning
  * \param theForward - The search direction
  * \retval TCollection_AsciiString - The found word
 */
//================================================================================

TCollection_AsciiString _pyCommand::GetWord( const TCollection_AsciiString & theString,
                                            int &      theStartPos,
                                            const bool theForward,
                                            const bool dotIsWord )
{
  int beg = theStartPos, end = theStartPos;
  theStartPos = EMPTY;
  if ( beg < 1 || beg > theString.Length() )
    return theEmptyString;

  if ( theForward ) { // search forward
    // beg
    while ( beg <= theString.Length() && !isWord( theString.Value( beg ), dotIsWord))
      ++beg;
    if ( beg > theString.Length() )
      return theEmptyString; // no word found
    // end
    end = beg + 1;
    char begChar = theString.Value( beg );
    if ( begChar == '"' || begChar == '\'' || begChar == '[') {
      char endChar = ( begChar == '[' ) ? ']' : begChar;
      // end is at the corresponding quoting mark or bracket
      while ( end < theString.Length() &&
              ( theString.Value( end ) != endChar || theString.Value( end-1 ) == '\\'))
        ++end;
    }
    else {
      while ( end <= theString.Length() && isWord( theString.Value( end ), dotIsWord))
        ++end;
      --end;
    }
  }
  else {  // search backward
    // end
    while ( end > 0 && !isWord( theString.Value( end ), dotIsWord))
      --end;
    if ( end == 0 )
      return theEmptyString; // no word found
    beg = end - 1;
    char endChar = theString.Value( end );
    if ( endChar == '"' || endChar == '\'' ) {
      // beg is at the corresponding quoting mark
      while ( beg > 1 &&
              ( theString.Value( beg ) != endChar || theString.Value( beg-1 ) == '\\'))
        --beg;
    }
    else {
      while ( beg > 0 && isWord( theString.Value( beg ), dotIsWord))
        --beg;
      ++beg;
    }
  }
  theStartPos = beg;
  //cout << theString << " ---- " << beg << " - " << end << endl;
  return theString.SubString( beg, end );
}

//================================================================================
/*!
 * \brief Look for position where not space char is
  * \param theString - The string 
  * \param thePos - The position to search from and which returns result
  * \retval bool - false if there are only space after thePos in theString
 * 
 * 
 */
//================================================================================

bool _pyCommand::SkipSpaces( const TCollection_AsciiString & theString, int & thePos )
{
  if ( thePos < 1 || thePos > theString.Length() )
    return false;

  while ( thePos <= theString.Length() && isspace( theString.Value( thePos )))
    ++thePos;

  return thePos <= theString.Length();
}

//================================================================================
/*!
 * \brief Modify a part of the command
  * \param thePartIndex - The index of the part
  * \param thePart - The new part string
  * \param theOldPart - The old part
 */
//================================================================================

void _pyCommand::SetPart(int thePartIndex, const TCollection_AsciiString& thePart,
                        TCollection_AsciiString& theOldPart)
{
  int pos = GetBegPos( thePartIndex );
  if ( pos <= Length() && theOldPart != thePart)
  {
    TCollection_AsciiString seperator;
    if ( pos < 1 ) {
      pos = GetBegPos( thePartIndex + 1 );
      if ( pos < 1 ) return;
      switch ( thePartIndex ) {
      case RESULT_IND: seperator = " = "; break;
      case OBJECT_IND: seperator = "."; break;
      case METHOD_IND: seperator = "()"; break;
      default:;
      }
    }      
    myString.Remove( pos, theOldPart.Length() );
    if ( !seperator.IsEmpty() )
      myString.Insert( pos , seperator );
    myString.Insert( pos, thePart );
    // update starting positions of the following parts
    int posDelta = thePart.Length() + seperator.Length() - theOldPart.Length();
    for ( int i = thePartIndex + 1; i <= myBegPos.Length(); ++i ) {
      if ( myBegPos( i ) > 0 )
        myBegPos( i ) += posDelta;
    }
    theOldPart = thePart;
  }
}

//================================================================================
/*!
 * \brief Set agrument
  * \param index - The argument index, it counts from 1
  * \param theArg - The argument string
 */
//================================================================================

void _pyCommand::SetArg( int index, const TCollection_AsciiString& theArg)
{
  FindAllArgs();
  int argInd = ARG1_IND + index - 1;
  int pos = GetBegPos( argInd );
  if ( pos < 1 ) // no index-th arg exist, append inexistent args
  {
    // find a closing parenthesis
    if ( GetNbArgs() != 0 && index <= GetNbArgs() ) {
      int lastArgInd = GetNbArgs();
      pos = GetBegPos( ARG1_IND + lastArgInd  - 1 ) + GetArg( lastArgInd ).Length();
      while ( pos > 0 && pos <= Length() && myString.Value( pos ) != ')' )
        ++pos;
    }
    else {
      pos = Length();
      while ( pos > 0 && myString.Value( pos ) != ')' )
        --pos;
    }
    if ( pos < 1 || myString.Value( pos ) != ')' ) { // no parentheses at all
      myString += "()";
      pos = Length();
    }
    while ( myArgs.Length() < index ) {
      if ( myArgs.Length() )
        myString.Insert( pos++, "," );
      myArgs.Append("None");
      myString.Insert( pos, myArgs.Last() );
      SetBegPos( ARG1_IND + myArgs.Length() - 1, pos );
      pos += myArgs.Last().Length();
    }
  }
  SetPart( argInd, theArg, myArgs( index ));
}

//================================================================================
/*!
 * \brief Empty arg list
 */
//================================================================================

void _pyCommand::RemoveArgs()
{
  if ( int pos = myString.Location( '(', 1, Length() ))
    myString.Trunc( pos );
  myString += ")";
  myArgs.Clear();
  if ( myBegPos.Length() >= ARG1_IND )
    myBegPos.Remove( ARG1_IND, myBegPos.Length() );
}

//================================================================================
/*!
 * \brief Set dependent commands after this one
 */
//================================================================================

bool _pyCommand::SetDependentCmdsAfter() const
{
  bool orderChanged = false;
  list< Handle(_pyCommand)>::const_reverse_iterator cmd = myDependentCmds.rbegin();
  for ( ; cmd != myDependentCmds.rend(); ++cmd ) {
    if ( (*cmd)->GetOrderNb() < GetOrderNb() ) {
      orderChanged = true;
      theGen->SetCommandAfter( *cmd, this );
      (*cmd)->SetDependentCmdsAfter();
    }
  }
  return orderChanged;
}
//================================================================================
/*!
 * \brief Insert accessor method after theObjectID
  * \param theObjectID - id of the accessed object
  * \param theAcsMethod - name of the method giving access to the object
  * \retval bool - false if theObjectID is not found in the command string
 */
//================================================================================

bool _pyCommand::AddAccessorMethod( _pyID theObjectID, const char* theAcsMethod )
{
  if ( !theAcsMethod )
    return false;
  // start object search from the object, i.e. ignore result
  GetObject();
  int beg = GetBegPos( OBJECT_IND );
  if ( beg < 1 || beg > Length() )
    return false;
  bool added = false;
  while (( beg = myString.Location( theObjectID, beg, Length() )))
  {
    // check that theObjectID is not just a part of a longer ID
    int afterEnd = beg + theObjectID.Length();
    Standard_Character c = myString.Value( afterEnd );
    if ( !isalnum( c ) && c != ':' ) {
      // check if accessor method already present
      if ( c != '.' ||
           myString.Location( (char*) theAcsMethod, afterEnd, Length() ) != afterEnd+1) {
        // insertion
        int oldLen = Length();
        myString.Insert( afterEnd, (char*) theAcsMethod );
        myString.Insert( afterEnd, "." );
        // update starting positions of the parts following the modified one
        int posDelta = Length() - oldLen;
        for ( int i = 1; i <= myBegPos.Length(); ++i ) {
          if ( myBegPos( i ) > afterEnd )
            myBegPos( i ) += posDelta;
        }
        added = true;
      }
    }
    beg = afterEnd; // is a part - next search
  }
  return added;
}

//================================================================================
/*!
 * \brief Return method name giving access to an interaface object wrapped by python class
  * \retval const char* - method name
 */
//================================================================================

const char* _pyObject::AccessorMethod() const
{
  return 0;
}
//================================================================================
/*!
 * \brief Return ID of a father
 */
//================================================================================

_pyID _pyObject::FatherID(const _pyID & childID)
{
  int colPos = childID.SearchFromEnd(':');
  if ( colPos > 0 )
    return childID.SubString( 1, colPos-1 );
  return "";
}

//================================================================================
/*!
 * \brief SelfEraser erases creation command if no more it's commands invoked
 */
//================================================================================

void _pySelfEraser::Flush()
{
  if ( GetNbCalls() == 0 )
    GetCreationCmd()->Clear();
}

//================================================================================
/*!
 * \brief count invoked commands
 */
//================================================================================

void _pySubMesh::Process( const Handle(_pyCommand)& theCommand )
{
  _pyObject::Process(theCommand); // count calls of Process()
  GetCreationCmd()->AddDependantCmd( theCommand );
}

//================================================================================
/*!
 * \brief Clear creation command if no commands invoked
 */
//================================================================================

void _pySubMesh::Flush()
{
  if ( GetNbCalls() == 0 ) // move to the end of all commands
    theGen->GetLastCommand()->AddDependantCmd( GetCreationCmd() );
  else if ( !myCreator.IsNull() )
    // move to be just after creator
    myCreator->GetCreationCmd()->AddDependantCmd( GetCreationCmd() );
}
